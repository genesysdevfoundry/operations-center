/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.backendengine.handlers;

import com.nordicoffice.backendengine.Log;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.nordicoffice.backendengine.Handler;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class FileHandler implements Handler {

    public String convertDate(Date date) {
        if (date == null) {
            return "";
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(date);
    }

    public void deleteOldFiles(String folderName,int timeSpanMinutes){
        try{
            Log.getLogger().info("Delete files older then " + timeSpanMinutes + "minutes from folder " + folderName);
            File folder = new File(folderName);
            File[] listOfFiles = folder.listFiles();             
            for (int i = 0; i < listOfFiles.length; i++) 
            {
                long timeSpan=(new Date()).getTime()-(60*1000*timeSpanMinutes);
                String fileName=listOfFiles[i].getName();
                if (listOfFiles[i].isFile()&&listOfFiles[i].lastModified()<timeSpan){
                    listOfFiles[i].delete();
                }                
            }
        } catch (Exception e){
            Log.getLogger().error("Error in deleteOldFiles",e);                    
        }
    }
    
    public void deleteFile(String sourceFile) throws Exception {
        try {
            File file = new File(sourceFile);
            boolean success = file.delete();
            if (!success) {
                Log.getLogger().error("Can't delete file");
                throw new Exception("Can't delete file");
            }
        } catch (Exception e) {
            Log.getLogger().error("Error in deleteFile", e);
            throw new Exception("Error in deleteFile");
        }
    }

    /**
     * Reads a xlsx file and returns as an array of maps. Will treat first row
     * as a header row.
     *
     * @param sourceFile
     * @return
     */
    public ArrayList readExcel(String sourceFile) {
        ArrayList returnArray = new ArrayList();
        File source = new File(sourceFile);
        if (source.exists() && source.isFile()) {
            try {

                FileInputStream excelFile = new FileInputStream(source);
                Workbook workbook = new XSSFWorkbook(excelFile);
                Map returnRow = new LinkedHashMap();
                DataFormatter df = new DataFormatter();
                Sheet sheet = workbook.getSheetAt(0);
                ArrayList<String> columnNames = new ArrayList();
                int rowsCount = sheet.getLastRowNum();

                for (int j = 0; j <= rowsCount; j++) { // j is sheet row 
                    Row row = sheet.getRow(j);
                    if (row != null) {
                        int colCounts = row.getLastCellNum();
                        for (int k = 0; k < colCounts; k++) { // k is sheet column
                            Cell cell = row.getCell(k);
                            String value = df.formatCellValue(cell).trim();
                            //First row, store column names and their column index.
                            if (j == 0 && value.length() != 0) {
                                columnNames.add(value);
                            } else if (columnNames.get(k) != null) {
                                returnRow.put(columnNames.get(k), value);
                            }
                        }
                        if (j != 0) {
                            returnArray.add(returnRow);
                        }
                        returnRow = new LinkedHashMap();
                    }
                }

            } catch (FileNotFoundException e) {
                Log.getLogger().error(e);
            } catch (IOException e) {
                Log.getLogger().error(e);
            }
        }
        Log.getLogger().info(returnArray.toString());
        return returnArray;
    }

    /**
     * TODO fixa return-format
     * @param sourceFile
     * @param separator
     * @return
     * @throws FileNotFoundException 
     */
    public ArrayList readCSV(String sourceFile, String separator) throws FileNotFoundException {
        ArrayList returnArray = new ArrayList();
        ArrayList returnRow;
        try (Scanner fileScanner = new Scanner(new File(sourceFile))) {
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                line = line.replace("\uFEFF", "");
                returnRow = new ArrayList();
                try (Scanner lineScanner = new Scanner(line)) {
                    lineScanner.useDelimiter(separator);
                    while (lineScanner.hasNext()) {
                        String token = lineScanner.next().trim();
                        returnRow.add(token);
                    }
                }
                returnArray.add(returnRow);
            }
        }
        return returnArray;
    }

    /**
     * Returns all the names of all files (not folders) within given directory
     *
     * @param sourceFolder
     * @return
     */
    public ArrayList getFileNamesFromFolder(String sourceFolder) {
        ArrayList returnArray = new ArrayList();

        File folder = new File(sourceFolder);

        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                returnArray.add(listOfFile.getName());
                System.out.println("File " + listOfFile.getName());
            } else if (listOfFile.isDirectory()) {
                System.out.println("Directory " + listOfFile.getName());
            }
        }

        return returnArray;
    }

    /**
     * Moves all files (not folders or files in subfolders) from given source to
     * given destination
     *
     * @param sourceFolder
     * @param targetFolder
     */
    public void moveAllFiles(String sourceFolder, String targetFolder) {
        File source = new File(sourceFolder);
        File destination = new File(targetFolder);
        if (source.exists() && source.isDirectory() && destination.exists() && destination.isDirectory()) {
            File[] listOfFiles = source.listFiles();
            for (File listOfFile : listOfFiles) {
                if (listOfFile.isFile()) {
                    listOfFile.renameTo(new File(targetFolder + "/" + listOfFile.getName()));
                } else if (listOfFile.isDirectory()) {
                    //Do nothing
                }
            }
        }
    }

    /**
     * Moves given source file to given destination
     *
     * @param sourceFile
     * @param targetFolder
     */
    public void moveFile(String sourceFile, String targetFolder) {
        File source = new File(sourceFile);
        File destination = new File(targetFolder);
        if (source.exists() && source.isFile() && destination.exists() && destination.isDirectory()) {
            source.renameTo(new File(targetFolder + "/" + source.getName()));
        }
    }

    @Override
    public void close() {
    }

    
    
    public void createPath(String path){
        try{
            File file = new File(path);            
            boolean success = file.mkdirs();            
            
            if (!success){
                Log.getLogger().error("Error creating path");                
            }
        } catch (Exception e){
                Log.getLogger().error("Error in createPath",e);                                
        }        
    }
}
