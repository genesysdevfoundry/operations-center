/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.backendengine;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * Small class that is first used to encrypt the password once. Then it is used
 * at runtime to decrypt the password. It is only used so that password won't be
 * in clear text in jar file. The encryption password is still in plain text so
 * anyone with a bit of more skills will be able to decrypt it.
 *
 * Run the main method to encrypt password
 *
 * @author okaver
 *
 */
public class EncryptDecrypt {

    final String decryptedUserPassword = null;


    public static String encryptPassword(String pUserPasswordKey) throws Exception{
        Log.getLogger().info("Starting encryption operation");

        //Encrypt
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        // This is a required password for Jasypt. You will have to use the same password to
        // retrieve decrypted password later. T
        // This password is not the password we are trying to encrypt taken from config.
        encryptor.setPassword("averysecretpassword");
        String encryptedPassword = encryptor.encrypt(pUserPasswordKey);
        Log.getLogger().info("Encryption done and encrypted password is : " + encryptedPassword);
        return encryptedPassword;
    }

    public static String decryptPassword(String passwordEncrypted) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("averysecretpassword");
        String decryptedPropertyValue = encryptor.decrypt(passwordEncrypted);

        return decryptedPropertyValue;
    }

    public static void main(String[] args) {
        try {
            System.out.println("Enter password to encrypt: ");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String userPwdKey = bufferedReader.readLine();
            System.out.println("you entered: " + userPwdKey);
            System.out.println("Encrypted password is: " + encryptPassword(userPwdKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public static void main2(String[] args) {
        try {
            System.out.println("Enter encrypted password: ");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String userPwdKey = bufferedReader.readLine();
            System.out.println("you entered: " + userPwdKey);
            System.out.println(decryptPassword(userPwdKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
;
}
