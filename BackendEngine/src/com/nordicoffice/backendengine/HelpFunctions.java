/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.backendengine;

import com.google.gson.Gson;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.http.HttpServletRequest;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 *
 * @author fanderss
 */
public class HelpFunctions {

    /**
     * *
     *
     * @param fullErrorDescriiption - Error for logging
     * @param friendlyError - User friendly text
     * @return
     */
    static public String GenerateErrorObject(String fullErrorDescription, String friendlyError) {
        Map errorObjectRoot = new LinkedHashMap();
        Map errorObjectKVP = new LinkedHashMap();
        
        

        if (!MainApplication.getInstance().isSendFullErrorDescription()&&fullErrorDescription!=null&&(fullErrorDescription.indexOf("CLIENT_MESSAGE:")>=0||fullErrorDescription.indexOf("CUSTOM_ERROR:")>=0))    {
            friendlyError=fullErrorDescription;
        }
        
        
        errorObjectKVP.put("fullErrorDescription", (MainApplication.getInstance().isSendFullErrorDescription())?fullErrorDescription:friendlyError);        
        errorObjectKVP.put("friendlyError", friendlyError);
        errorObjectRoot.put("Error", errorObjectKVP);

        Gson gson = new Gson();

        return gson.toJson(errorObjectRoot);
    }
    


    static String javaToJSONString(Object object) {
        try {
            String outStr;

            //Arraylists
            if (object instanceof ArrayList) {
                ArrayList arrayList = (ArrayList) object;
                outStr = "[";
                for (int i = 0; i < arrayList.size(); i++) {
                    outStr += ((i != 0) ? "," : "") + javaToJSONString(arrayList.get(i));
                }
                outStr += "]";
                return outStr;
            }

            //ScriptObjectMirror
            if (object instanceof ScriptObjectMirror) {
                ScriptObjectMirror scriptObjectMirror = (ScriptObjectMirror) object;
                //Type array
                if (scriptObjectMirror.isArray()) {
                    if (scriptObjectMirror.isEmpty()) {
                        return "[]";
                    }

                    outStr = "[";
                    for (int i = 0; i < scriptObjectMirror.size(); i++) {
                        outStr += ((i != 0) ? "," : "") + javaToJSONString(scriptObjectMirror.get(Integer.toString(i)));
                    }
                    outStr += "]";
                    return outStr;
                } else {
                    //Type Object
                    outStr = "{";
                    boolean first = true;
                    for (Map.Entry<String, Object> entry : scriptObjectMirror.entrySet()) {
                        if (entry.getKey().toLowerCase().indexOf("password") >= 0) {
                            outStr += ((!first) ? "," : "") + "\"" + entry.getKey() + "\":\"***********\"";
                        } else {
                            outStr += ((!first) ? "," : "") + "\"" + entry.getKey() + "\":" + javaToJSONString(entry.getValue());
                        }

                        first = false;
                    }
                    outStr += "}";
                    return outStr;
                }
            }

            if (object instanceof Integer) {
                return object.toString();
            }

            if (object instanceof Boolean) {
                return ((Boolean) object) ? "true" : "false";
            }

            if (object instanceof Map) {
                Map map = (Map) object;

                outStr = "{";
                boolean first = true;
                Iterator it = map.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    if (pair.getKey().toString().toLowerCase().indexOf("password") >= 0) {
                            outStr += ((!first) ? "," : "") + "\"" + pair.getKey() + "\":\"***********\"";
                        } else {
                            outStr += ((!first) ? "," : "") + "\"" + pair.getKey() + "\":" + javaToJSONString(pair.getValue());
                        }
                    first = false;
                }
                outStr += "}";
                return outStr;
            }

            if (object == null) {
                return "null";
            }
            return "\"" + object.toString().replace("\\", "\\\\").replace("\"", "\\\"").replace("\r", "\\r").replace("\n", "\\n").replace("\t", "\\t") + "\"";
        } catch (Exception ex) {
            return ex.getMessage();
        }

    }

    static public Map convertParameters(HttpServletRequest request) {
        Map parameterList = new LinkedHashMap();

        Enumeration keys = request.getParameterNames();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            //To retrieve a single value   
            String value = request.getParameter(key);
            parameterList.put(key, value);

        }
        return parameterList;
    }

    /**
     * Method that will generate a string based on user name. Will generate same
     * String every time it is used.
     *
     * @param userName
     * @return
     */
    public static String getUserHash(String userName) {
        try {
            byte[] salt = new byte[16];
            KeySpec spec = new PBEKeySpec(userName.toCharArray(), salt, 65536, 128);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = factory.generateSecret(spec).getEncoded();
            Base64.Encoder enc = Base64.getEncoder();
            return enc.encodeToString(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException noSuchAlgorithmException) {
            return null;
        }
    }

}
