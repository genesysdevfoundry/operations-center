/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.backendengine;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 * Logger class that will log to file according to settings in log4j2.xml
 * Log level can be changed at run time by code.
 * @author 
 *
 */
public class Log {
	private static Logger logger;
        static String separator=File.separator;
        static String log4jConfigFile = System.getProperty("catalina.base") + separator + "conf"  + separator + "log4j2.xml";
	        
        static {
		
                
                               
		try {
			ConfigurationSource source = new ConfigurationSource(new FileInputStream(log4jConfigFile));
			Configurator.initialize(null, source);
			logger = LogManager.getLogger("Generic");
			logger.debug("log4j config file set to: " + log4jConfigFile);                                                
		} catch (IOException e) {
			logger = LogManager.getLogger("Generic");
			logger.debug("catalina.base logging file not found. Setting log4j2 file to default.");
		}
	}

	public static Logger getLogger(){
            return logger;
	}

        /**
         * TODO!!!
         * @param loggerName
         * @return 
         */
        public static Logger getLogger(String loggerName){
		return logger;
	}
         
        /***
         * TODO!!!
         * 
         */
        public static void setLogFile(String fileName){
            fileName=fileName.replace("{catalina.home}", System.getProperty("catalina.base"));
            logger.info("Changing log file path to " + fileName);
            
            
           
            String appenderName="FileAppender";
            String loggerName="Generic";

            LoggerContext context = (LoggerContext) LogManager.getContext(false);
            Configuration configuration = context.getConfiguration();
            Layout<? extends Serializable> oldLayout = configuration.getAppender(appenderName).getLayout();

            //delete old appender/logger
            configuration.getAppender(appenderName).stop();
            configuration.removeLogger(loggerName);

            //create new appender/logger
            LoggerConfig loggerConfig = new LoggerConfig(loggerName, Level.INFO, false);
            FileAppender appender = FileAppender.createAppender(fileName, "true", "false", appenderName, "true", "true", "true",
                    "8192", oldLayout, null, "false", "", configuration);
            appender.start();
            loggerConfig.addAppender(appender, Level.DEBUG, null);
            configuration.addLogger(loggerName, loggerConfig);
            context.updateLoggers();
            
            logger.info("Start new log");
            
            
        }
        
        
	/**
	 * Sets log level to given values.
	 * @param level
	 */
	public static void setLogLevel(Level level){
		logger.info("Loggername: "+logger.getName());
		logger.info("Setting log level to " + level.toString());
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration configuration = ctx.getConfiguration();
		LoggerConfig loggerConfig = configuration.getLoggerConfig("Generic");
		loggerConfig.setLevel(level);
		ctx.updateLoggers();
                
                
                
                        
		logger.info("Finished updating log level.");
	}
}