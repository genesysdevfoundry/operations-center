/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.backendengine;

import com.nordicoffice.backendengine.handlers.FileHandler;
import com.nordicoffice.backendengine.handlers.DBHandler;
import com.google.gson.Gson;
import com.nordicoffice.backendengine.handlers.DBConnectorPool;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
//import com.nordicoffice.backendengine.Log;

/**
 *
 * @author fanderss
 */
public class ServiceHandler {

    private Gson gson = new Gson();
    private ScriptEngine engine = null;

    //Handlers
    private Map<String, Handler> handlers = new LinkedHashMap();

    private Map inputParameters;
    private HttpSession httpSession;
    private ServletContext servletContext;
    private String httpMethod;
    private MainApplication mainApplication = MainApplication.getInstance();

    public String getHttpMethod() {
        return httpMethod;
    }

    public ServiceHandler(Map inputParameters, HttpSession httpSession, ServletContext servletContext, String httpMethod) {
        this.inputParameters = inputParameters;
        this.httpSession = httpSession;
        this.servletContext = servletContext;
        this.httpMethod = httpMethod;

    }

    public void initDBConnectorPool(Map<?, ?> parameters) throws Exception {
        DBConnectorPool.getInstance().init(parameters);
    }

    public Handler getHandler(String handlerName) throws Exception {
        //Check if it is already instantiated
        Handler returnHandler = handlers.get(handlerName);
        if (returnHandler != null) {
            return returnHandler;
        }

        //Internal handlers
        if (handlerName.equals("DBHandler")) {
            returnHandler = (Handler) (new DBHandler());
        }

        if (handlerName.equals("FileHandler")) {
            returnHandler = (Handler) (new FileHandler());
        }

        if (returnHandler == null) {
            //Check Custom handlers
            if (Handler.class.isAssignableFrom(Class.forName(handlerName))) {
                returnHandler = (Handler) Class.forName(handlerName).newInstance();
            }
        }

        //Check if we have any handler
        if (returnHandler == null) {
            throw new Exception("Can't find handler " + handlerName);
        }

        //Add handler to handlers
        handlers.put(handlerName, returnHandler);

        return returnHandler;

    }

    public Map readPropertiesFileParameters(String fileName) throws Exception {
        Map output = new LinkedHashMap();

        String filePath;
        if (fileName.contains("/") || fileName.contains("\\")) {
            filePath = fileName;
        } else {
            String separator = File.separator;
            filePath = System.getProperty("catalina.base") + separator + "conf" + separator + fileName;
        }

        InputStream propertiesFileStream = null;
        Properties prop = new Properties();
        propertiesFileStream = new FileInputStream(filePath);
        prop.load(propertiesFileStream);

        Enumeration<?> e = prop.propertyNames();

        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = prop.getProperty(key);
            output.put(key.trim(), value.trim());
        }
        Log.getLogger().info("Content of properties file " + filePath + ": " + output.toString());
        return output;
    }

    public Map getContextParameters() {

        Enumeration<?> names = servletContext.getInitParameterNames();
        Map newParams = new LinkedHashMap();
        while (names.hasMoreElements()) {
            String name = (String) names.nextElement();

            newParams.put(name, (String) servletContext.getInitParameter(name));
        }
        return newParams;
    }

    public void addService(String serviceName, Integer serviceId, String jsScript) {
        MainApplication.getInstance().setCustomService(serviceName, serviceId, jsScript);
    }

    public void close() {
        //Iterate through all handlers and close them
        for (String key : handlers.keySet()) {
            handlers.get(key).close();
        }
    }

    public void loadHandler() throws Exception {

    }

    public void writeToLog(String text) {
        Log.getLogger().info("Log output from script:" + text);
    }

    public void setLogFileName(String fileName) {
        Log.setLogFile(fileName);
    }

    public String encryptPassword(String fileName) throws Exception {
        return EncryptDecrypt.encryptPassword(fileName);
    }

    public String hashPerson(String name) {
        return HelpFunctions.getUserHash(name);
    }

    /**
     * *
     * Set username, is used only for logging
     *
     * @param userName
     */
    public void setLoggingUserName(String userName) {
        httpSession.setAttribute("userName", userName);
    }

    /**
     * *
     * Import script is using ServerSideScriptFolder folder as root
     */
    public void importScript(String scriptName) throws Exception {
        String scriptPath = mainApplication.getServerSideScriptFolder() + "/" + scriptName;

        InputStream resourceAsStream = servletContext.getResourceAsStream(scriptPath);
        if (resourceAsStream != null) {
            engine.eval(new InputStreamReader(resourceAsStream));
        } else {
            throw new Exception("Can't find script " + scriptName);
        }

    }

    /**
     * *
     * This is used if a new service is executed within a service
     *
     * @param serviceName
     * @throws Exception
     */
    private void runService(String serviceName, Integer serviceId) throws Exception {
        //Run service
        String serviceScript = mainApplication.getCustomServiceScript(serviceName, serviceId);

        if (serviceScript == null) {  //Check if we have a custom service that matches the service name
            //Run the service that matches the name
            InputStream resourceAsStream = servletContext.getResourceAsStream(mainApplication.getServiceFolder() + "/" + serviceName + ".js");
            if (resourceAsStream != null) {
                engine.eval(new InputStreamReader(resourceAsStream));
            } else {
                throw new Exception("Can not find service " + ((serviceName != null) ? serviceName : "ID_" + serviceId));
            }
        } else {
            //Execute custom service
            try {
                engine.eval(serviceScript);
            } catch (Exception ex) {
                Log.getLogger().error("Error in runService", ex);
                throw ex;
            }
        }

    }

    /**
     * Execution of standalone scripts
     *
     * @param scriptName
     * @throws Exception
     */
    public void executeScript(String scriptName) throws Exception {
        executeScript(scriptName, null);
    }

    /**
     * Execution of standalone scripts
     *
     * @param scriptName
     * @throws Exception
     */
    public void executeScript(String scriptName, Map<?, ?> inputData) throws Exception {
        engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.put("application", MainApplication.getInstance().getApplicationData());

        if (mainApplication.getDefaultScript() != null) {
            //Init default scripts                
            engine.eval(new InputStreamReader(servletContext.getResourceAsStream(mainApplication.getServerSideScriptFolder() + "/" + mainApplication.getDefaultScript())));
        }

        if (inputData != null) {
            engine.put("input", inputData);
        }
        engine.put("serviceHandler", this);

        importScript(scriptName);

        this.close();
    }

    public String executeService(String serviceName) throws Exception {
        return executeService(serviceName, null);
    }

    public String executeService(int serviceId) throws Exception {
        return executeService(null, serviceId);
    }

    /**
     * *
     * Execute service script with the same name as serviceName
     *
     * @param serviceName
     * @return
     */
    private String executeService(String serviceName, Integer serviceId) throws Exception {
        Object outputData;
        Object sessionData = null;

        if (engine != null) {

            try {
                runService(serviceName, serviceId);
                return "";
            } catch (Exception ex) {
                String message = "";
                if (ex.getMessage() != null) {                                  
                    message = ex.getMessage();
                } else {
                    //Script error                    
                    message = "Exception is null. " + ex.toString();                    
                }
                Log.getLogger().error("Error in sub service " + serviceName, ex);
                throw (new Exception("Error in sub service " + serviceName + " Error message:" + message));
            }
        }
        //engine is not null if executeService is executed from a script

        if (httpSession != null) {
            sessionData = (Object) httpSession.getAttribute("data");
        }

        if (sessionData == null) {
            sessionData = new LinkedHashMap();
        }

        Map dummy = new LinkedHashMap();

        outputData = null;

        engine = new ScriptEngineManager().getEngineByName("nashorn");

        //Prepare the javascript context
        engine.put("session", sessionData);
        engine.put("application", MainApplication.getInstance().getApplicationData());
        engine.put("output", dummy);
        engine.put("serviceHandler", this);
        String jsonData = (String) inputParameters.get("jsonData");
        if (jsonData != null) {
            engine.put("__jsonData", jsonData);
            try {
                engine.eval("input=JSON.parse(__jsonData)");
            } catch (Exception ex) {
                Log.getLogger().error("Error in executeService, can not convert to JSON. jsonData=" + jsonData, ex);
            }
        } else {
            engine.put("input", inputParameters);
        }

        try {
            if (mainApplication.getDefaultScript() != null) {
                //Init core scripts
                InputStream resourceAsStream = servletContext.getResourceAsStream(mainApplication.getServerSideScriptFolder() + "/" + mainApplication.getDefaultScript());

                if (resourceAsStream != null) {
                    engine.eval(new InputStreamReader(resourceAsStream));
                } else {
                    Log.getLogger().error("Can not find Script " + mainApplication.getDefaultScript());
                    return HelpFunctions.GenerateErrorObject("Can not find Script", "Can not find Script");
                }
            }

            runService(serviceName, serviceId);

            outputData = engine.eval("output");
            sessionData = (Map) engine.eval("session");
        } catch (Exception ex) {
            String message = "";
            if (ex.getMessage() != null ) {                
                message = ex.getMessage();
            } else {
                //Script error
                
                message = "Exception is null. " + ex.toString();
            }            
            this.close();
            Log.getLogger().error("Error in service " + serviceName, ex);
            return HelpFunctions.GenerateErrorObject("Error in service " + serviceName + ": " + message, "Error in service " + serviceName);
        }

        //Set session data 
        if (httpSession != null) {
            httpSession.setAttribute("data", sessionData);
        }

        this.close();

        return HelpFunctions.javaToJSONString(outputData);

    }

    public void addApplicationData(String key, Object data) {

        if (!key.toLowerCase().contains("password")) {
            Log.getLogger().info("addApplicationData key=" + key + " data = " + data.toString());
        } else {
            Log.getLogger().info("addApplicationData key=" + key + " data = *********");
        }

        Map applicationData = MainApplication.getInstance().getApplicationData();

        applicationData.put(key, data);
    }
}
