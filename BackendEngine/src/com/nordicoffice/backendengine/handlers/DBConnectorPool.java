/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.backendengine.handlers;

import com.nordicoffice.backendengine.EncryptDecrypt;
import com.nordicoffice.backendengine.Log;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 *
 * @author fanderss
 */
public class DBConnectorPool {

    private PoolProperties poolProperties;
    private DataSource dataSource;

    /**
     * Singleton instance
     */
    private static DBConnectorPool INSTANCE;

    /**
     * Singleton accessor
     *
     * @return
     */
    public static DBConnectorPool getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DBConnectorPool();
        }
        return INSTANCE;
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void destroy() {
        try {
            dataSource.close();
            Log.getLogger().info("DB Connection pool closed", this);
        } catch (Exception e) {

        }
    }

    public void init(Map<?, ?> parameters) throws Exception {
        String tmpValue;
        Boolean tmpValueBol;
        Integer tmpValueInt;

        if (dataSource != null) {
            return; //init can only run once
        }

        poolProperties = new PoolProperties();

        Log.getLogger().info("Open connection pool");

        //URL
        tmpValue = (String) parameters.get("url");
        if (tmpValue == null) {
            throw new Exception("No url parameter");
        }
        poolProperties.setUrl(tmpValue);
        Log.getLogger().info("url=" + tmpValue);

        //Driver class name
        tmpValue = (String) parameters.get("driverClassName");
        if (tmpValue == null) {
            throw new Exception("No driverClassName parameter");
        }
        poolProperties.setDriverClassName(tmpValue);
        Log.getLogger().info("driverClassName=" + tmpValue);

        //Username
        tmpValue = (String) parameters.get("userName");
        if (tmpValue == null) {
            throw new Exception("No userName parameter");
        }
        poolProperties.setUsername(tmpValue);
        Log.getLogger().info("userName=" + tmpValue);

        //Password
        tmpValue = (String) parameters.get("password");
        if (tmpValue == null) {
            throw new Exception("No password parameter");
        }
        try{
            String decryptedPwd=EncryptDecrypt.decryptPassword(tmpValue);
            poolProperties.setPassword(decryptedPwd);
            Log.getLogger().info("password=******");
        } catch (Exception ex){
            Log.getLogger().error("Cannot decrypt password",ex);
            throw ex;
        }

        poolProperties.setJmxEnabled(true);

        poolProperties.setTestWhileIdle(false);
        poolProperties.setTestOnBorrow(true);

        // valdationQuery
        tmpValue = (String) parameters.get("valdationQuery");
        if (tmpValue == null) {
            tmpValue = "SELECT 1";
        }
        poolProperties.setValidationQuery(tmpValue);
        Log.getLogger().info("valdationQuery=" + tmpValue);

        // TestOnReturn
        tmpValueBol = (Boolean) parameters.get("testOnReturn");
        if (tmpValueBol == null) {
            tmpValueBol = false;
        }
        poolProperties.setTestOnReturn(tmpValueBol);
        Log.getLogger().info("testOnReturn=" + tmpValueBol.toString());

        // ValidationInterval
        tmpValueInt = (Integer) parameters.get("validationInterval");
        if (tmpValueInt == null) {
            tmpValueInt = 30000;
        }
        poolProperties.setValidationInterval(tmpValueInt);
        Log.getLogger().info("validationInterval=" + tmpValueInt.toString());

        poolProperties.setTimeBetweenEvictionRunsMillis(30000);

        // maxActive
        tmpValueInt = (Integer) parameters.get("maxActive");
        if (tmpValueInt == null) {
            tmpValueInt = 10;
        }
        poolProperties.setMaxActive(tmpValueInt);
        Log.getLogger().info("maxActive=" + tmpValueInt.toString());

        //InitialSize
        tmpValueInt = (Integer) parameters.get("initialSize");
        if (tmpValueInt == null) {
            tmpValueInt = 1;
        }
        poolProperties.setInitialSize(tmpValueInt);
        Log.getLogger().info("initialSize=" + tmpValueInt.toString());

        //maxWait
        tmpValueInt = (Integer) parameters.get("maxWait");
        if (tmpValueInt == null) {
            tmpValueInt = 10000;
        }
        poolProperties.setMaxWait(tmpValueInt);
        Log.getLogger().info("maxWait=" + tmpValueInt.toString());

        //removeAbandonedTimeout
        tmpValueInt = (Integer) parameters.get("removeAbandonedTimeout");
        if (tmpValueInt == null) {
            tmpValueInt = 60;
        }
        poolProperties.setRemoveAbandonedTimeout(tmpValueInt);
        Log.getLogger().info("removeAbandonedTimeout=" + tmpValueInt.toString());

        poolProperties.setMinEvictableIdleTimeMillis(30000);

        //MinIdle
        tmpValueInt = (Integer) parameters.get("minIdle");
        if (tmpValueInt == null) {
            tmpValueInt = 1;
        }
        poolProperties.setMinIdle(tmpValueInt);
        Log.getLogger().info("minIdle=" + tmpValueInt.toString());

        poolProperties.setLogAbandoned(true);
        poolProperties.setRemoveAbandoned(true);
        poolProperties.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        dataSource = new DataSource();
        dataSource.setPoolProperties(poolProperties);

        Log.getLogger().info("connection pool created", this);
    }
}
