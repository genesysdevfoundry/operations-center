/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.backendengine.handlers;

import com.nordicoffice.backendengine.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import com.nordicoffice.backendengine.Handler;


public class DBHandler implements Handler{
    Connection connection;
    
    public DBHandler() throws Exception {
        connection=DBConnectorPool.getInstance().getConnection();
    }
    
    private String camelConversion(String inputString){   
        if (inputString==null||inputString.equals("")){
            return "";
        }
        
        
        return inputString.substring(0,1).toLowerCase() + inputString.substring(1);
    }
    
    public ArrayList executeQueryRecordset(String sqlQuery,boolean camel) throws Exception{
        try{
            ArrayList returnArray=new ArrayList();
            
            Log.getLogger().info("Execute sql: " + sqlQuery);

            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            
            
            ResultSet rs = statement.executeQuery();
            //Get meta data 
            ResultSetMetaData metaData = rs.getMetaData();
            while (rs.next()){
                Map row=new LinkedHashMap();
                for (int i=1;i<=metaData.getColumnCount();i++){
                    String columnName;
                    if (camel){
                        columnName=camelConversion(metaData.getColumnName(i));                   
                    } else {
                        columnName=metaData.getColumnName(i);                    
                    }
                    int columnType = metaData.getColumnType(i);
                    if (columnType == Types.INTEGER  ||
                        columnType == Types.DECIMAL||
                        columnType == Types.DOUBLE||
                        columnType == Types.FLOAT||
                        columnType == Types.NUMERIC) {
                        row.put(columnName,rs.getObject(columnName));
                    } else if (columnType == Types.DATE) {
                        //
                    } else {                        
                        row.put(columnName,rs.getString(columnName));
                    }
                }                            
                returnArray.add(row);
            }
            rs.close();
            statement.close();
            return returnArray;        
        } catch(Exception ex){
            Log.getLogger().error("Error in executeQueryRecordset",ex);            
            throw ex;            
        }        
    }
    
    public String getIdentity() throws Exception{
        //Get new identity field (inserts)
        
        ArrayList result=executeQueryRecordset("SELECT @@IDENTITY AS newId",false);
        if (result.size()>0){                    
            return ((Map)(result.get(0))).get("newId").toString();                    
        }                
        return null;
    }
    
    public long executeQueryStatement(String sqlQuery) throws Exception{
        long recordsAffected=0;
        try{
    
            Log.getLogger().info("executeQueryStatement: " + sqlQuery);

            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            recordsAffected=statement.executeUpdate();
            statement.close();
            
        } catch (Exception e){
            Log.getLogger().error("Error in executeQueryStatement",e);            
            throw e;
        }
        return recordsAffected;
    }
    
    public Map executeQuerySingleRecord(String sqlQuery,boolean camel) throws Exception{
        try{
            ArrayList tmpArray=executeQueryRecordset(sqlQuery,camel);
            if (tmpArray.size()>0){
                return (Map)tmpArray.get(0);                
            } 
            
        } catch (Exception e){
            Log.getLogger().error(e.toString(),e);
        }
        return new LinkedHashMap();
    }

    public Map executeKVPQuery(String sqlQuery,String key,String value,boolean camel) throws Exception{
        try{
            
            ArrayList tmpArray=executeQueryRecordset(sqlQuery,camel);
            
            Map kvpList=new LinkedHashMap();
            for (int i=0;i<tmpArray.size();i++){
                Map tmpRecord=(Map)tmpArray.get(i);
                String keyData;
                if (camel){
                    keyData=camelConversion(tmpRecord.get(key).toString());
                } else {
                    keyData=tmpRecord.get(key).toString();
                }
                
                String valueData=tmpRecord.get(value).toString();
                if (!keyData.equals("")){
                    kvpList.put(keyData,valueData);
                }                
            }
            return kvpList;
            
        } catch (Exception e){            
            Log.getLogger().error("Error in executeKVPQuery",e);
            throw e;
        }        
    }
    
    @Override
    public void close() {
           try {
            connection.close();
        } catch (SQLException e) {
            Log.getLogger().error("Error in closeConnection",e);
        }
    }
}
