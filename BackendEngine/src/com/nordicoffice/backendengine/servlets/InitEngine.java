/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.backendengine.servlets;

import com.nordicoffice.backendengine.Log;
import com.nordicoffice.backendengine.MainApplication;
import com.nordicoffice.backendengine.ServiceHandler;
import com.nordicoffice.backendengine.handlers.DBConnectorPool;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;


@WebServlet(loadOnStartup=1,
            urlPatterns="/Startup_qr532")

public class InitEngine extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init(); 
        
        ServletContext sc = getServletContext();
        
        //Instanciate main application (a singleton)
        MainApplication myApplication=MainApplication.getInstance();
        myApplication.setServletContext(sc);
        
        
        //Set serverSideScriptFolder
        String serverSideScriptFolder=sc.getInitParameter("serverSideScriptFolder");        
        if (serverSideScriptFolder!=null&&!serverSideScriptFolder.equals("")){
            myApplication.setServerSideScriptFolder(serverSideScriptFolder);
        }
        
        
        //Set serviceFolder
        String serviceFolder=sc.getInitParameter("serviceFolder");        
        if (serviceFolder!=null&&!serviceFolder.equals("")){
            myApplication.setServiceFolder(serviceFolder);
        }
        
        //Set defaultScript
        String defaultScript=sc.getInitParameter("defaultScript");        
        if (defaultScript!=null&&!defaultScript.equals("")){
            myApplication.setDefaultScript(defaultScript);            
        }
        
        //Set setFullErrorDescription
        String sendFullErrorDescription=sc.getInitParameter("sendFullErrorDescription");        
        if (sendFullErrorDescription!=null&&sendFullErrorDescription.toLowerCase().equals("true")){
            myApplication.setSendFullErrorDescription(true);
        }
        
        String startupScript=sc.getInitParameter("startupScript");                
        if (startupScript!=null&&!startupScript.equals("")){
        
            //Initiate service handler
            ServiceHandler serviceHandler=new ServiceHandler(null,null,sc,null);

            try {
                serviceHandler.executeScript(startupScript);
            } catch (Exception ex) {
                Log.getLogger().error("Error in Init:" + ex.getMessage(),ex);
            }
        }        
        
        
        //Check if schedule script exist
        String schedulerScript=sc.getInitParameter("schedulerScript");        
        if (schedulerScript!=null&&!schedulerScript.equals("")){
            myApplication.setSchedulerScript(schedulerScript);            
            String schedulerInterval=sc.getInitParameter("schedulerInterval");
            if (schedulerInterval==null){
                schedulerInterval="60"; //Every minute as default
            }       
            myApplication.startScheduleThread(Integer.parseInt(schedulerInterval));
        }
    }

    @Override
    public void destroy() {
        super.destroy(); 
    
        ServletContext sc = getServletContext();
        
        String shutdownServiceName=sc.getInitParameter("shutdownService");
        
        if (shutdownServiceName!=null&&!shutdownServiceName.equals("")){
        
            //Initiate service handler
            ServiceHandler serviceHandler=new ServiceHandler(null,null,sc,null);

            try {
                serviceHandler.executeService(shutdownServiceName);
            } catch (Exception ex) {
                Log.getLogger().error(ex);
            }
        }
        
        DBConnectorPool.getInstance().destroy();
    }
    
}
