/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.backendengine.servlets;

import com.google.gson.Gson;
import com.nordicoffice.backendengine.HelpFunctions;
import com.nordicoffice.backendengine.ServiceHandler;
import java.io.File;
import java.io.FileReader;
import com.nordicoffice.backendengine.Log;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fanderss
 */
@WebServlet("/Services/*") 
public class ServiceDispatcher extends HttpServlet {
    private Gson gson=new Gson();
    
    
     

    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param httpMethod hich http method that was used in the request
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response,String httpMethod)
            throws ServletException, IOException {
        
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
                
        PrintWriter out = response.getWriter();
        
        try {
            
            //Get survice name from url
            String serviceName=request.getPathInfo().substring(1);
                
            //Get Parameterlist
            Map inputParasmeters=HelpFunctions.convertParameters(request);
            
            HttpSession httpSession = request.getSession();            
            ServletContext servletContext = getServletContext();   
            
            String passWord=null;
            //Mask password  in log file
            if (inputParasmeters.get("password")!=null){
                passWord=(String)inputParasmeters.get("password");
                inputParasmeters.put("password","*********");
            }
            
            //Log input variables
            Log.getLogger().info("Request execute service: " + serviceName + " UserName: " + request.getSession().getAttribute("userName") + " Arguments: " + gson.toJson(inputParasmeters));
            
            //Set back password 
            if (passWord!=null){
                inputParasmeters.put("password",passWord);
            }
            
            
            
            if (serviceName==null||serviceName.equals("")){
                out.println(HelpFunctions.GenerateErrorObject("No service to execute","No service to execute"));
                return;
            }                        
                
            
            //Initiate service handler
            ServiceHandler serviceHandler=new ServiceHandler(inputParasmeters,httpSession,servletContext,httpMethod);
            
            //Execute serive and write back the result to the client
            out.print(serviceHandler.executeService(serviceName));
             
        } catch(Exception e){
            Log.getLogger().error("Error in servlet ServiceDispatcher",e);            
            out.println(HelpFunctions.GenerateErrorObject("Error in processRequest. Error description: " + e.getMessage(),"Error in processRequest"));
        } finally {            
            
        }
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response,"GET");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response,"POST");
    }

    /**
     * Handles the HTTP <code>PUT</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response,"PUT");
    }
    
    /**
     * Handles the HTTP <code>DELETE</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response,"DELETE");
    }
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
