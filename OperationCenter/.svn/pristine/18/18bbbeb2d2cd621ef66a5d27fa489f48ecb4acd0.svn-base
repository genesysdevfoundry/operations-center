var ObjectTemplate={};
ObjectTemplate.objectTemplateParameters=[
    {
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Object Template",
        type:"TEXT"
    },{
        name:"type",
        displayName:"Type",
        mandatory:true,
        type:"SELECT",
        defaultValue:"",
        listData:[
                {text:"Unbound - Single Object",id:"UNBOUND_OBJECT"},
                {text:"Unbound - Object List",id:"UNBOUND_OBJECT_LIST"},
                {text:"Config Server List Object - Single Object",id:"TRANSACTION_LIST_OBJECT"},
                {text:"Config Server List Object - Object List",id:"TRANSACTION_LIST_OBJECT_LIST"},
                {text:"Calendar - Single Object",id:"CALENDAR_OBJECT"},
                {text:"Calendar - Object List",id:"CALENDAR_OBJECT_LIST"},
                {text:"Workflow - Single Object",id:"WORKFLOW_OBJECT"},
                {text:"Workflow - Object List",id:"WORKFLOW_OBJECT_LIST"},
                {text:"Prompts List",id:"PROMPT_LIST"}
        ]
    },{
        type:"COLUMN",    
        columns:4,
        content:[{
            name:"history",
            displayName:"Enable History",        
            defaultValue:true,
            type:"CHECKBOX"
        },{
            name:"belongToDepartment",
            displayName:"Belongs to a department",        
            defaultValue:true,
            type:"CHECKBOX"
        }]
    },{
        type:"SELECT",
        name:"accessGroups",
        displayName:"Access Groups",
        listGridName:"Access Groups",
        helpText:"Users that are members of one of the selected access groups will have access to this object template",
        mandatory:0,
        scriptName:"GetAllAccessGroups",
        multiselect:1,
        readOnlyMode:"EDITABLE"
    },{
        type:"SECTION",        
        title:"Left Menu",
        collapsable:true,
        collapsed:true,
        solid:true,
        content:[
            {
                type:"COLUMN",
                columns:3,
                content:[
                    {
                        name:"menuGroup",
                        displayName:"Menu Group",
                        mandatory:false,
                        defaultValue:"",
                        type:"SELECT",
                        scriptName:"KVPListGetMenuGroups"
                        
                    },{
                        name:"menuName",
                        displayName:"Menu Name",
                        mandatory:false,
                        defaultValue:"",
                        type:"TEXT"    
                    },{
                        name:"icon",
                        displayName:"Icon Name",
                        mandatory:false,
                        defaultValue:"",
                        type:"TEXT"    
                    }    
                ]
            }            
        ]
    }        
];
   
ObjectTemplate.scriptSection=[{
        type:"SECTION",        
        title:"Action Scripts",
        collapsable:true,
        collapsed:true,
        solid:true,
        content:[{type:"COLUMN",
                columns:2,
                content:[
                    {
                        displayName:"Script Create",
                        name:"scriptCreate",
                        mandatory:false,
                        defaultValue:"",
                        newScriptNamePart:"FormActionCreate",
                        scriptType:"OBJECT_ACTION",
                        type:"SCRIPT_SELECT"        
                    },{
                        displayName:"Script Save",
                        name:"scriptSave",
                        mandatory:false,
                        defaultValue:"",
                        newScriptNamePart:"FormActionSave",
                        scriptType:"OBJECT_ACTION",
                        type:"SCRIPT_SELECT"        
                    },{
                        displayName:"Script Get",
                        name:"scriptGet",
                        mandatory:false,
                        defaultValue:"",
                        newScriptNamePart:"FormActionGet",
                        scriptType:"OBJECT_ACTION",
                        type:"SCRIPT_SELECT"        
                    },{
                        displayName:"Script Get All",
                        name:"scriptGetAll",
                        mandatory:false,
                        defaultValue:"",
                        newScriptNamePart:"FormActionGetAll",
                        scriptType:"OBJECT_ACTION",
                        type:"SCRIPT_SELECT"        
                    },{
                        displayName:"Script Delete",
                        name:"scriptDelete",
                        mandatory:false,
                        defaultValue:"",
                        newScriptNamePart:"FormActionDelete",
                        scriptType:"OBJECT_ACTION",
                        type:"SCRIPT_SELECT"        
                    },{
                        name:"connectedField",
                        displayName:"Connected Field",
                        mandatory:true,                        
                        defaultValue:"name",
                        type:"TEXT",
                        helpText:"This field needs to be unique"
                    },{
                        name:"runGetScriptOnce",
                        displayName:"Only execute Get Script the first time",                        
                        defaultValue:false,
                        type:"CHECKBOX"
                    }
                ]}
        ]}
];


/**
 * UNBOUND_OBJECT
 * 
 */
ObjectTemplate.parameters_unbound_object=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:true,
        defaultValue:"",        
        type:"FORM_SELECT"
    }
];
ObjectTemplate.parameters_unbound_object=ObjectTemplate.parameters_unbound_object.concat(ObjectTemplate.scriptSection);



/**
 * UNBOUND_OBJECT_LIST
 * 
 */
ObjectTemplate.parameters_unbound_object_list=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:true,
        defaultValue:"",
        type:"FORM_SELECT"
    }
];

ObjectTemplate.parameters_unbound_object_list=ObjectTemplate.parameters_unbound_object_list.concat(ObjectTemplate.scriptSection);

/**
 * TRANSACTION_LIST_OBJECT
 * 
 */
ObjectTemplate.parameters_transaction_list_object=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:true,
        defaultValue:"",
        type:"FORM_SELECT"
    },{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        name:"sectionName",
        displayName:"Section Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        name:"configServerFirst",
        displayName:"Get Data First From Config Server",
        defaultValue:false,
        type:"CHECKBOX"
    }
];

ObjectTemplate.parameters_transaction_list_object=ObjectTemplate.parameters_transaction_list_object.concat(ObjectTemplate.scriptSection);

/**
 * TRANSACTION_LIST_OBJECT_LIST
 * 
 */
ObjectTemplate.parameters_transaction_list_object_list=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:true,
        defaultValue:"",
        type:"FORM_SELECT"
    },{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        name:"configServerFirst",
        displayName:"Get Data First From Config Server",
        defaultValue:false,
        type:"CHECKBOX"
    }
];

ObjectTemplate.parameters_transaction_list_object_list=ObjectTemplate.parameters_transaction_list_object_list.concat(ObjectTemplate.scriptSection);


/**
 * CALENDAR_OBJECT 
 */
ObjectTemplate.parameters_calendar_object=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:false,
        defaultValue:"",
        type:"FORM_SELECT"
    },{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        name:"sectionName",
        displayName:"Section Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    }
];

/**
 * CALENDAR_OBJECT_LIST 
 */
ObjectTemplate.parameters_calendar_object_list=[
    {
        name:"id_Form",
        displayName:"Form",
        mandatory:false,
        defaultValue:"",
        type:"FORM_SELECT"
    },{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    }
];

/**
 * WORKFLOW_OBJECT
 */
ObjectTemplate.parameters_workflow_object=[{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        name:"sectionName",
        displayName:"Section Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        type:"CHILD_SECTION",
        name:"workflowBlocks",
        content:[
        {
            type:"OBJECT_SELECT",
            name:"workflowBlock",
            displayName:"Workflow Block",
            defaultValue:"",
            listGridName:"",
            mandatory:1,
            id_ObjectTemplate:"17",
            applyPermissions:0,
            scriptRequestString:"NAME_KVPListGetObjects|17"
        }
    ],
        displayName:"Workflow Blocks",
        sortOrderFieldName:"sortOrder",
        sortOrderFieldDisplayName:"",
        collapsed:0,
        fixedSortOrder:1
}];
    

/**
 * WORKFLOW_OBJECT_LIST
 */
ObjectTemplate.parameters_workflow_object_list=[{
        name:"transactionListName",
        displayName:"Transaction List Name",
        mandatory:true,
        defaultValue:"",
        type:"TEXT"
    },{
        type:"CHILD_SECTION",
        name:"workflowBlocks",
        content:[
        {
            type:"OBJECT_SELECT",
            name:"workflowBlock",
            displayName:"Workflow Block",
            defaultValue:"",
            listGridName:"",
            mandatory:1,
            id_ObjectTemplate:"17",
            applyPermissions:0,
            scriptRequestString:"NAME_KVPListGetObjects|17"
        }
    ],
        displayName:"Workflow Blocks",
        sortOrderFieldName:"sortOrder",
        sortOrderFieldDisplayName:"",
        collapsed:0,
        fixedSortOrder:1
}];       