Classes.ObjectHandler.ObjectList=function(objectHandlerController,data,_viewTemplates,id_ObjectTemplate){
    var _this=this;
    var _objectHandlerController=objectHandlerController;
    var _table;    
    var _data=data;
    var _$panel=null;    
    var _btReloadAllHanlde;
    var _columnsParameters=[];
    var _columnsInfo=[];
    var _columnsHistory=[];
    var _showDeleted=false;
    var _getAll=false;
    var _objectSelectFields=[];
    var _languages;
    
    /***
     * Public methods
     * 
     */    
    this.get$panel=function(){
        return _$panel;
    };
    
    this.getObjectTemplateId=function(){
        return id_ObjectTemplate;
    };  
      
    this.panelSelected=function(){
        _table.draw();
    };  
    
    this.setBtReloadAllHandle=function(value){
        _btReloadAllHanlde=value;
    };
    
    this.updateObjectList=function(){
        Functions.ajaxRequest("GetObjects",{id_ObjectTemplate:id_ObjectTemplate,showDeleted:_showDeleted,getAll:_getAll},function(data){
            _table.updateData(_convertValues(data.values));
            if (_btReloadAllHanlde){
                _btReloadAllHanlde.removeClass("btn-warning").addClass("btn-primary");
            }
        });
    };
    
    
    this.close=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    
    this.deleteObject=function(){
        var selData=_table.getSelectedRecord();
        if (!selData.id_Object){
            bootbox.alert("Cannot delete objects that are not stored in local database.");
            return;
        }
        
        if (selData.usage>0){
            bootbox.alert("This object is in use and cannot be deleted.");
            return;
        }
        
        if (selData){
           bootbox.confirm("Are you sure you want to delete selected object?", function(data){
                if (data){
                    Functions.ajaxRequest("DeleteObject",{id_Object:selData.id_Object},function(){
                        _this.updateObjectList();
                    });
                }
            });   
        }
    };
    
    this.openObject=function(){
        var selData=_table.getSelectedRecord();        
        if (selData){
            var connectedField=data.objectTemplate.connectedField||"name";
            var params={id_Object:selData.id_Object,id_ObjectTemplate:id_ObjectTemplate};
            params[connectedField]=selData[connectedField];
           _objectHandlerController.getMainApplication().getEventHandler().fire("OPEN_OBJECT",params);                      
        }
    };

    this.copyObject=function(){
        var selData=_table.getSelectedRecord();        
        if (selData){
            var connectedField=data.objectTemplate.connectedField||"name";
            var params={id_Object:selData.id_Object,id_ObjectTemplate:id_ObjectTemplate};
            params[connectedField]=selData[connectedField];
           _objectHandlerController.getMainApplication().getEventHandler().fire("COPY_OBJECT",params);
        }
    };
    
    /***
     * Private methods
     * 
     */
    var _playRecording_Click=function(event){
        var filename=$(this).attr("data-link");
        Functions.ajaxRequest("PrepareMP3File",{filename:filename},function(data){            
            mainApplication.getEventHandler().fire("PLAY_PROMPT",{filename:data.tmpFilename});
        });
        event.stopPropagation();
    };
    
    
    var _openObject_Click=function(event){
        var id_Object=$(this).attr("data-id-object");
        var objectName=$(this).text();
        
        if (id_Object){
            var params={id_Object:id_Object,name:objectName};                
            _objectHandlerController.getMainApplication().getEventHandler().fire("OPEN_OBJECT",params);    
            event.stopPropagation();
        }
    };
    
    
    var _getAllParameters=function(formData){        
        var parameterArray=[];
        for (var sectionIndex=0;sectionIndex<formData.sections.length;sectionIndex++){
            var section=formData.sections[sectionIndex];        
            for (var parameterIndex=0;parameterIndex<section.parameters.length;parameterIndex++){
                parameterArray.push(section.parameters[parameterIndex]);
            }
        }
        return parameterArray;
    };
    
    
    //Add extra info to the value so the linked object can be opened directly from the grid
    var _convertValues=function(values){
        if (!values){
            return [];
        }
        
        //Special treatment for object linkes
        if (_objectSelectFields.length!==0){            
            for (var i=0;i<values.length;i++){
                for (var n=0;n<_objectSelectFields.length;n++){
                    if (values[i]["_" + _objectSelectFields[n]]){
                        values[i][_objectSelectFields[n]]=values[i][_objectSelectFields[n]] + "|" + values[i]["_" + _objectSelectFields[n]];
                    }
                }
            } 
        }
        
        //Special treatment for object prompt lists            
        if (_data.objectTemplate.type==="PROMPT_LIST"){        
            for(var valueIndex=0;valueIndex<values.length;valueIndex++){                
                value=values[valueIndex];
                for(var i=0;i<_languages.length;i++){
                    value[_languages[i].languageCode]="";
                    if (value.languagePrompts){
                       for(var n=0;n<value.languagePrompts.length;n++){
                            if (_languages[i].languageCode===value.languagePrompts[n].languageCode){
                                value[_languages[i].languageCode]=value.languagePrompts[n].prompt_File||"";
                            }                    
                        }
                    }
                }
            }
        }        
        return values;
    };
        
    var _fillTable=function(form,values){
        var _hasColumns=false;
        
        var _renderCell=function(data){
            if (typeof data ==="undefined"){
                return "";
            }
            else {
                if (typeof data ==="string"&&data.length>100){
                    return data.substring(0,99) + "...";
                } else {
                   return data;
                }
            }
        };
        
        var _renderDate=function(data){
            if (!data){
                return "";
            }
            return data.substring(0,19);            
        };
        
        var _renderObjectCell=function(data){
            if (data&&typeof data==="string"){
                var dataArray=data.split("|");
                if (dataArray.length===2){
                    //return dataArray[0] + "<button class=\"listgrid-open-bt\" ><i class=\"fa fa-chevron-right text-primary\" data-id-object=\"" + dataArray[1] + "\"></i></button>&nbsp;";
                    return "<a class=\"link-open-object\" href=\"#\" data-id-object=\"" + dataArray[1] + "\">" + dataArray[0] + "<\a>";
                } else {
                    return data;
                }
                
            } else {
                return "";
            }
        };    
        
        var _renderAudioFile=function(data){            
            if (data){            
                var dataArray=data.split(",");
                var result="";                
                for (var i=0;i<dataArray.length;i++){
                    result+="<button class='btn btn-primary recording-button' data-link='" + dataArray[i] + "' style=\"padding-top:0;padding-bottom:0;\">Play</button>&nbsp;";
                }
                return result;
            } else {
                return "";        
            }
        };
            
        var _renderPromptCell=function(data){            
            if (data){            
                var dataArray=data.split(",");
                var result="";                
                for (var i=0;i<dataArray.length;i++){
                    result+=(result?", ":"") + "<a class=\"link-open-object\" href=\"#\" data-id-object=\"" + Number(dataArray[i].replace(".wav","")) + "\">" + "Id: " + dataArray[i].replace(".wav","") + "<\a>";                    
                }
                return result;
            } else {
                return "";        
            }
        };
        
        
        if (_table){
            _table.updateData(values); 
            return;
        }
        var columns=[];
        var colIndex=0;
        //Prepare columns
        try{
           var parameters=_objectHandlerController.getAllParametersFromForm(form);
        } catch(ex){
            alert("Error in form.jSONData is not valid json");
            return;
        }
        if (_data.objectTemplate.belongToDepartment&&!global.defaultDepartment){
           columns.push({data:"department",title:"Department",render:_renderCell});
           colIndex++;
        }
        
        for (var index=0;index<parameters.length;index++){
            if (!parameters[index].hidden&&parameters[index].listGridName&&parameters[index].listGridName!==""){
                _hasColumns=true;
                if (parameters[index].name==="name"){
                    columns.push({name:"name",data:parameters[index].name,title:parameters[index].listGridName,render:_renderCell});
                    colIndex++;
                } else {
                    _columnsParameters.push(colIndex);
                    if (parameters[index].type==="PROMPT_SELECT"){
                        columns.push({data:parameters[index].name,title:parameters[index].listGridName,render:_renderPromptCell});
                        colIndex++;                        
                    } else if (parameters[index].type==="OBJECT_SELECT"){
                        columns.push({data:parameters[index].name,title:parameters[index].listGridName,render:_renderObjectCell});
                        _objectSelectFields.push(parameters[index].name);
                        colIndex++; 
                    } else {
                        columns.push({data:parameters[index].name,title:parameters[index].listGridName,render:_renderCell});
                        colIndex++;
                    }       
                }
            }            
        } 
        
        if (!_hasColumns){
            //Name property is mandatory
            columns.push({data:"name",title:"Name",render:_renderCell});
            colIndex++;
            columns.push({data:"description",title:"Description",render:_renderCell});
            colIndex++;
        }
        
        //Special case when listing prompts
        if (data.objectTemplate.type==="PROMPT_LIST"){
            for(var i=0;i<_languages.length;i++){
                columns.push({data:_languages[i].languageCode,title:_languages[i].name,render:_renderAudioFile});
                colIndex++;
            }
        }

        columns.push({visible:false,data:"usage",title:"Usage",render:_renderCell});        
        _columnsInfo.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"changed",title:"Changed",render:_renderDate});
        _columnsInfo.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"changedBy",title:"Changed By",render:_renderCell});        
        _columnsInfo.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"created",title:"Created",render:_renderDate});
        _columnsInfo.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"createdBy",title:"Created By",render:_renderCell});
        _columnsInfo.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"deleted",title:"Deleted",render:_renderDate});
        _columnsHistory.push(colIndex);
        colIndex++;
        columns.push({visible:false,data:"deletedBy",title:"Deleted By",render:_renderCell});
        _columnsHistory.push(colIndex);
        colIndex++;
    
        _table=new Classes.Common.ListGrid(_$panel.find(".table-container"),_convertValues(values),columns,false,function(_$table){
            _$table.find("tbody").on( 'click', 'button', _playRecording_Click);
            _$table.find("tbody").on( 'click', '.link-open-object', _openObject_Click);    
            
            if (data.objectTemplate.type.indexOf("WORKFLOW")>=0||data.objectTemplate.type.indexOf("CALENDAR")>=0) {
                _showInfoMode();
            }
    
        });
        _table.on_DbClick=_this.openObject;        
    };
    
    var _showInfoMode=function(){
        if (_showDeleted||_getAll){            
            _showDeleted=false;
            _getAll=false;
            _this.updateObjectList();
        }    
        for(var i=0;i<_columnsParameters.length;i++){
            _table.getTable().column(_columnsParameters[i]).visible(false);
        }
        for(var i=0;i<_columnsInfo.length;i++){
            _table.getTable().column(_columnsInfo[i]).visible(true);
        }
        for(var i=0;i<_columnsHistory.length;i++){
            _table.getTable().column(_columnsHistory[i]).visible(false);
        }
    };
    
    var _showParameterAllMode=function(){
        if (_showDeleted||!_getAll){            
            _showDeleted=false;
            _getAll=true;
            _this.updateObjectList();
        }
        for(var i=0;i<_columnsParameters.length;i++){
            _table.getTable().column(_columnsParameters[i]).visible(true);
        }
        for(var i=0;i<_columnsInfo.length;i++){
            _table.getTable().column(_columnsInfo[i]).visible(false);
        }
        for(var i=0;i<_columnsHistory.length;i++){
            _table.getTable().column(_columnsHistory[i]).visible(false);
        }
    };
    
    var _showParameterMode=function(){
        if (_showDeleted||_getAll){            
            _showDeleted=false;
            _getAll=false;
            _this.updateObjectList();
        }
        for(var i=0;i<_columnsParameters.length;i++){
            _table.getTable().column(_columnsParameters[i]).visible(true);
        }
        for(var i=0;i<_columnsInfo.length;i++){
            _table.getTable().column(_columnsInfo[i]).visible(false);
        }
        for(var i=0;i<_columnsHistory.length;i++){
            _table.getTable().column(_columnsHistory[i]).visible(false);
        }
    };
    
    var _showHistoryMode=function(){
        if (!_showDeleted){            
            _showDeleted=true;
            _getAll=false;
            _this.updateObjectList();
        }
        
        for(var i=0;i<_columnsParameters.length;i++){
            _table.getTable().column(_columnsParameters[i]).visible(false);
        }
        for(var i=0;i<_columnsInfo.length;i++){
            _table.getTable().column(_columnsInfo[i]).visible(false);
        }
        for(var i=0;i<_columnsHistory.length;i++){
            _table.getTable().column(_columnsHistory[i]).visible(true);
        }
    };
    
    /***
     * CONSTRUCTOR
     * 
     */    
     
    
     
     var renderData={
         showGetAll:(data.objectTemplate.configServerFirst||data.objectTemplate.scriptGetAll)?true:false,
         showParameters:(data.objectTemplate.type.indexOf("WORKFLOW")>=0||data.objectTemplate.type.indexOf("CALENDAR")>=0)?false:true         
    };
    _$panel=$(Mustache.render(_viewTemplates.objectListView.result,renderData));    
    
    _$panel.find("a[href='#info']").on('shown.bs.tab',_showInfoMode);
    
    _$panel.find("a[href='#parameters']").on('shown.bs.tab',_showParameterMode);
    
    _$panel.find("a[href='#history']").on('shown.bs.tab',_showHistoryMode);
    
    _$panel.find("a[href='#parametersAll']").on('shown.bs.tab',_showParameterAllMode);    
    

    
    
    //Special case when listing prompts
    if (data.objectTemplate.type==="PROMPT_LIST"){
        Functions.ajaxRequest("GetLanguages",null,function(data){
            _languages=data;
            _fillTable(_data.form,_data.values);
        });
    } else {
        _fillTable(_data.form,_data.values);
    }
        
    
      
     
    
};