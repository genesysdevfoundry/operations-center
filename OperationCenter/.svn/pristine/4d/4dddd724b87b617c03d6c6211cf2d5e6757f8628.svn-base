Classes.ObjectHandler.ObjectHandlerController=function(mainApplication){
    var _this=this;
    var _mainApplication=mainApplication;     
    var _viewTemplates={            
        objectView:{
            url:"Classes/ObjectHandler/ObjectView.html"},
        objectListView:{
            url:"Classes/ObjectHandler/ObjectListView.html"}       
    };
    
    var _idPanelCounter=0;
    var _openObjects=[];
    var _openObjectLists=[];
    

    this.getMainApplication=function(){
        return _mainApplication;
    };
    
    //A help function
    this.getAllParametersFromForm=function(data){
        var parameterArray=[];
        
        var getParameterRecursive=function(data){
            if (data instanceof Array){            
                for (var i=0;i<data.length;i++){
                    getParameterRecursive(data[i]);
                }
            } else if (data instanceof Object){            
                var parameter=data;
                if (parameter.name&&parameter.type&&parameter.type!=="SECTION"){
                    parameterArray.push(parameter);
                }
                if (parameter.content){
                    getParameterRecursive(parameter.content);
                }
            }
        };
        getParameterRecursive(data);
        return parameterArray;
    };
    
    var _openObjectListPanel=function(data,id_ObjectTemplate){
        _idPanelCounter++;
        var newPanelId="OBJECT_LIST_" + _idPanelCounter;
        var objectList=new Classes.ObjectHandler.ObjectList(_this,data,_viewTemplates,id_ObjectTemplate);
        objectList.id_Panel=newPanelId;        
        _openObjectLists.push(objectList);    
        
        var buttons=[                
                {caption:"Open",name:"btOpen",onClick:objectList.openObject},
                {caption:"New",name:"btNew",onClick: function(){mainApplication.getEventHandler().fire("NEW_OBJECT",{id_ObjectTemplate:id_ObjectTemplate});}},
                {caption:"Delete",name:"btDelete",onClick:objectList.deleteObject},
                {caption:"Reload",name:"btReload",onClick:objectList.updateObjectList}                
            ];
        if (data.scriptGetAll){
            buttons.push({caption:"Reload All",name:"btReloadAll",onClick:objectList.updateObjectListAll,setButtonHandle:function(value){objectList.setBtReloadAllHandle(value);}});
        }
            
        
        
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:data.templateName + " List",
            id_Panel:newPanelId,
            icon:"fa-files-o",
            $panel:objectList.get$panel(),
            buttons:buttons
        });

    };
    
    
    var _openObjectPanel=function(data){        
        //Create new script Object and store it in open surveys
        _idPanelCounter++;
        var newObject={};
        var tabId= Math.trunc(Math.random()*100000);
        data.tabIdMain="tab_" + tabId;
        data.tabIdHistory="tab_" + (tabId +1);    
        newObject=new Classes.ObjectHandler.Object(_this,data,_viewTemplates);
        newObject.id_Panel="OBJECT_" + _idPanelCounter;
        _openObjects.push(newObject);
        
        var tabs=[];       
        if (data.objectTemplate.history){
            tabs=[{caption:"Edit",active:true,tabId:data.tabIdMain},                
                {caption:"History",active:false,tabId:data.tabIdHistory,onActive:newObject.historyActive}];
        }
       
        //Create the panel
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:data.objectTemplate.name + " - " + data.values.name,
            id_Panel:newObject.id_Panel,
            icon:"fa-file-text-o",
            $panel:newObject.get$Panel(),                    
            buttons:[
                {caption:"Save",name:"btSave",onClick:newObject.save},
                {caption:"Save & Close",name:"btSaveClose",onClick:newObject.saveAndClose},
                {caption:"Close",name:"btClose",onClick:newObject.close}
            ],
            tabs:tabs
        });
    };
    
        
        
    var _openObject=function(data){
        //Check if object is already opened
        for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].getObjectId()===data.id_Object){
                //Select the object
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjects[i].id_Panel);
                return;
            }
        }        
        if (!data.id_Object){
            data.id_Object=0;
        }
        Functions.ajaxRequest("GetObject",data,_openObjectPanel);
    };
      
    _openObjectFromObjectTemplateId=function(id_ObjectTemplate){
        //Check if object is already opened
        for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].getObjectTemplateId()===id_ObjectTemplate){
                //Select the object
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjects[i].id_Panel);
                return;
            }
        }        
        Functions.ajaxRequest("GetObjectByObjectTemplateId",{id_ObjectTemplate:id_ObjectTemplate},_openObjectPanel);
        
    }  
    
    var _openObjectList=function(id_ObjectTemplate){
        //Check if Object  is already opened
        for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].getObjectTemplateId()===id_ObjectTemplate){
                //Select the object list
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjectLists[i].id_Panel);
                return;
            }
        }
        Functions.ajaxRequest("GetObjects",{id_ObjectTemplate:id_ObjectTemplate},function(data){_openObjectListPanel(data,id_ObjectTemplate)});
    };
  
    var _objectClosed=function(id_Panel){
         for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].id_Panel===id_Panel){
                //Remove object from open objects
                _openObjects.splice(i,1);
                break;
            }
        }
    };
    
    var _updateObjectList=function(id_ObjectTemplate){
         for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].getObjectTemplateId()===id_ObjectTemplate){
                _openObjectLists[i].updateObjectList();
                
                break;
            }
        }
    };
    
    
    var _objectListClosed=function(id_Panel){
         for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].id_Panel===id_Panel){
                //Remove survey from open surveys
                _openObjectLists.splice(i,1);
                break;
            }
        }
    };
    
    var _newObject=function(paramData){
        Functions.ajaxRequest("GetObjectTemplate",{id_ObjectTemplate:paramData.id_ObjectTemplate},function(data){
            var newData={
               values:{
                   id_Object:0,                                                              
                   name:paramData.name||"New " + data.objectTemplate.name
               },
               objectTemplate:data.objectTemplate,
               history:[],
               newObject:true,
               callback:paramData.callback||null
           };
           _openObjectPanel(newData);                
        });        
    };
    
    
    var _objectListPanelSelected=function(id_Panel){
        for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].id_Panel===id_Panel){
                _openObjectLists[i].panelSelected();
                break;
            }
        }
    }
    _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_OBJECT":
                _openObject(eventData);   
            break;
            case "OPEN_OBJECT_FROM_OBJECT_TEMPLATE_ID":
                _openObjectFromObjectTemplateId(eventData);   
            break;
            case "PANEL_REMOVED":
                if (eventData.indexOf("OBJECT_LIST")===0){
                    //The closed tab is a object list tab
                    _objectListClosed(eventData);
                } else if (eventData.indexOf("OBJECT_")===0){
                    //The closed tab is a object tab
                    _objectClosed(eventData);
                }
            break;
            case "OPEN_OBJECT_LIST":          
                _openObjectList(eventData);   
            break;            
            case "UPDATE_OBJECT_LIST":
                _updateObjectList(eventData);
            break;
            case "NEW_OBJECT":
                _newObject(eventData);
            break;  
            case "PANEL_SELECTED":
                if (eventData.indexOf("OBJECT_LIST")===0){
                    //The closed tab is a object list tab
                    _objectListPanelSelected(eventData);
                }
            break;    
            
        }
    };
        
    /**Constructor*/
     mainApplication.getEventHandler().register(_eventListener);
     Functions.multiHTMLLoad(_viewTemplates);
    
};