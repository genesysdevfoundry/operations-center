Classes.WorkflowDiagram.Workflow=function(objectHandlerController,data,viewTemplates){
    var _this=this;
    var _objectHandlerController=objectHandlerController;
    var _$panel=null;        
    var _data=data;
    var _name=data.values.name;    
    var _id_Object=data.values.id_Object;
    var _id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
    var _historyTable;
    var _form;    
    var _workflowDiagram;
    var _$diagramPanel;
    var _$addBlocksPanel;
    var _blockTemplates=[];
    var _btRevertHandler;
    var _rollbackData;
    var _revertData=null;
    var _selectDepartmentHandler;
    
    this.setSelectDepartmentHandler=function(value){
        _selectDepartmentHandler=value;     
        if (_data.values.id_Department){
            _selectDepartmentHandler.val(_data.values.id_Department);
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        } else {
            _selectDepartmentHandler.val(objectHandlerController.getLastDepartmentId());
        }    
    };
    
    this.getObjectHandlerController=function(){
        return _objectHandlerController;
    };
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getObjectId=function(){
        return _id_Object;
    };
    
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };
    
    this.revert=function(){
        _form.setValues(_revertData);
        _workflowDiagram.clear();
        _workflowDiagram.setData(_revertData.workflow);        
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _revertData.name});
        _btRevertHandler.hide();
        _revertData=null;
    };
    
    this.setBtRevertHandle=function(value){
        _btRevertHandler=value;
        value.hide();
        value.removeClass("btn-primary").addClass("btn-warning");        
    };
    
    this.historyActive=function(){
        _historyTable.draw();
    };
    
    this.saveAndClose=function(){
        _this.save(true);
    };
    
    this.close=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    this.getBlockTemplates=function(){
        return _blockTemplates;
    };
    
    this.diagramActive=function(){
        if (_rollbackData){
            _form.setValues(_rollbackData);
            
            _workflowDiagram.clear();
            _workflowDiagram.setData(_rollbackData.workflow);
            
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + " (" + _rollbackData.dateStamp + ")"});
            _name="";     
            _rollbackData=null;            
        }
    };
    
    this.getBlockTemplatesById=function(objectId){
        for (var i=0;i<_blockTemplates.length;i++){
            if (objectId===_blockTemplates[i].id_Object){
                return _blockTemplates[i];
            }                
        }        
    };
    
    this.formChanged=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };

    /***
     * Open block parameters in the sidebar
     * @returns {undefined}
     */
    this.showBlockParameters=function(block){
        if (!block){
            _this.closeAddBlockPanel();
            return;
        }
        //Set a default form with description as the only parameter
        var formContent={content:[
                   {type:"SECTION",                        
                    content:[
                        {
                            type:"TEXTAREA",
                            name:"description",
                            displayName:"Description",
                            defaultValue:"",
                            listGridName:"Description",
                            helpText:"",
                            mandatory:0
                        }
                    ],
                    title:"General",                        
                    collapsable:0,
                    collapsed:0,
                    solid:0,
                    colorSchema:"box-primary"
                }
            ]
        };

        //Render parameter form

        if (block.getData().form&&block.getData().form.content){
            //Put description section as the first element
            formContent.content=formContent.content.concat(block.getData().form.content);        
        } 

        var blockParameterForm=new Classes.DynamicForm.Form(_objectHandlerController.getMainApplication(),formContent,true);
        blockParameterForm.renderForm(function(){
            _$panel.find(".parameter-panel").find(".dynamic-form").remove(); 
            _$panel.find(".parameter-panel").append(blockParameterForm.get$Panel());            
            blockParameterForm.bindData(block.getData().values);
            blockParameterForm.subscribeChangeEvent("description",function(value){
                block.setDescription(value);
            });            
            blockParameterForm.eventChanged=block.changed;            
        });        

        _this.setSidePanelName(block.getData().name + " Block (Id:" + block.getIndex() + ")");
        
        _$panel.find(".sidenav").width("450px");        
        _$panel.find(".add-blocks-panel").hide();
        _$panel.find(".parameter-panel").show();
        _$panel.find(".workflow-diagram-container").css("width","calc(100% - 450px");
        _$panel.find(".workflow-paste-button").css("right","520px");
        _$panel.find(".workflow-copy-button").css("right","495px");        
        _$panel.find(".workflow-add-block-button").show().css("right","470px");        
        
        
    };
    
    /***
     * 
     * @returns BlockParameterPanel as a jquery object
     */
    this.isBlockParametersPanelVisible=function(){
        return _$panel.find(".parameter-panel").css("display")!=="none";
    };
    
    
    this.setSidePanelName=function(name){
        _$panel.find(".sidepanel-title").text(name);
    };
    
    this.save=function(close){   
       
        var saveData=_form.getValues();
        
        saveData.newObject=(_data.newObject)?true:false;
        
        saveData.id_Object=_id_Object;
        saveData.id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
        saveData.workflow=_workflowDiagram.getData();        
        
        if (_selectDepartmentHandler){
            saveData.id_Department=_selectDepartmentHandler.val();
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        }
        
        Functions.ajaxRequest("SaveObject",saveData,function(data){            
            _revertData=null;
            _btRevertHandler.hide();
            
            if (!_id_Object){
                _id_Object=parseInt(data.id_Object);                
            }
            
            _data.newObject=false;
            
                        
            if (_name!==saveData.name){
                _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + saveData.name});
                _name=saveData.name;
            }
            
            _form.isSaved();   
                        
            _objectHandlerController.getMainApplication().getEventHandler().fire("UPDATE_OBJECT_LIST",_data.objectTemplate.id_ObjectTemplate);
        
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (close===true){
               _this.close();            
            } else {
               _fillHistoryTable(data.history);
            }
            
            //Callback points back to the object select parameter, when the "new" button is clicked
            if (_data.callback){
                _data.callback(_id_Object);
                _data.callback=null;
            }
            _workflowDiagram.isSaved();
        });
    };
    
    
    var _renderCell=function(data){ 
        if (typeof data ==="undefined"){
            return "";
        }
        
        if (data.toString().indexOf("$")===0){
            return "<font style=\"color:red;font-weight:bolder;\">" + data.replace("$CH$","") + "</font>";
        } else {
            return data;
        }
    };
        
    var _fillHistoryTable=function(historyData){
        
        //Prepare data
        
        if (_historyTable){
            _historyTable.updateData(historyData);
        } else {
            var columns=[
                {data:"dateStamp",title:"Date"},
                {data:"userName",title:"Saved By"}                
            ];
            
            _historyTable=new Classes.Common.ListGrid(_$panel.find(".table-container"),historyData,columns);
            _historyTable.on_DbClick=_rollback; 
        }        
    };
    


    var renderBlock=function(){
        
    };

 
    
    var _createTemplateBlock=function(data){
        var renderData={template:true,
                        name:data.name,
                        icon:data.icon,
                        description:""}
        
        var blockTemplate=$(Mustache.render(viewTemplates.workflowBlockView.result,renderData));

        blockTemplate.addClass("bg-default")                
                .css("position","relative")
                .css("left","2px")
                .css("top","10px")
                .css("margin-bottom","10px")                
                .attr("data-index",data.index);
                
                
       
        blockTemplate.draggable({            
            appendTo: _$panel.find(".drag-area"),
            helper: "clone",            
            revert: "invalid",
            scroll: false
        }).on('dragstart', function (e, ui) {
            $(ui.helper).css('z-index','9999');
        });
       
        
        //blockTemplate.find(".workflow-block-description").text("");
        
        
        _$addBlocksPanel.append(blockTemplate);
    };
    
    
    var _addBlock=function(){
        _$panel.find(".sidenav").width("200px"); 
        _this.setSidePanelName("Available Blocks");
        _$panel.find(".add-blocks-panel").show();
        _$panel.find(".parameter-panel").hide();
        _$panel.find(".workflow-diagram-container").css("width","calc(100% - 200px");
        _$panel.find(".workflow-paste-button").css("right","245px");
        _$panel.find(".workflow-copy-button").css("right","220px"); 
        _$panel.find(".workflow-add-block-button").hide();
    };
    
    this.closeAddBlockPanel=function(){        
        _$panel.find(".sidenav").width("0px");
        _$panel.find(".parameter-panel").hide();
        _$panel.find(".workflow-diagram-container").css("width","100%");
        _$panel.find(".workflow-paste-button").css("right","70px");
        _$panel.find(".workflow-copy-button").css("right","45px");        
        _$panel.find(".workflow-add-block-button").css("right","20px").show();
    };
    
    var _createTemplateBlocks=function(data){
        _blockTemplates=data;
        for (var i=0;i<data.length;i++){
            data[i].index=i;
            _createTemplateBlock(data[i]);
        }
    };
    
    var _rollback=function(){
        
        _rollbackData=_historyTable.getSelectedRecord();
                
        if (_rollbackData){
            if (!_revertData){
                _revertData=_form.getValues();
                _revertData.workflow=_workflowDiagram.getData();
            }
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + "<font style='color:red;'> Old Version From " + _rollbackData.dateStamp + "</font>"});
            _name="";
        }
        
        _objectHandlerController.getMainApplication().getEventHandler().fire("SHOW_TAB",{panelId:_this.id_Panel,tabIndex:1});
        
        _btRevertHandler.show();
           
    };
    
    
    var _paste=function(){
        var clipboardData=objectHandlerController.getClipboard();
        if (clipboardData&&typeof clipboardData==="object"&&clipboardData.type==="WORKFLOW"){
            //Center the position of the pasted data
            var minTop=9999999,minLeft=9999999;
            for (key in clipboardData){
                if(key.indexOf("BLOCK_")===0){
                    if (clipboardData[key].positionData.top<minTop){
                        minTop=clipboardData[key].positionData.top;
                    }
                    if (clipboardData[key].positionData.left<minLeft){
                        minLeft=clipboardData[key].positionData.left;
                    }
                }
            }            
            for (key in clipboardData){
                if(key.indexOf("BLOCK_")===0){
                    clipboardData[key].positionData.top=clipboardData[key].positionData.top-minTop+_$panel.find(".workflow-diagram-container").scrollTop();                
                    clipboardData[key].positionData.left=clipboardData[key].positionData.left-minLeft+_$panel.find(".workflow-diagram-container").scrollLeft();
                }
            }
            _workflowDiagram.setData(objectHandlerController.getClipboard(),true);
        }
    };
    
    var _copySelection=function(){
        //Save slected data to the clipboard (is stored in the object handler object for enableing copy object between callflows)
        var clipboardData=_workflowDiagram.getSelectedBlocks();
        clipboardData.type="WORKFLOW";
        objectHandlerController.setClipboard(clipboardData);
        _workflowDiagram.resetSelection();
    };
    
    /***
     * 
     * Constructor
     * 
     */    
    
    
    //Render form
    _$panel=$(Mustache.render(viewTemplates.workflowView.result,data));
    
    _$panel.find("button[name='btRollback']").click(_rollback);    
    _$panel.find(".workflow-add-block-button").click(_addBlock);
    _$panel.find(".workflow-close-add-blocks-panel").click(_this.closeAddBlockPanel);
    _$panel.find(".workflow-paste-button").click(_paste);
    _$panel.find(".workflow-copy-button").click(_copySelection);    
  
  
    //Declare tooltip functionality 
    _$panel.find(".workflow-add-block-button").tooltip({
        placement: 'top',
        animated:"fade",
        title: 'Open Block List',
        container: 'body'            
    });

    
    _$panel.find(".workflow-paste-button").tooltip({
        placement: 'top',
        animated:"fade",
        title: 'Paste',
        container: 'body'            
    });

    
    _$panel.find(".workflow-copy-button").tooltip({
        placement: 'top',
        animated:"fade",
        title: 'Copy Selection',
        container: 'body'            
    });

    
    
    _$addBlocksPanel=_$panel.find(".add-blocks-panel");
    
        
    var params={content:[{type:"SECTION",collapsed:false,title:"General",content:Workflow.workflowParameters}]};
    
    _form=new Classes.DynamicForm.Form(objectHandlerController.getMainApplication(),params,true);
    _form.renderForm(function(){
        _$panel.find(".panel-properties").append(_form.get$Panel());
        _form.bindData(data.values);
        _form.eventChanged=_this.formChanged;        
    });
    
    
    
    _workflowDiagram=new Classes.WorkflowDiagram.WorkflowDiagram(this,viewTemplates,_$panel.find(".panel-diagram"));
    _fillHistoryTable(data.history);
            
    Functions.ajaxRequest("GetWorkflowBlocks",{id_ObjectTemplate:_id_ObjectTemplate},function(blockTemplates){
        _createTemplateBlocks(blockTemplates);
        setTimeout(function(){
            if (data.values.workflow){
                _workflowDiagram.setData(data.values.workflow);
            } else {
                _workflowDiagram.newWorkflow();
            }
        },200);    
    });
};