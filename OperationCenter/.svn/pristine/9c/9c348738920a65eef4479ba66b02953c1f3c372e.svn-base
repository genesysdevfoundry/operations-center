/***
 * 
 * Base Attributes
 */

var parameterAttributes={};
parameterAttributes.baseParameterAttributes=[
    {
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"displayName",
        displayName:"Display Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"defaultValue",
        displayName:"Default Value",
        mandatory:false,            
        type:"TEXT"
    },{
        name:"listGridName",
        displayName:"List Grid Name",
        mandatory:false,            
        type:"TEXT"
    }
];



/***
 * 
 * Checkbox
 */

parameterAttributes.checkbox=[
    {
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"displayName",
        displayName:"Display Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"defaultValue",
        displayName:"Default Value",        
        type:"CHECKBOX"
    },{
        name:"listGridName",
        displayName:"List Grid Name",
        mandatory:false,            
        type:"TEXT"
    },{
        name:"readOnlyMode",
        displayName:"Read Only Mode",
        mandatory:true,
        type:"SELECT",        
        defaultValue:"EDITABLE",
        listData:[
                {text:"Editable",id:"EDITABLE"},
                {text:"Read Only",id:"READ_ONLY"},
                {text:"Edit Once",id:"EDIT_ONCE"}                
        ]
    },{
        type:"COLUMN",
        columns:2,
        content:[
            {
                name:"onNameDisplay",
                displayName:"On Name Display",
                mandatory:false,            
                defaultValue:"On",
                type:"TEXT"
            },{
                name:"offNameDisplay",
                displayName:"Off Name Display",
                mandatory:false,            
                defaultValue:"Off",
                type:"TEXT"
            },{
                name:"onName",
                displayName:"On Name",
                mandatory:false,            
                defaultValue:"",
                type:"TEXT"
            },{
                name:"offName",
                displayName:"Off Name",
                mandatory:false,            
                defaultValue:"",
                type:"TEXT"
            }
        ]
    }    
];


/***
 * 
 * Text
 */

parameterAttributes.text=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.text.push({
    name:"mandatory",
    displayName:"Mandatory",    
    defaultValue:"New Parameter",
    type:"CHECKBOX"
});

parameterAttributes.text.push({
        name:"readOnlyMode",
        displayName:"Read Only Mode",
        mandatory:true,
        type:"SELECT",        
        defaultValue:"EDITABLE",
        listData:[
                {text:"Editable",id:"EDITABLE"},
                {text:"Read Only",id:"READ_ONLY"},
                {text:"Edit Once",id:"EDIT_ONCE"}                
        ]
});
    
parameterAttributes.text.push({
    name:"helpText",
    displayName:"Help Text",        
    type:"TEXTAREA"
});

/***
 * 
 * Text Area
 */

parameterAttributes.textarea=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.textarea.push({
    name:"mandatory",
    displayName:"Mandatory",        
    type:"CHECKBOX"
});
parameterAttributes.textarea.push({
    name:"helpText",
    displayName:"Help Text",        
    type:"TEXTAREA"
});

/***
 * 
 * Text Area
 */

parameterAttributes.number=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.number.push({
    name:"mandatory",
    displayName:"Mandatory",        
    type:"CHECKBOX"
});
parameterAttributes.number.push({
    name:"helpText",
    displayName:"Help Text",        
    type:"TEXTAREA"
});
parameterAttributes.number.push({
    name:"min",
    displayName:"Min",        
    defaultValue:"0",
    type:"TEXT"
});
parameterAttributes.number.push({
    name:"max",
    displayName:"Max",        
    defaultValue:"100",
    type:"TEXT"
});
parameterAttributes.number.push({
    name:"step",
    displayName:"Step",        
    defaultValue:"1",
    type:"TEXT"
});

/***
 * 
 * Select
 */

parameterAttributes.select=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.select.push({
    name:"mandatory",
    displayName:"Mandatory",        
    type:"CHECKBOX"
});
parameterAttributes.select.push({
    name:"helpText",
    displayName:"Help Text",       
    type:"TEXTAREA"
});
    
parameterAttributes.select.push({
    name:"scriptId",
    displayName:"Script",  
    mandatory:false,
    type:"SCRIPT_SELECT",
    scriptType:"PARAMETER",    
    jsCode:"output=[{text:\"Name 1\",id:\"name_1\"},\r{text:\"Name 2\",id:\"name_2\"},\r{text:\"Name 3\",id:\"name_3\"}];"
});


parameterAttributes.select.push({
    name:"multiselect",    
    displayName:"Multi Select",
    type:"CHECKBOX"
});

parameterAttributes.select.push({
        name:"readOnlyMode",
        displayName:"Read Only Mode",
        mandatory:true,
        type:"SELECT",        
        defaultValue:"EDITABLE",
        listData:[
                {text:"Editable",id:"EDITABLE"},
                {text:"Read Only",id:"READ_ONLY"},
                {text:"Edit Once",id:"EDIT_ONCE"}                
        ]
});

parameterAttributes.select.push({
    type:"CHILD_SECTION",
    name:"listData",
    displayName:"Options",
    content:[
        {type:"TEXT",name:"text",displayName:"Text",defaultValue:"",listGridName:"",mandatory:0,readOnlyMode:"EDITABLE"},
        {type:"TEXT",name:"id",displayName:"Id",defaultValue:"",listGridName:"",mandatory:0,readOnlyMode:"EDITABLE"}
    ],
    sortOrderFieldName:"sortOrder",
    sortOrderFieldDisplayName:"",
    collapsed:0,
    fixedSortOrder:1
});



/***
 * 
 * Prompt Select
 */

parameterAttributes.prompt_select=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.prompt_select.push({
    name:"mandatory",
    displayName:"Mandatory",    
    defaultValue:"New Parameter",
    type:"CHECKBOX"
});
    
parameterAttributes.prompt_select.push({
    type:"CHILD_SECTION",
    name:"listData",
    displayName:"Options",
    content:[
        {type:"TEXT",name:"text",displayName:"Text",defaultValue:"",listGridName:"",mandatory:0,readOnlyMode:"EDITABLE"}        
    ],
    sortOrderFieldName:"sortOrder",
    sortOrderFieldDisplayName:"",
    collapsed:0,
    fixedSortOrder:1
});


/***
 * 
 * Section
 */

parameterAttributes.section=[{
        name:"title",    
        displayName:"Title",  
        type:"TEXT",
        defaultValue:"New Section...",
        mandatory:true
    },{
        name:"helpText",
        displayName:"Help Text",       
        type:"TEXTAREA"
    },{
        type:"COLUMN",
        columns:4,
        content:[
            {
                name:"collapsable",
                displayName:"Collapsable",    
                defaultValue:false,
                type:"CHECKBOX"
            },{
                name:"collapsed",
                displayName:"Collapsed From Start",    
                defaultValue:false,
                type:"CHECKBOX"
            },{
                name:"solid",
                displayName:"Filled",    
                defaultValue:false,
                type:"CHECKBOX"
            }
        ]
    },{
    name:"colorSchema",
    displayName:"Color Schema",            
    defaultValue:"box-primary",
    mandatory:"true",
    type:"SELECT",
    listData:[
            {id:"box-none",text:"Empty"},
            {id:"box-default",text:"Grey"},
            {id:"box-primary",text:"Blue"},
            {id:"box-info",text:"Light Blue"},
            {id:"box-warning",text:"Yellow"},                
            {id:"box-success",text:"Green"},
            {id:"box-danger",text:"Red"}
            
        ]
    }
    
];



/***
 * 
 * Script select
 */

parameterAttributes.script_select=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.script_select.push({
    name:"mandatory",
    displayName:"Mandatory",    
    defaultValue:"New Parameter",
    type:"CHECKBOX"
});
parameterAttributes.script_select.push({
    name:"scriptType",
    displayName:"Script Type",            
    defaultValue:"New Parameter",
    type:"SELECT",
    listData:[
            {text:"Object Action",id:"OBJECT_ACTION"},
            {text:"Parameter",id:"PARAMETER"},
            {text:"Web Service",id:"WEB_SERVICE"}                
    ]
});

/***
 * 
 * Object Select
 */


parameterAttributes.object_select=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.object_select.push({
    name:"mandatory",
    displayName:"Mandatory",    
    defaultValue:"New Parameter",
    type:"CHECKBOX"
});
parameterAttributes.object_select.push({
    name:"id_ObjectTemplate",
    displayName:"Object Template",    
    defaultValue:"",
    mandatory:true,
    type:"SELECT",
    scriptName:"KVPListGetObjectTemplates"
});

parameterAttributes.object_select.push({
    type:"CHILD_SECTION",
    name:"listData",
    displayName:"Options",
    content:[
        {type:"TEXT",name:"text",displayName:"Text",defaultValue:"",listGridName:"",mandatory:0,readOnlyMode:"EDITABLE"}        
    ],
    sortOrderFieldName:"sortOrder",
    sortOrderFieldDisplayName:"",
    collapsed:0,
    fixedSortOrder:1
});


/***
 * 
 * Form
 */

parameterAttributes.form=[{
        name:"name",
        displayName:"Name",            
        type:"TEXT"
    },{
        name:"description",
        displayName:"Description",            
        type:"TEXT"
    },{
        name:"transparentData",        
        displayName:"Transparent Data", 
        defaultValue:true,
        type:"CHECKBOX"
    },{
        type:"CHILD_SECTION",
        name:"staticParameters",
        displayName:"Static Parameters",
        collapsed:"true",
        content:[
            {
                name:"parameterName",
                displayName:"Parameter Name",
                mandatory:true,
                defaultValue:"",
                type:"TEXT"
            },{
                name:"value",
                displayName:"Value",
                mandatory:false,
                defaultValue:"",
                type:"TEXT"
            }
        ]
    }
];

/***
 * 
 * Form select
 */

parameterAttributes.form_select=parameterAttributes.baseParameterAttributes.slice(0);
parameterAttributes.form_select.push({
    name:"mandatory",
    displayName:"Mandatory",    
    defaultValue:"New Parameter",
    type:"CHECKBOX"
});

/***
 * 
 * Column
 */

parameterAttributes.column=[{
    name:"columns",
    displayName:"Number of Columns",    
    defaultValue:2,
    type:"SELECT",
    listData:[
        {text:"2",id:2},
        {text:"3",id:3},
        {text:"4",id:4}        
    ]
}];

/***
 * 
 * Child Section
 */

parameterAttributes.child_section=[{
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"displayName",
        displayName:"Display Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"collapsed",
        displayName:"Collapsed From Start",    
        defaultValue:false,
        type:"CHECKBOX"
    },{
        name:"fixedSortOrder",
        displayName:"Fixed Sort Order",    
        defaultValue:false,
        type:"CHECKBOX"    
    },{
        name:"sortOrderFieldName",              
        displayName:"Sort Order Field Name",
        mandatory:true,
        defaultValue:"sortOrder",
        type:"TEXT"
    },{
        name:"sortOrderFieldDisplayName",
        displayName:"Sort Order Field Display Name",
        mandatory:true,
        defaultValue:"Sort Order",
        type:"TEXT"
    }
];


/***
 * 
 * Expression builder
 */
parameterAttributes.expression_builder=[{
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"displayName",
        displayName:"Display Name",
        mandatory:true,
        defaultValue:"New Parameter",
        type:"TEXT"
    },{
        name:"listGridName",
        displayName:"List Grid Name",
        mandatory:false,            
        type:"TEXT"
    },{
        name:"scriptId",
        displayName:"Script",  
        mandatory:true,
        scriptType:"PARAMETER",
        type:"SCRIPT_SELECT",        
        jsCode:"output=[\"Skill 1\",\"Skill 2\",\"Skill 3\"];"
    }
];



/***
 * 
 * Access permissions
 * 
 */

parameterAttributes.applyPermissions={
    name:"applyPermissions",
    displayName:"Apply Permissions",    
    defaultValue:false,
    type:"CHECKBOX"
};

parameterAttributes.accessPermissions_1={
    type:"CHILD_SECTION",
    name:"accessPermissions",
    displayName:"Access Permissions",
    content:[{
            name:"accessGroup",
            displayName:"Access Group",
            mandatory:true,
            type:"SELECT",
            defaultValue:"",
            scriptName:"GetAllAccessGroups"
        },{
            name:"permission",
            displayName:"Permission",
            mandatory:true,
            type:"SELECT",
            defaultValue:"READ_WRITE",
            listData:[
                    {text:"Read / Write",id:"READ_WRITE"},
                    {text:"Read Only",id:"READ"}                                    
            ]
        }   
    ],
    collapsed:0,
    fixedSortOrder:0
};

parameterAttributes.accessPermissions_2={    
        type:"SELECT",
        name:"accessGroups",
        displayName:"Access Groups",
        listGridName:"Access Groups",        
        mandatory:0,
        scriptName:"GetAllAccessGroups",
        multiselect:1,
        readOnlyMode:"EDITABLE"    
};

