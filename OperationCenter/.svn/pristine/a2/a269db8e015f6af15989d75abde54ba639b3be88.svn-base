Classes.DynamicForm.Form=function(mainApplication,data){
    var _this=this;
    var _mainApplication=mainApplication;
    var _$panel=null;        
    var _data=data;            
    var _allParameterObjects=[];
    var _childElements=[];
    var _scriptParameters=[];
    var _changed=false;
    
    var _viewTemplates={            
        textView:{
            url:"Classes/Parameters/TextView.html"},
        textAreaView:{
            url:"Classes/Parameters/TextAreaView.html"},        
        selectView:{
            url:"Classes/Parameters/SelectView.html"},
        multiSelectView:{
            url:"Classes/Parameters/MultiSelectView.html"},
        expressionBuilderView:{
            url:"Classes/Parameters/ExpressionBuilderView.html"},
        objectSelectView:{
            url:"Classes/Parameters/ObjectSelectView.html"},
        numberView:{
            url:"Classes/Parameters/NumberView.html"},
        sectionView:{
            url:"Classes/Parameters/SectionView.html"},        
        checkboxView:{
            url:"Classes/Parameters/CheckboxView.html"},
        scriptSelectView:{
            url:"Classes/Parameters/ScriptSelectView.html"},
        formSelectView:{
            url:"Classes/Parameters/FormSelectView.html"},
        childSectionView:{
            url:"Classes/Parameters/ChildSectionView.html"},
        childSectionDialogView:{        
            url:"Classes/Parameters/ChildSectionDialogView.html"},
        promptSelectPromptView:{        
            url:"Classes/Parameters/PromptSelectPromptView.html"},
        promptSelectView:{        
            url:"Classes/Parameters/PromptSelectView.html"}        
    };
        
    this.setValues=function(values){
        for(var i=0;i<_allParameterObjects.length;i++){
            var paramName=_allParameterObjects[i].getName();
            if (paramName){
                _allParameterObjects[i].setValue(values[paramName]);
                if (_allParameterObjects[i].setHiddenValue){
                    _allParameterObjects[i].setHiddenValue(values["_" + paramName]);
                }
            }
        }
    };
    
    this.subscribeChangeEvent=function(elementName,eventHandler){
        for(var i=0;i<_allParameterObjects.length;i++){
            if (_allParameterObjects[i].getName()===elementName&&_allParameterObjects[i].subscribeChangeEvent){                
                _allParameterObjects[i].subscribeChangeEvent(eventHandler);
            }
        }
    };
    
    this.bindData=function(data){
        for(var i=0;i<_allParameterObjects.length;i++){
            if (_allParameterObjects[i].bindData){
                _allParameterObjects[i].bindData(data);
            }
        }        
        
        this.setValues(data);
    };
    
    this.checkValidation=function(){
        for(var i=0;i<_allParameterObjects.length;i++){
            if (!_allParameterObjects[i].validate()){
                return false;
            }
        }
        return true;
    };
    
    this.getValues=function(){        
        var result={};
        for(var i=0;i<_allParameterObjects.length;i++){
            result[_allParameterObjects[i].getName()]=_allParameterObjects[i].getValue();
            if (_allParameterObjects[i].getHiddenValue){
                result["_" + _allParameterObjects[i].getName()]=_allParameterObjects[i].getHiddenValue();
            }
        }
        return result;
    };
    
    this.isSaved=function(){
        for(var i=0;i<_childElements.length;i++){
            _childElements[i].isSaved();
        }
        _changed=false;
    };
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getMainApplication=function(){
        return mainApplication;
    };
    
    var _collectScriptParameters=function(data){
        if (data instanceof Array){            
            for (var i=0;i<data.length;i++){
                _collectScriptParameters(data[i]);
            }
        } else if (data instanceof Object){            
            var parameter=data;
            
            if (parameter.scriptId){
                parameter.scriptRequestString="ID_" + parameter.scriptId;
                _scriptParameters.push(parameter);
            }
            
            if (parameter.scriptName){
                parameter.scriptRequestString="NAME_" + parameter.scriptName;
                _scriptParameters.push(parameter);
            }
            
            if (parameter.type==="OBJECT_SELECT"){
                parameter.scriptRequestString="NAME_KVPListGetObjects|" + parameter.id_ObjectTemplate;
                _scriptParameters.push(parameter);
            }
            
            if (parameter.type==="SCRIPT_SELECT"){
                parameter.scriptRequestString="NAME_KVPListGetScripts|" + parameter.scriptType;
                _scriptParameters.push(parameter);
            }
            

            if (parameter.type==="FORM_SELECT"){
                parameter.scriptRequestString="NAME_KVPListGetForms";
                _scriptParameters.push(parameter);
            }
            
            if (parameter.type==="PROMPT_SELECT"){
                parameter.scriptRequestString="NAME_KVPListGetPrompts";
                _scriptParameters.push(parameter);
            }



            
            
            if (parameter.content){
                _collectScriptParameters(parameter.content);
            }
        }
    };
    
    var _getScriptRequestArray=function(parameters){        
        _collectScriptParameters(parameters);
        
        var scriptRequestArray=[];
        
        var match;
        for(var i=0;i<_scriptParameters.length;i++){
            match=false;
            for(var n=0;n<scriptRequestArray.length;n++){
                if (scriptRequestArray[n]===_scriptParameters[i].scriptRequestString){
                    match=true;
                }            
            }
            if (!match){
                scriptRequestArray.push(_scriptParameters[i].scriptRequestString);
            }
            
        }
        return scriptRequestArray;        
    };
            
    var _prepareParameters=function(scriptData){
        
        //Get scriptdata from key
        var getScriptData=function(scriptId){
            for (var key in scriptData){
                if (key===scriptId){
                    return scriptData[key];
                }
            }
        };
       
         //Get parameters that points to other objects        
        for(var i=0;i<_scriptParameters.length;i++){
            _scriptParameters[i].scriptData=getScriptData(_scriptParameters[i].scriptRequestString);
            if (!(_scriptParameters[i].scriptData instanceof Array)){
                _scriptParameters[i].scriptData=[{name:"[ERROR fetching data]",value:"No Data"}];                
            }
        }
    };
    
    //Is called from elements in childElements array
    var _changedEventHandler=function(){        
        if (!_changed){
            _changed=true;
            if (_this.eventChanged){
                _this.eventChanged();
            }
        }
    };
    
    
    
    this.renderFormElements=function(content,$panel,changedEventHandler){        
        var elements=[];
        if (content&&content.length){
            for (var elemIndex=0;elemIndex<content.length;elemIndex++){
                var newElement=null;                    
                switch (content[elemIndex].type){
                    case "OBJECT_SELECT":
                        newElement=new Classes.Parameters.ObjectSelect(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "SELECT":
                        newElement=new Classes.Parameters.Select(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "TEXT":
                        newElement=new Classes.Parameters.Text(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "TEXTAREA":
                        newElement=new Classes.Parameters.TextArea(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "SECTION":
                        newElement=new Classes.Parameters.Section(_this,content[elemIndex],_viewTemplates);
                        break;
                    case "NUMBER":
                        newElement=new Classes.Parameters.Number(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "MULTI_SELECT":
                        newElement=new Classes.Parameters.MultiSelect(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "EXPRESSION_BUILDER":
                        newElement=new Classes.Parameters.ExpressionBuilder(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "PROMPT_SELECT":
                        newElement=new Classes.Parameters.PromptSelect(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "CHECKBOX":
                        newElement=new Classes.Parameters.Checkbox(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "SCRIPT_SELECT":
                        newElement=new Classes.Parameters.ScriptSelect(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "FORM_SELECT":
                        newElement=new Classes.Parameters.FormSelect(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;
                    case "CHILD_SECTION":
                        newElement=new Classes.Parameters.ChildSection(_this,content[elemIndex],_viewTemplates);
                        _allParameterObjects.push(newElement);
                        break;                    
                    case "COLUMN":
                        newElement=new Classes.Parameters.Column(_this,content[elemIndex],_viewTemplates);                        
                        break; 
                    default:
                        break;
                }   
                if (newElement){
                    elements.push(newElement);
                    $panel.append(newElement.get$Panel());                        
                    newElement.eventChanged=changedEventHandler;
                }
                if (content[elemIndex].type==="EMPTY"){
                    $panel.append($("<div>"));                        
                }
            }
        }  
        return elements;
    };    
    
    this.renderForm=function(callback){
        //Make sure tahe we have all parameter view loaded
        if (!staticViewTemplate){
            Functions.multiHTMLLoad(_viewTemplates,function(){
                staticViewTemplate=_viewTemplates;
                _this.renderForm(callback)
            });
            return;
        } else{
            _viewTemplates=staticViewTemplate;
        }
        
        //Render form
        _$panel=$("<div>");
        _$panel.addClass("dynamic-form");

        
        _mainApplication.getCacheHandler().getData(_getScriptRequestArray(data),function(resultData){        
            _prepareParameters(resultData);            
            _childElements=_this.renderFormElements(data.content,_$panel,_changedEventHandler);
            if (callback){
                callback();
            }
        });     
    };
    
    /***
     * 
     * Constructor
     * 
     */
};

//Static
var staticViewTemplate;