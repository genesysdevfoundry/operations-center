Classes.Calendar.Calendar=function(objectHandlerController,data,viewTemplates){
    var _this=this;
    var _objectHandlerController=objectHandlerController;
    var _$panel=null;        
    var _data=data;
    var _name=data.values.name;
    var _id_Object=data.values.id_Object;
    var _id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
    var _historyTable;
    var _form;    
    var _weekCalendar;
    var _specialDyasCalendar;
    var _specialDaysCalendar;
    var _datepickerAddDate;
    var _isActiv
    var _rollbackData;
    var _btRevertHandler;
    var _revertData;
    
    this.getObjectHandlerController=function(){
        return _objectHandlerController;
    };
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getObjectId=function(){
        return _id_Object;
    };
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };
    
    this.historyActive=function(){
        _historyTable.draw();
    };
    
    this.editActive=function(){
        if (_rollbackData){
            _form.setValues(_rollbackData);
            _weekCalendar.setValue(_setChangedFlag(_rollbackData.weekData));
            _specialDaysCalendar.setValue(_setChangedFlag(_rollbackData.specialDayData));
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + " (" + _rollbackData.dateStamp + ")"});
            _name="";     
            _rollbackData=null;            
        }
    };

    this.revert=function(){
        _form.setValues(_revertData);
        _weekCalendar.setValue(_revertData.weekData);
        _specialDaysCalendar.setValue(_revertData.specialDayData);
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _revertData.name});
        _btRevertHandler.hide();
    };
    
    this.setBtRevertHandle=function(value){
        _btRevertHandler=value;
        value.hide();
        value.removeClass("btn-primary").addClass("btn-warning");
        
    };
    
    
    
    
    this.saveAndClose=function(){
        _this.save(true);
    };
    
    this.close=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    this.save=function(close){   
       
        var saveData=_form.getValues();
        
        saveData.newObject=(_data.newObject)?true:false;
        
        saveData.id_Object=_id_Object;
        saveData.id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
        saveData.weekData=_weekCalendar.getValue();
        saveData.specialDayData=_specialDaysCalendar.getValue();
        
        Functions.ajaxRequest("SaveObject",saveData,function(data){  
            _btRevertHandler.hide();
            if (!_id_Object){
                _id_Object=parseInt(data.id_Object);                
            }
            
            _data.newObject=false;
            
            _data.values=saveData;
            
            if (_name!==saveData.name){
                _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + saveData.name});
                _name=saveData.name;
            }
            
            _form.isSaved();   
            _weekCalendar.isSaved();
            _specialDaysCalendar.isSaved();
            _$panel.find(".box-week").removeClass("box-warning").addClass("box-primary");
            _$panel.find(".box-special-days").removeClass("box-warning").addClass("box-primary");
            
            
            _objectHandlerController.getMainApplication().getEventHandler().fire("UPDATE_OBJECT_LIST",_data.objectTemplate.id_ObjectTemplate);
        
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (close===true){
               _this.close();            
            } else {
               _fillHistoryTable(data.history);
            }
            
            //Callback points back to the object select parameter, when the "new" button is clicked
            if (_data.callback){
                _data.callback(_id_Object);
                _data.callback=null;
            }
        });
    };
    
    var _setChangedFlag=function(calendarData){
        for(var dayIndex=0;dayIndex<calendarData.length;dayIndex++){
            for(var n=0;n<calendarData[dayIndex].calendarBlocks.length;n++){
                calendarData[dayIndex].calendarBlocks[n].changed=true;
            }
        }
        return calendarData;
    };
    
    var _renderCell=function(data){ 
        if (typeof data ==="undefined"){
            return "";
        }
        
        if (data.toString().indexOf("$")===0){
            return "<font style=\"color:red;font-weight:bolder;\">" + data.replace("$CH$","") + "</font>";
        } else {
            return data;
        }
        
    };
    
    
    /**
     * Add new special day
     * 
     * @returns {undefined}
     */
    var _addDate=function(){
        if (_datepickerAddDate.val()){
            _specialDaysCalendar.addDate(_datepickerAddDate.val());
            _datepickerAddDate.val("");            
        }
    };
    
    
    var _fillHistoryTable=function(historyData){
        
        //Prepare data
        
        if (_historyTable){
            _historyTable.updateData(historyData);
        } else {
            var columns=[
                {data:"dateStamp",title:"Date"},
                {data:"userName",title:"Saved By"}                
            ];
            
            _historyTable=new Classes.Common.ListGrid(_$panel.find(".table-container"),historyData,columns);
        }        
    };
    
    var _formChanged=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };

    var _rollback=function(){        
        _rollbackData=_historyTable.getSelectedRecord();        
        if (!_rollbackData){
            return;
        }
        _revertData=_form.getValues();        
        _revertData.weekData=_weekCalendar.getValue(true);
        _revertData.specialDayData=_specialDaysCalendar.getValue(true);
        
        _objectHandlerController.getMainApplication().getEventHandler().fire("SHOW_TAB",{panelId:_this.id_Panel,tabIndex:1});
        _btRevertHandler.show();
           
    };
       
    /***
     * 
     * Constructor
     * 
     */
    
    
    //Render form
    _$panel=$(Mustache.render(viewTemplates.calendarView.result,data));
    
    
    _$panel.find("button[name='btRollback']").click(_rollback);
    
    _$panel.find("button[name='btAddDate']").click(function(){
        _$panel.find("input[name='datePicker']").datepicker("show");        
    });
    
    _datepickerAddDate=_$panel.find("input[name='datePicker']").datepicker({    
        calendarWeeks: true,
        autoclose: true,
        todayBtn: true,
        orientation: "bottom right",
        format: "yyyy-mm-dd"        
    });    
    
    _$panel.find("input[name='datePicker']").datepicker().on("changeDate",_addDate);
    
    _weekCalendar=new Classes.Calendar.CalendarComponent(_this,data.values.weekData,viewTemplates);
    
    _weekCalendar.eventChanged=function(){
        _$panel.find(".box-week").removeClass("box-primary").addClass("box-warning");       
        _formChanged();
    };
    
    _$panel.find(".week-calendar").append(_weekCalendar.get$Panel());
        
    //Special days data
    _specialDaysCalendar=new Classes.Calendar.CalendarComponent(_this,data.values.specialDayData,viewTemplates,true);
    
    _specialDaysCalendar.eventChanged=function(){
        _$panel.find(".box-special-days").removeClass("box-primary").addClass("box-warning");
        _formChanged();
    };
    
        
    _$panel.find(".special-days-calendar").append(_specialDaysCalendar.get$Panel());
    
    
    var params={content:[{type:"SECTION",collapsed:true,collapsable:true,title:"General",content:Calendar.calendarParameters}]};
    _form=new Classes.DynamicForm.Form(objectHandlerController.getMainApplication(),params,true);
    _form.renderForm(function(){
        _$panel.find(".general-section").append(_form.get$Panel());
        _form.bindData(data.values);
        _form.eventChanged=_formChanged;        
    });
    
    _fillHistoryTable(data.history);
};