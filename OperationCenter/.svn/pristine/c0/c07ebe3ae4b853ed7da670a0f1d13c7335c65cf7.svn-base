Classes.ScriptEditor.ScriptEditor=function(parent,data,_viewTemplates){
    var _this=this;
    var _$panel=null;    
    var _parent=parent;
    var id_Script=data.id_Script;
    var _data=data;
    var _historyTable;    
    var _formChangedFlag=false;
    var _codeChangedFlag=false;
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getScriptId=function(){
        return id_Script;
    };
    
    this.historyActive=function(){
        _historyTable.draw();
    };
    
    this.saveAndCloseScript=function(){
        _this.saveScript(true);
    };
    
    this.closeScript=function(){
        _parent.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    
    
    this.saveScript=function(close){                    
        var saveData={};
        saveData.name=_$panel.find("input[name='name']").val();
        saveData.description=_$panel.find("textarea[name='description']").val();
        saveData.testInputData=_$panel.find("textarea[name='testInputData']").val();
        saveData.type=_$panel.find("select[name='type']").val();
        saveData.id_Script=id_Script;
        saveData.jSCode=editor.getValue();
     
        Functions.ajaxRequest("SaveScript",saveData,function(data){
            if (!id_Script){
                id_Script=data.id_Script;                
            }
            
            if (_data.name!==saveData.name){
                _parent.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:"Script - " + saveData.name});                
                _data.name=saveData.name;
            }
            _parent.getMainApplication().getEventHandler().fire("UPDATE_SCRIPT_LIST",_this.id_Panel);

            
            //Set back changed flags            
            _codeChangedFlag=false;
            _formChangedFlag=false;
            _$panel.find(".section-code-area").removeClass("box-warning").addClass("box-primary");
            _$panel.find(".section-general").removeClass("box-warning").addClass("box-primary");
            _parent.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            

            if (close==true){
                _this.closeScript();
            }
            
            _historyTable.updateData(data.history);
            
            //Callback points back to the script select parameter, when the "new" button is clicked
            if(_data.callback){
                _data.callback(id_Script);
                _data.callback=null;
            }
        });        
    };
    
    var _populateSnippetData=function(data){
        data=_parent.getCodeSnippets();
        
        if (!data||!data instanceof Array){
            data=[{name:"[No Data]",value:"No Data"}];            
        }
        var el=_$panel.find("select[name='codeSnippets']").get(0);
        el.options.length = 0;
        if (data.length > 0)
            el.options[0] = new Option('[Select]', '');
        $.each(data, function () {
            el.options[el.options.length] = new Option(this.name, this.name);
        });         
    };    
    
    
    var _btAddCodeSnippet=function(){
        var snippetName=_$panel.find("select[name='codeSnippets']").val();
        var snippets=_parent.getCodeSnippets();
        if (snippetName){
            for (var i=0;i<snippets.length;i++){
                if (snippets[i].name===snippetName){
                    editor.session.insert(editor.getCursorPosition(), snippets[i].code)
                    editor.focus();
                }
            }
            _$panel.find("select[name='codeSnippets']").val("");
        }        
    };
    
            
    var _formChanged=function(){
        if (!_formChangedFlag){
           _$panel.find(".section-general").removeClass("box-primary").addClass("box-warning");
           _parent.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
           _formChangedFlag=true;
        }
            
                
    };
    
    var _btTestScript=function(){
        var inputDataJSON=_$panel.find("textarea[name='testInputData']").val();
        
        if (!inputDataJSON){
            inputDataJSON="{}";
        }
        
        try{
            var inputObject=JSON.parse(inputDataJSON);
        } catch (ex){
            _$panel.find("textarea[name='testOutputData']").val(ex.message);            
            return;
        }
        inputObject.jSCode=editor.getValue();
        Functions.ajaxRequest("TestScript",inputObject,function(data){
            _$panel.find("textarea[name='testOutputData']").val(JSON.stringify(data));
        },true);        
    };
    
    
    var _fillHistoryTable=function(historyData){
        var columns=[
            {data:"dateStamp",title:"Date"},
            {data:"userName",title:"Saved By"},
            {data:"name",title:"Name"},
            {data:"description",title:"Description"},                        
        ];
        _historyTable=new Classes.Common.ListGrid(_$panel.find(".table-container"),historyData,columns);
    };    
        
    _$panel=$(Mustache.render(_viewTemplates.scriptEditorView.result,data));
    
    //SetChangedfunction    
    _$panel.find("input[name='name']").keyup(_formChanged);
    _$panel.find("textarea[name='description']").keyup(_formChanged);
    _$panel.find("textarea[name='testInputData']").keyup(_formChanged);
    _$panel.find("select[name='type']").keyup(_formChanged);
    
    
    ace.require("ace/ext/language_tools");
    var editor = ace.edit(_$panel.find(".ace-editor").get(0));
    editor.setTheme("ace/theme/chrome");
    editor.getSession().setMode("ace/mode/javascript");    
    
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });
    
    data.jSCode=(data.jSCode)?data.jSCode:"//Put your Java Script Code here...";
    editor.setValue(data.jSCode,-1);
    
    //Set on change
    editor.getSession().on('change', function() {  
        if (!_codeChangedFlag){
            _$panel.find(".section-code-area").removeClass("box-primary").addClass("box-warning");
            _parent.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
            _codeChangedFlag=true;
        }       
    });

    
    _$panel.find("select[name='type']").val(data.type||"");    
    _$panel.find("button[name='btAddSnippet']").click(_btAddCodeSnippet);
    _$panel.find("button[name='btTestScript']").click(_btTestScript);
    
    _fillHistoryTable(data.history);
    _populateSnippetData();
};