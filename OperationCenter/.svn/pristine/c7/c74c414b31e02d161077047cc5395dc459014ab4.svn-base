Classes.FormDesigner.FormDesigner=function(formDesignerController,data,_viewTemplates){
    var _this=this;    
    var _formDesignerController=formDesignerController;
    var _$panel=null;    
    var _$properties;
    var _id_Form=data.id_Form;
    var _displayNameTypeMapping={"SECTION":"Section","SELECT":"Select","TEXTAREA":"Text Area","TEXT":"Text","NUMBER":"Number","SCRIPT_SELECT":"Script","CHECKBOX":"Checkbox","OBJECT_SELECT":"Object Select","NUMBER":"Number","FORM_SELECT":"Form","COLUMN":"Column","CHILD_SECTION":"Child Section","EXPRESSION_BUILDER":"Expression Builder","PROMPT_SELECT":"Prompt Select","DATE_TIME":"Date Time","BUTTON":"Button"};
    var _data=data;    
    var _form;
    var _previewForm;
    var _treeNodePointer;
    var _nodeCounter;
    var _treeview;
    var _selectedNode;
    var _nodeArrayCounter;
    var _isChanged=false;
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.activatePreview=function(){       
        _$panel.find(".form-preivew").find(".dynamic-form").remove();
        _previewForm=new Classes.DynamicForm.Form(_formDesignerController.getMainApplication(),{content:_data.content});
        _previewForm.renderForm(function(){
            _$panel.find(".form-preivew").append(_previewForm.get$Panel());                    
        });     
    };
    
    this.getFormId=function(){
        return _id_Form;
    };
    
    this.saveAndClose=function(){
        _this.save(true);
    };
        
    this.close=function(){
        _formDesignerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    //Recursive clean
    var _cleanTreeData=function(data){         
    
        if (!data){
            data=_data;
        }
        if (data instanceof Array){                        
            for (var i=0;i<data.length;i++){
                _cleanTreeData(data[i]);                
            }            
        } else if (data instanceof Object){
            if (data.content){
                _cleanTreeData(data.content); 
            }
            delete data.parameterId;
            delete data.selected;
            delete data.changed;
            delete data.indexInArray;
            delete data.parentId;
            delete data.endElement;
            delete data.scriptData;
            delete data.scriptRequestString;
            delete data.arrayId;
        }
        return null;    
    };
    
    this.save=function(close){        
         //Check that we have a name field
        /*var tmpData=JSON.stringify({content:data.content});
        if (tmpData.indexOf("\"name\":\"name\"")<0){
            bootbox.alert("The \"name\"-parameter is missing.");
            return;
        }
        */
        _cleanTreeData();
        
        var saveData={
            name:_data.name,
            description:_data.description,
            transparentData:_data.transparentData,
            id_Form:_id_Form
        };
        
        saveData.content=data.content;
        

        
        Functions.ajaxRequest("SaveForm",saveData,function(data){
            if (!_id_Form){                
                _id_Form=data.id_Form;
            }
            _isChanged=false;
            
             _formDesignerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (_data.name!==saveData.name){
                _formDesignerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:saveData.name});
                _data.name=saveData.name;
            }
            
            _formDesignerController.getMainApplication().getEventHandler().fire("UPDATE_FORM_LIST",_this.id_Panel);
            
            if (close===true){
                _this.close();
            }   
            
            //Callback points back to the form select parameter, when the "new" button is clicked
            if(_data.callback){
                _data.callback(_id_Form);
                _data.callback=null;
            }

            
        });
        _createTreeView(_data);
        _$panel.find('.treeview-parameters').treeview("selectNode",0);
    };
    
    
  
    //Recursive function
    var _createTreeData=function(data,parentId){
        var returnData;
        if (data instanceof Array){            
            returnData=[];
            for (var i=0;i<data.length;i++){
                data[i].indexInArray=i;
                data[i].parentId=parentId;
                data[i].endElement=(i===data.length-1)?true:false;
                returnData.push(_createTreeData(data[i]));
            }
            return returnData;
        } else if (data instanceof Object){
            _nodeCounter++;
            data.parameterId=parseInt(_nodeCounter);
            
            if (data.content&&data.type==="SECTION"){
                _nodeArrayCounter++;
                data.arrayId=_nodeArrayCounter;
                returnData={text: "Section - " + ((data.title)?data.title:""),
                    icon: "fa fa-th-large",
                    nodes:_createTreeData(data.content,parseInt(_nodeCounter)),
                    parameterData:data,
                    btDelete:true,
                    btNodeUp:(data.indexInArray)?true:false,                    
                    btNodeDown:(data.endElement)?false:true
                };
            }  else if (data.content&&data.type==="COLUMN"){
                _nodeArrayCounter++;
                data.arrayId=_nodeArrayCounter;
                returnData={text: data.columns + " Columns",
                    icon: "fa fa-th",
                    nodes:_createTreeData(data.content,parseInt(_nodeCounter)),
                    parameterData:data,
                    btDelete:true,
                    btNodeUp:(data.indexInArray)?true:false,                    
                    btNodeDown:(data.endElement)?false:true
                };
            } else if (data.content&&data.type==="CHILD_SECTION"){
                _nodeArrayCounter++;
                data.arrayId=_nodeArrayCounter;
                returnData={text: "Child Section - " + ((data.name)?data.name:""),
                    icon: "fa fa-th",
                    nodes:_createTreeData(data.content,parseInt(_nodeCounter)),
                    parameterData:data,
                    btDelete:true,
                    btNodeUp:(data.indexInArray)?true:false,                    
                    btNodeDown:(data.endElement)?false:true
                };
            } else if (data.content&&data.type==="FORM"){
                _nodeArrayCounter++;
                data.arrayId=_nodeArrayCounter;
                returnData={text: "Form" + " - " + ((data.name)?data.name:""),                    
                    nodes:_createTreeData(data.content,parseInt(_nodeCounter)),
                    parameterData:data                   
                };
            } else {
                returnData={text: _displayNameTypeMapping[data.type] + " - " + ((data.name)?data.name:""),
                    icon: "fa fa-square-o",
                    parameterData:data,
                    btDelete:true,
                    btNodeUp:(data.indexInArray)?true:false,
                    btNodeDown:(data.endElement)?false:true
                };
            }
            if (data.selected){
                returnData.state={selected:true};
                delete data.selected;
            }
            if (data.changed){
                returnData.backColor="#f2dede";
            }
            return returnData;
        }
    };

    //Recursive node search
    var _findNodeByParameterId=function(parameterId,data){
        if (!data){
            data=_data;
        }
        if (data instanceof Array){                        
            for (var i=0;i<data.length;i++){
                var node=_findNodeByParameterId(parameterId,data[i]);
                if (node){
                    return node; 
                }
            }            
        } else if (data instanceof Object){
            if (data.parameterId&&data.parameterId===parameterId){
                return data;
            }
            if (data.content){
                return _findNodeByParameterId(parameterId,data.content); 
            }
        }
        return null;
    };
    
    var _addElement=function(parameterType){
        if (!_selectedNode){
            return;
        }
        var parentNodeParameter;
        var insertIndex=0;        
        
        if (_selectedNode&&_selectedNode.parameterData.content){            
            parentNodeParameter=_findNodeByParameterId(_selectedNode.parameterData.parameterId);                
        } else {
            insertIndex=_selectedNode.parameterData.indexInArray+1;
            var parentNode=_$panel.find('.treeview-parameters').treeview("getParent",_selectedNode);
            if (parentNode.parameterData&&parentNode.parameterData.parameterId){
                var parentNodeParameter=_findNodeByParameterId(parentNode.parameterData.parameterId);                
            } else {
                parentNodeParameter=_data;                
            }
        }
        
        var newParameter={type:parameterType,name:"New Parameter",selected:true,changed:true};
        if (parameterType==="SECTION"){
            newParameter.content=[];
            newParameter.title="New Section";
        }
        
        if (parameterType==="COLUMN"){
            newParameter.content=[];
            newParameter.columns="2";
        }
        
        if (parameterType==="CHILD_SECTION"){
            newParameter.content=[];
            newParameter.name="New Child Section";
        }
        
        if (parameterType==="BUTTON"){            
            newParameter.name="New Button";
        }
        
        
        if (parentNodeParameter){            
            parentNodeParameter.content.splice(insertIndex,0,newParameter);
        }        
        _formChanged();
        _createTreeView(_data);
        $(function(){
            var tmpNode=_$panel.find('.treeview-parameters').treeview("getSelected");        
            _treeNodeSelected(null,tmpNode[0]);
        });
    };

    var _onDeleteNode=function(event,node){
        var parentNode=_$panel.find('.treeview-parameters').treeview("getParent",node);        
        if (parentNode.parameterData&&parentNode.parameterData.parameterId){
            var parentNodeParameter=_findNodeByParameterId(parentNode.parameterData.parameterId);
            parentNodeParameter.content.splice(node.parameterData.indexInArray,1);
        } else {
            _data.content.splice(node.parameterData.indexInArray,1);
        }      
        _createTreeView(_data);
    };
    
    var _nameChanged=function(name){
        var parameterData=_findNodeByParameterId(_selectedNode.parameterData.parameterId);
        parameterData.name=name;
        parameterData.selected=true;        
        _createTreeView(_data);
    };
    
    var _titleChanged=function(name){
        var parameterData=_findNodeByParameterId(_selectedNode.parameterData.parameterId);
        parameterData.title=name;
        parameterData.selected=true;        
        _createTreeView(_data);
    };
    
    var _columnsChanged=function(cols){
        var parameterData=_findNodeByParameterId(_selectedNode.parameterData.parameterId);
        parameterData.columns=cols;
        parameterData.selected=true;        
        _createTreeView(_data);
    };
    
    
    var _treeNodeSelected=function(event,node){                
        if (!node.parameterData.type){
            return;
        }    
        _selectedNode=node;
        /*if (node.parameterData.type==="FORM"){
            _$panel.find(".box-properties").hide();
            return;
        }*/
        
        var attributes={};
        
        attributes.content=parameterAttributes[node.parameterData.type.toLowerCase()].slice(0);
        
        if (node.parameterData.type==="SECTION"||node.parameterData.type==="COLUMN"){
            attributes.content.push({type:"SECTION",colorSchema:"box-primary",collapsed:true,collapsable:true,title:"Permissions",content:[parameterAttributes.applyPermissions,parameterAttributes.accessPermissions_2]});
        }  else {
            attributes.content.push({type:"SECTION",colorSchema:"box-primary",collapsed:true,collapsable:true,title:"Permissions",content:[parameterAttributes.applyPermissions,parameterAttributes.accessPermissions_1]});
        }
        
        _form=new Classes.DynamicForm.Form(_formDesignerController.getMainApplication(),attributes);
        _form.renderForm(function(){
            _$panel.find(".dynamic-form").remove();            
            _$panel.find(".parameter-attributes").append(_form.get$Panel());            
            _form.bindData(_findNodeByParameterId(node.parameterData.parameterId));
            _form.subscribeChangeEvent("name",_nameChanged);
            _form.subscribeChangeEvent("title",_titleChanged);
            _form.subscribeChangeEvent("columns",_columnsChanged);            
            _form.eventChanged=_paramChanged;            
        });        
    };

    var _paramChanged=function(){
        if (!_selectedNode.parameterData.changed){
            var myNode=_findNodeByParameterId(_selectedNode.parameterData.parameterId);
            myNode.changed=true;
            myNode.selected=true;
            _createTreeView(_data);  
            _formChanged();
        }        
    };

    var _moveNodeUp=function(event,node){        
        //An ugly node switch (Had been easier if javascript supported pointers)
        var tmpNode=_findNodeByParameterId(node.parameterData.parameterId);
        var parent=_findNodeByParameterId(tmpNode.parentId);
        
        parent.content[tmpNode.indexInArray]=parent.content[tmpNode.indexInArray-1];
        parent.content[tmpNode.indexInArray-1]=tmpNode;
        tmpNode.selected=true;
        _createTreeView(_data);
        $(function(){
            var tmpNode=_$panel.find('.treeview-parameters').treeview("getSelected");        
            _treeNodeSelected(null,tmpNode[0]);
        });
    };

    var _moveNodeDown=function(event,node){
        //An ugly node switch (Had been easier if javascript supported pointers)
        var tmpNode=_findNodeByParameterId(node.parameterData.parameterId);
        var parent=_findNodeByParameterId(tmpNode.parentId);
        
        parent.content[tmpNode.indexInArray]=parent.content[tmpNode.indexInArray+1];
        parent.content[tmpNode.indexInArray+1]=tmpNode;
        tmpNode.selected=true;
        _createTreeView(_data);
        $(function(){
            var tmpNode=_$panel.find('.treeview-parameters').treeview("getSelected");        
            _treeNodeSelected(null,tmpNode[0]);
        });
    };
    
    var _formChanged=function(){
        if (!_isChanged){
            _isChanged=true;
            _formDesignerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
        }        
    };

    
    
    var _pasteFromClipboard=function(){
        if (!_selectedNode){
            return;
        }
        
    
        //Sets selected=true on every object recursively 
        var _setChangedFlag=function(data){         
            
            if (!data){
                data=_data;
            }
            if (data instanceof Array){                        
                for (var i=0;i<data.length;i++){
                    _setChangedFlag(data[i]);                
                }            
            } else if (data instanceof Object){
                if (data.content){
                    _setChangedFlag(data.content); 
                }                
                data.changed=true;
            }
            
            return null;    
        };
        
        //Create a new deep clone of the object in the clipboard 
        var clipboard=jQuery.extend(true, {}, _formDesignerController.getClipboard());        
        if (!clipboard){
            return;
        }
               
        var parentNodeParameter;
        var insertIndex=0;        
        
        if (_selectedNode&&_selectedNode.parameterData.content){            
            parentNodeParameter=_findNodeByParameterId(_selectedNode.parameterData.parameterId);                
        } else {
            insertIndex=_selectedNode.parameterData.indexInArray+1;
            var parentNode=_$panel.find('.treeview-parameters').treeview("getParent",_selectedNode);
            if (parentNode.parameterData&&parentNode.parameterData.parameterId){
                var parentNodeParameter=_findNodeByParameterId(parentNode.parameterData.parameterId);                
            } else {
                parentNodeParameter=_data;                
            }
        }
        
        
        _setChangedFlag(clipboard);
        clipboard.selected=true;
        
        parentNodeParameter.content.splice(insertIndex,0,clipboard);    
        
        
        _formChanged();
                
        _createTreeView(_data);
        $(function(){
            var tmpNode=_$panel.find('.treeview-parameters').treeview("getSelected");        
            _treeNodeSelected(null,tmpNode[0]);
        });
    };
    
    var _createTreeView=function(data){
        _nodeCounter=0;
        _nodeArrayCounter=0;
        
        _$panel.find('.treeview-parameters').treeview({
            levels: 99,
            onNodeSelected:_treeNodeSelected,
            onDeleteNodeClicked:_onDeleteNode,
            onNodeDownClicked:_moveNodeDown,
            onNodeUpClicked:_moveNodeUp,
            data:[_createTreeData(data)]
        });
    };

    /**
     * Constructor
     */
   
    _$panel=$(Mustache.render(_viewTemplates.formDesignerView.result,data));
    _$panel.find(".btAddElement").click(function(){
        _addElement($(this).attr("data-element"));
    });
    
    _$panel.find("button[name='btCopy']").click(function(){
        var tmpClip=_findNodeByParameterId(_selectedNode.parameterData.parameterId);        
        _formDesignerController.setClipboard(tmpClip);        
    });
    
    _$panel.find("button[name='btCut']").click(function(){
        if (_selectedNode){
            var tmpClip=_findNodeByParameterId(_selectedNode.parameterData.parameterId);        
            _formDesignerController.setClipboard(tmpClip);
            _onDeleteNode(null,_selectedNode);
        }
    });
    
    
    _$panel.find("button[name='btPaste']").click(_pasteFromClipboard);
    
    
    _data.type="FORM";    
    _data.changed=false;
    _createTreeView(_data);
    _$panel.find('.treeview-parameters').treeview("selectNode",0);
};