Classes.ObjectHandler.Object=function(objectHandlerController,data,viewTemplates){
    var _this=this;
    var _objectHandlerController=objectHandlerController;
    var _$panel=null;        
    var _data=data;
    var _name=data.values.name;
    var _id_Object=data.values.id_Object;
    var _id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
    var _historyTable;
    var _form;
    var _parameterObjects=[];
    var _sectionObjects=[];
    var _selectDepartmentHandler;
    var _revertData;
    var _btRevertHandler;
        
    this.getObjectHandlerController=function(){
        return _objectHandlerController;
    };
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getObjectId=function(){
        return _id_Object;
    };
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };
    
    /**
     * Is set by contentController. 
     * _selectDepartmentHandler is a handler to the department select control in the tool bar.
     * @param {type} value
     * @returns {undefined}
     */
    
    this.setSelectDepartmentHandler=function(value){
        _selectDepartmentHandler=value;     
        if (_data.values.id_Department){
            _selectDepartmentHandler.val(_data.values.id_Department);
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        } else {
            _selectDepartmentHandler.val(objectHandlerController.getLastDepartmentId());
        }    
    };
    
    this.historyActive=function(){
        _historyTable.draw();
    };
    
    this.editActive=function(){

    };
    
    this.revert=function(){
        _form.setValues(_revertData);
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _revertData.name});
        _btRevertHandler.hide();
    };
    
    this.setBtRevertHandle=function(value){
        _btRevertHandler=value;
        _btRevertHandler.hide();   
        value.removeClass("btn-primary").addClass("btn-warning");
    };
    
    this.saveAndClose=function(){
        _this.save(true);
    };
    
    this.close=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    this.save=function(close){   
        //check validation
        if (!_form.checkValidation()){
            bootbox.alert("Can't save due to blank mandatory fields.");
            return;
        }
        var saveData;
        
        if (_data.objectTemplate.form.transparentData){
            //Overwrite parameter values from the form with the originalparameter data.
            saveData=_data.values;
            var tmpData=_form.getValues();
            for (var key in tmpData){
                saveData[key]=tmpData[key];
            }
        } else {
            saveData=_form.getValues();
        }
        
        saveData.relationData=_form.getObjectRelations();
        
        saveData.newObject=(_data.newObject)?true:false;
        if (_data.values.deleted){
            saveData.newObject=true;
        }
        
        //check if the form has a name property
        if (typeof saveData.name ==='undefined'){
            bootbox.alert("The form does not have a name property.");
            return;
        }
        
        
        saveData.id_Object=_id_Object;
        saveData.id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
        if (_selectDepartmentHandler){
            saveData.id_Department=_selectDepartmentHandler.val();
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        }
        
        Functions.ajaxRequest("SaveObject",saveData,function(data){
            _data.values.deleted=false;
            if (!_id_Object){
                _id_Object=parseInt(data.id_Object);                
            }
            if (saveData.newObject){
                _form.setFirstTime(false);
            }
            _data.newObject=false;
            
            //Update _data with all fileds from the return data where it matches
            for (key in data){
                if (key!="history"){
                   saveData[key]=data[key];
                }
            }
            _form.setValues(saveData);
            _data.values=saveData;
            
            if (_name!==saveData.name){
                _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + saveData.name});
                _name=saveData.name;
            }
            
            _objectHandlerController.getMainApplication().getEventHandler().fire("UPDATE_OBJECT_LIST",_data.objectTemplate.id_ObjectTemplate);
            
            _form.isSaved();           
            

            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (close===true){
               _this.close();            
            } else {
               _fillHistoryTable(data.history);
            }
            
            //Callback points back to the object select parameter, when the "new" button is clicked
            if (_data.callback){
                _data.callback(_id_Object);
                _data.callback=null;
            }
        });
    };
    
    
    var _renderCell=function(data){ 
        if (typeof data ==="undefined"||data===null){
            return "";
        }
        
        if (data.toString().indexOf("$")===0){
            return "<font style=\"color:red;font-weight:bolder;\">" + data.replace("$CH$","") + "</font>";
        } else {
            return data;
        }
        
    };
    
        
    
    var _fillHistoryTable=function(historyData){
        
        //Prepare data
        var preparedHistoryData=[];
        var lastRow=null;
        for (var index=0;index<historyData.length;index++){
            var row=historyData[index];
            for (var key in row){
                if (!(key==="dateStamp"||key==="userName")){
                    if (lastRow){                        
                        if (row[key]!==null&&(lastRow[key]===null||typeof(lastRow[key]) ==="undefined"||lastRow[key].toString().replace("$CH$","")!==row[key].toString())){
                            row[key]="$CH$" + ((row[key])?row[key].toString():"");
                        }            
                    } else {
                        row[key]="$CH$" + ((row[key])?row[key].toString():"");
                    }
                }
            }
            preparedHistoryData.push(row);
            lastRow=row;
        }
        
        if (_historyTable){
            _historyTable.updateData(preparedHistoryData);
        } else {
            var columns=[
                {data:"dateStamp",title:"Date"},
                {data:"userName",title:"Saved By"}                
            ];
            
            //Prepare columns
            var parameters=_objectHandlerController.getAllParametersFromForm(data.objectTemplate.form);
            for (var index=0;index<parameters.length;index++){
                if (parameters[index].listGridName&&parameters[index].listGridName!==""){                
                    columns.push({data:parameters[index].name,title:parameters[index].listGridName,render:_renderCell});
                }            
            }     
            
            _historyTable=new Classes.Common.ListGrid(_$panel.find(".table-container"),preparedHistoryData,columns);
            _historyTable.on_DbClick=_rollback; 
        }        
    };
    
    var _formChanged=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };

    
    var _rollback=function(){        
        var rollbackData=_historyTable.getSelectedRecord();
        

        
        if (rollbackData){
            _revertData=_form.getValues();        
            _form.setValues(rollbackData);
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + " (" + rollbackData.dateStamp + ")"});
            _name="";                 
        }
        
        _objectHandlerController.getMainApplication().getEventHandler().fire("SHOW_TAB",{panelId:_this.id_Panel,tabIndex:1});
        _btRevertHandler.show();
           
    };
    
       
    /***
     * 
     * Constructor
     * 
     */
   
    
    //Render form
    _$panel=$(Mustache.render(viewTemplates.objectView.result,data));    
    _$panel.find("button[name='btRollback']").click(_rollback);
    
    _form=new Classes.DynamicForm.Form(_objectHandlerController.getMainApplication(),data.objectTemplate.form);       
    _form.renderForm(function(){        
        if (_data.newObject){
            _form.setFirstTime(true);
        } 
    
        _$panel.find(".main-panel").append(_form.get$Panel());
        _form.setValues(data.values);        
        _form.eventChanged=_formChanged;
        _fillHistoryTable(data.history);
    });
    
    if (data.values.deleted){
        $(function(){
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + "<font style='color:red;'> DELETED " + data.values.deleted + " by " +  data.values.deletedBy + "</font>"});
            _name="";
        });
        
    }

};