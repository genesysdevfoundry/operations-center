/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

/****** Object:  Table [dbo].[CodeSnippet]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CodeSnippet](
	[Id_CodeSnippet] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[General] [int] NULL,
	[Name] [varchar](500) NULL,
	[Category] [varchar](500) NULL,
	[JSCode] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Department]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[Id_Department] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[JSONData] [varchar](max) NULL,
	[Id_Tenant] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Form]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Form](
	[Id_Form] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [varchar](150) NULL,
	[Changed] [datetime] NULL,
	[ChangedBy] [varchar](150) NULL,
	[Hidden] [int] NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[TransparentData] [int] NULL,
	[JSONData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MenuGroup]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MenuGroup](
	[Id_MenuGroup] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[SortOrder] [int] NULL,
	[Name] [varchar](250) NULL,
	[Icon] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Object]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Object](
	[Id_Object] [int] IDENTITY(1,1) NOT NULL,
	[Id_ObjectTemplate] [int] NULL,
	[Id_Department] [int] NULL,
	[Id_Tenant] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [varchar](150) NULL,
	[Changed] [datetime] NULL,
	[ChangedBy] [varchar](150) NULL,
	[Deleted] [datetime] NULL,
	[DeletedBy] [varchar](150) NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[JSONData] [varchar](max) NULL,
 CONSTRAINT [PK_Object] PRIMARY KEY CLUSTERED 
(
	[Id_Object] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ObjectHistory]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ObjectHistory](
	[Id_ObjectHistory] [int] IDENTITY(1,1) NOT NULL,
	[Id_Object] [int] NULL,
	[DateStamp] [datetime] NULL,
	[UserName] [varchar](250) NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[JSONData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ObjectRelation]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ObjectRelation](
	[Id_ObjectRelation] [int] IDENTITY(1,1) NOT NULL,
	[ParentObjectType] [varchar](50) NULL,
	[ChildObjectType] [varchar](50) NULL,
	[ParentObjectId] [int] NULL,
	[ChildObjectId] [int] NULL,
 CONSTRAINT [PK_ObjectRelation] PRIMARY KEY CLUSTERED 
(
	[Id_ObjectRelation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ObjectTemplate]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ObjectTemplate](
	[Id_ObjectTemplate] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[Id_Form] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [varchar](250) NULL,
	[Changed] [datetime] NULL,
	[ChangedBy] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[Hidden] [int] NULL,
	[Type] [varchar](150) NULL,
	[SectionName] [varchar](250) NULL,
	[ScriptCreate] [varchar](150) NULL,
	[ScriptSave] [varchar](150) NULL,
	[ScriptGet] [varchar](150) NULL,
	[ScriptGetAll] [varchar](150) NULL,
	[ScriptDelete] [varchar](150) NULL,
	[Name] [varchar](250) NULL,
	[MenuGroup] [varchar](150) NULL,
	[MenuName] [varchar](150) NULL,
	[ConnectedField] [varchar](50) NULL,
	[RunGetScriptOnce] [int] NULL,
	[Icon] [varchar](50) NULL,
	[History] [int] NULL,
	[System] [int] NULL,
	[TransactionListName] [varchar](250) NULL,
	[Permissions] [varchar](max) NULL,
	[BelongToDepartment] [int] NULL,
	[ConfigServerFirst] [int] NULL,
	[WorkflowBlocks] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Permission](
	[Id_Permission] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[AccessGroup] [varchar](150) NULL,
	[ObjectType] [varchar](50) NULL,
	[ObjectId] [int] NULL,
	[PermissionType] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchedulerHistory]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchedulerHistory](
	[Id_SchedulerHistory] [int] IDENTITY(1,1) NOT NULL,
	[DateStamp] [datetime] NULL,
	[ExecutionTime] [int] NULL,
	[Result] [varchar](50) NULL,
	[Error] [varbinary](2500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Script]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Script](
	[Id_Script] [int] IDENTITY(1,1) NOT NULL,
	[Id_Tenant] [int] NULL,
	[Created] [datetime] NULL,
	[CreatedBy] [varchar](250) NULL,
	[Changed] [datetime] NULL,
	[ChangedBy] [varchar](250) NULL,
	[Hidden] [int] NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[Type] [varchar](50) NULL,
	[JSCode] [varchar](max) NULL,
	[TestInputData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScriptHistory]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScriptHistory](
	[Id_ScriptHistory] [int] IDENTITY(1,1) NOT NULL,
	[Id_Script] [int] NOT NULL,
	[DateStamp] [datetime] NULL,
	[UserName] [varchar](250) NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](max) NULL,
	[JSCode] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tenant]    Script Date: 2018-09-02 22:53:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tenant](
	[Id_Tenant] [int] NULL,
	[Name] [varchar](150) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
