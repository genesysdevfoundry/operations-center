# Operation Center
Configuration layer on top of Genesys Pure Engage

## Architecture

The application is built using Java 8 and is installed on a Tomcat 8.X, thus making it platform independent. The connection towards Genesys is handled with Platform SDK for Java. The application is shipped as a war file and contains an auto-started servlet that will init and destroy itself together with the Tomcat start and stop.
The UI uses bootstrap for the look and feel.
A database need to be used, and at the moment only MSSQL is supported.
Runs in Chrome or Firefox.

## Installation

### Prerequisites

#### Tomcat

The application runs on a Tomcat. We recommend version 8.5 or higher.

#### Java

The application runs on a Tomcat and is therefore dependent on Java. It is built using functionality that only exist in later versions of Java 8 (build 100 or later), so it is recommended that the latest jre 8 is installed.

#### Database

A separate database is needed. At the moment only MSSQL is supported but this can easily be adapted to include other ones, such as Oracle. A database user with read and write permissions is necessary.

### Steps of installation

#### Database

Run the provided SQL script in the empty MSSQL database. This will create all the tables and some initial configuration that is needed for OperationCenter.

#### Application Object

Create an application object in CME/Admninstrator/GAX of type Third Party Application. If this template does not exist, just create an empty one through the Application Templates folder.

Import the following options into the new application. The bold ones should be changed according to environment specifications. Note that the DB password must be encrypted, this is described in section 3.2.4:

[audio]
ALawConverter=**C:/AudioConverter/convertToALaw.bat**
AudioFileStorage=**C:/tmp/AudioFile**
MP3Converter=**C:/AudioConverter/convertAudioFileToMp3.bat**
TemporaryFiles_File=**${catalina.home}/webapps/ROOT/TempPrompts**
TemporaryFiles_HTTP=**/TempPrompts**

[database]
pool.property.driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver
pool.property.initialSize=10
pool.property.jdbcInterceptors=org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer
pool.property.jmxEnabled=true
pool.property.logAbandoned=true
pool.property.maxActive=100
pool.property.maxWait=10000
pool.property.minEvictableIdleTimeMillis=30000
pool.property.minIdle=10
pool.property.password=**password**
pool.property.removeAbandoned=true
pool.property.removeAbandonedTimeout=60
pool.property.testOnBorrow=true
pool.property.testOnReturn=false
pool.property.testWhileIdle=false
pool.property.timeBetweenEvictionRunsMillis=30000
pool.property.url=**jdbc:sqlserver://192.168.10.98:1433;databaseName=OperationCenter;**
pool.property.username=**OperationCenterUser**
pool.property.validationInterval=30000
pool.property.validationQuery=SELECT 1

[general]
adminAccessGroup=**Administrators**
ocUserAccessGroup=**Administrators**

[ocUserFolderDBIDs]
1=**292**
101=**281**

Users that should log in to OperationCenter need to have access to this application object.

#### Tomcat

Stop the Tomcat. Drop the application war file into the tomcat/webapps directory.

In the tomcat/conf folder, create a new file called operationcenter.properties. In this file, add the following lines and adjust according to installation environment. The password can be left blank since we will encrypt it at a later stage, described in section 3.2.4. The user here need to have full access to the environment.

configAppName=OperationCenter
configServerName=192.168.10.98,192.168.10.99
configServerPort=2020
configUser=default
configPassword=password

Replace the favicon.ico under tomcat/webapps/ROOT with the provided favicon.ico to give a more personalized icon in the browser tab.

Start the Tomcat. I will not be able to connect to anything due to the passwords not being encrypted, but it will enable the encryption web service.

#### Password encryption

With the tomcat started, go to a browser and copy the following link, where you change the password in blue to the one you want to encrypt:

http://localhost:9090/OperationCenter/Services/EncryptPassword?password=**ChangeMe**

The generated encrypted password is displayed in the browser. Copy it without the starting and trailing quotation marks and paste into properties file for the Genesys user or application object option for the DB.

Restart the tomcat to initialize with correct passwords.

#### Logging

The application uses log4j2 and will by default log to tomcat/logs folder. This is controlled by the log4j2.xml file situated by default in tomcat/webapps/OperationCenter/WEB-INF/classes. This file can be copied and put directly under the tomcat/conf folder if needs to be altered. In this way it won’t be reverted to default settings when new war files are deployed.

## License

This software is released under MIT license. See LICENSE.md

