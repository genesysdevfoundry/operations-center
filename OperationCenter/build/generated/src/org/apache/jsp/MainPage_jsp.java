package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class MainPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_pack_script_enabled;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_pack_script_enabled = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_pack_script_enabled.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<html style=\"background: #ecf0f5\">\n");
      out.write("<head>\n");
      out.write("  <meta charset=\"utf-8\">\n");
      out.write("  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("  <title>Operation Center</title>\n");
      out.write("  <!-- Tell the browser to be responsive to screen width -->\n");
      out.write("  <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">\n");
      out.write("  <link href=\"Libraries/jquery-ui.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  <!-- Bootstrap 3.3.6 -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/bootstrap/css/bootstrap-custom.css\">\n");
      out.write("  <!-- Font Awesome 4.7 -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/plugins/font-awesome-4.7/css/font-awesome.min.css\">\n");
      out.write("  <!-- Ionicons -->  \n");
      out.write("  \n");
      out.write("  <!--link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css\"-->\n");
      out.write("  <!-- DataTables 1.10.13, Buttons 1.2.4, Column visibility 1.2.4, ColReorder 1.3.2, FixedColumns 3.2.2, FixedHeader 3.1.2, KeyTable 2.2.0, Responsive 2.1.1, RowReorder 1.2.0, Scroller 1.4.2, Select 1.2.0-->\n");
      out.write("  <link href=\"Libraries/plugins/DataTable/datatables.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    <!-- iCheck 1.0.1 -->  \n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/plugins/iCheck/all.css\">\n");
      out.write("  <!-- Bootstrap Color Picker -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/plugins/colorpicker/bootstrap-colorpicker.min.css\">\n");
      out.write("  <!-- Bootstrap time Picker -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/plugins/timepicker/bootstrap-timepicker.min.css\">\n");
      out.write("  <!-- Select2 -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/plugins/select2/select2.min.css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/AdminLTE/dist/css/AdminLTE.min.css\">\n");
      out.write("  <!-- AdminLTE Skins. Choose a skin from the css/skins\n");
      out.write("       folder instead of downloading all of them to reduce the load. -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"Libraries/AdminLTE/dist/css/skins/_all-skins.min.css\">\n");
      out.write("  \n");
      out.write("  <!-- bootstrap datepicker -->\n");
      out.write("  <link href=\"Libraries/plugins/datepicker/bootstrap-datepicker3.standalone.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    <!--jQuery QueryBuilder 2.4.1-->  \n");
      out.write("  <link href=\"Libraries/plugins/QueryBuilder/query-builder.default.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  \n");
      out.write("  <!--bootstrap-treeview.js v1.2.0-->\n");
      out.write("  <link href=\"Libraries/plugins/bootstrap-treeview/bootstrap-treeview.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("\n");
      out.write("  <!--bootstrap-toggle.js v2.2.0-->\n");
      out.write("  <link href=\"Libraries/plugins/BootstrapToggle/bootstrap-toggle.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  \n");
      out.write("  <link href=\"Libraries/Fonts/fonts.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  <!-- Datetimepicker for Bootstrap 3 version : 4.17.47-->\n");
      out.write("  <link href=\"Libraries/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  <!--Custom CSS-->\n");
      out.write("  <link href=\"CustomCSS.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  \n");
      out.write("  <!--Custom theme-->\n");
      out.write("  <link href=\"CustomTheme.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("  \n");
      out.write("    <script>\n");
      out.write("      //Global namespaces\n");
      out.write("      var Classes={};      \n");
      out.write("      var Functions={};      \n");
      out.write("      Classes.MainModule={};\n");
      out.write("      Classes.Common={};\n");
      out.write("      Classes.PromptManagement={};\n");
      out.write("      Classes.FormDesigner={};\n");
      out.write("      Classes.ScriptEditor={};\n");
      out.write("      Classes.Parameters={};\n");
      out.write("      Classes.DynamicForm={};\n");
      out.write("      Classes.ObjectHandler={};\n");
      out.write("      Classes.ObjectTemplate={};\n");
      out.write("      Classes.Calendar={};\n");
      out.write("      Classes.WorkflowDiagram={};\n");
      out.write("      \n");
      out.write("      //Global variables\n");
      out.write("      var global={};\n");
      out.write("      global.allAccessGroups;\n");
      out.write("      global.userName;\n");
      out.write("      global.departments;\n");
      out.write("      global.defaultDepartment;\n");
      out.write("      global.promptObjectTemplateId;\n");
      out.write("      global.translations;\n");
      out.write("    </script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body class=\"hold-transition skin-green fixed sidebar-mini \" style=\"position:relative;overflow:hidden;background: #ecf0f5;height:100vh;min-width:850px;overflow-x: auto;\">\n");
      out.write("    \n");
      out.write("<header class=\"main-header\" >        \n");
      out.write("</header>\n");
      out.write("\n");
      out.write("<!-- Left side column. contains the logo and sidebar -->\n");
      out.write("<aside class=\"main-sidebar\">\n");
      out.write("</aside>\n");
      out.write("\n");
      out.write("<!-- Content Wrapper. Contains page content -->\n");
      out.write("<div class=\"content-wrapper\" style=\"position:relative;height:100%\">\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<!-- /.content-wrapper -->\n");
      out.write("<!--footer class=\"main-footer\"></footer-->  <!-- /.control-sidebar -->\n");
      out.write("\n");
      out.write("<!-- ./wrapper -->\n");
      out.write("\n");
      out.write("<div class=\"modal fade generic-dialog\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\".modal-title\">\n");
      out.write("  <div class=\"modal-dialog\" role=\"document\">\n");
      out.write("    \n");
      out.write("  </div>    \n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- jQuery 3.1.1 -->\n");
      out.write("<script src=\"Libraries/jquery-3.1.1.min.js\"></script>\n");
      out.write("<!-- jquery-ui-1.12.1 -->\n");
      out.write("<script src=\"Libraries/jquery-ui.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!-- Bootstrap 3.3.6 -->\n");
      out.write("<script src=\"Libraries/bootstrap/js/bootstrap.min.js\"></script>\n");
      out.write("<!-- Mustache 2.3.0 -->\n");
      out.write("<script src=\"Libraries/mustache.min.js\"></script>\n");
      out.write("<!-- Select2 -->\n");
      out.write("<script src=\"Libraries/plugins/select2/select2.full.min.js\"></script>\n");
      out.write("<!-- DataTables 1.10.13, Buttons 1.2.4, Column visibility 1.2.4, ColReorder 1.3.2, FixedColumns 3.2.2, FixedHeader 3.1.2, KeyTable 2.2.0, Responsive 2.1.1, RowReorder 1.2.0, Scroller 1.4.2, Select 1.2.0-->\n");
      out.write("<script src=\"Libraries/plugins/DataTable/datatables.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!-- SlimScroll -->\n");
      out.write("<script src=\"Libraries/plugins/slimScroll/jquery.slimscroll.min.js\"></script>\n");
      out.write("<!-- FastClick -->\n");
      out.write("<script src=\"Libraries/plugins/fastclick/fastclick.js\"></script>\n");
      out.write("<!-- AdminLTE App -->\n");
      out.write("<script src=\"Libraries/AdminLTE/dist/js/app.min.js\"></script>\n");
      out.write("<!-- iCheck 1.0.1 -->\n");
      out.write("<script src=\"Libraries/plugins/iCheck/icheck.min.js\"></script>\n");
      out.write("<!--bootbox.js v4.4.0-->\n");
      out.write("<script src=\"Libraries/plugins/bootbox.min.js\"></script>\n");
      out.write("<!--moment.js v4.4.0-->\n");
      out.write("<script src=\"Libraries/plugins/moment/moment.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--bootstrap datepicker-->\n");
      out.write("<script src=\"Libraries/plugins/datepicker/bootstrap-datepicker.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--jQuery.extendext 0.1.2-->\n");
      out.write("<script src=\"Libraries/jQuery.extendext.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--Laura Doktorova https://github.com/olado/doT -->\n");
      out.write("<script src=\"Libraries/doT.min.js\" type=\"text/javascript\"></script>'\n");
      out.write("<!--jQuery QueryBuilder 2.4.1-->\n");
      out.write("<script src=\"Libraries/plugins/QueryBuilder/query-builder.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--* @author Eugene Maslovich <ehpc@em42.ru>-->\n");
      out.write("<script src=\"Libraries/loadingDialog.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--Ace version 1.2.6-->\n");
      out.write("<script src=\"Libraries/plugins/Ace/ace.js\" type=\"text/javascript\"></script>\n");
      out.write("<script src=\"Libraries/plugins/Ace/ext-language_tools.js\" type=\"text/javascript\"></script>\n");
      out.write("\n");
      out.write("<!--JSPlumb 2.5.1-->\n");
      out.write("<script src=\"Libraries/jsplumb.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--bootstrap-treeview.js v1.2.0-->\n");
      out.write("<script src=\"Libraries/plugins/bootstrap-treeview/bootstrap-treeview.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--bootstrap-toggle.js v2.2.0-->\n");
      out.write("<script src=\"Libraries/plugins/BootstrapToggle/bootstrap-toggle.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!--RecordRTC v5.4.1-->\n");
      out.write("<script src=\"Libraries/plugins/RecordRTC/RecordRTC.min.js\" type=\"text/javascript\"></script>\n");
      out.write("<!-- Datetimepicker for Bootstrap 3 version : 4.17.47-->\n");
      out.write("<script src=\"Libraries/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js\" type=\"text/javascript\"></script>\n");
      out.write("\n");
      out.write("\n");
      if (_jspx_meth_pack_script_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<script>    \n");
      out.write("    //cacheId=1141;\n");
      out.write("    cacheId=(Math.random()*1000);\n");
      out.write("    mainApplication=new Classes.MainModule.MainApplication($(\".main-header\"),$(\".main-sidebar\"),$(\"footer\"),$(\".content-wrapper\"));   \n");
      out.write("    var CustomModalDialog=$(\".generic-dialog\");\n");
      out.write("        \n");
      out.write("    Date.prototype.ISO = function() {\n");
      out.write("      var mm = this.getMonth() + 1; // getMonth() is zero-based\n");
      out.write("      var dd = this.getDate();\n");
      out.write("\n");
      out.write("      return [this.getFullYear(),\n");
      out.write("              (mm>9 ? '' : '0') + mm,\n");
      out.write("              (dd>9 ? '' : '0') + dd\n");
      out.write("             ].join('-');\n");
      out.write("    };\n");
      out.write("</script>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_pack_script_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  pack:script
    net.sf.packtag.tag.ScriptTag _jspx_th_pack_script_0 = (net.sf.packtag.tag.ScriptTag) _jspx_tagPool_pack_script_enabled.get(net.sf.packtag.tag.ScriptTag.class);
    _jspx_th_pack_script_0.setPageContext(_jspx_page_context);
    _jspx_th_pack_script_0.setParent(null);
    _jspx_th_pack_script_0.setEnabled(false);
    int _jspx_eval_pack_script_0 = _jspx_th_pack_script_0.doStartTag();
    if (_jspx_eval_pack_script_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_pack_script_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_pack_script_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_pack_script_0.doInitBody();
      }
      do {
        out.write("   \n");
        out.write("   <src>Classes/**</src>         \n");
        out.write("   <src>Functions/**</src>    \n");
        int evalDoAfterBody = _jspx_th_pack_script_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_pack_script_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_pack_script_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_pack_script_enabled.reuse(_jspx_th_pack_script_0);
      return true;
    }
    _jspx_tagPool_pack_script_enabled.reuse(_jspx_th_pack_script_0);
    return false;
  }
}
