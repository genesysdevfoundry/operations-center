/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Calendar.Calendar=function(objectHandlerController,data,viewTemplates){
    var _this=this;
    var _objectHandlerController=objectHandlerController;
    var _$panel=null;        
    var _data=data;
    var _name=data.values.name;
    var _id_Object=data.values.id_Object;
    var _id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
    var _historyTable;
    var _form;    
    var _weekCalendar;
    var _specialDyasCalendar;
    var _specialDaysCalendar;
    var _datepickerAddDate;    
    var _rollbackData;
    var _btRevertHandler;
    var _revertData=null;;
    var _selectDepartmentHandler;
    var _renderSpecialDaysData=data.values.specialDayData;
    
    
    this.getObjectHandlerController=function(){
        return _objectHandlerController;
    };
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getObjectId=function(){
        return _id_Object;
    };
    
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };
    
    this.historyActive=function(){
        _historyTable.draw();
    };
    
    this.editActive=function(){
        if (_rollbackData){
            _$panel.find("a[href='#week_" + _data.randomNumber + "']").tab("show");
            _form.setValues(_rollbackData);
            _weekCalendar.setValue(_rollbackData.weekData);
            _renderSpecialDaysData=_rollbackData.specialDayData;            
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _data.values.name + " (" + _rollbackData.dateStamp + ")"});
            _name="";     
            _rollbackData=null;            
        }
    };

    this.revert=function(){
        _form.setValues(_revertData);
        _$panel.find("a[href='#week_" + _data.randomNumber + "']").tab("show");
        _weekCalendar.setValue(_revertData.weekData);
        _renderSpecialDaysData=_revertData.specialDayData;        
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + _revertData.name});
        _btRevertHandler.hide();
        _revertData=null;
    };
    
    this.setBtRevertHandle=function(value){
        _btRevertHandler=value;
        value.hide();
        value.removeClass("btn-primary").addClass("btn-warning");
        
    };
    
            /**
     * Is set by contentController. 
     * _selectDepartmentHandler is a handler to the department select control in the tool bar.
     * @param {type} value
     * @returns {undefined}
     */
    
    this.setSelectDepartmentHandler=function(value){
        _selectDepartmentHandler=value;     
        if (_data.values.id_Department){
            _selectDepartmentHandler.val(_data.values.id_Department);
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        } else {
            _selectDepartmentHandler.val(objectHandlerController.getLastDepartmentId());
        }    
    };
    
    
    this.openBlockParameters=function($calendarBlock,formValues,day){
         //Set a default form with description as the only parameter
        var formContent={content:[
                   {
                       type:"TEXTAREA",
                        name:"description",
                        displayName:"Description",
                        defaultValue:"",
                        listGridName:"Description",
                        helpText:"",
                        mandatory:0
                }
            ]
        };

        //Render parameter form
        if (formValues){
            //Put description section as the first element
            if (_data.objectTemplate.form&&_data.objectTemplate.form.content){
                formContent.content=formContent.content.concat(_data.objectTemplate.form.content);        
            }
            
            var blockParameterForm=new Classes.DynamicForm.Form(_objectHandlerController.getMainApplication(),formContent,true);
            blockParameterForm.renderForm(function(){
                _$panel.find(".parameter-panel").find(".dynamic-form").remove(); 
                _$panel.find(".parameter-panel").append(blockParameterForm.get$Panel());            
                blockParameterForm.bindData(formValues);
                if ($calendarBlock){
                    blockParameterForm.subscribeChangeEvent("description",function(value){
                        $calendarBlock.find(".calendar-description-text").text(value);
                    });            
                }                
            });        
        } 

        if ($calendarBlock){
            _$panel.find(".sidepanel-title").text(day + " " +  $calendarBlock.find(".calendar-time-text").text());
        } else {
            _$panel.find(".sidepanel-title").text(day); 
        }
        
        _$panel.find(".sidenav").width("400px"); 
        _$panel.find(".main-panel").css("width","calc(100% - 400px");        
        _$panel.find(".parameter-panel").show();
    };
    
    this.saveAndClose=function(){
        _this.save(true);
    };
    
    this.close=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
     /***
     * 
     * @returns BlockParameterPanel as a jquery object
     */
    this.isBlockParametersPanelVisible=function(){
        return _$panel.find(".parameter-panel").css("display")!=="none";
    };
    
    this.save=function(close){   
       
        var saveData=_form.getValues();
        
        saveData.newObject=(_data.newObject)?true:false;
        
        saveData.id_Object=_id_Object;
        saveData.id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
        saveData.weekData=_weekCalendar.getValue();
        if (!_renderSpecialDaysData){
            saveData.specialDayData=_specialDaysCalendar.getValue();
        } else {
            saveData.specialDayData=_renderSpecialDaysData;
        }
        
        if (_selectDepartmentHandler){
            saveData.id_Department=_selectDepartmentHandler.val();
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        }
        
        Functions.ajaxRequest("SaveObject",saveData,function(data){  
            _btRevertHandler.hide();
            _revertData=null;

            if (!_id_Object){
                _id_Object=parseInt(data.id_Object);                
            }
            
            _data.newObject=false;
            
            _data.values=saveData;
            
            if (_name!==saveData.name){
                _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + saveData.name});
                _name=saveData.name;
            }
            
            _form.isSaved();   
            _weekCalendar.isSaved();
            if (_specialDaysCalendar){
                _specialDaysCalendar.isSaved();
            }
            _$panel.find(".box-week").removeClass("box-warning").addClass("box-primary");
            _$panel.find(".box-special-days").removeClass("box-warning").addClass("box-primary");
            
            
            _objectHandlerController.getMainApplication().getEventHandler().fire("UPDATE_OBJECT_LIST",_data.objectTemplate.id_ObjectTemplate);
        
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (close===true){
               _this.close();            
            } else {
               _fillHistoryTable(data.history);
               _this.closeBlockParameter();
            }
            
            //Callback points back to the object select parameter, when the "new" button is clicked
            if (_data.callback){
                _data.callback(_id_Object);
                _data.callback=null;
            }
        });
    };
    
    var _setChangedFlag=function(calendarData){
        for(var dayIndex=0;dayIndex<calendarData.length;dayIndex++){
            for(var n=0;n<calendarData[dayIndex].calendarBlocks.length;n++){
                calendarData[dayIndex].calendarBlocks[n].changed=true;
            }
        }
        return calendarData;
    };
    
    var _renderCell=function(data){ 
        if (typeof data ==="undefined"){
            return "";
        }
        
        if (data.toString().indexOf("$")===0){
            return "<font style=\"color:red;font-weight:bolder;\">" + data.replace("$CH$","") + "</font>";
        } else {
            return data;
        }
        
    };
    
    
    
    this.closeBlockParameter=function(){        
        _$panel.find(".sidenav").width("0px");        
        _$panel.find(".main-panel").css("width","100%");        
        _$panel.find(".parameter-panel").hide();
    };
    
    
    /**
     * Add new special day
     * 
     * @returns {undefined}
     */
    var _addDate=function(){
        if (_datepickerAddDate.val()){
            _specialDaysCalendar.addDate(_datepickerAddDate.val());
            _datepickerAddDate.val("");            
        }
    };
    
    
    var _fillHistoryTable=function(historyData){
        
        //Prepare data
        
        if (_historyTable){
            _historyTable.updateData(historyData);
        } else {
            var columns=[
                {data:"dateStamp",title:"Date"},
                {data:"userName",title:"Saved By"}                
            ];
            
            _historyTable=new Classes.Common.ListGrid(_$panel.find(".table-container"),historyData,columns);
            _historyTable.on_DbClick=_rollback; 
        }        
    };
    
    var _formChanged=function(){
        _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };

    var _rollback=function(){        
        _rollbackData=_historyTable.getSelectedRecord();        
        if (!_rollbackData){
            return;
        }
        
        if (!_revertData){
            _revertData=_form.getValues();        
            _revertData.weekData=_weekCalendar.getValue(true);
            if (!_renderSpecialDaysData){
                _revertData.specialDayData=_specialDaysCalendar.getValue(true);
            } else {
                _revertData.specialDayData=_renderSpecialDaysData;
            }            
        }
        _$panel.find("a[href='#week_" + _data.randomNumber + "']").tab("show");
        
        _objectHandlerController.getMainApplication().getEventHandler().fire("SHOW_TAB",{panelId:_this.id_Panel,tabIndex:1});
        _btRevertHandler.show();
           
    };
       
       
    var _openSpecialDays=function(){
        if (!_specialDaysCalendar){
            _specialDaysCalendar=new Classes.Calendar.CalendarComponent(_this,data.values.specialDayData,viewTemplates,true);
             _specialDaysCalendar.eventChanged=function(){                
                _$panel.find(".box-special-days").removeClass("box-primary").addClass("box-warning");
                _formChanged();
            };
            _$panel.find(".special-days-calendar").append(_specialDaysCalendar.get$Panel());
            _renderSpecialDaysData=null;
        } else {
            if (_renderSpecialDaysData){
                _specialDaysCalendar.setValue(_renderSpecialDaysData);                
            }
        }
    };
       
    /***
     * 
     * Constructor
     * 
     */
    
    
    //Render form
    data.randomNumber=Math.trunc(Math.random()*100000);
    _$panel=$(Mustache.render(viewTemplates.calendarView.result,data));
    
    
    _$panel.find("a[href='#special_days_" + _data.randomNumber + "']").on('shown.bs.tab',_openSpecialDays);
    
    _$panel.find("button[name='btRollback']").click(_rollback);
    
    _$panel.find(".close-side-panel").click(_this.closeBlockParameter);
    
    _$panel.find("button[name='btAddDate']").click(function(){
        _$panel.find("a[href='#special_days_" + _data.randomNumber + "']").tab("show");
        _$panel.find("input[name='datePicker']").datepicker("show");        
    });
    
    _datepickerAddDate=_$panel.find("input[name='datePicker']").datepicker({    
        calendarWeeks: true,
        autoclose: true,
        todayBtn: true,
        orientation: "bottom right",
        format: "yyyy-mm-dd"        
    });    
    
    _$panel.find("input[name='datePicker']").datepicker().on("changeDate",_addDate);
    
    _weekCalendar=new Classes.Calendar.CalendarComponent(_this,data.values.weekData,viewTemplates);
    
    _weekCalendar.eventChanged=function(){
        _$panel.find(".box-week").removeClass("box-primary").addClass("box-warning");       
        _formChanged();
    };
    
    _$panel.find(".week-calendar").append(_weekCalendar.get$Panel());
        
    //Special days data
    
       
    var params={content:[{type:"SECTION",collapsed:true,collapsable:true,title:"General",content:Calendar.calendarParameters}]};
    _form=new Classes.DynamicForm.Form(objectHandlerController.getMainApplication(),params,true);
    _form.renderForm(function(){
        _$panel.find(".general-section").append(_form.get$Panel());
        _form.bindData(data.values);
        _form.eventChanged=_formChanged;        
    });
    
    _fillHistoryTable(data.history);
};