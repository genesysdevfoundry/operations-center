/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Calendar.CalendarComponent=function(parent,data,viewTemplates,_specialDays){
    var _this=this;
    var _parent=parent;
    var _$panel;
    var _data=data;
    var _leftMouseButton=false;
    var _moveOrResizeCalendarBlock=null;
    var _$currentGridColumn;
    var _formValues={};
    var _selectedBlock;
    var _days=[
        {name:"Monday"},        
        {name:"Tuesday"},
        {name:"Wednesday"},
        {name:"Thursday"},
        {name:"Friday"},
        {name:"Saturday"},
        {name:"Sunday"}
    ];
    
    
    
    var _dayTimeArray=[];
    for (var i=0;i<48;i++){
        _dayTimeArray[i]=[];
    }
    
    var _startY=0; 
    var _gridBlockHeight=20;
    
    this.get$Panel=function(){        
        return _$panel;
    };
  
    var _removeCalendarBlock=function(){
        _parent.closeBlockParameter();
        $(this).parent().remove();
        _setChanged();
    };


    this.getValue=function(withChangeFlag){
        var returnData=[];
        for (var dayIndex=0;dayIndex<_days.length;dayIndex++){
            var day={day:_days[dayIndex].name,calendarBlocks:[],parameters:_formValues["DAY_" + dayIndex]};
            
            _days[dayIndex].$blockContainer.find(".calendar_block").each(function(){
                var $calendarBlock=$(this);
                var from=Math.round(parseInt($calendarBlock.css("top").replace("px","")));
                var to=from+Math.round($calendarBlock.height())+2;
                var newBlockObject={startTime:from/40.0,endTime:to/40.0,parameters:_formValues[$calendarBlock.attr("data-block-id")]||null};
                if (withChangeFlag){
                    newBlockObject.changed=$calendarBlock.hasClass("bg-danger-extra");
                }                
                day.calendarBlocks.push(newBlockObject);
            }); 
            returnData.push(day);
        }
        return returnData;
    };

    this.addDate=function(date){
        _data=_this.getValue(true);
        var dataToInsert={day:date,calendarBlocks:[]};
        var inserted=false;
        for(var i=0;i<_data.length;i++){
            if (_data[i].day===date){
                return;
            }
            if (_data[i].day>date){
                _data.splice(i,0,dataToInsert);   
                inserted=true;
                break;
            } 
        }
        if (!inserted){
            _data.push(dataToInsert);
        }
        _this.setValue(_data);
    };
    
    this.setValue=function(data){        
        _data=data;
        if (_specialDays||(_data&&_data.length)){
            //Prepare day object for special days
            _days=[];
            if (_data&&_data.length>0){
                for (var i=0;i<_data.length;i++){
                    _days.push({name:data[i].day,parameters:data[i].parameters});
                }
            }
        }
        _generatBackgound();
        if (_data){
            _createAllCalenderBlocks();
        }
    };
    
    this.isSaved=function(){
        for (var dayIndex=0;dayIndex<_days.length;dayIndex++){                    
            _days[dayIndex].$blockContainer.find(".calendar_block").removeClass("bg-danger-extra").addClass("bg-primary");
        }
    };
     
     
    var _openBlockParameters=function($block){             
        var day=$block.attr("data-day");                  
        day=_days[parseInt(day)].name;  
        var tmpFormValues=null;
                
        if (_selectedBlock!==$block.attr("data-block-id")){
            _selectedBlock=$block.attr("data-block-id");
            tmpFormValues=_formValues[_selectedBlock];
        } else {
            if (!parent.isBlockParametersPanelVisible()){
                tmpFormValues=_formValues[_selectedBlock];
            }             
        }
        parent.openBlockParameters($block,tmpFormValues,day);
    };
    
    var _openDayParameters=function(dayIndex){
        day=_days[parseInt(dayIndex)].name;
        _selectedBlock=null;
        parent.openBlockParameters(null,_formValues["DAY_" + dayIndex],day);
    };
     
    var _createAllCalenderBlocks=function(){        
        for (var i=0;i<_data.length;i++){
            for(var dayIndex=0;dayIndex<_days.length;dayIndex++){
                if (_data[i].day===_days[dayIndex].name){
                    for(var n=0;n<_data[i].calendarBlocks.length;n++){                            
                        _createCalenderBlock(dayIndex,Number(_data[i].calendarBlocks[n].startTime),Number(_data[i].calendarBlocks[n].endTime),_data[i].calendarBlocks[n].changed,_data[i].calendarBlocks[n].parameters);
                    }
                    break;
                }
            }
        }
    };

    
    var _createCalenderBlock=function(day,startTime,endTime,setChanged,parameterData){
        var $tmpElement=$(viewTemplates.calendarBlockView.result);
        $tmpElement.height((endTime-startTime)*40.0-2)
            .addClass("ui-widget-content")
            .addClass("bg-primary")            
            .css("position","absolute")
            .css("top",(startTime*40.0) + "px")
    
            .mousedown(function(event){
                _moveOrResizeCalendarBlock=$(this);
                
                if (_moveOrResizeCalendarBlock.hasClass("calendar-block-selected")){
                    return;
                }
                
                
                if (parent.isBlockParametersPanelVisible()){
                    _openBlockParameters(_moveOrResizeCalendarBlock);
                }
                
                _$panel.find(".calendar-block-selected,.calendar-block-changed-selected").removeClass("calendar-block-selected").removeClass("calendar-block-changed-selected");
                if (_moveOrResizeCalendarBlock.hasClass("bg-danger-extra")){
                    _moveOrResizeCalendarBlock.addClass("calendar-block-changed-selected");                
                } else {
                    _moveOrResizeCalendarBlock.addClass("calendar-block-selected");                
                }
                
            })
            
            .mouseup(function(event){                
                _mergeCalendarBlocks(_moveOrResizeCalendarBlock);
                _moveOrResizeCalendarBlock=null;
            }) 
            .resizable({
                        grid: 10,
                        containment: "parent",
                        handles: "n, s",
                        scroll: true,
                        resize:_resizeOrMove
                    })
            .draggable({
                        grid: [10,10],
                        containment: "parent",
                        scroll: true,
                        drag:_resizeOrMove
                    });
        
        $tmpElement.on('click', '.calendar-remove-block',_removeCalendarBlock); 
        $tmpElement.dblclick(function(){_openBlockParameters($(this));});     
        var blockId="ID_" + Math.floor(Math.random() * 100000);
        $tmpElement.attr("data-block-id",blockId);        
        $tmpElement.attr("data-day",day);        
        
        _formValues[blockId]=parameterData||{};
        
        if (parameterData&&parameterData.description){
            $tmpElement.find(".calendar-description-text").text(parameterData.description);
        }
        
        _days[day].$blockContainer.append($tmpElement);
        
        _setTimeText($tmpElement);
        
        _mergeCalendarBlocks($tmpElement);
        if (setChanged){
            _setChanged($tmpElement);
        }
    };
   
       
    var _mergeCalendarBlocks=function($tmpElement){        
        var $gridColumn=$tmpElement.parent();
        var calendarBlockFrom=$tmpElement.position().top;
        var calendarBlockTo=calendarBlockFrom+$tmpElement.height();
        var blockToRemove=[];
        $gridColumn.find(".calendar_block").not($tmpElement).each(function(){
            var $iteratorCalendarBlock=$(this);
            var itFrom=$iteratorCalendarBlock.position().top;
            var itTo=itFrom+$iteratorCalendarBlock.height();
            
            if (calendarBlockFrom<itFrom&&calendarBlockTo>itFrom||calendarBlockFrom<itTo&&calendarBlockTo>itTo||
                itFrom<calendarBlockFrom&&itTo>calendarBlockFrom||itFrom<calendarBlockTo&&itTo>calendarBlockTo){
                if(calendarBlockFrom>itFrom){
                    calendarBlockFrom=itFrom;
                }
                if(calendarBlockTo<itTo){
                    calendarBlockTo=itTo;
                }                
                blockToRemove.push($iteratorCalendarBlock);
            }
        });
        
        //Remove calendarBlocks
        for(var i=0;i<blockToRemove.length;i++){
            blockToRemove[i].remove();
        }
        
        $tmpElement.css("height",calendarBlockTo-calendarBlockFrom);
        $tmpElement.css("top",calendarBlockFrom);        
        _setTimeText($tmpElement);
    };
   
   
   var _gridCell_mousemove=function(e){        
        if (_moveOrResizeCalendarBlock){
            return;
        }
        e.stopPropagation();
        if (_leftMouseButton&&_$currentGridColumn){             
            var dayIndex=_$currentGridColumn.attr("data-day-index");
            var _posY=e.pageY-_$currentGridColumn.parent().offset().top;
            
            var fromGridBlock=Math.floor(_startY/_gridBlockHeight);
            var toGridBlock=Math.floor(_posY/_gridBlockHeight);
            
            for (var timeIndex=0;timeIndex<48;timeIndex++){
                if (timeIndex>=fromGridBlock&&timeIndex<=toGridBlock){
                    _dayTimeArray[dayIndex][timeIndex].addClass("calendar-grid-block-selected");
                } else {
                    _dayTimeArray[dayIndex][timeIndex].removeClass("calendar-grid-block-selected");
                }
            }    
        }
    };
    
    var _gridCell_mousedown=function(e){        
        if (_moveOrResizeCalendarBlock){
            return;
        }

        e.stopPropagation();
        _$currentGridColumn=$(this);
        _leftMouseButton=true;
         
        _startY=e.pageY-_$currentGridColumn.parent().offset().top;
                 
    };
    
    var _gridCell_mouseup=function(e){
        
        if (_moveOrResizeCalendarBlock){
            _mergeCalendarBlocks(_moveOrResizeCalendarBlock);
            _moveOrResizeCalendarBlock=null;
            return;
        }
        
        if (_leftMouseButton&&_$currentGridColumn){             
            var dayIndex=_$currentGridColumn.attr("data-day-index");            
            var _posY=e.pageY-_$currentGridColumn.parent().offset().top;
            
            var fromGridBlock=Math.floor(_startY/_gridBlockHeight);
            var toGridBlock=Math.floor(_posY/_gridBlockHeight)+1;           

            _leftMouseButton=false;
            for (var timeIndex=0;timeIndex<48;timeIndex++){            
                _dayTimeArray[dayIndex][timeIndex].removeClass("calendar-grid-block-selected");                
            }
            if (toGridBlock-fromGridBlock>1){
                _createCalenderBlock(dayIndex,fromGridBlock/2.0,toGridBlock/2.0,true);
            }            
        }
    };
    
    
    
    var _setChanged=function($tmpCalendarBlock){
        _this.eventChanged();
        if ($tmpCalendarBlock){
            $tmpCalendarBlock.removeClass("bg-primary").addClass("bg-danger-extra");
            if ($tmpCalendarBlock.hasClass("calendar-block-selected")){
                $tmpCalendarBlock.removeClass("calendar-block-selected").addClass("calendar-block-changed-selected");
            } 
        }
    };  
        
    var _resizeOrMove=function(event,u){
        var $tmpCalendarBlock=$(this);
        if (parent.isBlockParametersPanelVisible()){
            _openBlockParameters($tmpCalendarBlock);
        }
        _setChanged($tmpCalendarBlock);
        _setTimeText($tmpCalendarBlock);
    };
    
    var _removeDay=function(){
        _data=_this.getValue(true);
        _this.setValue(_data);           
        var $header=$(this);
        var dayIndexToRemove=$header.attr("data-day-index");
        _data.splice(parseInt(dayIndexToRemove),1);
        _this.setValue(_data);
    };
   
    var _setTimeText=function(_$calendarBlock){      
               
        var _fromTime=((Math.round(_$calendarBlock.position().top))/40);
        
        var _toTime=_fromTime+(2+Math.round(_$calendarBlock.height()))/40;
        
        _$calendarBlock.find(".calendar-time-text").text(
                ("0" + parseInt(_fromTime)).slice(-2) + ":" + ("0"  + (_fromTime*60)%60).slice(-2) + "-" + 
                ("0" + parseInt(_toTime)).slice(-2) + ":" + ("0"  + (_toTime*60)%60).slice(-2)
        );        
        return true;    
    };
   
    var _generatBackgound=function(){
        _$panel.find(".calendar-headers").empty();
        _$panel.find(".calendar-time-elements").empty();
        _$panel.find(".calendar-grid").empty();
        _$panel.find(".calendar-block-containers").empty();
        
        //Generate headers
        var $headers=_$panel.find(".calendar-headers");
        for (var dayIndex=0;dayIndex<_days.length;dayIndex++){
            var $tmpElement=$(Mustache.render(viewTemplates.calendarHeadresView.result,{text:_days[dayIndex].name}));
            if (!_specialDays){
               $tmpElement.find(".calendar-remove-block").hide();
            } else {
                $tmpElement.find(".calendar-remove-block").click(_removeDay);
                $tmpElement.find(".calendar-remove-block").attr("data-day-index",dayIndex);                
            }            
            
            $tmpElement.find(".calendar_colheader").attr("data-day-index",dayIndex);
            _formValues["DAY_" + dayIndex]=_days[dayIndex].parameters||{};
            
            $tmpElement.on('click', '.calendar-button-properties',function(){
                _openDayParameters($(this).parent().attr("data-day-index"));
            });
            
            $headers.append($tmpElement);            
        }
        
        //Generate times
        var $timeHeaders=_$panel.find(".calendar-time-elements");
        for (var i=0;i<=23;i++){
            var day={text:((i<10)?"0":"") + i};
            var $tmpElement=$(Mustache.render(viewTemplates.calendarTimeHeaderView.result,day));
            $timeHeaders.append($tmpElement);        
        }
        
      
        //Generate background grid
        var $grid=_$panel.find(".calendar-grid");
        for (var timeIndex=0;timeIndex<=47;timeIndex++){
            var $gridRow=$("<tr></tr>");
            for (var dayIndex=0;dayIndex<_days.length;dayIndex++){
                var $tmpElement=$(viewTemplates.calendarBackgroundCellView.result);
                $tmpElement.dayIndex=dayIndex;
                $tmpElement.timeIndex=timeIndex;
                _dayTimeArray[dayIndex][timeIndex]=$tmpElement.find("div");
                $gridRow.append($tmpElement);
            }
            $grid.append($gridRow);
        }
        
        //Generate block containers
        var $blockContainers=_$panel.find(".calendar-block-containers");
        for (var dayIndex=0;dayIndex<_days.length;dayIndex++){
            var $tmpElement=$(viewTemplates.calendarBlockContainerView.result);
            _days[dayIndex].$blockContainer=$tmpElement.find(".calendar-block-container");
            $blockContainers.append($tmpElement);
            $tmpElement.attr("data-day-index",dayIndex);
            $tmpElement.mousedown(_gridCell_mousedown)
                        .mousemove(_gridCell_mousemove)                    
                        .mouseup(_gridCell_mouseup);  
                
        }        
    };
    
    //Render form
    _$panel=$(viewTemplates.calendarTemplateView.result);            
    $(function(){
        _this.setValue(data);
        _$panel.find(".scroll-div").scrollTop(281);
            _$panel.find(".scroll-div").on('scroll', function () {
                _$panel.find(".header-scroll-area").scrollLeft($(this).scrollLeft());
        });
    });        
};
