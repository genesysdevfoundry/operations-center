/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.FormDesigner.FormList=function(mainApplication){
    var _this=this;
    var _table;    
    var _viewTemplates={
            formListView:{
                url:"Classes/FormDesigner/FormListView.html"},
          };
    var _$panel=null;
      
    var _updateFormList=function(){
        Functions.ajaxRequest("GetForms",{},function(data){
            _table.updateData(data);
        });
    };
    
    var _drawPanel=function(data){                
        _$panel=$(_viewTemplates.formListView.result);        
        
        var columns=[
            {data:"name",title:"Name"},
            {data:"description",title:"Description"},            
            {data:"created",title:"Created"},
            {data:"createdBy",title:"Created By"},
            {data:"changed",title:"Changed"},
            {data:"changedBy",title:"Changed By"}
        ];
        _table=new Classes.Common.ListGrid(_$panel.find(".table-container"),data,columns);
        _table.on_DbClick=_btOpenForm_Click;
                
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:"Form List",
            id_Panel:"FORM_LIST",
            icon:"fa-files-o",
            $panel:_$panel,
            buttons:[
                {caption:"Open",name:"btOpen",onClick:_btOpenForm_Click},
                {caption:"New",name:"btNew",onClick: function(){mainApplication.getEventHandler().fire("NEW_FORM",{});}},
                {caption:"Delete",name:"btDelete",onClick:_btDeleteForm_click},
                {caption:"Reload",name:"btReload",onClick:_updateFormList},
                {caption:"Close",name:"btClose",onClick:function(){mainApplication.getEventHandler().fire("REMOVE_PANEL","FORM_LIST");}}            
            ]
        });
    };
    
    var _btDeleteForm_click=function(){
        var selData=_table.getSelectedRecord();        
        if (selData){
           bootbox.confirm("Are you sure you want to delete selected form?", function(data){
                if (data){
                    Functions.ajaxRequest("DeleteForm",{id_Form:selData.id_Form},function(){
                        _updateFormList();
                    });
                }
            });   
        }
    };
    
    var _btOpenForm_Click=function(){
        var selData=_table.getSelectedRecord();        
        if (selData){
           mainApplication.getEventHandler().fire("OPEN_FORM_DESIGNER",selData.id_Form);                      
        }
    };

    var _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_FORM_LIST":          
                if (_table&&_$panel){
                    mainApplication.getEventHandler().fire("SELECT_PANEL","FORM_LIST");
                } else {
                    Functions.ajaxRequest("GetForms",{},_drawPanel);
                }
            break;
            case "UPDATE_FORM_LIST":
                if (_table&&_$panel){
                   _updateFormList();
                }
            break;
            case "PANEL_REMOVED":
                if (eventData==="FORM_LIST"){
                    _table.destroy();
                    _table=null;
                    _$panel=null;
                }
            break;
            case "PANEL_SELECTED":
                if (eventData.indexOf("FORM_LIST")===0){
                    _table.draw();
                }
            break; 
        }
    };
        
    /**Constructor*/
    mainApplication.getEventHandler().register(_eventListener);
    Functions.multiHTMLLoad(_viewTemplates);
};

