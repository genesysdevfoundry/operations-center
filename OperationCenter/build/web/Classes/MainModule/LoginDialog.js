/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.MainModule.LoginDialog=function(service,parameters,callBack){    
    var _this=this;    
    var _viewTemplates={
            loginDialogView:{
                url:"Classes/MainModule/LoginDialogView.html"                
            }
        };
        
    var _btOk_click=function(){
        //Rerquest the login service
        $.ajax({
            url:"Services/login",
            dataType: "json",
            data:{userName:global.userName,password:CustomModalDialog.find("input[name='txtPassword']").val()},
            type:"POST",
            success:function(data){
                if (data&&data=="SUCCESS"){
                    CustomModalDialog.modal("hide");
                    setTimeout(function(){
                        //Get generic data from Genesys config server
                        Functions.ajaxRequest("GetUserProfile",{},function(data){
                            //complete the initial ajax request
                            Functions.ajaxRequest(service,parameters,callBack);
                        });                        
                    },200);
                    
                } else {                    
                    CustomModalDialog.find("input[name='txtPassword']").val("");
                    CustomModalDialog.find(".modal-title").addClass("text-danger").text("Wrong password, try again!");                    
                }
            }
        });  
    };
 
    var _viewTemplatesLoaded=function(){        
        CustomModalDialog.find(".modal-dialog").empty();
        CustomModalDialog.find(".modal-dialog").append(_viewTemplates.loginDialogView.result);
        CustomModalDialog.find("input[name='txtUserName']").val(global.userName);
        CustomModalDialog.find("button[name='btOk']").click(_btOk_click);            
        CustomModalDialog.modal("show");
    };
    
    
    Functions.multiHTMLLoad(_viewTemplates,_viewTemplatesLoaded);        
};