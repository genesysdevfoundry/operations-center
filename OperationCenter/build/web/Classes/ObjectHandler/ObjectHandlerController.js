Classes.ObjectHandler.ObjectHandlerController=function(mainApplication){
    var _this=this;
    var _mainApplication=mainApplication;     
    var _viewTemplates={            
        objectView:{
            url:"Classes/ObjectHandler/ObjectView.html"},
        objectListView:{
            url:"Classes/ObjectHandler/ObjectListView.html"},
        calendarView:{
            url:"Classes/Calendar/CalendarView.html"},
        calendarTemplateView:{
            url:"Classes/Calendar/CalendarTemplateView.html"},       
        calendarHeadresView:{
            url:"Classes/Calendar/CalendarHeadresView.html"},       
        calendarTimeHeaderView:{
            url:"Classes/Calendar/CalendarTimeHeaderView.html"},       
        calendarBackgroundCellView:{
            url:"Classes/Calendar/CalendarBackgroundCellView.html"},
        calendarBlockContainerView:{
            url:"Classes/Calendar/CalendarBlockContainerView.html"},
        calendarBlockView:{
            url:"Classes/Calendar/CalendarBlockView.html"},
        workflowView:{
            url:"Classes/WorkflowDiagram/WorkflowView.html"},
        workflowBlockView:{
            url:"Classes/WorkflowDiagram/WorkflowBlockView.html"},
        promptView:{
            url:"Classes/PromptManagement/PromptView.html"},
        promptLanguageView:{
            url:"Classes/PromptManagement/PromptLanguageView.html"},    
        connectionDialogView:{
            url:"Classes/WorkflowDiagram/ConnectionDialogView.html"}        
    };

    
    
    var _idPanelCounter=0;
    var _openObjects=[];
    var _openObjectLists=[];   
    var _lastDepartmentId=0;
    var _clipboard={};
    
    this.getMainApplication=function(){
        return _mainApplication;
    };
    
    //A help function
    this.getAllParametersFromForm=function(data){
        var parameterArray=[];
        
        var getParameterRecursive=function(data){
            if (data instanceof Array){
                for (var i=0;i<data.length;i++){
                    getParameterRecursive(data[i]);
                }
            } else if (data instanceof Object){
                var parameter=data;
                if (parameter.name&&parameter.type&&parameter.type!=="SECTION"&&parameter.type!=="COLUMN"){
                    parameterArray.push(parameter);
                }
                
                if (parameter.content&&parameter.type!=="CHILD_SECTION"){
                    getParameterRecursive(parameter.content);
                }
            }
        };
        getParameterRecursive(data);
        return parameterArray;
    };
    
    this.getLastDepartmentId=function(){
        
        if (!_lastDepartmentId&&global.departments.length>0){
            _lastDepartmentId=global.departments[0];
        } 
        return _lastDepartmentId;
    };
    
    this.setLastDepartmentId=function(value){
        _lastDepartmentId=value;
        
        
    };
    
    this.getClipboard=function(){
        return _clipboard;
    };
    
        
    this.setClipboard=function(value){
        _clipboard=value;
    };
    
    var _openObjectListPanel=function(data){
        _idPanelCounter++;
        var newPanelId="OBJECT_LIST_" + _idPanelCounter;
        var objectList=new Classes.ObjectHandler.ObjectList(_this,data,_viewTemplates,data.objectTemplate.id_ObjectTemplate);
        objectList.id_Panel=newPanelId;        
        _openObjectLists.push(objectList);    
        
        
        
        buttons
        var buttons=[];
        var permissions=data.objectTemplate.permissions||"";
        if (permissions.indexOf("A")>0||permissions.indexOf("E")>0){
            buttons.push({caption:"Open",name:"btOpen",onClick:objectList.openObject});
        }
        if (permissions.indexOf("A")>0){
            buttons.push({caption:"New",name:"btNew",onClick: function(){mainApplication.getEventHandler().fire("NEW_OBJECT",{id_ObjectTemplate:data.objectTemplate.id_ObjectTemplate});}});
            buttons.push({caption:"Copy",name:"btCopy",onClick:objectList.copyObject});
        }
        if (permissions.indexOf("D")>0){
            buttons.push({caption:"Delete",name:"btDelete",onClick:objectList.deleteObject});
        }
        
        buttons.push({caption:"Reload",name:"btReload",onClick:objectList.updateObjectList});
        buttons.push({caption:"Close",name:"btClose",onClick:objectList.close});
        
        
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:data.objectTemplate.name + " List",
            id_Panel:newPanelId,
            icon:data.objectTemplate.icon||"fa-files-o",
            $panel:objectList.get$panel(),            
            buttons:buttons
        });
    };    
    
    var _openObjectPanel=function(data){        
        //Create new script Object and store it in open surveys
        _idPanelCounter++;
        var newObject={};
        var tabId= Math.trunc(Math.random()*100000);
        var noPadding=false;
        var tabs=[];
        var buttons;
        data.tabIdMain="tab_" + tabId;
        data.tabIdHistory="tab_" + (tabId +1);        
        
        //Get right type of object depending on the objectTemplate Type
        if (data.objectTemplate.type.indexOf("CALENDAR_OBJECT")===0){
            newObject=new Classes.Calendar.Calendar(_this,data,_viewTemplates);
            noPadding=true;
        } else if (data.objectTemplate.type.indexOf("WORKFLOW_OBJECT")===0){            
            
            data.tabIdDiagram="tab_" + (tabId+3);
            newObject=new Classes.WorkflowDiagram.Workflow(_this,data,_viewTemplates);
            
            tabs.push({caption:"Diagram",active:true,tabId:data.tabIdDiagram,onActive:newObject.diagramActive});
            tabs.push({caption:"General",active:false,tabId:data.tabIdMain});
            noPadding=true;
            
        } else if (data.objectTemplate.type.indexOf("PROMPT_LIST")===0){
            newObject=new Classes.PromptManagement.Prompt(_this,data,_viewTemplates);
        } else {
            newObject=new Classes.ObjectHandler.Object(_this,data,_viewTemplates);
        }
        
        if (data.objectTemplate.type.indexOf("STANDALONE")===0){
            buttons=[
                    {caption:"Close",name:"btClose",onClick:newObject.close},
                    {caption:"Revert",name:"btRevert",onClick:newObject.revert,setButtonHandle:function(value){newObject.setBtRevertHandle(value);}}
            ];
        } else {
            buttons=[
                    {caption:"Save",name:"btSave",onClick:newObject.save},
                    {caption:"Save & Close",name:"btSaveClose",onClick:newObject.saveAndClose},
                    {caption:"Close",name:"btClose",onClick:newObject.close},
                    {caption:"Revert",name:"btRevert",onClick:newObject.revert,setButtonHandle:function(value){newObject.setBtRevertHandle(value);}}
            ];
        }
        
        newObject.id_Panel="OBJECT_" + _idPanelCounter;
        _openObjects.push(newObject);
                
        if (data.objectTemplate.history){
            if (!tabs.length){
                tabs.push({caption:"Edit",active:true,tabId:data.tabIdMain,onActive:newObject.editActive});
            }
            tabs.push({caption:"History",active:false,tabId:data.tabIdHistory,onActive:newObject.historyActive});
        }
       
        //Create the panel
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:data.objectTemplate.name + (data.values.name?" - ":"") + (data.values.name||""),
            id_Panel:newObject.id_Panel,
            icon:data.objectTemplate.icon||"fa-file-text-o",
            $panel:newObject.get$Panel(),                    
            buttons:buttons,
            showSelectDepartment:(data.objectTemplate.belongToDepartment&&!global.defaultDepartment)?true:false,
            setSelectDepartmentHandler:function(value){newObject.setSelectDepartmentHandler(value);},
            noPadding:noPadding,
            tabs:tabs,
            unsaved:(data.values.id_Object===0)?true:false
        });        
    };
    
    var _copyObject=function(data){
        if (!data.id_Object){
            data.id_Object=0;
        }
        Functions.ajaxRequest("GetObject",data,function(data){
            data.values.name="Copy of " + data.values.name;
            data.history=[];
            data.newObject=true;
            data.values.id_Object=0;
            _openObjectPanel(data);
        });
    };

    var _openObject=function(data){
        //Check if object is already opened
        for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].getObjectId()===data.id_Object){
                //Select the object
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjects[i].id_Panel);
                return;
            }
        }
        if (!data.id_Object){
            data.id_Object=0;
        }
        Functions.ajaxRequest("GetObject",data,_openObjectPanel);
    };
      
    var _openObjectFromObjectTemplateId=function(id_ObjectTemplate){
        //Check if object is already opened
        for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].getObjectTemplateId()===id_ObjectTemplate){
                //Select the object
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjects[i].id_Panel);
                return;
            }
        }        
        Functions.ajaxRequest("GetObjectByObjectTemplateId",{id_ObjectTemplate:id_ObjectTemplate},_openObjectPanel);        
    }  
    
    var _openObjectList=function(id_ObjectTemplate){
        //Check if Object  is already opened
        for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].getObjectTemplateId()===id_ObjectTemplate){
                //Select the object list
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjectLists[i].id_Panel);
                return;
            }
        }
        Functions.ajaxRequest("GetObjects",{id_ObjectTemplate:id_ObjectTemplate},_openObjectListPanel);
    };
  
    var _objectClosed=function(id_Panel){
         for (var i=0;i<_openObjects.length;i++){
            if (_openObjects[i].id_Panel===id_Panel){
                //Remove object from open objects
                _openObjects.splice(i,1);
                break;
            }
        }
    };
    
    var _updateObjectList=function(id_ObjectTemplate){
         for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].getObjectTemplateId()===id_ObjectTemplate){
                _openObjectLists[i].updateObjectList();
                
                break;
            }
        }
    };    
    
    var _objectListClosed=function(id_Panel){
         for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].id_Panel===id_Panel){
                //Remove survey from open surveys
                _openObjectLists.splice(i,1);
                break;
            }
        }
    };
    
    var _newObject=function(paramData){
        Functions.ajaxRequest("GetObjectTemplate",{id_ObjectTemplate:paramData.id_ObjectTemplate},function(data){
            var newData={
               values:{
                   id_Object:0,                                                              
                   name:paramData.name||"New " + data.objectTemplate.name
               },
               objectTemplate:data.objectTemplate,
               history:[],
               newObject:true,
               callback:paramData.callback||null
           };
           _openObjectPanel(newData);                
        });        
    };
        
    var _objectListPanelSelected=function(id_Panel){
        for (var i=0;i<_openObjectLists.length;i++){
            if (_openObjectLists[i].id_Panel===id_Panel){
                _openObjectLists[i].panelSelected();
                break;
            }
        }
    }
    
    _eventListener=function(eventName,eventData){
        switch (eventName){
            case "COPY_OBJECT":
                _copyObject(eventData);               
            break;
            case "OPEN_OBJECT":
                _openObject(eventData);   
            break;
            case "OPEN_OBJECT_FROM_OBJECT_TEMPLATE_ID":
                _openObjectFromObjectTemplateId(eventData);   
            break;
            case "PANEL_REMOVED":
                if (eventData.indexOf("OBJECT_LIST")===0){
                    //The closed tab is a object list tab
                    _objectListClosed(eventData);
                } else if (eventData.indexOf("OBJECT_")===0){
                    //The closed tab is a object tab
                    _objectClosed(eventData);
                }
            break;
            case "OPEN_OBJECT_LIST":          
                _openObjectList(eventData);   
            break;            
            case "UPDATE_OBJECT_LIST":
                _updateObjectList(eventData);
            break;
            case "NEW_OBJECT":
                _newObject(eventData);
            break;  
            case "PANEL_SELECTED":
                if (eventData.indexOf("OBJECT_LIST")===0){
                    
                    _objectListPanelSelected(eventData);
                }
            break;    
            
        }
    };
        
    /**Constructor*/
    mainApplication.getEventHandler().register(_eventListener);
    Functions.multiHTMLLoad(_viewTemplates);

    if (global.departments&&global.departments.length>0){
        _lastDepartmentId=global.defaultDepartment||global.departments[0].id;
    }    
};