/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.ObjectTemplate.ObjectTemplate=function(mainApplication,data,_viewTemplates){
    var _this=this;
    var _$panel=null;    
    var _name=data.name;
    var _id_ObjectTemplate=data.id_ObjectTemplate;
    var _data=data;
    var _historyTable;
    var _baseForm;
    var _extendedForm;
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };

    this.saveAndClose=function(){
        _this.save(_this.close);
    };
    
    this.close=function(){
        mainApplication.getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
        
    this.save=function(callback){          
        if (!_baseForm.checkValidation()||(_extendedForm&&!_extendedForm.checkValidation())){
            bootbox.alert("Cannot save due to empty mandatory fields."); 
            return;
        }
        var saveData=_baseForm.getValues();
        if (_extendedForm){
            var tmpData=_extendedForm.getValues();            
            for (var key in tmpData){
                saveData[key]=tmpData[key];
            }
            saveData.relationData=_extendedForm.getObjectRelations();            
        }
        
        saveData.id_ObjectTemplate=_id_ObjectTemplate;
        
        Functions.ajaxRequest("SaveObjectTemplate",saveData,function(data){
            if (!_id_ObjectTemplate){
                _id_ObjectTemplate=data.id_ObjectTemplate;
            }
            
            if (_data.name!==_name){
                mainApplication.getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:"Object Template - " + _data.name});
                _name=_data.name;
                
            }
            
            _baseForm.isSaved();
            if (_extendedForm){
                _extendedForm.isSaved();
            }
            mainApplication.getEventHandler().fire("UPDATE_OBJECT_TEMPLATE_LIST");
                    
            mainApplication.getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            mainApplication.getEventHandler().fire("UPDATE_SIDEPANEL");
            
                        
            if (callback&&typeof(callback)==="function"){
                callback();
            }
            
                  
            //Callback points back to the script select parameter, when the "new" button is clicked
            if(_data.callback){
                _data.callback(_id_ObjectTemplate);
                _data.callback=null;
            }            
        }); 
    };
    
    var _changeObjectTemplateType=function(type){        
        _$panel.find(".form-extended").find(".dynamic-form").remove();        
        if (ObjectTemplate["parameters_" + type.toLowerCase()]){
            var params={content:[{type:"SECTION",collapsable:true,title:"Parameters",content:ObjectTemplate["parameters_" + type.toLowerCase()]}]};
            _extendedForm=new Classes.DynamicForm.Form(mainApplication,params,true);
            _extendedForm.renderForm(function(){        
                _$panel.find(".form-extended").append(_extendedForm.get$Panel());
                _extendedForm.bindData(data);
                _extendedForm.eventChanged=_formChanged;
                _extendedForm.subscribeChangeEvent(_changeObjectTemplateType);
                _extendedForm.baseForm=_baseForm;
            });
        } else {
            _extendedForm=null;
        }
    };
    
    var _formChanged=function(){
        mainApplication.getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };
    
    _$panel=$(Mustache.render(_viewTemplates.objectTemplateView.result,data));

    var params={content:[{type:"SECTION",collapsable:true,title:"General",content:ObjectTemplate.objectTemplateParameters}]};
    _baseForm=new Classes.DynamicForm.Form(mainApplication,params,true);
    _baseForm.renderForm(function(){
        _$panel.find(".form-base").append(_baseForm.get$Panel());
        _baseForm.bindData(data);
        _baseForm.eventChanged=_formChanged;
        _baseForm.subscribeChangeEvent("type",_changeObjectTemplateType);
    });
    
    _changeObjectTemplateType(data.type);    
};