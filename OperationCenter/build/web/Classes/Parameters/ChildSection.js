/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.ChildSection=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value=[];
    var _data=data;
    var _table;
    var _form=form;
    var _dialogForm;
    var _name=data.name;
    var _boundData;
    var _firstTime=true;
    var _action;
    var _maxSortOrder=0;
    var _keyFieldValue;
    var _oldValue;

    this.bindData=function(data){
        _boundData=data;
    };
    
    this.eventChanged=null;    
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getName=function(){
        return _name;
    };
    
    this.getValue=function(){
        if (_data.hidden){
            return _value;
        }                
        
        var returnData=[];
        if (_data.fixedSortOrder){
            for(var i=0;i<_value.length;i++){
                var newData={};
                _value[i][_data.sortOrderFieldName]=i+1;
                for (var key in _value[i]){
                    if (key!=="_buttons"){
                        newData[key]=_value[i][key];
                    }
                }
                returnData.push(newData);
            }
            return returnData;
        } else {
            return _value;
        }
    };
        
    this.setValue=function(value){
        if (typeof value !=="undefined"){
            _value=value;
        } else {
            _value=[];
        }

        if (_data.hidden){
            return;
        }
        if (!(_value instanceof Array)){
            _value=[];
        }
        
        if (_table){                    
            if (_data.fixedSortOrder){
                _value=_prepareData(_value);
                _table.updateData(_value);
            } else {
                _table.updateData(_value);
            }            
            return;
        }
        
        _checkMandatory();
    };
        
    this.populateData=function(data){
    };        
    
    this.validate=function(){
        if (data.mandatory&&!_this.getValue()){
            return false;            
        }
        return true;
    };
    
    this.isSaved=function(){
        _$panel.removeClass("box-warning").addClass("box-default");
    };
    
    var _prepareData=function(data){
        var outputData=[];
        
        for(var i=0;i<data.length;i++){
            
            var newData={};
            data[i][_data.sortOrderFieldName]=i+1;
            for (var key in data[i]){
                newData[key]=data[i][key];
            }
            
            newData["_buttons"]=(i?"UP_":"") + (i!==data.length-1?"DOWN_":"") + data[i][_data.sortOrderFieldName];
            outputData.push(newData);
        }        
        return outputData;        
    };
        
    var _checkMandatory=function(){
        if (data.mandatory){
            if (_this.getValue().length>0){
                _$panel.find(".fa-warning").hide();
            } else {
                _$panel.find(".fa-warning").show();
            }
        } else {
            _$panel.find(".fa-warning").hide();
        }
    };
    
        
    var _playRecording_Click=function(event){
        var filename=$(this).attr("data-link");
        Functions.ajaxRequest("PrepareMP3File",{filename:filename},function(data){            
            form.getMainApplication().getEventHandler().fire("PLAY_PROMPT",{filename:data.tmpFilename});
        });
        event.stopPropagation();
    };
    
    var _moveDown_Click=function(){
        var index=$(this).attr("data-sort-order")-1;
        var tmpValue=_value[index];
        _value[index]=_value[index+1];
        _value[index+1]=tmpValue;
        _this.setValue(_value);
        _changed();
        event.stopPropagation();
    };
    
    var _moveUp_Click=function(){
        var index=$(this).attr("data-sort-order")-1;
        var tmpValue=_value[index];
        _value[index]=_value[index-1];
        _value[index-1]=tmpValue;
        _this.setValue(_value);
        _changed();
        event.stopPropagation();
    };
   
   
   var _gridCheckbox_Click=function(){        
       var $checkbox=$(this);
       
       var name=$checkbox.attr("data-name");
       var index=$checkbox.attr("data-row");       
       _value[index][name]=1-(_value[index][name]||0);       
       _this.setValue(_value);
       _changed();
       
   };
       
    //A help function
    var _getAllParametersFromForm=function(data){
        var parameterArray=[];
        
        var getParameterRecursive=function(data){
            if (data instanceof Array){
                for (var i=0;i<data.length;i++){
                    getParameterRecursive(data[i]);
                }
            } else if (data instanceof Object){
                var parameter=data;
                if (parameter.name&&parameter.type&&parameter.type!=="SECTION"&&parameter.type!=="COLUMN"){
                    parameterArray.push(parameter);
                }
                
                if (parameter.content&&parameter.type!=="CHILD_SECTION"){
                    getParameterRecursive(parameter.content);
                }
            }
        };
        getParameterRecursive(data);
        return parameterArray;
    };
    
    var _createTable=function(data){
        
        var _renderCell=function(data){
            if (typeof data ==="undefined"){
                return "";
            }
            else {
                return Functions.htmlEncode(data);
            }
        };
        
        var _renderCheckbox=function(data,a,b,meta){
            var checked="";
            
            if (data===1||data==="1"||data==="true"){
                checked="checked";
            }
            
            return "<input type=\"checkbox\" " + checked + " data-name=\"" + columns[meta.col].data + "\" data-row=\"" + meta.row + "\" style=\"width:100%;margin:auto;\" class=\"grid-checkbox\"  />";
        };
        
        var _renderUpAndDownButtons=function(data){
            if (data){
                var resultString="";
                var sortOrderIndex=data.replace("UP_","").replace("DOWN_","");
                if (data.indexOf("UP")>=0){                    
                    resultString+="<button class=\"grid-button bt-move-up\" data-sort-order=\"" + sortOrderIndex + "\"><i class=\"fa fa fa-level-up\"></i></button>";
                }
                if (data.indexOf("DOWN")>=0){
                    resultString+="<button class=\"grid-button bt-move-down\" data-sort-order=\"" + sortOrderIndex + "\"><i class=\"fa fa fa-level-down\"></i></button>";
                }
                return resultString;
            }
            return "";
        };
        
        var columns=[];
        var parameters=_getAllParametersFromForm(data);
        
        if (_data.fixedSortOrder&&_data.sortOrderFieldDisplayName){
            columns.push({data:_data.sortOrderFieldName,title:_data.sortOrderFieldDisplayName,width: 100});
        }
        
        for(var i=0;i<parameters.length;i++){
            if (parameters[i].displayName){                
                columns.push({data:parameters[i].name,title:parameters[i].displayName,render:parameters[i].type==="CHECKBOX"&&parameters[i].gridButton?_renderCheckbox:_renderCell,orderable:_data.fixedSortOrder?false:true});
            }
        }
        
        if (_data.fixedSortOrder){
            columns.push({data:"_buttons",title:"",render:_renderUpAndDownButtons,width: 20,orderable: false});
        }
        
        var options={
            paging:false,        
            searching:false,
            info:false,
            ordering:(_data.fixedSortOrder?false:true)
        };
        
        _table=new Classes.Common.ListGrid(_$panel.find(".table-container"),[],columns,options,function(_$table){
            _$table.find("tbody").on( 'click', '.recording-button', _playRecording_Click);
            _$table.find("tbody").on( 'click', '.grid-checkbox', _gridCheckbox_Click);            
            _$table.find("tbody").on( 'click', '.bt-move-up', _moveUp_Click);
            _$table.find("tbody").on( 'click', '.bt-move-down', _moveDown_Click);            
        });
        _table.on_DbClick=_editRow;        
    };
    
    var _changed=function(){
        if (_this.eventChanged){
            _this.eventChanged();
        }
        if (_boundData){
            _boundData[_data.name]=_this.getValue();
        }
        _$panel.removeClass("box-default").addClass("box-warning");
    };
    
    
    var _btOk_Click=function(){
        if (!_dialogForm.checkValidation()){            
            return;            
        }
        
        
        var newData=_dialogForm.getValues();
        
        
        
        //Check if key field already exists  -  ADD
        if (_data.keyField&&newData[_data.keyField]&&(_action==="ADD")){
            for (var i=0;i<_value.length;i++){
                if (_value[i][_data.keyField]===newData[_data.keyField]){
                    bootbox.alert("Can't add duplicated records.");
                    return;
                }
            }                
        } 

        //Check if key field already exists  -  EDIT
        if (_data.keyField&&newData[_data.keyField]&&(_action==="EDIT")&&_keyFieldValue!==newData[_data.keyField]){
            for (var i=0;i<_oldValue.length;i++){ //We need to use _oldValue because _value is updated directly when any of the controls on the form is changed
                if (_oldValue[i][_data.keyField]===newData[_data.keyField]){
                    bootbox.alert("Can't add duplicated records.");
                    return;
                }
            }                
        } 


        
        if (_action==="ADD"){            
            _value.push(_dialogForm.getValues());            
        } 
        
        _oldValue=null;
        CustomModalDialog.modal("hide");
        _changed();
        _this.setValue(_value);
    };


    var _showModalDialog=function(data){
        CustomModalDialog.find(".modal-dialog").empty();
        CustomModalDialog.find(".modal-dialog").append($(_viewTemplates.childSectionDialogView.result));
        
        _dialogForm=new Classes.DynamicForm.Form(_form.getMainApplication(),{content:_data.content},false,true);
        _dialogForm.renderForm(function(){
            if (data){
                _dialogForm.bindData(data);
            }
            CustomModalDialog.find(".modal-body").append(_dialogForm.get$Panel());                    
        });    
        CustomModalDialog.find("button[name='btOk']").click(_btOk_Click);   
        CustomModalDialog.off('hidden.bs.modal')
        CustomModalDialog.on('hidden.bs.modal', function(){            
            if (_action==="EDIT"&&_oldValue){
                _value=_oldValue;                
                _this.setValue(_value);
                 if (_boundData){
                    _boundData[_data.name]=_this.getValue();
                }
            }    
        });         
        
        
        CustomModalDialog.modal("show");        
    };
    
            

    var _addRow=function(){    
        _action="ADD";
        _showModalDialog();
    };
    
    
    
    
    var _editRow=function(){
        _action="EDIT";
        _oldValue=_arrayClone(_value);
        var rowData=_table.getSelectedRecord();
        
        if (_data.keyField&&rowData[_data.keyField]){
            _keyFieldValue=rowData[_data.keyField];
        }
        
        if (rowData){
           _showModalDialog(rowData);
        }
    };

    var _arrayClone=function(inputArray){
        if (!inputArray){
            return [];
        }
        var outputArray=[];
        for (var i=0;i<inputArray.length;i++){
            outputArray.push(jQuery.extend(true, {}, inputArray[i]));                               
        }
        return outputArray;
    };

    var _deleteRow=function(){
        var rowData=_table.getSelectedRecord();
        for (var i=0;i<_value.length;i++){
            if (Object.is(_value[i],rowData)){
                _value.splice(i,1);
            }
        }
        _changed();
        _this.setValue(_value);
    };
    
    /***
     * Constructor
     */
    
    if (!_data.hidden){
    
        _$panel=$(Mustache.render(_viewTemplates.childSectionView.result,data));    
        _$panel.find("button[name='btAdd']").click(_addRow);
        _$panel.find("button[name='btEdit']").click(_editRow);
        _$panel.find("button[name='btDelete']").click(_deleteRow);        
        if (form.fixedSortOrder){
            if (!data.sortOrderFieldName){
                data.sortOrderFieldName="sortOrder";
            }
        }
        _createTable(data.content);
    }
};