/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.Column=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _childElements=[];
    var _form=form;
    var _unsaved=false;
    
    this.eventChanged=null;
    
    this.get$Panel=function(){
        return _$panel;
    };   
    
    var _get$Content=function(){
        return _$panel.find(".parameter-content");  

    };
    
    this.isSaved=function(){
        if (_unsaved){            
            _unsaved=false;
        }
         
        for (var i=0;i<_childElements.length;i++){
            if (_childElements[i].isSaved){
                _childElements[i].isSaved();
            }
        }
    };
    
    var _changedEventHandler=function(){
        if (!_unsaved){            
            _unsaved=true;
            _this.eventChanged();
        }           
    };
            
    var _render=function(columns){
        if (columns>4){
            return;
        }
        var cols=[];
            
        for (var rowIndex=0;rowIndex<data.content.length/columns;rowIndex++){
            for (var colIndex=0;colIndex<columns;colIndex++){
                if (!rowIndex){
                    cols[colIndex]=[];
                }            
                if ((rowIndex*columns)+colIndex<data.content.length){            
                   cols[colIndex].push(data.content[(rowIndex*columns)+colIndex]);
                } else {
                    cols[colIndex].push({type:"EMPTY"})
                }
            }
        }
    
        for (var colIndex=0;colIndex<columns;colIndex++){
            var $col=$("<div class=\"col-md-" + 12/columns + "\">");
            _$panel.append($col);
            _childElements=_childElements.concat(_form.renderFormElements(cols[colIndex],$col,_changedEventHandler));
        }
    };    
    
    /***
     * Constructor
     */        
    _$panel=$("<div>").addClass("row");    
    _render(data.columns);
    
};