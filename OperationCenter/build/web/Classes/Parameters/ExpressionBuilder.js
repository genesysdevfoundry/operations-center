/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.ExpressionBuilder=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _hiddenValue;
    var _name=data.name;
    var _boundData;
    var _data=data;
    var _form=form;
    var _value;
    var _disableEvents=false;
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    this.getName=function(){
        return _name;
    };
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.validate=function(){
        return true;
    };
    
    this.setValue=function(value){
        if (data.hidden){
            _value=value||"";
            return;
        }
        var emptyExpression={"condition":"AND","rules":[],"valid":true};
        if (!value){
            _disableEvents=true;        
            _$panel.find(".expression-builder").queryBuilder("setRules",emptyExpression); 
            _$panel.find(".box-title").text("");
            _disableEvents=false;
            return;
        }    
        _$panel.find(".box-title").text(value);
        var expressionObject=Function.getExpressionBuilderFromSkillExpression(value);        
        _disableEvents=true;        
        if (expressionObject){
            try{                
                _$panel.find(".expression-builder").queryBuilder("setRules",expressionObject); 
            } catch (ex){

            }
        
        }else{
            _$panel.find(".expression-builder").queryBuilder("setRules",emptyExpression); 
        }
        _disableEvents=false;
    };
    
    var _getMergedData=function(){
        if (_data.listData&&data.listData.length){
            //Merge data                
            var newData=[];
            for (var i=0;i<_data.listData.length;i++){
                newData.push(_data.listData[i].skill);
            }
            if (_data.scriptData){             
                for (var i=0;i<_data.scriptData.length;i++){
                    newData.push(_data.scriptData[i]);
                }                
            } 
            return newData;                       
        } else {            
            return data.scriptData;
        }
    };
    
    this.setHiddenValue_old=function(value){
        if (value){
            try{
                _disableEvents=true;
                _$panel.find(".expression-builder").queryBuilder("setRules",value);  
            } catch (ex){
                
            }
        } else {
            //_$panel.find(".expression-builder").queryBuilder("setRules",{});  
        }
        _disableEvents=false;
    };
    
    
    this.getValue=function(){
        if (data.hidden){
            return _value||"";
        }
        
        var _skillExp=_$panel.find(".expression-builder").queryBuilder("getRules");                
        if (_skillExp){
            var skillExp_Disp=Functions.getSkillExpressionFromExpressionBuilder(_skillExp).replace("()","");
            if (skillExp_Disp==="()"){
                skillExp_Disp="";
            }
            return skillExp_Disp;
        } else {
            return "";                
        }
    };
    
    this.getHiddenValue_old=function(){        
        var _skillExp=_$panel.find(".expression-builder").queryBuilder("getRules");        
        if (_skillExp){
            return _skillExp;
        } else {
            return {};                
        }
    };
    
    var _reload=function(){
        _form.getMainApplication().getCacheHandler().getData(data.scriptRequestString,function(scriptData){
            _$panel.find(".expression-builder").queryBuilder("setFilters",true,_getQueryBuilderFilter(scriptData));
        },true);
    };
    
    
    
    var _getQueryBuilderFilter=function(data){

        var filters=[];
        for (var i=0;i<data.length;i++){
            filters.push({
                        id: data[i],
                        label: data[i],
                        type: 'integer',
                        input: 'select',                        
                        values: [0,1,2,3,4,5,6,7,8,9,10]
            });
        }
        return filters;
    };
    
    var _initQueryBuilder=function(data){
        var _queryBuilder=_$panel.find(".expression-builder").queryBuilder({                
                allow_empty:true,
                operators: [
                    {type:'equal'},
                    {type:'not_equal'},
                    {type:'greater'},
                    {type:'less'},
                    {type:'greater_or_equal'},
                    {type:'less_or_equal'}
                ],
                filters: _getQueryBuilderFilter(data)
            });

            _queryBuilder.on('afterUpdateRuleValue.queryBuilder.filter afterUpdateGroupCondition.queryBuilder.filter afterDeleteGroup.queryBuilder.filter afterDeleteRule.queryBuilder afterAddRule.queryBuilder', function(e, level) {
                if (!_disableEvents){
                    var value=_this.getValue();
                    if (_boundData){
                        _boundData[data.name]=value;
                        _boundData["_" + data.name]=_this.getHiddenValue();                    
                    }
                    
                    _$panel.find(".box-title").text(value);
                    
                    /*if (_eventChangedSubscription){
                        _eventChangedSubscription(_this.getValue());
                    }*/
                    if (_this.eventChanged){
                        _this.eventChanged();
                    }            
                }
            });           
    };
    if (!data.hidden){
        _$panel=$(Mustache.render(_viewTemplates.expressionBuilderView.result,data));    
        _$panel.find("button[name='btReload']").click(_reload);
        
        if (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY"){
            _$panel.find(".fa-plus").remove();
        }

        
        _initQueryBuilder(_getMergedData());
                
    }
    
};