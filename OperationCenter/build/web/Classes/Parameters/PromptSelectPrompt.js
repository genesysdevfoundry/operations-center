/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.PromptSelectPrompt=function(parent,data,_viewTemplates,selectedValue,form){
    var _this=this;
    var _$panel;   
    var _disableEvents=false;
    var _select;
    var _dataList;
    var _index=0;
    
    this.setIndex=function(value){
        _index=value;
    };
    
    this.eventChanged;
    
    this.getValue=function(){
        var selValue=_select.val();        
        if (selValue&&selValue!=="0"){
            return selValue;
        }            
        else {            
            return null;
        }
    };
        
    this.setValue=function(value){
     
        var match=false;
        for (var i=0;i<_dataList.length;i++){
            if (_dataList[i].id + ""===value + ""){
                match=true;
                break;
            }
            if (_dataList[i].children){                   
                //Check within a group 
                for (var n=0;n<_dataList[i].children.length;n++){
                    if (_dataList[i].children[n].id + ""===value + ""){
                        match=true;
                        break;
                    }
                }
            }
        }


        if (!match){
            _dataList.push({
                text: 'Not in List',
                children: [{id:value,text:"Recording ID:" + value}]
            });
            //Refresh the control
            if (_select){
                _$panel.find(".select2").empty();
            }                         
            _select=_$panel.find(".select2").select2({data:_dataList});
            _select.select2().on("change",_changed);
        }
                        
        if (value){
            _select.val(value).trigger("change");
        }        
    };
    
    this.get$Panel=function(){
        return _$panel;
    };
        
    var _changed=function(){
        if (_this.eventChanged){
           _this.eventChanged();
        }
    };
    
    var _removePrompt=function(){        
        if (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY"){
            bootbox.alert("The parameter is in read only mode");
            return;
        }
        parent.removePrompt(_this);
    };
    
    
    var _playRecording=function(){
        var filename=Function.getFileName(_select.val());
        Functions.ajaxRequest("PrepareMP3File",{filename:filename},function(data){            
            mainApplication.getEventHandler().fire("PLAY_PROMPT",{filename:data.tmpFilename});
        });
    };
    
    var _fillData=function(){
        _dataList=[{id:"0",text:"[None]"}];
        
        //Add list data
        if (data.listData&&data.listData.length>0){
            for (var i=0;i<data.listData.length;i++){
                _dataList.push({id:data.listData[i].text,text:data.listData[i].text});
            }
        }
        
        if (data.scriptData.length>0){            
            var groupName;
            var children;        
            for (var i=0;i<data.scriptData.length;i++){
                if (data.scriptData[i].group){
                    if (groupName!==data.scriptData[i].group){
                        groupName=data.scriptData[i].group;
                        children=[];
                        _dataList.push({text:groupName,children:children});
                    }
                    children.push({id:data.scriptData[i].id,text:data.scriptData[i].text});
                } else {
                    _dataList.push({id:data.scriptData[i].id,text:data.scriptData[i].text});
                }
                
            }            
        }
        
        if (_select){
            _$panel.find(".select2").empty();
        }                         
        _select=_$panel.find(".select2").select2({
            data:_dataList,
            dropdownParent: (form.isModal()?_$panel:false),  //Needs to be true for modal dialog windows
            disabled: (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY")
        }).on("change",_changed);
    };
    
    var _newPromptCallback=function(id){
        parent.getForm().getMainApplication().getCacheHandler().getData("NAME_KVPListGetPrompts",function(scriptData){
            data.scriptData=scriptData;
            _fillData();
            _this.setValue(id);
        },true);
    };
    
    _$panel=$(Mustache.render(_viewTemplates.promptSelectPromptView.result,data));   
    _$panel.find("button[name='btRemove']").click(_removePrompt);
    _$panel.find("button[name='btReload']").click(function(){
        _newPromptCallback(_select.val());
    });
    
    _$panel.find("button[name='btNew']").click(function(){
        if (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY"){
            bootbox.alert("The parameter is in read only mode");
            return;
        }
        parent.getForm().getMainApplication().getEventHandler().fire("NEW_OBJECT", {
            id_ObjectTemplate:global.promptObjectTemplateId,
            callback:_newPromptCallback,
            name:parent.getNewPromptNamePrefix() + " " + _index
        });        
    });
    
    
    //_$panel.find("button[name='btPlay']").click(_playRecording);
    
    _$panel.find("button[name='btOpen']").click(function(){ 
        parent.getForm().getMainApplication().getEventHandler().fire("OPEN_OBJECT", {id_Object:_select.val()});
    });
    
    
    _fillData();    
    
    if (selectedValue){        
        _this.setValue(selectedValue);
    }          
};