/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.Section=function(form,sectionData,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _childElements=[];
    var _form=form;
    var _unsaved=false;
    
    this.eventChanged=null;
    
    this.get$Panel=function(){
        return _$panel;
    };
   
    
    var _get$Content=function(){
        if (!sectionData.hidden){
            return _$panel.find(".parameter-content");  
        }
        else {
            return $("<div/>"); //Retrun an empty div
        }

    };
    
    this.isSaved=function(){
        if (_unsaved){
            _$panel.find(".box").first().removeClass("box-warning").addClass("box-" + (sectionData.colorSchema||"primary"));
            _unsaved=false;
        }
        for (var i=0;i<_childElements.length;i++){
            if (_childElements[i].isSaved){
                _childElements[i].isSaved();
            }
        }
    };
    
    var _changedEventHandler=function(){
        if (!_unsaved){
            _$panel.find(".box").first().removeClass("box-" + (sectionData.colorSchema||"primary")).addClass("box-warning");        
            _unsaved=true;
            _this.eventChanged();
        }           
    };
            
            
    /***
     * Constructor
     */       
    
    if (!sectionData.hidden){
        _$panel=$(Mustache.render(_viewTemplates.sectionView.result,sectionData));  
    }
    

    if (sectionData.content){
       _childElements=_form.renderFormElements(sectionData.content,_get$Content(),_changedEventHandler);
    }


};