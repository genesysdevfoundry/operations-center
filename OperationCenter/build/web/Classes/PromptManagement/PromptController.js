/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.PromptManagement.PromptController=function(mainApplication){
    var _this=this;
    var _table; 
    var _$promptList;
    
    var _viewTemplates={
            promptView:{
                url:"Classes/PromptManagement/PromptView.html"}          
    };
    
    var _idPanelCounter=0;
    var _openPrompts=[];
        
    
    var _openPromptPanel=function(data){
         //Create new prompt Object and store it in open surveys
        _idPanelCounter++;
        var newPrompt={};            
        newPrompt=new Classes.PromptManagement.Prompt(mainApplication,data,_viewTemplates);
        newPrompt.id_Panel="PROMPT_" + _idPanelCounter;
        _openPrompts.push(newPrompt);

        //Create the panel
        mainApplication.getEventHandler().fire("ADD_PANEL",{
                name:"Prompt - " + data.name,
                id_Panel:newPrompt.id_Panel,
                icon:"fa-file-text-o",                
                $panel:newPrompt.get$Panel(),                    
                buttons:[
                    {caption:"Save",name:"btSave",onClick:newPrompt.save},
                    {caption:"Save & Close",name:"btSaveClose",onClick:newPrompt.saveAndClose},
                    {caption:"Close",name:"btClose",onClick:newPrompt.close}
                ]                
       });        
    };
    
        
        
    var _openPrompt=function(id_Prompt){
        //Check if survey is already opened
        for (var i=0;i<_openPrompts.length;i++){
            if (_openPrompts[i].getPromptId()===id_Prompt){
                //Select the survey
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openPrompts[i].id_Panel);
                return;
            }
        }
        
        Functions.ajaxRequest("GetPrompt",{id_Prompt:id_Prompt},_openPromptPanel);
    };
  
    var _promptClosed=function(id_Panel){
         for (var i=0;i<_openPrompts.length;i++){
            if (_openPrompts[i].id_Panel===id_Panel){
                //Remove survey from open surveys
                _openPrompts.splice(i,1);
                break;
            }
        }
    };
    
    var _newPrompt=function(data){
        var newData={
            id_Prompt:0,
            name:(data&&data.name)?data.name:"New Prompt",
            description:"",           
            type:(data&&data.type)?data.type:"",
            callback:(data&&data.callback)?data.callback:null,
            jSCode:data.jSCode||""
        };
        _openPromptPanel(newData);
    };
    
    _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_PROMPT":                
                _openPrompt(eventData);               
            break;
            case "PANEL_REMOVED":
                if (eventData.indexOf("PROMPT_")==0){
                    //The closed tab is a survey tab
                   _promptClosed(eventData);
                }
            break;
            case "NEW_PROMPT":
                _newPrompt(eventData);
            break;                        
        }
    };
        
    /**Constructor*/
     mainApplication.getEventHandler().register(_eventListener);
     Functions.multiHTMLLoad(_viewTemplates);
    
};