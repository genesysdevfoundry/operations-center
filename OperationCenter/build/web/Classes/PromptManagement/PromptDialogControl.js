/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.PromptManagement.PromptDialogControl = function (mainApplication) {
    var _this = this;
    var _$panel;
    var _parent;
    var _callbackFunction;
    var _data;
    var _parameterString;
    var _recordingStart;
    var _recordRTC;
    var _mediaStream;
    var _viewTemplates = {
        createPromptDialogView: {
            url: "Classes/PromptManagement/CreatePromptDialogView.html"},
        playPromptDialogView: {
            url: "Classes/PromptManagement/PlayPromptDialogView.html"}
    };

    var _prepareParameterString = function (data) {
        var paramStr;
        for (var key in data) {
            if (key !== "callback" && key !== "text") {
                if (!paramStr) {
                    paramStr = "?" + key + "=" + data[key];
                } else {
                    paramStr += "&" + key + "=" + data[key];
                }
            }
        }
        return paramStr;
    };

    var _uploadFiles = function (files, recordBlob) {

        // Create a formdata object and add the files
        var data = new FormData();
        if (files) {
            $.each(files, function (key, value)
            {
                data.append(key, value);
            });
        } else {
            data.append('WebRecording', recordBlob);
        }

        //Send the file as an ajax request
        $.ajax({
            url: 'UploadServlet' + _parameterString,
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data, textStatus, jqXHR)
            {
                //CustomModalDialog.find("input[name='filLabel']").val("Done uploading " + filename);
                CustomModalDialog.modal("hide");
                if (_callbackFunction) {
                    _callbackFunction(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    };

    var _initRecording = function () {


        var onError = function (e) {
            //alert("Error " + e);
        };

        var session = {
            audio: true,
            video: false
        };


        navigator.mediaDevices.getUserMedia(session)
            .then(function (mediaStream) {
                _mediaStream = mediaStream;
                _recordRTC = RecordRTC(mediaStream, {
                    type: 'audio',
                    recorderType: StereoAudioRecorder}
                );
                CustomModalDialog.find("button[name='btStartStop']").prop("disabled", false);
            })
            .catch(function (err) {
                alert("Error " + err);
            });

        //Method below deprecated. Changed to above instead -- Oskar Kåver 2018-02-05

        /*navigator.getUserMedia(session, function (mediaStream) {
         _mediaStream=mediaStream;
         _recordRTC = RecordRTC(mediaStream,{
         type: 'audio',
         recorderType: StereoAudioRecorder}
         );
         CustomModalDialog.find("button[name='btStartStop']").prop("disabled",false);                
         }, onError);*/

    };

    var _startStopRecording = function () {
        if (!_recordingStart) {
            _recordingStart = true;
            CustomModalDialog.find("button[name='btStartStop']").text("Stop Recording");
            CustomModalDialog.find("button[name='btStartStop']").removeClass("btn-success").addClass("btn-danger");
            _recordRTC.startRecording();
        } else {
            _recordingStart = false;
            CustomModalDialog.find("button[name='btStartStop']").prop("disabled", true);
            _recordRTC.stopRecording(function (audioURL) {
                _uploadFiles(null, _recordRTC.getBlob());
                var tracks = _mediaStream.getTracks();
                tracks.forEach(function (track) {
                    track.stop();
                });
            });
        }
    };


    var _openDialog = function () {
        CustomModalDialog.off('hidden.bs.modal');
        CustomModalDialog.find(".modal-dialog").empty();
        CustomModalDialog.find(".modal-dialog").append($(Mustache.render(_viewTemplates.createPromptDialogView.result, _data)));
        CustomModalDialog.find("input[name='fileInput']").on('change', (function (event) {
            var filename = $(this).val();
            CustomModalDialog.find("input[name='filLabel']").val("Uploading " + filename)
            var files = event.target.files;
            _uploadFiles(files);
        }));

        CustomModalDialog.on('hidden.bs.modal', function () {
            if (_recordingStart) {
                _recordRTC.stopRecording()
                var tracks = _mediaStream.getTracks();
                tracks.forEach(function (track) {
                    track.stop();
                });
            }
        });

        CustomModalDialog.modal("show");


        _recordingStart = false;

        CustomModalDialog.find("button[name='btStartStop']").click(_startStopRecording);
        CustomModalDialog.find("button[name='btStartStop']").prop("disabled", true);
        _initRecording();
    };

    var _openRecordingIVR = function (data) {
        var ivrData = {
            id_Question: (data.id_Question) ? data.id_Question : 0,
            id_Survey: (data.id_Survey) ? data.id_Survey : 0,
            promptType: (data.promptName) ? data.promptName : "Question",
            fileName: data.recordingFilename
        };

        Functions.ajaxRequest("OpenRecordingIVR", ivrData, function (returnData) {
            _openDialog(data, returnData.result, returnData.recordingPhoneNum);
        });
    };


    var _playPrompt = function (data) {
        CustomModalDialog.find(".modal-dialog").empty();
        CustomModalDialog.find(".modal-dialog").append($(Mustache.render(_viewTemplates.playPromptDialogView.result, data)));
        CustomModalDialog.on('hidden.bs.modal', function () {
            CustomModalDialog.find(".modal-dialog").empty();
        });

        CustomModalDialog.modal("show");
    };

    var _eventListener = function (eventName, eventData) {
        switch (eventName) {
            case "OPEN_CREATE_PROMPT_DIALOG":
                _callbackFunction = eventData.callback;
                _parameterString = _prepareParameterString(eventData);
                _data = eventData;
                _openDialog();
                break;
            case "PLAY_PROMPT":
                _playPrompt(eventData);
                break;
        }
    };

    /**Constructor*/
    mainApplication.getEventHandler().register(_eventListener);
    Functions.multiHTMLLoad(_viewTemplates);
};