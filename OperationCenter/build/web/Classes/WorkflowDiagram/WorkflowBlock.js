/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.WorkflowDiagram.WorkflowBlock=function(parent,data,viewTemplates,positionData,blockIndex){
    var _this=this;
    var _parent=parent;
    var _$panel=null;        
    var _data=data;
    var _$endPoint;
    var _jsPlumb=parent.getJsPlumb();
    var _id;    
    var _deletedFlag=false;
    var _parameterForm;
    var _blockIndex=blockIndex;
    var _movePosX=0;
    var _movePosY=0;
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getId=function(){
        return _id;
    };
    
    this.getData=function(){
        return _data;
    };
    
    this.getIndex=function(){
        return blockIndex;
    };
    
    this.getDeletedFlag=function(){
        return _deletedFlag;
    };
    
    //Shortcut to the main application
    this.getMainApplication=function(){        
        return _parent.getParent().getObjectHandlerController().getMainApplication();
    };
    
    this.setDescription=function(description){
        _$panel.find(".workflow-block-description").text(description);
    };
    
    this.setValues=function(values){
        _data.values=values;
        if (values.description){
            _$panel.find(".workflow-block-description").text(values.description);
        }
    };
    
    this.getPositionData=function(){
        return {
            left:_$panel.css("left").replace("px",""),
            top:_$panel.css("top").replace("px",""),
            width:_$panel.css("width").replace("px",""),
            height:_$panel.css("height").replace("px","")
        };
    };
    
    var _openBlockParameterPanel=function(){                
        _parent.getParent().showBlockParameters(_this);
    };
    
    this.getNonConnectedConnectors=function(connectorLabel){
        var match=false;
        var connections=_this.getConnections();
        var returnArray=[];
        for (var i=0;i<_data.connectors.length;i++){                        
            match=false;
            for(var n=0;n<connections.length;n++){
                if (connections[n].getOverlay("label").getLabel()===_data.connectors[i].connector){
                    match=true;
                }
            }
            if (!match||connectorLabel===_data.connectors[i].connector){
                returnArray.push(_data.connectors[i].connector);
            }
        }  
        return returnArray;
    };
    
    this.getNextConnector=function(){
        var match=false;
        var connections=_this.getConnections();
        for (var i=0;i<_data.connectors.length;i++){                        
            match=false;
            for(var n=0;n<connections.length;n++){
                if (connections[n].getOverlay("label").getLabel()===_data.connectors[i].connector){
                    match=true;
                }
            }
            if (!match){
                return _data.connectors[i].connector;
            }
        }
    };
    
    
    //Get all connections connected to this block
    this.getConnections=function(){
        var _connections=_jsPlumb.getConnections();
        var returnData=[];
        for (var i=0;i<_connections.length;i++){            
            if (_connections[i].sourceId===_id){
                returnData.push(_connections[i]);
            }
        }      
        return returnData;
    };
    
    var _removeBlock=function(){           
        _jsPlumb.remove(_$panel);
        _$panel.find(".workflow-block-remove").tooltip("destroy");
        _deletedFlag=true;
        if (_parent.getParent().isBlockParametersPanelVisible()){ //Open parameter panel with dbclick, but if it is already open then change content with a single click
            _parent.getParent().showBlockParameters();
        }
    };
    
    this.changed=function(){
        _parent.get$DiagramPanel().find(".workflow-block-selected").addClass("workflow-block-changed");
        _parent.getParent().formChanged();
    };
    
    
    
    var _renderBlock=function(){
        _$panel=$(Mustache.render(viewTemplates.workflowBlockView.result,data));
        
        _id=jsPlumbUtil.uuid();

        _$panel.addClass("bg-default")
                .addClass("ui-widget-content")
                .css("position","absolute")
                .css("left",positionData.left + "px")
                .css("top",positionData.top + "px")                                      
                .attr("id",_id);
        
        if (positionData.width){
            _$panel.width(positionData.width);
        }

        if (positionData.height){
            _$panel.height(positionData.height);
        }
        
        //Set high z-index on selected block
        _$panel.mousedown(function(){ 
            _movePosX=_$panel.css("left");
            _movePosY=_$panel.css("top");
            
            if (_$panel.hasClass("workflow-block-selected")){
                return;
            }
            
            
            if (_parent.getParent().isBlockParametersPanelVisible()){ //Open parameter panel with dbclick, but if it is already open then change content with a single click
                _openBlockParameterPanel();
            }
            
            
            parent.get$DiagramPanel().find(".workflow-block").css("z-index",5);
            parent.resetSelection();
            
            _$panel.css("z-index",10).addClass("workflow-block-selected");
        });
    
        _$panel.mouseup(function(){
            if (!(_movePosX===_$panel.css("left")&&
            _movePosY===_$panel.css("top"))){
                _this.changed();
            }
        });

    
    
        parent.get$DiagramPanel().append(_$panel);

        //Declare tooltip functionality to remove an add connector
        _$panel.find(".workflow-block-add-connector").tooltip({
            placement: 'top',
            animated:"fade",
            title: 'Add New Connection',
            container: 'body'            
        });
        
        //
        _$panel.find(".workflow-block-remove").tooltip({
            placement: "top",
            animated:"fade",
            title: "Remove Block",
            container: 'body'            
        });

        //Set close event - Open parameters
        _$panel.find(".workflow-block-remove").click(_removeBlock);

        //Set double click event - Open parameters
        _$panel.dblclick(_openBlockParameterPanel);

        _$panel.resizable({        
            resize: function(event, ui) {
                if (ui.helper){
                    _jsPlumb.revalidate(ui.helper);
                }
            },
            handles: "all",
            minHeight: 40,
            minWidth: 120,
            grid:10
        });

        _jsPlumb.draggable(_$panel,{
            filter:".ui-resizable-handle",
            containment: 'parent',
            grid: [10, 10]            
        });    


        
            
        _jsPlumb.makeSource(_$panel, {                        
                filter:".workflow-block-add-connector",
                anchor:"Continuous",
                maxConnections:_data.connectors.length||0,            
                onMaxConnections: function (info, e) {
                    bootbox.alert("Maximum connections reached");
                },
                connectorStyle: { stroke: "#5c96bc", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 }                        
            });

        _jsPlumb.makeTarget(_$panel, {
            dropOptions:{ hoverClass:"dragHover" },
            anchor:"AutoDefault",
            allowLoopback: true
        });
   };
       
    
   
    /***
     * 
     * Constructor
     * 
     */
    
    if (!_data.values){
        _data.values={};
    }
    _renderBlock();
    
};