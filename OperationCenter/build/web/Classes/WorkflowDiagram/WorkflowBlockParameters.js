/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.WorkflowDiagram.WorkflowBlockParameters=function(parent,data,$blockParametersPanel,id_Block){
    var _this=this;
    var _parent=parent;
    var _$panel=$blockParametersPanel;
    var _data=data;
    var _name=data.values.name;    
    var _form;    
    
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    
    
    
    /***
     * 
     * Constructor
     * 
     */
    
    
    
    
    
    //Set a default form with description as the only parameter
    
    var formContent={content:[
               {type:"SECTION",                        
                content:[
                    {
                        type:"TEXTAREA",
                        name:"description",
                        displayName:"Description",
                        defaultValue:"",
                        listGridName:"Description",
                        helpText:"",
                        mandatory:0
                    }
                ],
                title:"General",                        
                collapsable:0,
                collapsed:0,
                solid:0,
                colorSchema:"box-primary"
            }
        ]
    };
    
    //Render form
    
    if (data.form&&data.form.content){
        //Put description section as the first element
        formContent.content=formContent.content.concat(data.form.content);        
    } 
        
    _form=new Classes.DynamicForm.Form(parent.getMainApplication(),formContent,true);
    _form.renderForm(function(){
        _$panel.find(".dynamic-form").remove(); 
        _$panel.append(_form.get$Panel());
        _form.setValues(data.values);
        //_form.eventChanged=_formChanged;        
    });        
    
};