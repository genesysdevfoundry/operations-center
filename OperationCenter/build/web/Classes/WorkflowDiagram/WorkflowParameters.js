/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

var Workflow={};
Workflow.workflowParameters=[{ 
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Workflow",
        type:"TEXT"
    },{       
        name:"description",
        displayName:"Description",
        mandatory:false,
        defaultValue:"",
        type:"TEXTAREA"
}];