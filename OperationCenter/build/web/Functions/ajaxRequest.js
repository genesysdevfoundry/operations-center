/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Functions.ajaxRequest=function(service,parameters,callBack,ignoreErrors){
    var isHierarchic=false;
    var preparedParameters;
    if (!parameters){
        parameters={};
    }
    //Check if Parameters are hierarchic
    /*for (var key in parameters){
        if (parameters[key] instanceof Object||parameters[key] instanceof Array){
            isHierarchic=true;
            break;
        }
    }
    if (isHierarchic){
      
    }    
    */
   //Send all parameers as json from the beginning....
    var jsonData=JSON.stringify(parameters);
    preparedParameters={jsonData:jsonData};
    
    //Show loading dialog after 1/2 seconds
    var timeoutHandler=setTimeout(function(){
        waitingDialog.show()
    }, 500);
    
    
    $.ajax({
        url:"Services/" + service,
        dataType: "json",
        data:preparedParameters,
        type:"POST",
        success:function(jsonData){            
            clearTimeout(timeoutHandler);
            waitingDialog.hide();
            if (jsonData&&jsonData.Error){
                if (ignoreErrors){   ///Only used when testing scripts
                    if (callBack){
                        callBack(jsonData);
                    }
                    return;
                }
                if (jsonData.Error.fullErrorDescription.indexOf("NO_LOGIN_SESSION")>=0){
                    if (global.userName){
                        //Open session expired dialog
                        var loginDialog=new Classes.MainModule.LoginDialog(service,parameters,callBack);
                    } else{
                        window.location.href="index.html";
                    }
                } else if (jsonData.Error.fullErrorDescription.indexOf("CLIENT_MESSAGE")>=0){
                    var message=jsonData.Error.fullErrorDescription;
                    message=message.substring(message.indexOf("CLIENT_MESSAGE")+15,message.indexOf("]]"));
                    bootbox.alert(message);                    
                } else {
                    bootbox.alert(jsonData.Error.fullErrorDescription); 
                }
            } else if (jsonData.infoMessage){
                //Message dialog to the user
                bootbox.alert(jsonData.infoMessage);
            } else {
                if (callBack){
                    callBack(jsonData);
                }
            }
        },
        error:function(jqXHR, exception){
            clearTimeout(timeoutHandler);
            waitingDialog.hide();
             var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested service not available. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                bootbox.alert("Error: " + msg);
        }
    });    
};