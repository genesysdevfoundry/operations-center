/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Functions.multiHTMLLoad=function(htmlContents,callbackFunction){
    var _loadCounter=0;
    
    var _loadContent=function(content){
        _loadCounter++;
        
        $.get(content.url + "?" + cacheId, function(data) {            
            content.result=data;            
        }).always(function(){
            _loadCounter--;
            if (!_loadCounter){
                if (callbackFunction){
                   callbackFunction();
                }
            }
        });    
    };    
    
    for (var key in  htmlContents) {
        if (htmlContents[key].url){                        
            _loadContent(htmlContents[key]);
        }        
    }
};