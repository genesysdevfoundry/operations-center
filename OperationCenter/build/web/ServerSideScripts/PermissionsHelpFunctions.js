/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */
 
//Recursive function
var applyPermissionsToFormData=function(data,setHidden){

    if (session.isAdmin){
        return;
    }
    
    if (data instanceof Array){
        for (var i=0;i<data.length;i++){
            applyPermissionsToFormData(data[i],setHidden);
        }
    } else if (data instanceof Object){
        var data;
        
        //Apply permissions
        if (data.applyPermissions&&!setHidden){
            var read=false;
            var readWrite=false;        
            if (data.accessPermissions&&data.accessPermissions.length){
                for (var i=0;i<data.accessPermissions.length;i++){        

                    if (("," + session.memberAccessGroups + ",").indexOf("," + data.accessPermissions[i].accessGroup + ",")>=0){
                        if (data.accessPermissions[i].permission==="READ_WRITE"){
                            readWrite=true;
                            read=true;
                            break;
                        } else {
                            read=true;
                        }
                    }
                }            
            }
            if (data.accessGroups&&data.accessGroups.length){ //Select boxes and columns
                for (var i=0;i<data.accessGroups.length;i++){        
                    if (("," + session.memberAccessGroups + ",").indexOf("," + data.accessGroups[i] + ",")>=0){
                        readWrite=true;
                        read=true;                        
                    }
                }            
            }
            
            if(!readWrite&&!read){
                data.hidden=true;
            } 
            if (!readWrite&&read){
                data.readOnlyMode="READ_ONLY";
            }
        } else {
            if (setHidden){
                data.hidden=true;
            }
        }
        
        if (data.content){
            applyPermissionsToFormData(data.content,data.hidden?true:false);
        }        
    }  
};