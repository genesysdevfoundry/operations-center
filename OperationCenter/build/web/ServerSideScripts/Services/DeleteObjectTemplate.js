/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}

executeSQLUpdate("DELETE FROM [ObjectRelation] WHERE ParentObjectType='OBJECT_TEMPLATE' AND ParentObjectId={{#input.id_ObjectTemplate}}");

executeSQLUpdate("delete from objectTemplate where id_ObjectTemplate={{#input.id_ObjectTemplate}}");



