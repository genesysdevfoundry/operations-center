/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}

executeSQLUpdate("Delete From script Where id_Script={{#input.id_Script}} and Id_Tenant={{session.tenantDBID}}");

