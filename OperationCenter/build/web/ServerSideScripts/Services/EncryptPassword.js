/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

writeToLog("Encrypting password");
var encryptedPassword = serviceHandler.encryptPassword(input.password);
writeToLog("Encrypted password is: " + encryptedPassword);
output = encryptedPassword;