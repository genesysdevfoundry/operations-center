/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

/* 
 *This script is executed when a form button is pressed
 *
 */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}



if (input.actionScriptId){
    var scriptId=parseInt(input.actionScriptId);
    serviceHandler.executeService(scriptId);
}

        

        