/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

var scriptIds=input.scripts.split(",");
var newOutput={};
for(var _index=0;_index<scriptIds.length;_index++){    
   
    //Execute custom script
    if (scriptIds[_index].indexOf("ID_")===0){
        var scriptId=parseInt(scriptIds[_index].replace("ID_",""));
        try{
            serviceHandler.executeService(scriptId);
        } catch (ex){
            output=ex.message;
        }
        newOutput[scriptIds[_index]]=output;
    }
    
    //Get object list based on a form id
    if (scriptIds[_index].indexOf("NAME_")===0){
        var scriptName=scriptIds[_index].replace("NAME_","");
        var tmpArray=scriptName.split("|");
        if (tmpArray.length>1){
            input.scriptParameter=tmpArray[1];
        } else {
            input.scriptParameter=null;
        }
        try{
            serviceHandler.executeService(tmpArray[0]);
        } catch (ex){
            output=ex.message;
        }
        newOutput[scriptIds[_index]]=output;        
    }        
}

output=newOutput;