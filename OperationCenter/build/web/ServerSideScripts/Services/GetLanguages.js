/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

output=addJSONDataToResult(executeSQLRecordset("\
    SELECT [Name],[JSONData] \
    FROM [Object] \
    WHERE id_ObjectTemplate=(select top 1 id_ObjectTemplate from ObjectTemplate where name='Language' and Id_Tenant={{#session.tenantDBID}}) and deleted is null ORDER BY Name"));