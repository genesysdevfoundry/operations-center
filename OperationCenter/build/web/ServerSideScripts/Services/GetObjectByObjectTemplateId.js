/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

var object=executeSQLRecord("select top 1 * from [Object] where id_ObjectTemplate={{input.id_ObjectTemplate}} and Id_Tenant={{session.tenantDBID}}");

if (!object.id_Object){
    //Insert new record
    executeSQLUpdate("INSERT INTO [dbo].[Object] ([Id_Tenant],[id_ObjectTemplate],[Created],[CreatedBy],[Changed],[ChangedBy],[JSONData]) \
    VALUES ({{#session.tenantDBID}},{{#input.id_ObjectTemplate}},getdate(),'{{session.userName}}',getdate(),'{{session.userName}}',\
           '{}')");
    input.id_Object=getIdentity();
} else {
    input.id_Object=object.id_Object;
}

serviceHandler.executeService("GetObject");
