/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//Return all scripts in a lists
output=executeSQLRecordset("select  id_ObjectTemplate,Name,Type, description,menuName,menuGroup, convert(char(19),Created,120) as [Created], CreatedBy, convert(char(19),Changed,120) as [Changed],[ChangedBy] \
    ,[Type],[System],[TransactionListName] \
     from ObjectTemplate where Id_Tenant={{#session.tenantDBID}} and (isnull(hidden,0)=0 or '{{session.sysAdmin}}'='true') ORDER BY Name");