/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

 //Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}


output=executeSQLRecord("SELECT [Id_ScriptHistory],[Id_Script],convert(char(19),[DateStamp],120) as [DateStamp],[UserName],[Name],[Description],[JSCode] \
            FROM [ScriptHistory] \
            WHERE Id_ScriptHistory={{#input.id_ScriptHistory}} \
            ORDER BY datestamp desc");
 