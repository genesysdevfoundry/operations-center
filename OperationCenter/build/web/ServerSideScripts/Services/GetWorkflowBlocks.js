/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */


//Get Objecttemplate connected to Object (Include the form data)
var objectTemplate=executeSQLRecord("select * from [ObjectTemplate] where id_ObjectTemplate={{#input.id_ObjectTemplate}} and Id_Tenant={{#session.tenantDBID}}");

var workflowBlocks=JSON.parse(objectTemplate.workflowBlocks);

var blockString="";

for (var i=0;i<workflowBlocks.length;i++){
    blockString+=(i?",":"") + workflowBlocks[i]._workflowBlock;
}

output=addJSONDataToResult(executeSQLRecordset("SELECT [Id_Object],[Id_ObjectTemplate],[Id_Department],[Name],[Description],[JSONData] \
            FROM [Object]\
            WHERE id_ObjectTemplate=(select top 1 id_ObjectTemplate from ObjectTemplate where Id_Tenant={{#session.tenantDBID}} and name='Workflow Blocks') and id_Object in ({{blockString}}) order by Name\
"));


var formId;
for (var i=0;i<output.length;i++){
    formId=output[i].form;
    if (formId){
        output[i].form=addJSONDataToResult(executeSQLRecord("SELECT name,JSONData from [form] where id_Form={{#formId}}"));
    }    
}