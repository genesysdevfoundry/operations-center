/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Return all scripts in a list
output=executeSQLRecordset("select  name as [text], name as [id] from MenuGroup where Id_Tenant={{#session.tenantDBID}} order by Name",true);
