/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

output=executeSQLRecordset("select  id_ObjectTemplate as [id],name as [text] from ObjectTemplate where Id_Tenant={{#session.tenantDBID}} and type!='PROMPT_LIST' and (isnull(hidden,0)=0 or '{{session.sysAdmin}}'='true') order by 2");