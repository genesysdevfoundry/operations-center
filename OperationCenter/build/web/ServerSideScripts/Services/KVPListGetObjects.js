/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Return all scripts in a list
var id_ObjectTemplate;

if (parseInt(input.scriptParameter) + ""!==input.scriptParameter){  //Check is Number
    var objectTemplate=executeSQLRecord("select id_ObjectTemplate from [ObjectTemplate] where name='{{input.scriptParameter}}' and Id_Tenant={{session.tenantDBID}}");
    if (objectTemplate&&objectTemplate.id_ObjectTemplate){
        id_ObjectTemplate=objectTemplate.id_ObjectTemplate;
    }
} else {
    id_ObjectTemplate=input.scriptParameter;    
}


output=executeSQLRecordset("select  id_Object as [id],case when o.id_Department={{#session.defaultDepartment}} then null else d.name end as [group], o.name as [text] \
    from [object] o \
    left join [Department] d on o.id_Department=d.id_Department \
    where o.Id_Tenant={{#session.tenantDBID}} and o.id_ObjectTemplate={{#id_ObjectTemplate}} and (o.id_Department in ({{session.departmentsReadOnly}}) or '{{session.isAdmin}}'='true') and deleted is null order by 2,3",true);


