/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Return all prompts in a list

output=executeSQLRecordset("select  id_Object as [id],case when o.id_Department={{#session.defaultDepartment}} then null else d.name end as [group], o.name as [text] \
    from [object] o \
    inner join [objectTemplate] ot on o.id_ObjectTemplate=ot.id_ObjectTemplate \
    left join [Department] d on o.id_Department=d.id_Department \
    where o.Id_Tenant={{#session.tenantDBID}} and ot.type='PROMPT_LIST' and (o.id_Department in ({{session.departmentsReadOnly}}) or '{{session.isAdmin}}'='true') and deleted is null order by 2,3",true);


