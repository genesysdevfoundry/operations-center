/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

var tmpFilename=parseInt(Math.random()*100000) + ".mp3";

//Run ALawConverter bat file 
serviceHandler.getHandler("com.nordicoffice.operationcenter.AudioFileConverter").convertAudioFile(
        application.audio.AudioFileStorage + "/" + input.filename,
        application.audio.TemporaryFiles_File + "/" + tmpFilename,        
        application.audio.MP3Converter);


//Delete files older then five minutes from temporary folder
serviceHandler.getHandler("com.nordicoffice.operationcenter.FileHandler").deleteOldFiles(application.audio.TemporaryFiles_File,5);


output.tmpFilename=application.audio.TemporaryFiles_HTTP + "/" +tmpFilename;