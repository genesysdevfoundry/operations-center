/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("{{245}}|Access denied");    
}



var extraData;

if (input.id_Script==0){
    extraData={
        createdBy:"'" + session.userName + "'",
        created:"getdate()",
        changedBy:"'" + session.userName + "'",
        changed:"getdate()",
        Id_Tenant:session.tenantDBID
    };
} else {
    extraData={
        changedBy:"'" + session.userName + "'",
        changed:"getdate()"
    };    
}

var id_Script=updateOrInsertRecord("Script",input,"id_Script",extraData,"");

//Insert into history table
executeSQLUpdate("insert into ScriptHistory (id_Script,DateSTamp,UserName,Name,Description,JSCode) VALUES \
                ({{#id_Script}},getdate(),'{{session.userName}}','{{input.name}}','{{input.description}}','{{input.jSCode}}') ");

if (input.type==="WEB_SERVICE"){
    serviceHandler.addService(input.name,null,input.jSCode);
} else {
    serviceHandler.addService(null,parseInt(id_Script),input.jSCode);
}

output.id_Script=id_Script;

//Also include script history
output.history=executeSQLRecordset(
        "SELECT [Id_ScriptHistory],[Id_Script],convert(char(19),[DateStamp],120) as [DateStamp],[UserName],[Name],[Description] \
            FROM [ScriptHistory] \
            WHERE id_script={{#input.id_Script}} \
            ORDER BY datestamp desc"
,true);

if (application.instanceURLs){
    writeToLog("Sending syncService ping to other OC intances configured in properties file.");
    var instanceURLs=application.instanceURLs.split(",");
    for (var i=0;i<instanceURLs.length;i++){
        writeToLog("Sending to instance " + instanceURLs[i]);
        var returnValue=httpGet(instanceURLs[i] + "/Services/SyncService?id_Script=" + id_Script);
        writeToLog("Return Value: " + JSON.stringify(returnValue));
    }    
}
