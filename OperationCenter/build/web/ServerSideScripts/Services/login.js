/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

var confServiceHandler;

if (!application.database){
    writeToLog("No applicationOption.databse config, try to run onLoad.js");
    serviceHandler.executeScript("OnLoad.js");
}

try {
    confServiceHandler = serviceHandler.getHandler("com.nordicoffice.operationcenter.ConfServerHandler");
    confServiceHandler.connect(
            input.userName,
            input.password,
            application.configAppName,
            application.configServerName,
            parseInt(application.configServerPort)
            );


    confServiceHandler.close();
    writeToLog("User " + input.userName + " is logged in");

    confServiceHandler.connect(
            application.userName,
            application.password,
            application.configAppName,
            application.configServerName,
            parseInt(application.configServerPort),
            true
            );

    var person = confServiceHandler.getPerson({name: input.userName});
    session.tenantDBID = person.tenantDBID;
    session.userName = person.name;
    session.password = input.password;
    session.firstName = person.firstName;
    session.lastName = person.lastName;

    writeToLog("session.userName : " + session.userName);
    writeToLog("session.tenantDBID : " + session.tenantDBID);
    memberAccessGroups = confServiceHandler.getAccessGroupsFromPerson({"name": session.userName, "tenantDBID": session.tenantDBID});
    allAccessGroups = confServiceHandler.getAllAccessGroups(parseInt(session.tenantDBID));

    var objectTemplates = {};
    var departments = "0";
    var departmentsReadOnly = "0";
    var contextParameters = serviceHandler.getContextParameters();
    session.isAdmin = false;
    session.sysAdmin=false;
    writeToLog(" Access groups of person: " + memberAccessGroups);
    var tmpDepartments = 0;

    //Get permissions to departments and object templates
    if (memberAccessGroups) {
        var memberStr = "";
        var memberStr2 = "";  //Without '
        for (var i = 0; i < memberAccessGroups.length; i++) {
            memberStr += ((i) ? "," : "") + "'" + memberAccessGroups[i] + "'";
            memberStr2 += ((i) ? "," : "") +  memberAccessGroups[i];
            
            if (application.general.adminAccessGroup === memberAccessGroups[i]) {
                session.isAdmin = true;
                session.sysAdmin=input.sysAdmin?true:false;
                session.hideAdminGroup = false;
            }
            if (application.general.lightAdminAccessGroup === memberAccessGroups[i]) {
                if (!session.isAdmin){
                    session.hideAdminGroup = true;
                }
                session.isAdmin = true;                
            }
        }
        var permissions = executeSQLRecordset("select [ObjectType],[ObjectId],[PermissionType] from [Permission] where Id_Tenant={{#session.tenantDBID}} and AccessGroup in (" + memberStr + ")");
        for (var i = 0; i < permissions.length; i++) {
            if (permissions[i].objectType === "OBJECT_TEMPLATE") {                
                var objectTemplateIndex="OT_ID_" + permissions[i].objectId;
                if (objectTemplates[objectTemplateIndex]){
                    var permissionString="R";
                    if (objectTemplates[objectTemplateIndex].indexOf("A")>0||permissions[i].permissionType.indexOf("A")>0){
                        permissionString+="A";
                    }
                    if (objectTemplates[objectTemplateIndex].indexOf("D")>0||permissions[i].permissionType.indexOf("D")>0){
                        permissionString+="D";
                    }
                    if (objectTemplates[objectTemplateIndex].indexOf("E")>0||permissions[i].permissionType.indexOf("E")>0){
                        permissionString+="E";
                    }
                    objectTemplates[objectTemplateIndex]=permissionString;
                    
                }  else {
                    objectTemplates[objectTemplateIndex]=permissions[i].permissionType;
                }             
            }

            if (permissions[i].objectType === "DEPARTMENT"&&permissions[i].permissionType==="READ_WRITE") {                
                departments += "," + permissions[i].objectId;
                departmentsReadOnly += "," + permissions[i].objectId;
                tmpDepartments++;
                defaultDepartment = permissions[i].objectId;
            }
            
            if (permissions[i].objectType === "DEPARTMENT"&&permissions[i].permissionType==="READ_ONLY") {                
                departmentsReadOnly += "," + permissions[i].objectId;                
            }
        }
    }

    if (tmpDepartments === 1) {
        session.defaultDepartment = defaultDepartment;
    } else {
        session.defaultDepartment = 0;
    }

    

    if (tmpDepartments === 0 && !session.isAdmin) {
        sendClientMessage("You do not have access to any department.");
    }

    session.departments = departments;
    session.departmentsReadOnly=departmentsReadOnly;
    session.objectTemplatePermissions = objectTemplates;
    session.memberAccessGroups=memberStr2;


    var ocUser = null;
    try {
        ocUser = confServiceHandler.getPerson({"name": input.userName + "_OC"});
        writeToLog("OC user fetched: " + ocUser.toString());

    } catch (err) {
        writeToLog("No OC user available, will create one.");

        try {
            //Create admin user
            var inputData = {};
            inputData.tenantDBID = session.tenantDBID;
            inputData.name = input.userName + "_OC";
            inputData.employeeId = input.userName + "_OC";
            inputData.password = serviceHandler.hashPerson(input.userName + "_OC");
            inputData.isAgent = false;
            if (application.ocUserFolderDBIDs && application.ocUserFolderDBIDs[session.tenantDBID.toString()]) {
                inputData.folderDBID = application.ocUserFolderDBIDs[session.tenantDBID.toString()];
            }
            ocUser = confServiceHandler.createAgent(inputData);

            //Adds agent to access group
            confServiceHandler.addPersonToAccessGroup(
                    {
                        "tenantDBID": session.tenantDBID,
                        "accessGroup": application.general.ocUserAccessGroup,
                        "name": ocUser.getUserName()
                    }
            );
            writeToLog("OC user " + ocUser.getUserName() + " created.");
        } catch (e) {
            writeToLog("Something failed while creating OC user. " + e.message);
        }
    }

    if (ocUser) {
        session.userNameOC =  input.userName + "_OC";
        session.passwordOC = serviceHandler.hashPerson(input.userName + "_OC");
    }

    serviceHandler.setLoggingUserName(input.userName);

    output = "SUCCESS";
} catch (err) {
    writeToLog("Error logging in " + input.userName + ": " + err.message);
    if (err.message.indexOf("CLIENT_MESSAGE")>=0){
        throw err;
    }
    
    
    session.userName = "";
    output = "FAILURE";
}
