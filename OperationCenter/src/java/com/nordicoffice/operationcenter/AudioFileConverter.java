/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.operationcenter;
import com.nordicoffice.backendengine.Handler;
import com.nordicoffice.backendengine.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Date;

public class AudioFileConverter implements Handler{    
       
    
    
    /***
     * Generic audio file conversion
     * @param sourceFile
     * @param destFile
     * @param convertScript
     * @return 
     */
    public boolean convertAudioFile(String sourceFile,String destFile,String convertScript){            
        try {            
            String commandStr=convertScript + " \"" + sourceFile + "\" \"" + destFile + "\"";
            
            Log.getLogger().info(commandStr);
            
            Process process = Runtime.getRuntime().exec(commandStr);
            BufferedReader in = new BufferedReader(   
                                new InputStreamReader(process.getInputStream()));   
            
            String line="";
            
            while ((line = in.readLine()) != null) {
                Log.getLogger().info(line);
            }

            process.waitFor();
            
        } catch (Exception e) {
            Log.getLogger().error("Error in convertAudioFile",e);
            return false;
        }
        return true;
    }   

    @Override
    public void close() {        
    }    
}
