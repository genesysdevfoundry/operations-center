/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.operationcenter;

public class ConfServerConnectionException extends Exception {
    public ConfServerConnectionException() {
    }

    public ConfServerConnectionException(String message) {
        super(message);
    }

    public ConfServerConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfServerConnectionException(Throwable cause) {
        super(cause);
    }

    public ConfServerConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
