/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.operationcenter;

import com.genesyslab.platform.applicationblocks.com.CfgObject;
import com.genesyslab.platform.applicationblocks.com.ConfServiceFactory;
import com.genesyslab.platform.applicationblocks.com.ConfigException;

import com.genesyslab.platform.applicationblocks.com.IConfService;
import com.genesyslab.platform.applicationblocks.com.objects.CfgAccessGroup;
import com.genesyslab.platform.applicationblocks.com.objects.CfgAgentGroup;
import com.genesyslab.platform.applicationblocks.com.objects.CfgAgentInfo;
import com.genesyslab.platform.applicationblocks.com.objects.CfgAgentLogin;
import com.genesyslab.platform.applicationblocks.com.objects.CfgAgentLoginInfo;
import com.genesyslab.platform.applicationblocks.com.objects.CfgApplication;
import com.genesyslab.platform.applicationblocks.com.objects.CfgCallingList;
import com.genesyslab.platform.applicationblocks.com.objects.CfgDN;
import com.genesyslab.platform.applicationblocks.com.objects.CfgDNGroup;
import com.genesyslab.platform.applicationblocks.com.objects.CfgDNInfo;
import com.genesyslab.platform.applicationblocks.com.objects.CfgField;
import com.genesyslab.platform.applicationblocks.com.objects.CfgFolder;
import com.genesyslab.platform.applicationblocks.com.objects.CfgFormat;
import com.genesyslab.platform.applicationblocks.com.objects.CfgGroup;
import com.genesyslab.platform.applicationblocks.com.objects.CfgID;
import com.genesyslab.platform.applicationblocks.com.objects.CfgObjectID;
import com.genesyslab.platform.applicationblocks.com.objects.CfgOwnerID;
import com.genesyslab.platform.applicationblocks.com.objects.CfgPerson;
import com.genesyslab.platform.applicationblocks.com.objects.CfgPlace;
import com.genesyslab.platform.applicationblocks.com.objects.CfgRole;
import com.genesyslab.platform.applicationblocks.com.objects.CfgRoleMember;
import com.genesyslab.platform.applicationblocks.com.objects.CfgScript;
import com.genesyslab.platform.applicationblocks.com.objects.CfgSkill;
import com.genesyslab.platform.applicationblocks.com.objects.CfgSkillLevel;
import com.genesyslab.platform.applicationblocks.com.objects.CfgTableAccess;
import com.genesyslab.platform.applicationblocks.com.objects.CfgTransaction;
import com.genesyslab.platform.applicationblocks.com.objects.CfgTreatment;
import com.genesyslab.platform.applicationblocks.com.queries.CfgAccessGroupQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgAgentGroupQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgAgentLoginQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgApplicationQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgCallingListQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgDNGroupQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgDNQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgFolderQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgFormatQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgPersonQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgPlaceQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgRoleQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgScriptQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgSkillQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgTableAccessQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgTransactionQuery;
import com.genesyslab.platform.applicationblocks.com.queries.CfgTreatmentQuery;
import com.genesyslab.platform.apptemplate.configuration.GCOMApplicationConfiguration;
import com.genesyslab.platform.commons.collections.KeyValueCollection;
import com.genesyslab.platform.commons.collections.KeyValuePair;

import com.genesyslab.platform.commons.protocol.ChannelState;
import com.genesyslab.platform.commons.protocol.Endpoint;

import com.genesyslab.platform.configuration.protocol.ConfServerProtocol;
import com.genesyslab.platform.configuration.protocol.types.CfgAppType;
import com.genesyslab.platform.configuration.protocol.types.CfgDNType;
import com.genesyslab.platform.configuration.protocol.types.CfgDataType;
import com.genesyslab.platform.configuration.protocol.types.CfgFlag;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectState;
import com.genesyslab.platform.configuration.protocol.types.CfgObjectType;
import com.genesyslab.platform.configuration.protocol.types.CfgPermissions;
import com.genesyslab.platform.configuration.protocol.types.CfgRecActionCode;
import com.genesyslab.platform.configuration.protocol.types.CfgRouteType;
import com.genesyslab.platform.configuration.protocol.types.CfgScriptType;
import com.genesyslab.platform.configuration.protocol.types.CfgTableType;
import com.genesyslab.platform.configuration.protocol.types.CfgTransactionType;
import com.genesyslab.platform.configuration.protocol.types.GctiCallState;
import com.nordicoffice.backendengine.EncryptDecrypt;
import com.nordicoffice.backendengine.Handler;
import com.nordicoffice.backendengine.Log;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class ConfServerHandler implements Handler {

    ConfServerProtocol confProtocol;
    IConfService confService;

    /**
     * Connect to ConfigServer with clear text password.
     *
     * @param user
     * @param password
     * @param clientName
     * @param confServerHost
     * @param confServerPort
     * @throws ConfServerConnectionException
     */
    public void connect(String user, String password, String clientName, String confServerHost, int confServerPort) throws ConfServerConnectionException {
        Log.getLogger().info("Connecting ConfigServer without encrypted password.");
        connect(user, password, clientName, confServerHost, confServerPort, false);
    }

    /**
     * Connect to ConfigServer with possibility to connect with encrypted
     * password.
     *
     * @param user
     * @param password
     * @param clientName
     * @param confServerHost
     * @param confServerPort
     * @param passwordEncrypted
     * @throws ConfServerConnectionException
     */
    public void connect(String user, String password, String clientName, String confServerHost, int confServerPort, boolean passwordEncrypted) throws ConfServerConnectionException {
        if (passwordEncrypted) {
            Log.getLogger().info("Connecting ConfigServer with encrypted password.");
            setupConnection("session" + Math.round(Math.random() * 1000), user, EncryptDecrypt.decryptPassword(password), clientName, confServerHost, confServerPort);
        } else {
            setupConnection("session" + Math.round(Math.random() * 1000), user, password, clientName, confServerHost, confServerPort);
        }
    }

    @SuppressWarnings("ThrowableResultIgnored")
    private void setupConnection1(String endpointName, String user, String password, String clientName, String confServerHost, int confServerPort) throws ConfServerConnectionException {
        try {
            if (confProtocol != null) {
                if (confProtocol.getState() == ChannelState.Opened || confProtocol.getState() == ChannelState.Opening) {
                    this.close();
                }
            }

            if (endpointName == null) {
                confProtocol = new ConfServerProtocol(new Endpoint(confServerHost, confServerPort));
            } else {
                confProtocol = new ConfServerProtocol(new Endpoint(endpointName, confServerHost, confServerPort));
            }

            confProtocol.setClientApplicationType(CfgAppType.CFGThirdPartyApp.asInteger());
            confProtocol.setClientName(clientName);
            confProtocol.setUserName(user);
            confProtocol.setUserPassword(password);

            //confService = ConfServiceFactory.createConfService(confProtocol);
            confService = ConfServiceFactory.createConfService(confProtocol, true);

            confProtocol.open();

            Log.getLogger().info("connection to config server opened");

        } catch (Exception ex) {
            close();
            Log.getLogger().error(ex.toString());
            throw new ConfServerConnectionException("Connection to configuration server failed", ex);
        }
    }

    private static volatile String lastWorkingConfServerHost;

    private void setupConnection(String endpointName, String user, String password, String clientName, String confServerHost, int confServerPort) throws ConfServerConnectionException {

        Log.getLogger().info("configServerHost (list) = '" + confServerHost + "'");

        List<String> hosts = Arrays.asList(confServerHost.replaceAll("\\s", "").split(","));

        Log.getLogger().debug("last working config server host = '" + lastWorkingConfServerHost + "'");
        Log.getLogger().debug("hosts in original order: " + hosts);

        if (hosts.size() == 2 && hosts.get(1).equals(lastWorkingConfServerHost)) {
            Collections.swap(hosts, 0, 1);
            Log.getLogger().debug("hosts after reordering: " + hosts);
        }

        try {
            Log.getLogger().info("Trying to connect using first host...");
            setupConnection1(endpointName, user, password, clientName, hosts.get(0), confServerPort);
            lastWorkingConfServerHost = hosts.get(0);
            Log.getLogger().debug("Connection to first host succeeded.");
        } catch (Exception e0) {
            Log.getLogger().error("Exception occured for first host.", e0);
            if (hosts.size() == 2) {
                try {
                    Log.getLogger().info("Trying to connect using second host...");
                    setupConnection1(endpointName, user, password, clientName, hosts.get(1), confServerPort);
                    lastWorkingConfServerHost = hosts.get(1);
                    Log.getLogger().debug("Connection to second host succeeded.");
                } catch (Exception e1) {
                    Log.getLogger().error("Exception occured for second host.", e1);
                    throw e1;
                }
            } else {
                throw e0;
            }
        }
    }

    public IConfService getConfServer() {
        return confService;
    }

    /**
     * Return GCOM application object for given application name
     *
     * @param genesysApplicationName
     * @return
     * @throws Exception
     */
    public GCOMApplicationConfiguration getGCOMApplicationConfig(String genesysApplicationName) throws Exception {
        Log.getLogger().info("Fetching GCOM object for application " + genesysApplicationName);
        CfgApplication cfgApplicationObject = confService.retrieveObject(CfgApplication.class, new CfgApplicationQuery(genesysApplicationName));
        return new GCOMApplicationConfiguration(cfgApplicationObject);
    }

    /**
     * Returns given argument from map. If mandatory flag is set to true and
     * argument is missing, an error is thrown. If it is set to false, null is
     * returned.
     *
     * @param input
     * @param argument
     * @param mandatory
     * @return
     * @throws com.nordicoffice.operationcenter.ConfServerHandler.InputException
     */
    private String getArgumentFromMap(Map<?, ?> input, String argument, boolean mandatory) throws InputException {
        if (input.containsKey(argument) && input.get(argument) != null) {
            return input.get(argument).toString();
        } else if (mandatory) {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            throw new InputException("Missing mandatory input parameter '" + argument + "' in method "
                    + stackTraceElements[2].getMethodName() + "(" + stackTraceElements[2].getLineNumber() + ").");
        } else {
            return null;
        }
    }

    public class InputException extends Exception {

        public InputException(String message) {
            super(message);
        }

        public InputException() {
            super();
        }
    }

    public class DontExistException extends Exception {

        public DontExistException(String message) {
            super(message);
        }

        public DontExistException() {
            super();
        }
    }

    /**
     * Returns all options of application from input
     *
     * @param genesysApplicationName
     * @return a Map of Maps. Genesys sections with objects
     * @throws Exception
     */
    public Map<String, Map<String, String>> getApplicationOptions(String genesysApplicationName) throws Exception {
        Map<String, Map<String, String>> retVal = new HashMap();
        //Read our application options
        CfgApplication cfgApplicationObject = confService.retrieveObject(CfgApplication.class, new CfgApplicationQuery(genesysApplicationName));
        GCOMApplicationConfiguration appConfig = new GCOMApplicationConfiguration(cfgApplicationObject);
        Log.getLogger().info(appConfig.getOptions());

        String catalinaBase = System.getProperty("catalina.base");

        for (Iterator<?> iteratorSection = appConfig.getOptions().iterator(); iteratorSection.hasNext();) {

            Map<String, String> sectionMap = new HashMap();
            KeyValuePair sectionKVC = (KeyValuePair) iteratorSection.next();

            for (Iterator<?> iteratorOptions = sectionKVC.getTKVValue().iterator(); iteratorOptions.hasNext();) {
                KeyValuePair option = (KeyValuePair) iteratorOptions.next();

                sectionMap.put(option.getStringKey(), option.getStringValue().replace("${catalina.home}", catalinaBase));
            }
            retVal.put(sectionKVC.getStringKey(), sectionMap);
        }
        return retVal;
    }

    /**
     *
     * @param input mandatory (tenantDBID, name)
     * @return
     * @throws Exception
     */
    public ArrayList getAccessGroupsFromPerson(Map<?, ?> input) throws Exception {
        Log.getLogger().error("Entering getAccessGroupsFromPerson()");
        try {
            ArrayList returnList = new ArrayList();
            CfgPerson cfgPerson = getCfgPerson(input);

            if (cfgPerson == null) {
                Log.getLogger("No person with name " + getArgumentFromMap(input, "name", true));
                return new ArrayList();
            }

            CfgAccessGroupQuery query = new CfgAccessGroupQuery(confService);
            Log.getLogger().info("persondbid: " + cfgPerson.getDBID());

            query.setPersonDbid(cfgPerson.getDBID());
            query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

            Collection<CfgAccessGroup> accessGroups = query.execute();

            if (accessGroups != null) {
                for (CfgAccessGroup accessGroup : accessGroups) {
                    returnList.add(accessGroup.getGroupInfo().getName());
                }
            }

            return returnList;

        } catch (Exception ex) {
            Log.getLogger().error("Error in getAccessGroupsFromPerson", ex);
        }
        return new ArrayList();
    }
    
    /**
     *
     * @param input mandatory (tenantDBID, name)
     * @return
     * @throws Exception
     */
    public ArrayList getAgentGroupsFromPerson(Map<?, ?> input) throws Exception {
        Log.getLogger().error("Entering getAgentGroupsFromPerson()");
        try {
            ArrayList returnList = new ArrayList();
            CfgPerson cfgPerson = getCfgPerson(input);

            if (cfgPerson == null) {
                Log.getLogger("No person with name " + getArgumentFromMap(input, "name", true));
                return new ArrayList();
            }

            CfgAgentGroupQuery query = new CfgAgentGroupQuery(confService);
            Log.getLogger().info("persondbid: " + cfgPerson.getDBID());

            query.setPersonDbid(cfgPerson.getDBID());
            query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

            Collection<CfgAgentGroup> agentGroups = query.execute();

            if (agentGroups != null) {
                for (CfgAgentGroup agentGroup : agentGroups) {
                    returnList.add(agentGroup.getGroupInfo().getName());
                }
            }

            return returnList;

        } catch (Exception ex) {
            Log.getLogger().error("Error in getAgentGroupsFromPerson", ex);
        }
        return new ArrayList();
    }    

    /**
     *
     * @param input mandatory (cfgPerson, excludeVAGs)
     * @return
     * @throws Exception
     */
    public ArrayList getAgentGroupsFromCfgPerson(CfgPerson cfgPerson, boolean excludeVAGs) throws Exception {
        Log.getLogger().error("Entering getAgentGroupsFromCfgPerson()");
        try {
            ArrayList<String> returnList = new ArrayList<String>();
            
            if (cfgPerson == null) {
                Log.getLogger("No vallid CfgPerson object provided.");
                return new ArrayList();
            }

            CfgAgentGroupQuery query = new CfgAgentGroupQuery(confService);
            Log.getLogger().info("persondbid: " + cfgPerson.getDBID());

            query.setPersonDbid(cfgPerson.getDBID());
            query.setTenantDbid(cfgPerson.getTenantDBID());

            Collection<CfgAgentGroup> agentGroups = query.execute();

            if (agentGroups != null) {
                for (CfgAgentGroup agentGroup : agentGroups) {
                    
                    if (excludeVAGs) {
                        //If the annex option is empty
                        if (agentGroup.getGroupInfo().getUserProperties().getList("virtual") == null) {
                            returnList.add(agentGroup.getGroupInfo().getName());
                        }
                    } else {
                        returnList.add(agentGroup.getGroupInfo().getName());
                    }
                }
            }

            return returnList;

        } catch (Exception ex) {
            Log.getLogger().error("Error in getAgentGroupsFromCfgPerson", ex);
        }
        return new ArrayList();
    }      
    /**
     * Returns all access groups as array
     *
     * @param tenantDBID
     * @return
     * @throws Exception
     */
    public ArrayList getAllAccessGroups(int tenantDBID) throws Exception {
        try {
            ArrayList returnList = new ArrayList();
            CfgAccessGroupQuery query = new CfgAccessGroupQuery(confService);

            query.setTenantDbid(tenantDBID);

            Collection<CfgAccessGroup> accessGroups = query.execute();

            if (accessGroups != null) {
                for (CfgAccessGroup accessGroup : accessGroups) {
                    returnList.add(accessGroup.getGroupInfo().getName());
                }
            }
            Collections.sort(returnList, new SortIgnoreCase());
            return returnList;

        } catch (Exception ex) {
            Log.getLogger().error("Error in getAccessGroupsFromPerson", ex);
        }
        return new ArrayList();
    }
    
    /**
     * Returns all dn groups as array
     *
     * @param tenantDBID
     * @return
     * @throws Exception
     */
    public ArrayList getAllDNGroups(int tenantDBID) throws Exception {
        try {
            ArrayList returnList = new ArrayList();
            CfgDNGroupQuery query = new CfgDNGroupQuery(confService);

            query.setTenantDbid(tenantDBID);

            Collection<CfgDNGroup> dnGroups = query.execute();

            if (dnGroups != null) {
                for (CfgDNGroup accessGroup : dnGroups) {
                    returnList.add(accessGroup.getGroupInfo().getName());
                }
            }
            Collections.sort(returnList, new SortIgnoreCase());
            return returnList;

        } catch (Exception ex) {
            Log.getLogger().error("Error in getAllDNGroups", ex);
        }
        return new ArrayList();
    }

    /**
     *
     * @param input mandatory (name or personDBID)
     * @return
     * @throws Exception
     */
    public CfgPerson getCfgPerson(Map<?, ?> input) throws Exception {
        CfgPersonQuery personQuery = new CfgPersonQuery(confService);
        if (input.containsKey("name")){
            personQuery.setUserName(getArgumentFromMap(input, "name", false));    
        }
        if (input.containsKey("personDBID")) {
            personQuery.setDbid(Integer.parseInt(getArgumentFromMap(input, "personDBID", false)));
        }
        
        CfgPerson person = (CfgPerson) confService.retrieveObject(personQuery);
        if (person == null) {
            throw new DontExistException("No user with name " + personQuery.getUserName() + " exist.");
        }
        Log.getLogger().info("Returning person: " + person.getUserName());

        return person;
    }

    /**
     *
     * @param input mandatory (name)
     * @return
     * @throws Exception
     */
    public CfgSkill getCfgSkill(Map<?, ?> input) throws Exception {
        CfgSkillQuery skillQuery = new CfgSkillQuery(confService);
        skillQuery.setName(getArgumentFromMap(input, "name", true));
        CfgSkill skill = (CfgSkill) confService.retrieveObject(skillQuery);
        if (skill == null) {
            throw new DontExistException("No skill with name " + skillQuery.getName() + " exist.");
        }
        Log.getLogger().info("Returning skill: " + skill.getName());

        return skill;
    }

    /**
     *
     * @param input mandatory (name, switchDBID)
     * @return
     * @throws Exception
     */
    public CfgAgentLogin getCfgAgentLogin(Map<?, ?> input) throws Exception {
        CfgAgentLoginQuery agentLoginQuery = new CfgAgentLoginQuery(confService);
        agentLoginQuery.setLoginCode(getArgumentFromMap(input, "name", true));
        agentLoginQuery.setSwitchDbid(Integer.parseInt(getArgumentFromMap(input, "switchDBID", true)));
        CfgAgentLogin agentLogin = (CfgAgentLogin) confService.retrieveObject(agentLoginQuery);
        if (agentLogin == null) {
            throw new DontExistException("No agent login with name " + agentLoginQuery.getLoginCode() + " exist.");
        }
        Log.getLogger().info("Returning agent login: " + agentLogin.getLoginCode());
        return agentLogin;
    }

    /**
     *
     * @param tenantDBID
     * @param transactionListName
     * @return
     */
    private CfgTransaction getTransactionList(int tenantDBID, String transactionListName) {
        try {
            CfgTransactionQuery query = new CfgTransactionQuery();
            query.setObjectType(CfgTransactionType.CFGTRTList);
            query.setTenantDbid(tenantDBID);
            query.setName(transactionListName);
            return (CfgTransaction) confService.retrieveObject(CfgTransaction.class, query);
        } catch (ConfigException ex) {
            Log.getLogger().error("Error in getTransactionList", ex);
        }
        return null;
    }

    /**
     * Returns a map of important information
     *
     * @param input (name or personDBID)
     * @return
     * @throws Exception
     */
    public Map<?, ?> getPerson(Map<?, ?> input) throws Exception {

        CfgPerson cfgPerson = getCfgPerson(input);

        //Get person data
        Map<Object, Object> returnObject = new HashMap();
        returnObject.put("name", cfgPerson.getUserName());
        returnObject.put("firstName", cfgPerson.getFirstName());
        returnObject.put("lastName", cfgPerson.getLastName());
        returnObject.put("emailAddress", cfgPerson.getEmailAddress());
        returnObject.put("tenantDBID", cfgPerson.getTenantDBID());
        returnObject.put("employeeId", cfgPerson.getEmployeeID());
        returnObject.put("isAgent", cfgPerson.getIsAgent() == CfgFlag.CFGTrue);

        CfgAgentInfo agentInfo = cfgPerson.getAgentInfo();

        if (agentInfo != null) {

            //Return skills data
            Collection<CfgSkillLevel> skillLevels = agentInfo.getSkillLevels();
            ArrayList skills = new ArrayList();
            for (Iterator<CfgSkillLevel> iterator = skillLevels.iterator(); iterator.hasNext();) {
                Map<Object, Object> skill = new HashMap();
                CfgSkillLevel cfgSkillLevel = iterator.next();
                skill.put("skill", cfgSkillLevel.getSkill().getName());
                skill.put("level", cfgSkillLevel.getLevel());
                skills.add(skill);
            }
            returnObject.put("skills", skills);
            
            //Return agent groups, excluding virtual groups
            ArrayList<String> agentGroups = getAgentGroupsFromCfgPerson(cfgPerson, true);
            ArrayList groups = new ArrayList();
            for (int i = 0; i < agentGroups.size(); i++) {
                Map<Object, Object> agentGroup = new HashMap();
                agentGroup.put("agentGroup", agentGroups.get(i));
                groups.add(agentGroup);
            }
            returnObject.put("agentGroups", groups);
            

            //Return capacity rule
            CfgScript cfgScript = agentInfo.getCapacityRule();
            if (cfgScript != null) {
                returnObject.put("capacityRule", cfgScript.getName());
            } else {
                returnObject.put("capacityRule", "");
            }

            CfgPlace cfgPlace = agentInfo.getPlace();
            if (cfgPlace != null) {
                returnObject.put("defaultPlace", cfgPlace.getName());
            } else {
                returnObject.put("defaultPlace", "");
            }
        }
        return returnObject;
    }

    /**
     *
     * @param input mandatory (tenantDBID, dnType)
     * @return an array of the DN numbers
     * @throws Exception
     */
    public ArrayList<String> getAllDNsByType(Map<?, ?> input) throws Exception {

        //TODO
        //Add switch DBID
        String dnType = "";
        Log.getLogger().debug("Entering getAllDNsByType");

        CfgDNQuery query = new CfgDNQuery();

        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        dnType = getArgumentFromMap(input, "dnType", true);
        query.setDnType(getDNTypeFromString(dnType));

        Collection<CfgDN> cfgDns = confService.retrieveMultipleObjects(CfgDN.class, query);

        ArrayList<String> returnArray = new ArrayList();
        for (Iterator<CfgDN> iterator = cfgDns.iterator();
                iterator.hasNext();) {
            CfgDN cfgDN = iterator.next();
            returnArray.add(cfgDN.getNumber());
        }
        Collections.sort(returnArray, new SortIgnoreCase());
        return returnArray;
    }

    /**
     *
     * @param input (tenantDBID)
     * @return an array of Maps (name, firstName, lastName, emailAddress,
     * isAgent)
     * @throws Exception
     */
    public ArrayList<Map<?, ?>> getPersons(Map<?, ?> input) throws Exception {

        CfgPersonQuery query = new CfgPersonQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        Collection<CfgPerson> persons = confService.retrieveMultipleObjects(CfgPerson.class, query);

        ArrayList<Map<?, ?>> returnArray = new ArrayList();
        persons.stream().map((cfgPerson) -> {
            Map<Object, Object> person = new HashMap();
            person.put("name", cfgPerson.getUserName());
            person.put("firstName", cfgPerson.getFirstName());
            person.put("lastName", cfgPerson.getLastName());
            person.put("emailAddress", cfgPerson.getEmailAddress());
            person.put("isAgent", cfgPerson.getIsAgent() == CfgFlag.CFGTrue);
            return person;
        }).forEachOrdered((person) -> {
            returnArray.add(person);
        });

        return returnArray;
    }
    
        /**
     *
     * @param input mandatory (folderDBID)
     * @param input optional (includeSub[default true])
     * @return an array of Maps (name, firstName, lastName, emailAddress,
     * isAgent)
     * @throws Exception
     */
    public ArrayList<Integer> getPersonDBIDsFromFolder(Map<?, ?> input) throws Exception {
        boolean includeSubFolders = true;
        ArrayList<Integer> returnArray = new ArrayList();
        if (getArgumentFromMap(input, "includeSub", false) != null) {
            includeSubFolders = Boolean.parseBoolean(getArgumentFromMap(input, "includeSub", false));
        }
        final boolean includeSubs = includeSubFolders; //needed to be allowed to refererence from forEach loop below
        CfgFolder folder = getCfgFolder(input); 
        if (folder != null){
            Collection<CfgObjectID> objectIDs = folder.getObjectIDs();
            objectIDs.forEach((next) -> {
                if (next.getType() == CfgObjectType.CFGFolder){
                    try {
                        if (includeSubs){
                            Map<String, String> tmpInput = new HashMap();
                            tmpInput.put("folderDBID", next.getDBID() + "");
                            returnArray.addAll(getPersonDBIDsFromFolder(tmpInput));
                        }
                    } catch (Exception exception) {

                    }
                } else if (next.getType() == CfgObjectType.CFGPerson){
                    returnArray.add(next.getDBID());    
                }
            });
        }
        return returnArray;
    }

    /**
     * Returns all skills for given tenant
     *
     * @param input mandatory (tenantDBID)
     * @return an array of skills
     * @throws Exception
     */
    public ArrayList<String> getSkills(Map<?, ?> input) throws Exception {

        CfgSkillQuery query = new CfgSkillQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        Collection<CfgSkill> skills = confService.retrieveMultipleObjects(CfgSkill.class, query);

        ArrayList<String> returnArray = new ArrayList();
        skills.forEach((next) -> {
            returnArray.add(next.getName());
        });
        Collections.sort(returnArray, new SortIgnoreCase());
        return returnArray;
    }

    /**
     * Returns all agent groups for given tenant
     *
     * @param input mandatory (tenantDBID)
     * @param input optional (excludeVAGs)
     * @return
     * @throws Exception
     */
    public ArrayList<String> getAgentGroups(Map<?, ?> input) throws Exception {

        CfgAgentGroupQuery query = new CfgAgentGroupQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        Collection<CfgAgentGroup> groups = confService.retrieveMultipleObjects(CfgAgentGroup.class, query);

        boolean excludeVAG = false;

        if (input.containsKey("excludeVAGs")) {
            if (Boolean.parseBoolean(getArgumentFromMap(input, "excludeVAGs", false)) || getArgumentFromMap(input, "excludeVAGs", false).equals("1")) {
                excludeVAG = true;
            }
        }

        ArrayList<String> returnArray = new ArrayList();
        for (CfgAgentGroup next : groups) {
            if (excludeVAG) {
                //If the annex option is empty
                if (next.getGroupInfo().getUserProperties().getList("virtual") == null) {
                    returnArray.add(next.getGroupInfo().getName());
                }
            } else {
                returnArray.add(next.getGroupInfo().getName());
            }
        }

        Collections.sort(returnArray, new SortIgnoreCase());
        return returnArray;
    }
    
    /**
     * Returns all agent logins for given tenant
     *
     * @param input mandatory (tenantDBID)
     * @return
     * @throws Exception
     */
    public ArrayList<String> getAgentLogins(Map<?, ?> input) throws Exception {

        CfgAgentLoginQuery query = new CfgAgentLoginQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        Collection<CfgAgentLogin> skills = confService.retrieveMultipleObjects(CfgAgentLogin.class, query);

        ArrayList<String> returnArray = new ArrayList();
        for (CfgAgentLogin next : skills) {

            returnArray.add(next.getLoginCode());
        }

        Collections.sort(returnArray, new SortIgnoreCase());
        return returnArray;
    }    

    /**
     * Returns all places for given tenant
     *
     * @param input mandatory (tenantDBID)
     * @return an array of place names
     * @throws Exception
     */
    public ArrayList<String> getPlaces(Map<?, ?> input) throws Exception {

        CfgPlaceQuery query = new CfgPlaceQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        Collection<CfgPlace> skills = confService.retrieveMultipleObjects(CfgPlace.class, query);

        ArrayList<String> returnArray = new ArrayList();

        for (Iterator<CfgPlace> iterator = skills.iterator(); iterator.hasNext();) {
            CfgPlace next = iterator.next();
            returnArray.add(next.getName());
        }

        Collections.sort(returnArray, new SortIgnoreCase());
        return returnArray;
    }

    /**
     *
     * @param input
     * @throws Exception
     */
    public void removePersonFromAccessGroup(Map<?, ?> input) throws Exception {

        CfgAccessGroupQuery query = new CfgAccessGroupQuery();
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        query.setName(getArgumentFromMap(input, "accessGroup", true));

        CfgAccessGroup accessGroup = confService.retrieveObject(CfgAccessGroup.class, query);

        Collection<CfgID> members = accessGroup.getMemberIDs();
        if (members == null) {
            return;
        }

        //Get person
        CfgPerson person = getCfgPerson(input);

        Collection<CfgID> accessGroupMembers = accessGroup.getMemberIDs();

        if (accessGroupMembers == null) {
            accessGroupMembers = new ArrayList<>();
        }

        //Remove the person
        for (CfgID cfgID : accessGroupMembers) {
            if (cfgID.getDBID().equals(person.getDBID())) {
                members.remove(cfgID);
                break;
            }
        }
        accessGroup.save();
    }

    /**
     *
     * @param input mandatory (agentGroup, name, tenantDBID)
     * @throws Exception
     */
    public void removePersonFromAgentGroup(Map<?, ?> input) throws Exception {

        Map<String, String> tmpInput = new HashMap();
        tmpInput.put("name", getArgumentFromMap(input, "agentGroup", true));
        tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));

        CfgAgentGroup agentGroup;
        try {
            agentGroup = getCfgAgentGroup(tmpInput);
            //Get person
            CfgPerson person = getCfgPerson(input);

            Collection<Integer> agents = agentGroup.getAgentDBIDs();
            try {
                if (agents.contains(person.getDBID())) {
                    Log.getLogger().info("Removing agent '" + person.getUserName() + "' from agent group '" + agentGroup.getGroupInfo().getName() + "'.");
                    agents.remove(person.getDBID());
                    agentGroup.save();
                }
            } catch (ConfigException configException) {
                //Ignore. This will happen if trying to remove agent from a VAG.
                Log.getLogger().error("Exception while removing agent " + person.getUserName() + "' from agent group '" + agentGroup.getGroupInfo().getName() + "'.", configException);
            }
        } catch (Exception exception) {
            //Do nothing. If not possible to remove person, just ignore.
            Log.getLogger().error("Exception: ", exception);
        }

    }

    /**
     *
     * @param annex
     * @param sectionName
     * @param key
     * @param value
     * @throws Exception
     */
    private void setAnnexDataPrivate(KeyValueCollection annex, String sectionName, String key, String value) throws Exception {

        KeyValueCollection section = annex.getList(sectionName);
        if (section == null) {
            section = new KeyValueCollection();
            annex.addList(sectionName, section);
        }
        KeyValuePair kvPair = section.getPair(key);
        if (kvPair == null) {
            kvPair = new KeyValuePair(key, value);
            section.addPair(kvPair);
        } else {
            kvPair.setStringValue(value);
        }
    }

    /**
     * Adds agent to access group
     *
     * @param input mandatory (tenantDBID, name, accessGroup)
     * @throws Exception
     */
    public void addPersonToAccessGroup(Map<?, ?> input) throws Exception {

        Map<String, String> tmpInput = new HashMap();
        tmpInput.put("name", getArgumentFromMap(input, "accessGroup", true));
        tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));

        CfgAccessGroup accessGroup = getCfgAccessGroup(tmpInput);

        Collection<CfgID> accessGroupMembers = accessGroup.getMemberIDs();
        if (accessGroupMembers == null) {
            accessGroupMembers = new ArrayList<CfgID>();
        }

        //Get person
        CfgPerson person = getCfgPerson(input);

        //Check if person is a member of the access group already
        for (CfgID cfgID : accessGroupMembers) {
            if (cfgID.getDBID().equals(person.getDBID())) {
                return;
            }
        }

        CfgID id = new CfgID(confService, person);
        id.setDBID(person.getDBID());
        id.setType(CfgObjectType.CFGPerson);

        accessGroupMembers.add(id);
        accessGroup.save();
    }

    /**
     * Adds agent to agent group
     *
     * @param input mandatory (tenantDBID, name, agentGroup)
     * @throws Exception
     */
    public void addPersonToAgentGroup(Map<?, ?> input) throws Exception {

        Map<String, String> tmpInput = new HashMap();
        tmpInput.put("name", getArgumentFromMap(input, "agentGroup", true));
        tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));

        CfgAgentGroup agentGroup = getCfgAgentGroup(tmpInput);

        //Get person
        CfgPerson person = getCfgPerson(input);
        Collection<CfgPerson> agents = agentGroup.getAgents();
        if (agents == null) {
            Log.getLogger().info("Agent group empty, creating new array.");
            agents = new ArrayList<CfgPerson>();
            agentGroup.setAgents(agents);
        }
        agents.add(person);
        agentGroup.setAgents(agents);
        agentGroup.save();
    }

    /**
     * Checks if object exist by fetching it. Mandatory parameters differ with
     * object type
     *
     * @param input mandatory (name, type, (tenantDBID))
     * @throws Exception
     */
    public boolean genesysObjectExist(Map<?, ?> input) throws Exception {

        try {
            String type = getArgumentFromMap(input, "type", true);
            String name = getArgumentFromMap(input, "name", true);
            switch (type.toLowerCase()) {
                case "skill":
                    getCfgSkill(input);
                    return true;
                case "person":
                    getCfgPerson(input);
                    return true;
                case "dn":
                    getCfgDN(input);
                    return true;
                case "agentlogin":
                    getCfgAgentLogin(input);
                    return true;
                case "agentgroup":
                    getCfgAgentGroup(input);
                    return true;
                case "accessgroup":
                    getCfgAccessGroup(input);
                    return true;
                default:
                    throw new Exception("No such type exist");
            }
        } catch (DontExistException dontExistException) {
            Log.getLogger().error("No such object exist, returning false. " + dontExistException.getMessage());

            return false;
        }
    }

    /**
     * Adds sections to annex tab. Will overwrite if already exist. Other
     * sections will be left untouched.
     *
     * Example input: {object: cfgObject, data: {"sssss":{"Option1":
     * "Value1","Option2": "Value2"},"Section2":{"Option3": "Value3","Option4":
     * "Value4"}}}
     *
     * @param input (object, data)
     * @throws Exception
     */
    public void setAnnexData(Map<?, ?> input) throws Exception {
        Log.getLogger().info("Execute setAnnexData");
        CfgObject object = null;
        if (input.containsKey("object")) {
            object = (CfgObject) input.get("object");
        } else {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            throw new InputException("Missing mandatory input parameter 'object' in method "
                    + stackTraceElements[1].getMethodName() + "(" + stackTraceElements[1].getLineNumber() + ").");
        }

        ScriptObjectMirror data = null;
        if (input.containsKey("data")) {
            data = (ScriptObjectMirror) input.get("data");
        } else {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            throw new InputException("Missing mandatory input parameter 'data' in method "
                    + stackTraceElements[1].getMethodName() + "(" + stackTraceElements[1].getLineNumber() + ").");
        }

        KeyValueCollection annex = null;
        Method getUserProps = null;
        Method setUserProps = null;
        Method getName = null;
        for (Method method : object.getClass().getMethods()) {
            if (method.getName().equals("getUserProperties")) {
                getUserProps = method;
            } else if (method.getName().equals("setUserProperties")) {
                setUserProps = method;
            } else if (method.getName().equals("getName")) {
                getName = method;
            }
        }

        if (getUserProps != null && setUserProps != null) {
            annex = ((KeyValueCollection) getUserProps.invoke(object));

            if (annex == null) {
                Log.getLogger().info("Annex was null, will set a new one before adding.");
                annex = new KeyValueCollection();
                setUserProps.invoke(object, annex);
            }
            Log.getLogger().info((getName != null ? getName.invoke(object) : "NoNameMethod ") + ": Old annex is: " + ((KeyValueCollection) getUserProps.invoke(object)).toString());

            for (Map.Entry<String, Object> sectionEntry : data.entrySet()) {
                String sectionName = sectionEntry.getKey();
                ScriptObjectMirror section = (ScriptObjectMirror) sectionEntry.getValue();
                for (Map.Entry<String, Object> entry : section.entrySet()) {
                    setAnnexDataPrivate(annex, sectionName, (String) entry.getKey(), (String) entry.getValue());
                }
            }
            Log.getLogger().info((getName != null ? getName.invoke(object) : "NoNameMethod ") + ": New annex is: " + ((KeyValueCollection) getUserProps.invoke(object)).toString());

            object.save();
        }
    }
    

    /**
     *
     * @param input mandatory(tenantDBID, transactionListName, sectionName)
     * @throws Exception
     */
    public void transactionListDeleteSection(Map<?, ?> input) throws Exception {
        CfgTransaction transactionList;

        String listObjectName = getArgumentFromMap(input, "transactionListName", true);
        String sectionName = getArgumentFromMap(input, "sectionName", true);

        if (sectionName.trim().length() == 0) {
            throw new Exception("Section name must not be empty.");
        }

        if (listObjectName.trim().length() == 0) {
            throw new Exception("List object name must not be empty.");
        }

        transactionList = getTransactionList(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)), listObjectName);

        Log.getLogger().info("Delete section " + sectionName + " in list " + listObjectName);

        if (transactionList != null) {
            try {
                transactionList.getUserProperties().remove(sectionName);
                transactionList.save();
            } catch (ConfigException ex) {
                Log.getLogger().error("Error in delete section");
                throw (Exception) ex;
            }
        }
    }

    /**
     * Creates a new section with given data. Will delete section first so if
     * options should be added, use transactionListAddOption() instead.
     *
     * @param input mandatory(tenantDBID, transactionListName, sectionName)
     * data)
     * @throws Exception
     */
    public void transactionListCreateSection(Map<?, ?> input) throws Exception {
        Log.getLogger().info("Execute transactionListCreateSection");
        if (confService != null) {
            if (input.containsKey("data")) {
                CfgTransaction transactionList;
                String sectionName = getArgumentFromMap(input, "sectionName", true);
                String transactionListName = getArgumentFromMap(input, "transactionListName", true);

                if (sectionName.trim().length() == 0) {
                    throw new Exception("Section name must not be empty.");
                }

                if (transactionListName.trim().length() == 0) {
                    throw new Exception("Transaction list name must not be empty.");
                }

                //Delete section
                transactionList = getTransactionList(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)), transactionListName);
                if (transactionList != null) {
                    try {
                        transactionList.getUserProperties().remove(sectionName);
                        transactionList.save();
                    } catch (ConfigException ex) {

                    }
                } else {
                    //Create a new transList             
                    transactionList = new CfgTransaction(confService);
                    transactionList.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
                    transactionList.setName(transactionListName);
                    transactionList.setAlias(transactionListName);
                    transactionList.setType(CfgTransactionType.CFGTRTList);
                    Log.getLogger().info(transactionListName + " transaction list created.");
                }

                //Recreate the section based on new data
                //Create getUserProperties object
                KeyValueCollection kvPairList = transactionList.getUserProperties();
                if (kvPairList == null) {
                    transactionList.setUserProperties(new KeyValueCollection());
                }

                //Create section
                KeyValueCollection kvSection = new KeyValueCollection();
                kvPairList.addList(sectionName, kvSection);

                ScriptObjectMirror tmp = (ScriptObjectMirror) input.get("data");
                for (Map.Entry<String, Object> entry : tmp.entrySet()) {
                    kvSection.addObject((String) entry.getKey(), (entry.getValue() != null ? (Object) entry.getValue() : ""));
                }

                confService.saveObject(transactionList);
            } else {
                StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
                throw new InputException("Missing mandatory input parameter 'data' in method "
                        + stackTraceElements[1].getMethodName() + "(" + stackTraceElements[1].getLineNumber() + ").");
            }
        }
    }

    /**
     * Add one or more options to existing section of transaction list. If
     * section or list does not exist, they will be created.
     *
     * @param input mandatory(tenantDBID, transactionListName, sectionName, data)
     * @throws Exception
     */
    public void transactionListAddOptions(Map<?, ?> input) throws Exception {
        Log.getLogger().info("Execute  transactionListAddOptions");
        if (confService != null) {
            if (input.containsKey("data")) {
                CfgTransaction transactionList;
                String sectionName = getArgumentFromMap(input, "sectionName", true);
                String transactionListName = getArgumentFromMap(input, "transactionListName", true);
                int tenantDBID = Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true));
                if (sectionName.trim().length() == 0) {
                    throw new Exception("Section name must not be empty.");
                }
                //Delete section
                transactionList = getTransactionList(tenantDBID, transactionListName);
                if (transactionList == null) {
                    //Create a new transList             
                    transactionList = new CfgTransaction(confService);
                    transactionList.setTenantDBID(tenantDBID);
                    transactionList.setName(transactionListName);
                    transactionList.setAlias(transactionListName);
                    transactionList.setType(CfgTransactionType.CFGTRTList);
                    Log.getLogger().info(transactionListName + " transaction list created.");

                }

                KeyValueCollection section = transactionList.getUserProperties().getList(sectionName);
                if (section == null) {
                    section = new KeyValueCollection();
                    transactionList.getUserProperties().addList(sectionName, section);
                }

                ScriptObjectMirror tmp = (ScriptObjectMirror) input.get("data");
                for (Map.Entry<String, Object> entry : tmp.entrySet()) {
                    section.addObject((String) entry.getKey(), (Object) entry.getValue());
                }

                transactionList.save();
            } else {
                StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
                throw new InputException("Missing mandatory input parameter 'data' in method "
                        + stackTraceElements[1].getMethodName() + "(" + stackTraceElements[1].getLineNumber() + ").");
            }
        }
    }

    /**
     *
     * @param input mandatory(tenantDBID, transactionListName, sectionName)
     * @throws Exception
     */
    public boolean transactionListSectionExists(Map<?, ?> input) throws Exception {
        Log.getLogger().info("Execute  transactionListSectionExists");
        if (getArgumentFromMap(input, "sectionName", true).equals("") || getArgumentFromMap(input, "transactionListName", true).equals("")) {
            return false;
        }

        CfgTransaction transactionList;

        //Match By name
        transactionList = getTransactionList(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)), getArgumentFromMap(input, "transactionListName", true));
        if (transactionList == null) {
            return false;
        }

        KeyValueCollection kvpair = transactionList.getUserProperties();

        if (kvpair == null) {
            return false;
        }

        //Match section
        if (kvpair.getList(getArgumentFromMap(input, "sectionName", true)) == null) {
            return false;
        }

        return true;
    }

    /**
     * Returns a transaction list as a map of options. If a section name is
     * given, only that section is returned. If an option and a section is
     * given, only that option is returned.
     *
     * @param input mandatory(tenantDBID, transactionListName)
     * @param input optional(sectionName, optionName)
     * @return
     * @throws Exception
     */
    public Map<?, ?> getTransactionList(Map<?, ?> input) throws Exception {

        int tenantDBID = Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true));
        String transactionListName = getArgumentFromMap(input, "transactionListName", true);

        HashMap<Object, Object> retVal = new HashMap();
        CfgTransaction transactionList;

        transactionList = getTransactionList(tenantDBID, transactionListName);
        if (transactionList == null) {

            throw new Exception(" There is no transaction list with name " + transactionListName);

        } else {
            KeyValueCollection transaction = transactionList.getUserProperties();

            if (transaction == null) {
                throw new Exception(" There are no sections in transaction list " + transactionListName);
            } else {

                if (input.containsKey("sectionName")) {
                    KeyValueCollection section = transaction.getList(getArgumentFromMap(input, "sectionName", false));

                    if (section == null) {
                        throw new Exception(" There is no section with name " + getArgumentFromMap(input, "sectionName", false));
                    } else {
                        if (input.containsKey("optionName")) {
                            KeyValuePair option = section.getPair(getArgumentFromMap(input, "optionName", false));
                            if (option == null) {
                                throw new Exception(" There is no option with name " + getArgumentFromMap(input, "optionName", false));
                            } else {
                                retVal.put(option.getStringKey(), option.getStringValue());
                            }
                        } else {
                            retVal = getKeyValueCollectionAsMap(section);
                        }
                    }
                } else {
                    retVal = getKeyValueCollectionAsMap(transaction);
                }
            }
        }
        return retVal;
    }

    /**
     * Returns a KeyValueCollection as a HashMap (recursively).
     *
     * @param input
     * @return
     */
    private HashMap<Object, Object> getKeyValueCollectionAsMap(KeyValueCollection input) {
        HashMap<Object, Object> retVal = new HashMap();

        for (Iterator<?> iterator = input.iterator(); iterator.hasNext();) {
            KeyValuePair item = (KeyValuePair) iterator.next();

            switch (item.getValueType().toString()) {
                case "STRING":
                    retVal.put(item.getStringKey(), item.getStringValue());
                    break;
                case "TKV_LIST":
                    retVal.put(item.getStringKey(), getKeyValueCollectionAsMap(item.getTKVValue()));
                    break;
                case "INT":
                    retVal.put(item.getStringKey(), item.getIntValue());
                    break;
                case "BINARY":
                    retVal.put(item.getStringKey(), item.getBinaryValue());
                    break;
                default:
                    retVal.put(item.getStringKey(), item.getValue());
                    break;
            }
        }

        return retVal;
    }

    /**
     * Example input:
     *
     * {"place":{"name":input.name,"tenantDBID":1},"dns":[{"tenantDBID":1,"number":"1000","switchDBID":101},{"tenantDBID":1,"number":"1001","switchDBID":101}]}
     *
     * @param input mandatory (place map, vector of dn maps)
     * @throws Exception
     */
    public void addDnsToPlace(Map<?, ?> input) throws Exception {

        if (input.containsKey("place") && input.containsKey("dns")) {

            CfgPlace place = getCfgPlace((Map<?, ?>) input.get("place"));

            Collection<CfgDN> dns = place.getDNs();
            HashMap<String, Object> newMap = new HashMap<>();
            ScriptObjectMirror tmp = (ScriptObjectMirror) input.get("dns");
            for (int i = 0; i < tmp.size(); i++) {
                ScriptObjectMirror oldMap = (ScriptObjectMirror) tmp.get(Integer.toString(i));
                for (Map.Entry<String, Object> entry : oldMap.entrySet()) {
                    newMap.put(entry.getKey(), entry.getValue());
                }
                dns.add(getCfgDN(newMap));
                newMap.clear();
            }

            place.setDNs(dns);
            place.save();
        } else {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            throw new InputException("Missing mandatory input parameter 'place' or 'dns' in method "
                    + stackTraceElements[1].getMethodName() + "(" + stackTraceElements[1].getLineNumber() + ").");
        }
    }

    /**
     * TODO Fix this, NOT WORKING!
     *
     * @param input
     * @throws Exception
     */
    public void addDNToDNGroup(Map<?, ?> input) throws Exception {
        CfgDN dn = getCfgDN(input);
        CfgDNGroup dnGroup = getCfgDNGroup(input);
        Collection<CfgDNInfo> dns = dnGroup.getDNs();
        for (Iterator<CfgDNInfo> iterator = dns.iterator(); iterator.hasNext();) {
            CfgDNInfo next = iterator.next();
            next.getDN();
        }

        CfgDNInfo dnInfo = new CfgDNInfo(confService, dnGroup);

        dns.add(dnInfo);
        dnGroup.setDNs(dns);
        dnGroup.save();
    }

    /**
     *
     * @param input mandatory (name, tenantDBID)
     * @return
     * @throws Exception
     */
    public CfgDNGroup getCfgDNGroup(Map<?, ?> input) throws Exception {
        CfgDNGroupQuery query = new CfgDNGroupQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        CfgDNGroup dnGroup = (CfgDNGroup) confService.retrieveObject(query);
        if (dnGroup == null) {
            throw new DontExistException("No dn group with name " + query.getName() + " exist.");
        }

        return dnGroup;
    }

    /**
     *
     * @param input mandatory (name, tenantDBID)
     * @param input optional (folderDBID, tableAccess, maxAttempt, dailyFrom,
     * dailyTill, treatments)
     * @return
     * @throws Exception
     */
    public CfgCallingList createCallingList(Map<?, ?> input) throws Exception {

        CfgCallingList list = new CfgCallingList(confService);
        Map<String, String> tmpInput = new HashMap();
        
        list.setName(getArgumentFromMap(input, "name", true));
        list.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        if (input.containsKey("folderDBID")) {
            list.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        if (input.containsKey("tableAccess")) {
            tmpInput.put("name", getArgumentFromMap(input, "tableAccess", false));
            tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));
            list.setTableAccess(getCfgTableAccess(tmpInput));
        }
        if (input.containsKey("maxAttempt")) {
            list.setMaxAttempts(Integer.parseInt(getArgumentFromMap(input, "maxAttempt", false)));
        }
        if (input.containsKey("dailyFrom")) {
            list.setTimeFrom(Integer.parseInt(getArgumentFromMap(input, "dailyFrom", false)));
        }
        if (input.containsKey("dailyTill")) {
            list.setTimeUntil(Integer.parseInt(getArgumentFromMap(input, "dailyTill", false)));
        }
        if (input.containsKey("treatments")) {
            ArrayList<CfgTreatment> treatments = new ArrayList();
            String[] inputTreatments = getArgumentFromMap(input, "treatments", false).split(",");

            for (int i = 0; i < inputTreatments.length; i++) {
                tmpInput.clear();
                tmpInput.put("name", inputTreatments[i]);
                tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));
                treatments.add(getCfgTreatment(tmpInput));
            }
            list.setTreatments(treatments);
        }

        list.save();

        return list;
    }

    /**
     *
     * @param input mandatory (name, tenantDBID, tableName, dap, format)
     * @param input optional (folderDBID)
     * @return
     * @throws Exception
     */
    public void createTableAccess(Map<?, ?> input) throws Exception {
        Map<String, String> tmpInput = new HashMap();
        CfgTableAccess tableAccess = new CfgTableAccess(confService);
        tableAccess.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        tableAccess.setName(getArgumentFromMap(input, "name", true));
        tableAccess.setType(CfgTableType.CFGTTCallingList);
        tableAccess.setIsCachable(CfgFlag.CFGFalse);

        if (input.containsKey("tableName")) {
            tableAccess.setDbTableName(getArgumentFromMap(input, "tableName", false));
        }

        if (input.containsKey("dap")) {
            tmpInput.put("name", getArgumentFromMap(input, "dap", false));
            CfgApplication app = getCfgApplication(tmpInput);
            Log.getLogger().info("Setting DAP: " + app.getName());
            tableAccess.setDbAccess(app);
        }
        if (input.containsKey("format")) {
            tmpInput.clear();
            tmpInput.put("name", getArgumentFromMap(input, "format", false));
            tmpInput.put("tenantDBID", getArgumentFromMap(input, "tenantDBID", true));
            CfgFormat format = getCfgFormat(tmpInput);
            Log.getLogger().info("Setting Format: " + format.getName());
            tableAccess.setFormat(getCfgFormat(tmpInput));
        }

        if (input.containsKey("folderDBID")) {
            tableAccess.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        tableAccess.save();
    }


    /**
     *
     * @param input mandatory (name, tenantDBID, tableName, dap, format)
     * @param input optional (folderDBID)
     * @return
     * @throws Exception
     */
    public void createTreatment(Map<?, ?> input) throws Exception {
        Map<String, String> tmpInput = new HashMap();
        CfgTreatment treatment = new CfgTreatment(confService);
        treatment.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        treatment.setName(getArgumentFromMap(input, "name", true));
        treatment.setFolderId(Integer.SIZE);

        if (input.containsKey("folderDBID")) {
            treatment.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        
        treatment.setCallResult(GctiCallState.GctiCStBusy);
        //treatment.setCallActionCode(CfgCallActionCode.CFGCACRoute);
        treatment.setCycleAttempt(1);
        treatment.setIncrement(2);
        treatment.setInterval(3);
        treatment.setRecActionCode(CfgRecActionCode.CFGRACRetryIn);
        treatment.setAttempts(4); //Number in sequence


        treatment.save();
    }    
    
    /**
     *
     * @param input mandatory (name)
     * @return
     * @throws Exception
     */
    public CfgApplication getCfgApplication(Map<?, ?> input) throws Exception {

        CfgApplicationQuery query = new CfgApplicationQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        
        if (getArgumentFromMap(input, "tenantDBID", false) != null){
            query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", false)));
        }
        
        CfgApplication application = (CfgApplication) confService.retrieveObject(query);
        if (application == null) {
            throw new DontExistException("No application with name " + query.getName() + " exist.");
        }
        Log.getLogger().info("Returning application: " + application.getName());

        return application;
    }

    /**
     *
     * @param input mandatory(folderDBID or name) if name, must be unique
     * @return
     * @throws Exception
     */
    public CfgFolder getCfgFolder(Map<?, ?> input) throws Exception {
        CfgFolderQuery query = new CfgFolderQuery(confService);
        query.setObjectType(CfgObjectType.CFGFolder.asInteger());

        if (input.containsKey("name")){
            query.setName(getArgumentFromMap(input, "name", false));    
        }
        if (input.containsKey("folderDBID")) {
            query.setDbid(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        
        CfgFolder folder = (CfgFolder) confService.retrieveObject(query);
        if (folder == null) {
            throw new DontExistException("No folder with name " + query.getName() + " exist.");
        }
        return folder;
    }

    /**
     *
     * @param input mandatory(tenantDBID, name)
     * @return
     * @throws Exception
     */
    public CfgFormat getCfgFormat(Map<?, ?> input) throws Exception {
        CfgFormatQuery query = new CfgFormatQuery(confService);
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        query.setName(getArgumentFromMap(input, "name", true));
        CfgFormat format = (CfgFormat) confService.retrieveObject(query);
        if (format == null) {
            throw new DontExistException("No format with name " + query.getName() + " exist.");
        }
        return format;
    }

    /**
     * Returns all fields of a format as an array, with relevant information
     * such as type, size etc.
     *
     * @param input mandatory(tenantDBID, name)
     * @return
     * @throws Exception
     */
    public ArrayList<Map<?, ?>> getFieldsFromFormat(Map<?, ?> input) throws Exception {
        ArrayList<Map<?, ?>> returnArray = new ArrayList();
        Map<Object, Object> fieldArray = new HashMap();
        CfgFormat format = getCfgFormat(input);
        Collection<CfgField> fields = format.getFields();
        for (CfgField field : fields) {
            fieldArray.put("name", field.getName());
            fieldArray.put("type", getDataTypeAsString(field.getType()));
            fieldArray.put("length", field.getLength());
            fieldArray.put("nullable", field.getIsNullable() == CfgFlag.CFGTrue ? true : false);
            returnArray.add(fieldArray);
            fieldArray = new HashMap();
            Log.getLogger().info(field.getName());

        }
        return returnArray;
    }

    /**
     * Returns true if calling list exist or false otherwise
     *
     * @param input mandatory(tenantDBID, name)
     * @return
     */
    public boolean callingListExist(Map<?, ?> input) {
        CfgCallingList callingList = null;
        try {
            CfgCallingListQuery query = new CfgCallingListQuery(confService);
            query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
            query.setName(getArgumentFromMap(input, "name", true));
            callingList = (CfgCallingList) confService.retrieveObject(query);
        } catch (NumberFormatException | ConfigException | InputException exception) {
            //Do nothing, return false
        }

        return callingList != null;
    }

    /**
     * Returns a CFGDataType as a non-genesys String (at the moment only on
     * MSSQL format)
     *
     * @param type
     * @return
     */
    private String getDataTypeAsString(CfgDataType type) {
        switch (type.name()) {
            case "CFGDTChar":
                return "char";
            case "CFGDTDateTime":
                return "datetime";
            case "CFGDTFloat":
                return "float";
            case "CFGDTInt":
                return "int";
            case "CFGDTMaxDataType":
                return "maxdatatype";
            case "CFGDTNoDataType":
                return "nodatatype";
            case "CFGDTVarChar":
                return "varchar";
        }
        return null;
    }

    /**
     *
     * @param input mandatory (name, tenantDBID)
     * @return
     * @throws Exception
     */
    public CfgTableAccess getCfgTableAccess(Map<?, ?> input) throws Exception {
        CfgTableAccessQuery query = new CfgTableAccessQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        CfgTableAccess tableAccess = (CfgTableAccess) confService.retrieveObject(query);
        if (tableAccess == null) {
            throw new DontExistException("No table access with name " + query.getName() + " exist.");
        }
        return tableAccess;
    }

    /**
     *
     * @param input mandatory(name,tenantDBID)
     * @return
     * @throws Exception
     */
    public CfgTreatment getCfgTreatment(Map<?, ?> input) throws Exception {
        CfgTreatmentQuery query = new CfgTreatmentQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        CfgTreatment treatment = (CfgTreatment) confService.retrieveObject(query);
        if (treatment == null) {
            throw new DontExistException("No treatment with name " + query.getName() + " exist.");
        }

        return treatment;
    }

    /**
     *
     * @param input mandatory (tenantDBID, name)
     * @param input optional (folderDBID)
     * @return
     * @throws Exception
     */
    public CfgPlace createPlace(Map<?, ?> input) throws Exception {

        CfgPlace place = new CfgPlace(confService);

        place.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        place.setName(getArgumentFromMap(input, "name", true));
        if (input.containsKey("folderDBID")) {
            place.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        place.save();
        Log.getLogger().info("Place: " + place.getName() + " created");
        return place;
    }

    /**
     *
     * @param input mandatory (name)
     * @return
     * @throws Exception
     */
    public CfgPlace getCfgPlace(Map<?, ?> input) throws Exception {
        CfgPlaceQuery placeQuery = new CfgPlaceQuery(confService);
        //placeQuery.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));  --Fredrik 2017-11-02
        placeQuery.setName(getArgumentFromMap(input, "name", true));
        CfgPlace place = (CfgPlace) confService.retrieveObject(placeQuery);
        if (place == null) {
            throw new DontExistException("No place with name " + placeQuery.getName() + " exist.");
        }
        Log.getLogger().info("Returning place: " + place.getName());
        return place;
    }
    
     /**
     * Returns role. Not possible to set tenantDBID so if same name exist in multiple tenants, this method will fail.
     * @param input mandatory (name)
     * @return
     * @throws Exception
     */
    public CfgRole getCfgRole(Map<?, ?> input) throws Exception {
        CfgRoleQuery roleQuery = new CfgRoleQuery(confService);
        roleQuery.setName(getArgumentFromMap(input, "name", true));
        CfgRole role = (CfgRole) confService.retrieveObject(roleQuery);
        if (role == null) {
            throw new DontExistException("No role with name " + roleQuery.getName() + " exist.");
        }
        Log.getLogger().info("Returning role: " + role.getName());
        return role;
    }

    /**
     * Returns DN with number given in name attribute
     *
     * @param input mandatory (tenantDBID, name, switchDBID)
     * @return
     * @throws Exception
     */
    public CfgDN getCfgDN(Map<?, ?> input) throws Exception {
        CfgDNQuery dnQuery = new CfgDNQuery(confService);
        dnQuery.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        dnQuery.setDnNumber(getArgumentFromMap(input, "name", true));
        dnQuery.setSwitchDbid(Integer.parseInt(getArgumentFromMap(input, "switchDBID", true)));
        CfgDN dn = (CfgDN) confService.retrieveObject(dnQuery);
        if (dn == null) {
            throw new DontExistException("No dn with name " + dnQuery.getName() + " exist.");
        }
        Log.getLogger().info("Returning dn: " + dn.getNumber());
        return dn;
    }

    /**
     *
     * @param input mandatory (tenantDBID, name, switchDBID)
     * @param input optional (folderDBID)
     * @return
     * @throws Exception
     */
    public CfgAgentLogin createAgentLogin(Map<?, ?> input) throws Exception {
        CfgAgentLogin login = new CfgAgentLogin(confService);
        login.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        login.setLoginCode(getArgumentFromMap(input, "name", true));
        login.setSwitchDBID(Integer.parseInt(getArgumentFromMap(input, "switchDBID", true)));
        if (input.containsKey("folderDBID")) {
            login.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", true)));
        }

        login.setSwitchSpecificType(1);
        login.save();
        Log.getLogger().info("AgentLogin: " + login.getLoginCode() + " created");
        return login;
    }

    /**
     *
     * @param input mandatory (tenantDBID, name)
     * @return
     * @throws Exception
     */
    public CfgFolder createConfigurationUnit(Map<?, ?> input) throws Exception {

        CfgOwnerID ownerId = new CfgOwnerID(confService, null);
        ownerId.setDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        ownerId.setType(CfgObjectType.CFGTenant);
        CfgFolder cu = new CfgFolder(confService);
        cu.setName(getArgumentFromMap(input, "name", true));
        cu.setOwnerID(ownerId);
        cu.setType(CfgObjectType.CFGFolder);
        cu.save();

        return cu;
    }

    /**
     *
     * @param input mandatory (name, type, parentDBID)
     * @return
     * @throws Exception
     */
    public CfgFolder createFolder(Map<?, ?> input) throws Exception {

        CfgFolderQuery cfgFolderQueryParent = new CfgFolderQuery(confService);
        cfgFolderQueryParent.setDbid(Integer.parseInt(getArgumentFromMap(input, "parentDBID", true)));
        CfgFolder cfgFolderParent = cfgFolderQueryParent.executeSingleResult();

        CfgOwnerID ownerId = new CfgOwnerID(confService, null);
        ownerId.setDBID(cfgFolderParent.getOwnerID().getDBID());
        ownerId.setType(cfgFolderParent.getOwnerID().getType());
        CfgFolder folder = new CfgFolder(confService);
        folder.setName(getArgumentFromMap(input, "name", true));
        folder.setFolderId(cfgFolderParent.getObjectDbid());
        folder.setOwnerID(ownerId);
        folder.setType(getObjectTypeFromString(getArgumentFromMap(input, "type", true)));
        folder.save();

        return folder;
    }

    /**
     *
     * Updates permissions for the provided CfgObject
     *
     * ReadAccess: 1 
     * CreateAccess: 2 
     * ChangeAccess: 4 
     * ExecuteAccess: 8
     * DeleteAccess: 16 
     * ReadPermissionsAccess: 32 
     * ChangePermissionsAccess: 64
     *
     * @param input mandatory(permissions,object (CfgObject), account (CfgObject))
     * @throws Exception
     */
    public void updateObjectAccess(Map<?, ?> input) throws Exception {

        Integer permissionMask = 0;
        String permissions = getArgumentFromMap(input, "permissions", true).toLowerCase();
        boolean propagate = Boolean.parseBoolean(getArgumentFromMap(input, "propagate", false));
        boolean fullControl = Boolean.parseBoolean(getArgumentFromMap(input, "fullControl", false));
        if (!propagate) {
            permissionMask += CfgPermissions.NoPropagation.asInteger();
        }
        if (fullControl) {
            permissionMask += CfgPermissions.FullAccess.asInteger();
        } else {
            for (int i = 0; i < permissions.length(); i++) {
                switch (permissions.charAt(i) + "") {
                    case "r":
                        permissionMask += CfgPermissions.ReadAccess.asInteger();
                        continue;
                    case "c":
                        permissionMask += CfgPermissions.CreateAccess.asInteger();
                        continue;
                    case "h":
                        permissionMask += CfgPermissions.ChangeAccess.asInteger();
                        continue;
                    case "x":
                        permissionMask += CfgPermissions.ExecuteAccess.asInteger();
                        continue;
                    case "d":
                        permissionMask += CfgPermissions.DeleteAccess.asInteger();
                        continue;
                    case "e":
                        permissionMask += CfgPermissions.ReadPermissionsAccess.asInteger();
                        continue;
                    case "p":
                        permissionMask += CfgPermissions.ChangePermissionsAccess.asInteger();

                }
            }
        }

        CfgObject objectToUpdate = (CfgObject) input.get("object");
        CfgObject account = (CfgObject) input.get("account");

        objectToUpdate.updateACL(account, permissionMask, false);
    }

    /**
     * Creates a DN. If no type is given, it defaults to Extension
     *
     * @param input mandatory (tenantDBID, name, switchDBID)
     * @param input optional (alias, folderDBID, type)
     * @return
     * @throws Exception
     */
    public CfgDN createDN(Map<?, ?> input) throws Exception {
        CfgDN dn = new CfgDN(confService);
        dn.setNumber(getArgumentFromMap(input, "name", true));
        dn.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        dn.setSwitchDBID(Integer.parseInt(getArgumentFromMap(input, "switchDBID", true)));

        if (input.containsKey("alias")) {
            dn.setName(getArgumentFromMap(input, "alias", false));
        }
        if (input.containsKey("folderDBID")) {
            dn.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        if (input.containsKey("type")) {
            dn.setType(getDNTypeFromString(getArgumentFromMap(input, "type", false)));
        } else {
            dn.setType(CfgDNType.CFGExtension);
        }

        dn.setRouteType(CfgRouteType.CFGDefault);
        dn.setSwitchSpecificType(1);
        dn.save();
        Log.getLogger().info("DN created: " + dn.getNumber());

        return dn;
    }

    /**
     * Takes a DN type string and converts to PSDK DN type object. Defaults to
     * Extension.
     *
     * @param type
     * @return
     */
    private CfgDNType getDNTypeFromString(String type) {
        switch (type.trim().toLowerCase()) {
            case "cfgextension":
                return CfgDNType.CFGExtension;
            case "cfgacdposition":
                return CfgDNType.CFGACDPosition;
            case "cfgroutingpoint":
                return CfgDNType.CFGRoutingPoint;
            case "cfgcommdn":
                return CfgDNType.CFGCommDN;
            case "cfggvpdid":
                return CfgDNType.CFGGVPDID;
            case "cfgtrunk":
                return CfgDNType.CFGTrunk;
            case "cfgtrunkgroup":
                return CfgDNType.CFGTrunkGroup;
            case "cfgvirtacdqueue":
                return CfgDNType.CFGVirtACDQueue;
            default:
                return CfgDNType.CFGExtension;
        }
    }

    /**
     * Takes an Object type string and converts to PSDK Object type object.
     * Defaults to Folder.
     *
     * @param type
     * @return
     */
    private CfgObjectType getObjectTypeFromString(String type) {
        switch (type.trim().toLowerCase()) {
            case "person":
                return CfgObjectType.CFGPerson;
            case "agentgroup":
                return CfgObjectType.CFGAgentGroup;
            case "accessgroup":
                return CfgObjectType.CFGAccessGroup;
            case "agentlogin":
                return CfgObjectType.CFGAgentLogin;
            case "application":
                return CfgObjectType.CFGApplication;
            case "callinglist":
                return CfgObjectType.CFGCallingList;
            case "campaign":
                return CfgObjectType.CFGCampaign;
            case "campaigngroup":
                return CfgObjectType.CFGCampaignGroup;
            case "dn":
                return CfgObjectType.CFGDN;
            case "dngroup":
                return CfgObjectType.CFGDNGroup;
            case "field":
                return CfgObjectType.CFGField;
            case "filter":
                return CfgObjectType.CFGFilter;
            case "folder":
                return CfgObjectType.CFGFolder;
            case "format":
                return CfgObjectType.CFGFormat;
            case "place":
                return CfgObjectType.CFGPlace;
            case "placegroup":
                return CfgObjectType.CFGPlaceGroup;
            case "script":
                return CfgObjectType.CFGScript;
            case "skill":
                return CfgObjectType.CFGSkill;
            case "tableaccess":
                return CfgObjectType.CFGTableAccess;
            case "timezone":
                return CfgObjectType.CFGTimeZone;
            case "transaction":
                return CfgObjectType.CFGTransaction;
            case "treatment":
                return CfgObjectType.CFGTreatment;
            default:
                return CfgObjectType.CFGFolder;
        }
    }

    /**
     *
     * @param input mandatory(name, tenantDBID)
     * @param input optional(folderDBID)
     * @return
     * @throws Exception
     */
    public CfgAccessGroup createAccessGroup(Map<?, ?> input) throws Exception {

        CfgAccessGroup accessGroup = new CfgAccessGroup(confService);

        CfgGroup group = new CfgGroup(confService, accessGroup);
        group.setName(getArgumentFromMap(input, "name", true));
        group.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));

        if (input.containsKey("folderDBID")) {
            accessGroup.setFolderId(Integer.parseInt(input.get("folderDBID").toString()));
        }

        accessGroup.setGroupInfo(group);
        accessGroup.save();

        Log.getLogger().info("Access group created: " + input.get("name").toString());

        return accessGroup;
    }

    /**
     * Creates a person object.
     *
     * @param input mandatory (tenantDBID, name, employeeId)
     * @param input optional (firstName, lastName, folderDBID, emailAddress,
     * password, externalId, enabled, isAgent )
     * @return
     * @throws Exception
     */
    public CfgPerson createAgent(Map<?, ?> input) throws Exception {
        CfgPerson person = new CfgPerson(confService);

        person.setUserName(getArgumentFromMap(input, "name", true));
        person.setTenantDBID(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        person.setEmployeeID(getArgumentFromMap(input, "employeeId", true));

        if (input.containsKey("firstName")) {
            person.setFirstName(getArgumentFromMap(input, "firstName", false));
        }
        if (input.containsKey("lastName")) {
            person.setLastName(getArgumentFromMap(input, "lastName", false));
        }
        if (input.containsKey("folderDBID")) {
            person.setFolderId(Integer.parseInt(getArgumentFromMap(input, "folderDBID", false)));
        }
        if (input.containsKey("emailAddress")) {
            person.setEmailAddress(getArgumentFromMap(input, "emailAddress", false));
        }
        if (input.containsKey("password")) {
            person.setPassword(getArgumentFromMap(input, "password", false));
        }
        if (input.containsKey("externalId")) {
            person.setExternalID(getArgumentFromMap(input, "externalId", false));
        }
        if (input.containsKey("enabled")) {
            if (Boolean.parseBoolean(getArgumentFromMap(input, "enabled", false)) || getArgumentFromMap(input, "enabled", false).equals("1")) {
                person.setState(CfgObjectState.CFGEnabled);
            } else {
                person.setState(CfgObjectState.CFGDisabled);
            }
        }
        if (input.containsKey("isAgent")) {
            if (Boolean.parseBoolean(getArgumentFromMap(input, "isAgent", false)) || getArgumentFromMap(input, "isAgent", false).equals("1")) {
                person.setIsAgent(CfgFlag.CFGTrue);
            } else {
                person.setIsAgent(CfgFlag.CFGFalse);
            }
        } else {
            person.setIsAgent(CfgFlag.CFGTrue);
        }

        person.save();
        Log.getLogger().info("Person: " + person.getUserName() + " created");
        return person;
    }

    /**
     *
     * @param input mandatory(name, tenantDBID)
     * @param input optional(script, folderDBID)
     * @return
     * @throws Exception
     */
    public CfgAgentGroup createAgentGroup(Map<?, ?> input) throws Exception {

        CfgAgentGroup agentGroup = new CfgAgentGroup(confService);

        CfgGroup group = new CfgGroup(confService, agentGroup);
        group.setName(input.get("name").toString());
        group.setTenantDBID(Integer.parseInt(input.get("tenantDBID").toString()));

        if (input.containsKey("script")) {

            KeyValueCollection userProperties = new KeyValueCollection();
            KeyValueCollection virtual = new KeyValueCollection();
            virtual.addString("script", input.get("script").toString());
            userProperties.addList("virtual", virtual);

            group.setUserProperties(userProperties);
        }

        if (input.containsKey("folderDBID")) {
            agentGroup.setFolderId(Integer.parseInt(input.get("folderDBID").toString()));
        }

        agentGroup.setGroupInfo(group);

        agentGroup.save();

        Log.getLogger().info("Agent group created: " + input.get("name").toString());

        return agentGroup;
    }

    /**
     *
     * @param input mandatory (name, tenantDBID)
     * @return
     * @throws Exception
     */
    public CfgAgentGroup getCfgAgentGroup(Map<?, ?> input) throws Exception {
        CfgAgentGroupQuery query = new CfgAgentGroupQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        CfgAgentGroup agentGroup = (CfgAgentGroup) confService.retrieveObject(query);
        if (agentGroup == null) {
            throw new DontExistException("No agent group with name " + input.get("name") + " exist.");
        }
        Log.getLogger().info("Returning agent group '" + agentGroup.getGroupInfo().getName() + "'.");

        return agentGroup;
    }

    /**
     *
     * @param input mandatory (name, tenantDBID)
     * @return
     * @throws Exception
     */
    public CfgAccessGroup getCfgAccessGroup(Map<?, ?> input) throws Exception {
        CfgAccessGroupQuery query = new CfgAccessGroupQuery(confService);
        query.setName(getArgumentFromMap(input, "name", true));
        query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
        CfgAccessGroup accessGroup = (CfgAccessGroup) confService.retrieveObject(query);
        if (accessGroup == null) {
            throw new DontExistException("No access group with name " + input.get("name") + " exist.");
        }
        Log.getLogger().info("Returning access group '" + accessGroup.getGroupInfo().getName() + "'.");

        return accessGroup;
    }

    /**
     * Updates a person object.
     *
     * @param input mandatory (name)
     * @param input optional (firstName, lastName, emailAddress,
     * password,externalId, enabled )
     * @return
     * @throws Exception
     */
    public CfgPerson updateAgent(Map<?, ?> input) throws Exception {

        CfgPerson person = getCfgPerson(input);

        if (person == null) {
            throw new Exception("No person with that name exist.");
        }

        if (input.containsKey("firstName")) {
            person.setFirstName(input.get("firstName").toString());
        }
        if (input.containsKey("lastName")) {
            person.setLastName(input.get("lastName").toString());
        }
        if (input.containsKey("employeeId")) {
            person.setEmployeeID(input.get("employeeId").toString());
        }
        if (input.containsKey("emailAddress")) {
            person.setEmailAddress(input.get("emailAddress").toString());
        }
        if (input.containsKey("password")) {
            person.setPassword(input.get("password").toString());
        }
        if (input.containsKey("externalId")) {
            person.setExternalID(input.get("externalId").toString());
        }
        if (input.containsKey("enabled")) {
            if (Boolean.parseBoolean(input.get("enabled").toString()) || input.get("enabled").toString().equals("1")) {
                person.setState(CfgObjectState.CFGEnabled);
            } else {
                person.setState(CfgObjectState.CFGDisabled);
            }
        }
        person.save();
        Log.getLogger().info("Person: " + person.getEmployeeID() + " updated.");
        return person;
    }

    /**
     *
     * @param input mandatory (tenantDBID, name, folderDBID)
     * @return
     * @throws Exception
     */
    public void moveAgent(Map<?, ?> input) throws Exception {

        CfgPersonQuery personQuery = new CfgPersonQuery();
        personQuery.setTenantDbid(Integer.parseInt(input.get("tenantDBID").toString()));
        personQuery.setUserName(input.get("name").toString());
        CfgPerson person = (CfgPerson) confService.retrieveObject(personQuery);

        if (person == null) {
            throw new Exception("No person with that name exist.");
        }

        CfgObjectID objectToMove = null;

        CfgFolderQuery cfgFolderSource = new CfgFolderQuery(confService);
        cfgFolderSource.setDbid(person.getFolderId());
        CfgFolder folderSource = (CfgFolder) confService.retrieveObject(cfgFolderSource);

        Iterator<CfgObjectID> it = folderSource.getObjectIDs().iterator();
        while (it.hasNext()) {

            CfgObjectID cfgObjectID = (CfgObjectID) it.next();
            if (cfgObjectID.getType() == CfgObjectType.CFGPerson && cfgObjectID.getDBID().intValue() == person.getDBID().intValue()) {
                objectToMove = cfgObjectID;
            }
        }

        if (objectToMove != null) {
            CfgFolderQuery cfgFolderDestination = new CfgFolderQuery(confService);
            cfgFolderDestination.setDbid(Integer.parseInt(input.get("folderDBID").toString()));
            CfgFolder folderDestination = (CfgFolder) confService.retrieveObject(cfgFolderDestination);

            if (folderDestination == null) {
                throw new Exception("Destination folder does not exist");
            }
            if (cfgFolderDestination.getDbid() == person.getFolderId()) {
                throw new Exception("Agent already in destination folder");
            }
            folderDestination.getObjectIDs().add(objectToMove);
            folderDestination.save();
            Log.getLogger().info("Agent " + person.getUserName() + " was moved to folder " + folderDestination.getName() + " (" + folderDestination.getDBID() + ").");
        } else {
            throw new Exception("The person does not exist in given source folder.");
        }
    }

    /**
     * Ads skills to a person. Mandatory attributes are 'name', 'tenantDBID' and
     * 'skills'
     *
     * Example:
     * {"name":"FrallBall","tenantDBID":1,"skills":{"English":2,"Swedish":3}}
     *
     * @param input
     * @return
     * @throws Exception
     */
    public void addSkillsToPerson(Map<?, ?> input) throws Exception {

        CfgPerson person = getCfgPerson(input);

        if (person == null || person.getIsAgent() != CfgFlag.CFGTrue) {
            throw new Exception("No person with that name exist.");
        }

        HashMap<String, Object> newMap = new HashMap<>();
        ScriptObjectMirror tmp = (ScriptObjectMirror) input.get("skills");
        person.getAgentInfo().getSkillLevels().clear();
        Vector<String> addedSkills = new Vector<String>();
        for (int i = 0; i < tmp.size(); i++) {
            ScriptObjectMirror skillObject = (ScriptObjectMirror) tmp.get(Integer.toString(i));

            Map<String, Object> skillMap = new HashMap();
            for (Map.Entry<String, Object> entry : skillObject.entrySet()) {
                skillMap.put(entry.getKey(), entry.getValue().toString());
            }

            // retrieve the skill and person given through their DBIDs.
            CfgSkillQuery skillQuery = new CfgSkillQuery();
            skillQuery.setName(skillMap.get("skill").toString());
            CfgSkill skill = (CfgSkill) confService.retrieveObject(skillQuery);

            if (skill == null) {
                //nonExist.add(skillMap.get("skill").toString());
                continue;
            }

            CfgSkillLevel skillLevel = new CfgSkillLevel(confService, person);
            skillLevel.setSkill(skill);
            skillLevel.setLevel(Integer.parseInt(skillMap.get("level").toString()));

            if (!addedSkills.contains(skillMap.get("skill").toString())){
                person.getAgentInfo().getSkillLevels().add(skillLevel);    
                addedSkills.add(skillMap.get("skill").toString());
                Log.getLogger().info("Adding skill '" + skillMap.get("skill").toString() + "' with level " + skillMap.get("level").toString() + " to agent " + person.getUserName());
            } else {
                Log.getLogger().info("It's only possible to add a skill once. Will not add " + skillMap.get("skill").toString() + " with level " + skillMap.get("level").toString());
            }
            

        }

        person.save();

        //Log.getLogger().info("Skills: " + updated + " updated to Person: " + input.get("name").toString());
        return;
    }

    /**
     * Takes person username, rule name or rule DBID as input. Rule name will
     * have precedence over DBID if both present. Blank name or DBID=0 will
     * remove rule.
     *
     * @param input mandatory (tenantDBID, name, ruleName OR ruleDBID)
     * @throws Exception
     */
    public void addCapacityToPerson(Map<?, ?> input) throws Exception {

        CfgPerson person = getCfgPerson(input);
        if (input.containsKey("ruleName")) {
            if (input.get("ruleName").toString().trim().length() == 0) {
                person.getAgentInfo().setCapacityRuleDBID(0);
                Log.getLogger().info("Removing capacity rule from person: " + person.getUserName());
            } else {
                CfgScriptQuery query = new CfgScriptQuery(confService);
                query.setScriptType(CfgScriptType.CFGCapacityRule);
                query.setName(input.get("ruleName").toString());
                query.setTenantDbid(Integer.parseInt(getArgumentFromMap(input, "tenantDBID", true)));
                CfgScript script = (CfgScript) confService.retrieveObject(query);
                if (script == null) {
                    throw new Exception("No capacity rule with name " + query.getName());
                }
                person.getAgentInfo().setCapacityRule(script);
                Log.getLogger().info("Adding capacity rule " + script.getName() + " to person: " + person.getUserName());
            }

        } else if (input.containsKey("ruleDBID")) {
            person.getAgentInfo().setCapacityRuleDBID(Integer.parseInt(input.get("ruleDBID").toString()));
        }
        person.save();
    }

    /**
     * Takes username and place name as input. Blank place name will remove
     * default place.
     *
     * @param input mandatory (name, placeName)
     * @throws Exception
     */
    public void addDefaultPlaceToPerson(Map<?, ?> input) throws Exception {

        CfgPerson person = getCfgPerson(input);
        if (input.containsKey("placeName")) {
            if (input.get("placeName").toString().trim().length() == 0) {
                person.getAgentInfo().setPlaceDBID(0);
                Log.getLogger().info("Removing default place from person: " + person.getUserName());
            } else {
                Map<Object, Object> placeInput = new HashMap();
                placeInput.put("name", input.get("placeName").toString());
                CfgPlace place = getCfgPlace(placeInput);
                person.getAgentInfo().setPlace(place);
                Log.getLogger().info("Adding default place " + place.getName() + " to person: " + person.getUserName());
            }
            person.save();
        } else {
            throw new Exception("No place name passed as argument");
        }
    }

    /**
     * Returns all capacity rules for given tenant
     *
     * @param input mandatory (tenantDBID)
     * @throws Exception
     */
    public ArrayList<String> getCapacityRules(Map<?, ?> input) throws Exception {

        CfgScriptQuery query = new CfgScriptQuery(confService);
        query.setScriptType(CfgScriptType.CFGCapacityRule);
        query.setTenantDbid(Integer.parseInt(input.get("tenantDBID").toString()));
        Collection<CfgScript> script = confService.retrieveMultipleObjects(CfgScript.class, query);

        ArrayList<String> returnArray = new ArrayList<String>();
        for (Iterator<CfgScript> iterator = script.iterator(); iterator.hasNext();) {
            CfgScript next = iterator.next();
            returnArray.add(next.getName());
        }
        return returnArray;
    }

    /**
     * Adds login id to person or updates wrapup if already present
     *
     * @param input mandatory (tenantDBID, switchDBID, loginId, name)
     * @param input optional (wrapup)
     * @return
     * @throws Exception
     */
    public void addLoginToPerson(Map<?, ?> input) throws Exception {

        //
        CfgAgentLoginQuery loginQuery = new CfgAgentLoginQuery(confService);
        loginQuery.setTenantDbid(Integer.parseInt(input.get("tenantDBID").toString()));
        loginQuery.setLoginCode(input.get("loginId").toString());
        CfgAgentLogin login = (CfgAgentLogin) confService.retrieveObject(loginQuery);

        if (login == null) {
            throw new Exception("There is no loginId with the name " + input.get("loginId").toString());
        }

        CfgPerson person = getCfgPerson(input);
        Collection<CfgAgentLoginInfo> logins = person.getAgentInfo().getAgentLogins();
        for (Iterator<CfgAgentLoginInfo> it = logins.iterator(); it.hasNext();) {
            CfgAgentLoginInfo next = it.next();
            if (next.getAgentLogin().getLoginCode().equals(login.getLoginCode())) {
                if (input.containsKey("wrapup") && next.getWrapupTime() == Integer.parseInt(input.get("wrapup").toString())) {
                    throw new Exception("LoginId " + login.getLoginCode() + " with wrapup " + next.getWrapupTime() + " already present on person " + person.getUserName());
                } else if (input.containsKey("wrapup")) {
                    Log.getLogger().info("Trying to remove agent login");
                    next.setWrapupTime(Integer.parseInt(input.get("wrapup").toString()));
                    person.save();
                    return;
                } else {
                    throw new Exception("LoginId " + login.getLoginCode() + " already present on person " + person.getUserName());
                }
            }
        }

        CfgAgentLoginInfo loginInfo = new CfgAgentLoginInfo(confService, person);
        loginInfo.setAgentLogin(login);
        if (input.containsKey("wrapup")) {
            loginInfo.setWrapupTime(Integer.parseInt(input.get("wrapup").toString()));
        }
        person.getAgentInfo().getAgentLogins().add(loginInfo);
        person.save();

    }
    
    /**
     * Adds a member to a role. Has to be of type CfgPerson or CfgAcccessGroup.
     *
     * @param input mandatory (name[of role], member[CfgPerson or CfgAccessGroup])
     * @return
     * @throws Exception
     */
    public void addMemberToRole(Map<?, ?> input) throws Exception { 
        CfgRole role = getCfgRole(input);
        CfgObject memberToAdd = (CfgObject) input.get("member");
        CfgRoleMember member = new CfgRoleMember(confService, role);
        member.setObjectDBID(memberToAdd.getObjectDbid());
        member.setObjectType(memberToAdd.getObjectType());
        
        role.getMembers().add(member);
        role.save();
        
    }

    /**
     * Creates a new skill (in optional folder)
     *
     * @param input mandatory (tenantDBID, name)
     * @param input optional (folderDBID)
     * @return
     * @throws Exception
     */
    public CfgSkill createSkill(Map<?, ?> input) throws Exception {

        CfgSkill skill = new CfgSkill(confService);

        if (input.containsKey("tenantDBID")) {
            skill.setTenantDBID(Integer.parseInt(input.get("tenantDBID").toString()));
        }
        if (input.containsKey("name")) {
            skill.setName(input.get("name").toString());
        }
        if (input.containsKey("folderDBID")) {
            skill.setFolderId(Integer.parseInt(input.get("folderDBID").toString()));
        }
        skill.save();

        Log.getLogger().info("Skill: " + skill.getName() + " created");
        return skill;
    }

    /**
     * Delete given person
     *
     * @param input mandatory (name)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deletePerson(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name")) {
                CfgPerson person = getCfgPerson(input);
                if (person != null) {
                    person.delete();
                    Log.getLogger().info("Person '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            //Do nothing, just return false
            Log.getLogger().error("Error caught, will return false.", exception);
        }
        return false;
    }

    /**
     * Delete given access group
     *
     * @param input mandatory (name, tenantDBID)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deleteAccessGroup(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name")) {
                CfgAccessGroup accessGroup = getCfgAccessGroup(input);
                if (accessGroup != null) {
                    accessGroup.delete();
                    Log.getLogger().info("Access group '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            //Do nothing, just return false
            Log.getLogger().error("Error caught, will return false.", exception);
        }
        return false;
    }

    /**
     * Delete given agent group
     *
     * @param input mandatory (name)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deleteAgentGroup(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name")) {
                CfgAgentGroup agentGroup = getCfgAgentGroup(input);
                if (agentGroup != null) {
                    agentGroup.delete();
                    Log.getLogger().info("Agent group '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            Log.getLogger().error("Error caught, will return false.", exception);
            //Do nothing, just return false
        }
        return false;
    }

    /**
     * Delete given place
     *
     * @param input mandatory(name, tenantDBID)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deletePlace(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name") && input.containsKey("tenantDBID")) {
                CfgPlace place = getCfgPlace(input);
                if (place != null) {
                    place.delete();
                    Log.getLogger().info("Place '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            //Do nothing, just return false
            Log.getLogger().error("Error caught, will return false.", exception);
        }
        return false;
    }

    /**
     * Delete given agent login
     *
     * @param input mandatory(tenantDBID, name, switchDBID)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deleteAgentLogin(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name") && input.containsKey("switchDBID") && input.containsKey("tenantDBID")) {
                CfgAgentLogin agentLogin = getCfgAgentLogin(input);
                if (agentLogin != null) {
                    agentLogin.delete();
                    Log.getLogger().info("LoginId '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            //Do nothing, just return false
            Log.getLogger().error("Error caught, will return false.", exception);
        }
        return false;
    }

    /**
     * Delete given DN
     *
     * @param input mandatory(tenantDBID, name, switchDBID)
     * @return true if deleted, false otherwise
     * @throws Exception
     */
    public boolean deleteDN(Map<?, ?> input) throws Exception {
        try {
            if (input.containsKey("name") && input.containsKey("switchDBID") && input.containsKey("tenantDBID")) {
                CfgDN dn = getCfgDN(input);
                if (dn != null) {
                    dn.delete();
                    Log.getLogger().info("DN '" + input.get("name") + "' deleted.");
                    return true;
                }
            }
        } catch (Exception exception) {
            //Do nothing, just return false
            Log.getLogger().error("Error caught, will return false.");
        }
        return false;
    }

    public static void main(String[] args) {

    }

    @Override
    public void close() {
        try {

            if (confService != null) {
                ConfServiceFactory.releaseConfService(confService);
            }

            if (confProtocol.getState() != ChannelState.Closed) {
                confProtocol.close();
                confService = null;
            }
            Log.getLogger().info("Disconnected properly from configserver");

        } catch (Exception e) {
            Log.getLogger().info("Exception in cleanup: " + e.toString());
        }
    }

    //Help function for sorting
    public class SortIgnoreCase implements Comparator<Object> {

        public int compare(Object o1, Object o2) {
            String s1 = (String) o1;
            String s2 = (String) o2;
            return s1.toLowerCase().compareTo(s2.toLowerCase());
        }
    }
}
