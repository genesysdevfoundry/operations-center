/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */
        
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nordicoffice.operationcenter;

import com.genesyslab.platform.commons.protocol.Message;
import com.genesyslab.platform.apptemplate.configuration.ClientConfigurationHelper;
import com.genesyslab.platform.apptemplate.configuration.ConfigurationException;
import com.genesyslab.platform.apptemplate.configuration.GCOMApplicationConfiguration;
import com.genesyslab.platform.apptemplate.configuration.IGApplicationConfiguration.IGAppConnConfiguration;
import com.genesyslab.platform.commons.connection.Connection;
import com.genesyslab.platform.commons.connection.configuration.ManagedConfiguration;
import com.genesyslab.platform.commons.protocol.ChannelState;
import com.genesyslab.platform.commons.protocol.Endpoint;
import com.genesyslab.platform.commons.protocol.MessageHandler;
import com.genesyslab.platform.commons.protocol.ProtocolException;
import com.genesyslab.platform.configuration.protocol.types.CfgAppType;
import com.genesyslab.platform.management.protocol.solutioncontrolserver.events.EventAlarmInfo;
import com.genesyslab.platform.outbound.protocol.OutboundServerProtocol;
import com.genesyslab.platform.outbound.protocol.outboundserver.requests.RequestLoadCampaign;
import com.genesyslab.platform.standby.WSConfig;
import com.genesyslab.platform.standby.WSHandler;
import com.genesyslab.platform.standby.WarmStandby;
import com.genesyslab.platform.standby.events.WSAllTriedUnsuccessfullyEvent;
import com.genesyslab.platform.standby.events.WSDisconnectedEvent;
import com.genesyslab.platform.standby.events.WSOpenedEvent;
import com.genesyslab.platform.standby.events.WSTriedUnsuccessfullyEvent;
import com.genesyslab.platform.standby.exceptions.WSException;
import com.nordicoffice.backendengine.Handler;
import com.nordicoffice.backendengine.Log;
import java.util.Iterator;

/**
 *
 * @author okaver
 */
public class OCSHandler extends WSHandler implements Handler {

    private OutboundServerProtocol protocol;
    private String ocsApplicationName;
    protected WarmStandby warmStandby;

    @Override
    public void onChannelOpened(WSOpenedEvent event) {
        String sourceClassName = event.getChannelOpenedEvent().getSource().getClass().getSimpleName();
        Log.getLogger().warn("WSOpenedEvent: " + sourceClassName + " - " + event.getChannelOpenedEvent().toString());
    }

    @Override
    public void onChannelDisconnected(WSDisconnectedEvent event) {
        String sourceClassName = event.getChannelClosedEvent().getSource().getClass().getSimpleName();
        Log.getLogger().warn("WSDisconnectedEvent: " + sourceClassName + " - " + event.getChannelClosedEvent().toString());
    }

    @Override
    public void onEndpointTriedUnsuccessfully(WSTriedUnsuccessfullyEvent event) {
        String sourceClassName = event.getChannelClosedEvent().getSource().getClass().getSimpleName();
        Log.getLogger().warn("WSTriedUnsuccessfullyEvent: " + sourceClassName + " - " + event.getChannelClosedEvent().toString());
    }

    @Override
    public void onAllEndpointsTriedUnsuccessfully(WSAllTriedUnsuccessfullyEvent event) {
        String sourceClassName = event.getSource().getClass().getSimpleName();
        Log.getLogger().warn("WSAllTriedUnsuccessfullyEvent: " + sourceClassName + " - " + event.toString());
    }

    public void connect(GCOMApplicationConfiguration appConfig, String useEncoding) throws ConfServerConnectionException {
        boolean hasOCSConnection = false;
        Log.getLogger().info("Opening connection to OCS.");
        Iterator<IGAppConnConfiguration> it = appConfig.getAppServers().iterator();
        while (it.hasNext()) {
            IGAppConnConfiguration connConfig = (IGAppConnConfiguration) it.next();
            CfgAppType type = connConfig.getTargetServerConfiguration().getApplicationType();
            if (type == CfgAppType.CFGCMServer) {
                hasOCSConnection = true;
                try {
                    ocsApplicationName = connConfig.getTargetServerConfiguration().getApplicationName();
                    Log.getLogger().warn("Enter connect() method for " + ocsApplicationName);

                    //returns configured endpoint.      
                    Endpoint endpoint = ClientConfigurationHelper.createEndpoint(appConfig, connConfig, connConfig.getTargetServerConfiguration());

                    //Helper method for new WarmStandby
                    WSConfig wsConfig = ClientConfigurationHelper.createWarmStandbyConfigEx(appConfig, connConfig);

                    //use protocol with configured endpoint
                    protocol = new OutboundServerProtocol(endpoint);

                    MessageHandler messageHandler = new MessageHandler() {
                        @Override
                        public void onMessage(Message message) {
                            if (message instanceof EventAlarmInfo) {
                                //EventAlarmInfo event = (EventAlarmInfo)message;
                            }
                            Log.getLogger().info(ocsApplicationName + " - Incoming Message: " + message);
                        }
                    };

                    ManagedConfiguration config = (ManagedConfiguration) protocol.getEndpoint().getConfiguration();
                    config.setOption(Connection.STR_ATTR_ENCODING_NAME_KEY, useEncoding);

                    protocol.setMessageHandler(messageHandler);

                    warmStandby = new WarmStandby(protocol);
                    warmStandby.setConfig(wsConfig);
                    warmStandby.setHandler(this);
                    warmStandby.open();//Connect syncronously
                } catch (ConfigurationException | InterruptedException | WSException ex) {
                    Log.getLogger().error(ex.toString());
                }
                break;
            }
        }
        if (!hasOCSConnection) {
            Log.getLogger().info("No OCS connection on application object");
        }
    }

    public void ghjk() {
        RequestLoadCampaign request = RequestLoadCampaign.create();

    }

    @Override
    public void close() {
        // Close the connection
        try {
            if (warmStandby != null) {
                Log.getLogger().info(ocsApplicationName + " connection closing...");
                warmStandby.close();
                Log.getLogger().info(ocsApplicationName + " connection closed.");

                if (protocol.getState() != ChannelState.Closed) {
                    protocol.close();
                    Log.getLogger().info(ocsApplicationName + " protocol closed.");
                }
            }
        } catch (IllegalStateException | ProtocolException | InterruptedException e) {
            Log.getLogger().error(e);
        }
    }

}
