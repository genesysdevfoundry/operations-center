/*
 * Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md
 */

package com.nordicoffice.operationcenter.servlet;

import com.google.gson.Gson;
import com.nordicoffice.backendengine.ServiceHandler;
import com.nordicoffice.backendengine.HelpFunctions;
import com.nordicoffice.backendengine.MainApplication;
import com.nordicoffice.backendengine.Log;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;


@WebServlet("/UploadServlet")
@MultipartConfig()   // 50MB
public class UploadServlet extends HttpServlet {
    Gson gson=new Gson();
    
    
    /**
     * Name of the directory where uploaded files will be saved, relative to
     * the web application directory.
     */
    private static final String SAVE_DIR = "uploadFiles";
     
    
    
    /**
     * handles file upload
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
          
        
        PrintWriter out = response.getWriter();

        try {
            
            String audioFileStorage=(String)MainApplication.getInstance().getApplicationData().get("audioFileStorage");
            
            Log.getLogger().info("Execute Upload Servlet. audioFileStorage=" + audioFileStorage);

            audioFileStorage+="/InProcess";
            
            String serviceName=request.getParameter("service");        
            String newFileName=request.getParameter("recordingFilename");        

            if (serviceName==null||serviceName.equals("")){
                out.println(HelpFunctions.GenerateErrorObject("No service to execute","No service to execute"));
                return;
            }                                    
            
            HttpSession httpSession = request.getSession();            
            ServletContext servletContext = getServletContext();   

            Map inputParasmeters=HelpFunctions.convertParameters(request);
            
            
            //Initiate service handler
            ServiceHandler serviceHandler=new ServiceHandler(inputParasmeters,httpSession,servletContext,"POST");


            
            // creates the save directory if it does not exists
            File fileSaveDir = new File(audioFileStorage);
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdir();
            }
            

            for (Part part : request.getParts()) {
                //String fileName = extractFileName(part);
                // refines the fileName in case it is an absolute path
                //fileName = new File(fileName).getName();
                part.write(audioFileStorage + "/" + newFileName);
            }

            out.print(serviceHandler.executeService(serviceName));            
            
        } catch(Exception ex){
            out.println(HelpFunctions.GenerateErrorObject("Error in processRequest. Error description: " + ex.getMessage(),"Error in processRequest"));
        }
        
    }
    /**
     * Extracts file name from HTTP header content-disposition
     */
    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length()-1);
            }
        }
        return "";
    }
}