/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

var Calendar={};
Calendar.calendarParameters=[{ 
        name:"name",
        displayName:"Name",
        mandatory:true,
        defaultValue:"New Calendar",
        type:"TEXT"
    },{       
        name:"description",
        displayName:"Description",
        mandatory:false,
        defaultValue:"",
        type:"TEXTAREA"
}];