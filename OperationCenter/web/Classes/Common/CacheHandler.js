/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Common.CacheHandler=function(){
    var cache={};
    
    this.resetCache=function(){
        cache={};
    };
        
    this.setData=function(scriptId,data){
        cache[scriptId]=data;        
    };
    
    /***
     * 1
     * 
     * @param {type} scriptId
     * @param {type} callback
     * @param {type} force
     * @returns {undefined}
     */
    this.getData=function(scriptId,callback,force){
        var output={};
        var requestString="";
        var scriptIds=[];
        var arrayInput=true;
        if (scriptId instanceof Array){
            scriptIds=scriptId;
        } else {
            scriptIds.push(scriptId);
            arrayInput=false;
        }

        

        var _handleReturnData=function(data){
            for (var key in data){
                if (arrayInput){
                   output[key]=data[key];
                } else {
                    output=data[key];
                }
                cache[key]=data[key]; 
            }
            callback(output);
        };
        
        for (var i=0;i<scriptIds.length;i++){
            if (cache[scriptIds[i]]&&!force){
                output[scriptIds[i]]=cache[scriptIds[i]];
            } else {
                requestString+=((requestString)?",":"") + scriptIds[i];
            }
        }
        
        if (requestString){
            Functions.ajaxRequest("ExecuteScripts",{scripts:requestString},_handleReturnData);
        } else {
            callback(output);                           
        }
    };
};