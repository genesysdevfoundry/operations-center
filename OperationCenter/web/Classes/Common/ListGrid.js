/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

  Classes.Common.ListGrid=function($parent,data,columns,options,callback){        
    var _this=this;
    var _firstTime=true;
    var _template="<table class=\"table table-striped table-bordered table-hover\" width=\"100%\"></table>";
    var _$parent=$parent;
    var _table;    
    var _data=data;
    var _columns=columns;
    
    this.on_DbClick=null;
    
    this.get$Panel=function(){
      return _table;
    };
    
    this.getTable=function(){
        return _table;
    };
    
    this.destroy=function(){
        _table.destroy();  
        $parent.empty();
    };
    
    this.setColumns=function(columns){
        _columns=columns;
    };
    
    this.draw=function(){
        _table.draw();
    };
    
    this.getSelectedRecord=function(){
        var selRow=_table.rows( { selected: true } );
        
        if (selRow&&selRow.data().length===1){
            //Send the underlayiing data to the db click function
            return selRow.data()[0];            
        }
        return null;
    };
    
    
    this.updateData=function(data){
        _data=data||_data;
        if (!_table){
            return;
        }
        _table.clear();
        _table.rows.add(_data);
        _table.draw();
    };
      
    
    this.createTable=function(){
        var _$table;
        _data=data||_data;
               
        
        if (!_firstTime){
            _table.destroy();  
            $parent.empty();
        }
        
        _$table=$(_template);
        _$parent.append(_$table);
        
        
        if (!options){
            options={
                paging:true,
                searching:true,
                info:true,
                ordering:true,
                lengthChange:true
            };
        } else {
            options={
                paging:typeof options.paging=="undefined"?true:options.paging,
                searching:typeof options.searching=="undefined"?true:options.searching,
                info: options.info=="undefined"?true:options.info,
                ordering:options.ordering=="undefined"?true:options.ordering,
                lengthChange:options.lengthChange=="undefined"?true:options.lengthChange
            };
        }
        
        _$parent.ready(function(){
            _table=_$table.DataTable({
                paging: options.paging,
                lengthChange: options.lengthChange,
                searching: options.searching,                
                info: options.info,
                colReorder: true,
                ordering:options.ordering,
                select: "single",                                 
                scrollX:true,
                scrollY:true,
                autoWidth: true,
                columns:_columns,
                lengthMenu: [[15,25,50,100,500,-1],[15,25,50,100,500,"All"]],
                pageLength:100,
                data:_data,
                createdRow: function( row, data, dataIndex){
                    if(data.rowCSS){
                        $(row).addClass(data.rowCSS);
                    }
                }
            });    
            
             //Add possibility to doubleclick            
            _table.on( 'dblclick', 'tr', function(){                
                //MMake sure that the row is selected ater a db click
                _table.row( this ).select();       
                
                //Find the selected row
                if (_this.on_DbClick){
                    var tmpSelData=_this.getSelectedRecord();
                    if (tmpSelData){
                        _this.on_DbClick(tmpSelData);
                    }
                }        
            });
            _$table.resize(function(){
                _table.fnAdjustColumnSizing();
            });  
            if (callback){
                callback(_$table);
            }
        });              
        _firstTime=false;
    };
 
    _this.createTable();
 
    
};