/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.FormDesigner.FormDesignerController=function(mainApplication){
    var _this=this;
    var _table; 
    var _$panel;
    var _openFormDesigners=[];
    var _idPanelCounter=0;
    var _clipboard;
    var _viewTemplates={
            formDesignerView:{
                url:"Classes/FormDesigner/FormDesignerView.html"}            
    };
    
    this.getMainApplication=function(){
        return mainApplication;
    };
    
    this.getClipboard=function(){
        return _clipboard;
    };

    this.setClipboard=function(value){
        _clipboard=value;
    };
    

    
    var _openFormDesigner=function(data){        
        //Create new script Object and store it in open surveys
        _idPanelCounter++;
        var newFormDesigner={};
        var tabId= Math.trunc(Math.random()*100000);
        data.tabIdDesign="tab_" + tabId;
        data.tabIdPreview="tab_" + (tabId +1);           
        newFormDesigner=new Classes.FormDesigner.FormDesigner(_this,data,_viewTemplates);
        newFormDesigner.id_Panel="FORM_DESIGNER_" + _idPanelCounter;
        _openFormDesigners.push(newFormDesigner);
        
         
        var tabs=[{caption:"Design",active:true,tabId:data.tabIdDesign},                
            {caption:"Preview",active:false,tabId:data.tabIdPreview,onActive:newFormDesigner.activatePreview}];

        //Create the panel
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:"Form" + " - " + data.name,
            id_Panel:newFormDesigner.id_Panel,
            icon:"fa-file-text-o",
            $panel:newFormDesigner.get$Panel(),                    
            buttons:[
                {caption:"Save",name:"btSave",onClick:newFormDesigner.save},
                {caption:"Save & Close",name:"btSaveClose",onClick:newFormDesigner.saveAndClose},
                {caption:"Close",name:"btClose",onClick:newFormDesigner.close}
            ],
            tabs:tabs,
            unsaved:(data.id_Form)?false:true
        });
        
        
    };

    var _newForm=function(data){
        _openFormDesigner({
           id_Form:0,
           name:data.name||"New Form",
           content:[],
           callback:data.callback
        });
    };
    
    
    var _formDesignerClosed=function(id_Panel){
         for (var i=0;i<_openFormDesigners.length;i++){
            if (_openFormDesigners[i].id_Panel===id_Panel){
                //Remove object from open objects
                _openFormDesigners.splice(i,1);
                break;
            }
        }
    };
    
    
    var _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_FORM_DESIGNER":          
                //Check if Object  is already opened
                for (var i=0;i<_openFormDesigners.length;i++){
                    if (_openFormDesigners[i].getFormId()===eventData){
                        //Select the survey
                        mainApplication.getEventHandler().fire("SELECT_PANEL",_openFormDesigners[i].id_Panel);
                        return;
                    }
                }
                Functions.ajaxRequest("GetForm",{id_Form:eventData},_openFormDesigner);
            break;  
            case "NEW_FORM":
                _newForm(eventData);
            break;
            case "PANEL_REMOVED":
                if (eventData.indexOf("FORM_DESIGNER_")==0){
                    _formDesignerClosed(eventData);
                }
            break;
        }
    };
        
    /**Constructor*/
     mainApplication.getEventHandler().register(_eventListener);
     Functions.multiHTMLLoad(_viewTemplates);
    
};