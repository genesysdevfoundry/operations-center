/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

/* global Classes */
Classes.MainModule.Footer=function(mainApplication,$parent){    
    var _this=this;
    var _contentURL="Classes/MainModule/FooterView.html";

    var _contentLoaded=function(){
        
    };    
    
    
    //Constructor
    $parent.load(_contentURL,_contentLoaded);        

};