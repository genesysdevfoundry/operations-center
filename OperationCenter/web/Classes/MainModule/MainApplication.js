/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

/* global Classes */
Classes.MainModule.MainApplication=function($header,$leftSidePanel,$footer,$content){
    var _this=this;
    var _leftSidePanel;
    var _eventHandler=new Classes.Common.EventHandler();
    var _cacheHandler=new Classes.Common.CacheHandler();
    var _modules=[];
    var _header
    var _footer
    var _contentController
    
    
    this.getEventHandler=function(){
        return _eventHandler;
    };
    
    this.getCacheHandler=function(){
        return _cacheHandler;    
    };
    
    this.updateSidePanel=function(){
        //Get data for the left menu
        Functions.ajaxRequest("GetSidePanelData",null,_renderSidePanel)    
    };
    
    var _renderSidePanel=function(data){
        $leftSidePanel.empty();
        _leftSidePanel=new Classes.MainModule.LeftSidePanel(_this,$leftSidePanel,data);
        _leftSidePanel.render();
    };
    
    var _loadModules=function(data){
        var classIndex=0;
        var tmpClassPart;
        var _classNameArray;
        for(var index=0;index<data.length;index++){
            tmpClassPart=Classes;
            _classNameArray=data[index].class.split(".");            
            for(classIndex=0;classIndex<_classNameArray.length;classIndex++){
                tmpClassPart=tmpClassPart[_classNameArray[classIndex]];
            }
            data[index].object=new tmpClassPart(_this);            
        }
        _modules=data;
    };
        
    var _eventListener=function(eventName,eventData){
        switch (eventName){
            case "LOG_OUT":
                Functions.ajaxRequest("LogOut",{},function(data){
                    window.location.href="index.html";
                });
            break;   
            case "UPDATE_SIDEPANEL":
                _this.updateSidePanel();
            break;
        }        
    };
    
    /**Constructor*/
    
    //Get generic data from Genesys config server
    Functions.ajaxRequest("GetUserProfile",{},function(data){
        global.allAccessGroups=data.allAccessGroups;
        global.userName=data.userName;
        global.departments=data.departments;
        global.defaultDepartment=data.defaultDepartment;
        global.promptObjectTemplateId=data.promptObjectTemplateId;
        
        _this.updateSidePanel();
        
        //Get all modules
        $.get("JSON/Modules.json?" + cacheId, _loadModules);     
        
        //Initiate event handler
        _eventHandler.register(_eventListener);
        
        //Initiate panels
        _header=new Classes.MainModule.Header(_this,$header,data.userFullName);   
        _footer=new Classes.MainModule.Footer(_this,$footer);
        _contentController=new Classes.MainModule.ContentController(_this,$content);


    });    
};