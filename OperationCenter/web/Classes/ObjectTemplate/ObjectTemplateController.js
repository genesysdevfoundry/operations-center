/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.ObjectTemplate.ObjectTemplateController=function(mainApplication){
    var _this=this;
    var _table; 
    var _$objectTemplateList;
    
    var _viewTemplates={
            objectTemplateView:{
                url:"Classes/ObjectTemplate/ObjectTemplateView.html"}          
    };
    
    var _idPanelCounter=0;
    var _openObjectTemplates=[];
        
    
    var _openObjectTemplatePanel=function(data){
         //Create new objectTemplate Object and store it in open surveys
        _idPanelCounter++;
        var newObjectTemplate={};            
        newObjectTemplate=new Classes.ObjectTemplate.ObjectTemplate(mainApplication,data,_viewTemplates);
        newObjectTemplate.id_Panel="OBJECT_TEMPLATE_" + _idPanelCounter;
        _openObjectTemplates.push(newObjectTemplate);

        //Create the panel
        mainApplication.getEventHandler().fire("ADD_PANEL",{
                name:"Object Template - " + data.name,
                id_Panel:newObjectTemplate.id_Panel,
                icon:"fa-file-text-o",                
                $panel:newObjectTemplate.get$Panel(),                    
                buttons:[
                    {caption:"Save",name:"btSave",onClick:newObjectTemplate.save},
                    {caption:"Save & Close",name:"btSaveClose",onClick:newObjectTemplate.saveAndClose},
                    {caption:"Close",name:"btClose",onClick:newObjectTemplate.close}
                ]                
        });        
    };
    
    var _openObjectTemplate=function(id_ObjectTemplate){
        //Check if survey is already opened
        for (var i=0;i<_openObjectTemplates.length;i++){
            if (_openObjectTemplates[i].getObjectTemplateId()===id_ObjectTemplate){
                //Select the survey
                mainApplication.getEventHandler().fire("SELECT_PANEL",_openObjectTemplates[i].id_Panel);
                return;
            }
        }        
        Functions.ajaxRequest("GetObjectTemplate2",{id_ObjectTemplate:id_ObjectTemplate},_openObjectTemplatePanel);
    };
  
    var _objectTemplateClosed=function(id_Panel){
        for (var i=0;i<_openObjectTemplates.length;i++){
            if (_openObjectTemplates[i].id_Panel===id_Panel){
                //Remove survey from open surveys
                _openObjectTemplates.splice(i,1);
                break;
            }
        }
    };
    
    var _newObjectTemplate=function(data){
        var newData={
            id_ObjectTemplate:0,
            name:(data&&data.name)?data.name:"New Object Template",
            description:"",
            type:""
        };
        _openObjectTemplatePanel(newData);
    };
    
    _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_OBJECT_TEMPLATE":                
                _openObjectTemplate(eventData);               
            break;
            case "PANEL_REMOVED":
                if (eventData.indexOf("OBJECT_TEMPLATE_")==0){
                    //The closed tab is a survey tab
                   _objectTemplateClosed(eventData);
                }
            break;
            case "NEW_OBJECT_TEMPLATE":
                _newObjectTemplate(eventData);
            break;                        
        }
    };
        
    /**Constructor*/
     mainApplication.getEventHandler().register(_eventListener);
     Functions.multiHTMLLoad(_viewTemplates);
    
};