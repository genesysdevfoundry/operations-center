/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.Button=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _data=data;
    var _value;
    var _name=data.name;
    var _boundData;
    var _eventChangedSubscription;
        
    this.getName=function(){
        return _name;
    };

    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getValue=function(){
        if (_data.hidden){
            return _value||"";
        }        
        return _$panel.find("input").val();
    };
    
    this.subscribeChangeEvent=function(eventHandler){
        _eventChangedSubscription=eventHandler;
    };
    
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    
    this.validate=function(){
        if (data.mandatory&&!_this.getValue()){
            return false;
            
        }
        return true;
    };
    
    
    
    this.setValue=function(value){    
    };
        
    this.populateData=function(data){    
    };        
    
    var _change=function(){    
    };
    
    var _click=function(){
        var formData=form.getValues();
        if (data.actionScriptId){
            formData.actionScriptId=data.actionScriptId;
            Functions.ajaxRequest("ExecuteActionScript",formData,function(data){    
                for (key in data){
                    formData[key]=data[key];                    
                }
                form.setValues(formData);
                if (_data.successDialogText){
                    bootbox.alert(Mustache.render(_data.successDialogText,data));
                }
            });
        } else {
            bootbox.alert("No configured action script.");
        }
    };
    
    if (!_data.hidden){
        
        _$panel=$(Mustache.render(_viewTemplates.buttonView.result,data));

        _$panel.find("button").click(_click);        
    }
    
};