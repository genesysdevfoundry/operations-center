/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.MultiSelect=function(mainApplication,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getValue=function(){
        
    };
    
    this.setValue=function(value){
        if (typeof value !=="undefined"){
            _value=value;
        } else             
        _$panel.find("select").val(_value);        
    };
    
    this.populateData=function(data){
        var el=_$panel.find("select").get(0);
        el.options.length = 0;
        if (data.length > 0)
            el.options[0] = new Option('[Select]', '');
        $.each(data, function () {
            el.options[el.options.length] = new Option(this.name, this.value);
        }); 
        _this.setValue();        
    };        
    
    _$panel=$(Mustache.render(_viewTemplates.objectListView.result,data));
    
};