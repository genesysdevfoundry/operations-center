/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.Number=function(form,data,_viewTemplates){
       var _this=this;
    var _$panel;
    var _data=data;
    var _value;
    var _name=data.name;
    var _boundData;
    var _eventChangedSubscription;
        
    this.getName=function(){
        return _name;
    };

    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getValue=function(){
        if (_data.hidden){
            return _value||"";
        } else {        
           return _$panel.find("input").val();
        }
    };
    
    this.subscribeChangeEvent=function(eventHandler){
        _eventChangedSubscription=eventHandler;
    };
    
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    this.validate=function(){
        if (data.mandatory&&!_this.getValue()){
            return false;
            
        }
        return true;
    };
    
    this.setFirstTime=function(value){
        if (_data.hidden){
            return;
        }    
        if (_data.readOnlyMode&&data.readOnlyMode==="EDIT_ONCE"){
            if (value){
                _$panel.find("input").prop('readonly', false);
            } else {
                _$panel.find("input").prop('readonly', true);
            }
        }
    };
    
    var _checkMandatory=function(){
        if (data.mandatory){
            if (_this.getValue()){
                _$panel.find(".fa-warning").hide();
            } else {
                _$panel.find(".fa-warning").show();
            }
        } else {
            _$panel.find(".fa-warning").hide();
        }
    };
    
    
    this.setValue=function(value){
        if (typeof value !=="undefined"){
            _value=value;            
        } else {
            if (_boundData){
            _boundData[_data.name]=_this.getValue();
            }
        }          
        
        if (_data.hidden){
            return;
        }
        
        _$panel.find("input").val(_value);
        _checkMandatory();
    };
    
    
    this.populateData=function(data){    
    };        
    
    var _change=function(){
        _checkMandatory();        
        if (_boundData){
            _boundData[_data.name]=_this.getValue();
        }
        
        if (_eventChangedSubscription){
            _eventChangedSubscription(_this.getValue());
        }
        
        if (_this.eventChanged){
            _this.eventChanged();
        }          
    };
    
    if (!_data.hidden){
        _$panel=$(Mustache.render(_viewTemplates.numberView.result,data));

        _$panel.find("input").keyup(_change);
        _$panel.find("input").change(_change);

        if (data.readOnlyMode&&(data.readOnlyMode==="READ_ONLY"||data.readOnlyMode==="EDIT_ONCE")){
            _$panel.find("input").prop('readonly', true);
        }
    }

    if (data.defaultValue){
        _this.setValue(data.defaultValue);
    } else {
        _this.setValue("");
    }   
    
};