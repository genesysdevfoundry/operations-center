/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.ObjectSelect=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _form=form;
    var _name=data.name;
    var _data=data;
    var _boundData;
    var _select;
    var _dataList;
    var _textValue;
    
    var _ignoreEvents=false;
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    this.eventChanged=null;
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getName=function(){
        return _name;
    };
    
    this.getValue=function(){
        if (_data.hidden){
            return _textValue;
        }
    
        if (!_this.getHiddenValue()){
            return "";
        }
        return _$panel.find("select option:selected").text();        
    };
        
    this.getHiddenValue=function(){
        if (_data.hidden){
            return _value;
        }
        
        var selValue=_select.val();        
        if (selValue&&selValue!=="__EMPTY"){
            return selValue;            
        }            
        else {            
            return "";
        }
    };
    
    this.setHiddenValue=function(value){
        var match=false;
        _value=value;
        
        if (_data.hidden){
            return;
        }
        
        if (!_value){
            _value="__EMPTY";
        } else {
        
            for (var i=0;i<_dataList.length;i++){
                if (_dataList[i].id + ""===value + ""){
                    match=true;
                    break;
                }
                if (_dataList[i].children){                   
                    //Check within a group 
                    for (var n=0;n<_dataList[i].children.length;n++){
                        if (_dataList[i].children[n].id + ""===value + ""){
                            match=true;
                            break;
                        }
                    }
                }
            }

            if (!match){
                _dataList.push({
                    text: 'Not in List',
                    children: [{id:value,text:_textValue}]
                });
                //Refresh the control
                if (_select){
                    _$panel.find(".select2").empty();
                }                         
                _select=_$panel.find(".select2").select2({data:_dataList});
                _select.select2().on("change",_changed);
            }
        }
                        
        if (value){
            _ignoreEvents=true;
            _select.val(value).trigger("change");
            _ignoreEvents=false;
        }        
        _checkMandatory();
    };
    
    this.setValue=function(value){
        _textValue=value;
    };
    
    var _fillData=function(){
        _dataList=[{id:"__EMPTY",text:"[None]"}];
        
        //Add list data
        if (data.listData&&data.listData.length>0){
            for (var i=0;i<data.listData.length;i++){
                _dataList.push({id:data.listData[i].text,text:data.listData[i].text});
            }
        }
        
        //Add script data
        if (data.scriptData.length>0){            
            var groupName;
            var children;        
            for (var i=0;i<data.scriptData.length;i++){
                if (data.scriptData[i].group){
                    if (groupName!==data.scriptData[i].group){
                        groupName=data.scriptData[i].group;
                        children=[];
                        _dataList.push({text:groupName,children:children});
                    }
                    children.push({id:data.scriptData[i].id,text:data.scriptData[i].text});
                } else {
                    _dataList.push({id:data.scriptData[i].id,text:data.scriptData[i].text});
                }
                
            }            
        }
        
        if (_select){
            _$panel.find(".select2").empty();
        }                         
        _select=_$panel.find(".select2").select2({
            data:_dataList,
            dropdownParent: (_form.isModal()?_$panel:false),  //Needs to be true for modal dialog windows
            disabled: (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY")
        }).on("change",_changed);        
    };    
    
    this.validate=function(){
        if (data.mandatory&&!_this.getValue()){
            return false;            
        }
        return true;
    };
    
    var _checkMandatory=function(){
        if (data.mandatory){
            if (_this.getValue()){
                _$panel.find(".fa-warning").hide();
            } else {
                _$panel.find(".fa-warning").show();
            }
        } else {
            _$panel.find(".fa-warning").hide();
        }
    };
    

    var _newObjectCallback=function(id){
        _form.getMainApplication().getCacheHandler().getData("NAME_KVPListGetObjects|" + data.id_ObjectTemplate,function(scriptData){
            data.scriptData=scriptData;
            _fillData();
            _this.setHiddenValue(id);
        },true);
    };
    
    var _changed=function(){
        if (_ignoreEvents){
            return;
        }
        _value=_this.getHiddenValue();
        if (_boundData){
            _boundData[data.name]=_this.getValue();
            _boundData["_" + data.name]=_value;
        }
        
        _this.eventChanged();
        _checkMandatory();
    };
    
    /***
     * Constructor
     */
    
    if (!_data.hidden){

        _$panel=$(Mustache.render(_viewTemplates.scriptSelectView.result,data));

        //Press reload button
        _$panel.find("button[name='btReload']").click(function(){
            _form.getMainApplication().getCacheHandler().getData("NAME_KVPListGetObjects|" + (data.id_ObjectTemplate||data.objectTemplateName),function(scriptData){
                data.scriptData=scriptData;
                _fillData();
                _this.setHiddenValue(_value);
            },true);
        });

        //Press open button
        _$panel.find("button[name='btOpen']").click(function(){        
            var id_Object=_this.getHiddenValue();
            if (id_Object){
                _form.getMainApplication().getEventHandler().fire("OPEN_OBJECT", {id_Object:_this.getHiddenValue()});
            }        
        });

        //Press new button
        _$panel.find("button[name='btNew']").click(function(){            
            if (data.readOnlyMode&&data.readOnlyMode==="READ_ONLY"){
                bootbox.alert("The parameter is in read only mode");
                return;
            }
            
            _form.getMainApplication().getEventHandler().fire("NEW_OBJECT", {
                id_ObjectTemplate:data.id_ObjectTemplate,
                callback:_newObjectCallback,
                name:(form.getValue("name")||"Object") + " " + data.displayName
            });
        });        
        _fillData();
        _checkMandatory();
        
    }
};