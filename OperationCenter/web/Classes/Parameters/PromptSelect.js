/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.PromptSelect=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _hiddenValue;
    var _form=form;
    var _data=data;
    var _name=data.name;
    var _boundData;
    var _disableEvents=false;
    var _prompts=[];
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    this.getName=function(){
        return _name;
    };
    
    this.getData=function(){
        return data;
    };
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getForm=function(){
        return form;
    };
    
    this.validate=function(){        
        if (_this.getValue()===""&&data.mandatory){
            return false;
        }        
        return true;
    };
    
        
    this.setValue=function(value){
        _value=value;
    };
    
    this.removePrompt=function(panel){
        panel.get$Panel().remove();
        for (var i=0;i<_prompts.length;i++){
            if (Object.is(_prompts[i],panel)){
                _prompts.splice(i,1);   
                break;
            }
        }
        _changed();
    };
            
    this.setHiddenValue=function(value){
        _hiddenValue=value;
        var emptyData=true;
        if (_data.hidden){            
            return;
        }
        var newPrompt;
        _prompts=[];
        _$panel.find(".prompt-object-container").empty();
        if (_hiddenValue){
            for(var i=0;i<_hiddenValue.length;i++){                
                emptyData=false;
                newPrompt=new Classes.Parameters.PromptSelectPrompt(_this,data,_viewTemplates,_hiddenValue[i],_form);                                
                newPrompt.eventChanged=_changed;                                
                _prompts.push(newPrompt);
                newPrompt.setIndex(_prompts.length);
                _$panel.find(".prompt-object-container").append(newPrompt.get$Panel());
                
            }
        }
        _checkMandatory();
        
        if (emptyData){
            _addPrompt();
        }
        
    };    
        
    var _checkMandatory=function(){               
        
        if (data.mandatory){
            if (_this.getValue()){
                _$panel.find(".fa-warning").hide();
            } else {
                _$panel.find(".fa-warning").show();
            }
        } else {
            _$panel.find(".fa-warning").hide();
        }
    };    
    
    this.getValue=function(){
        if (_data.hidden){
            return _value||"";
        }
        
        var returnStr="";
        for (var i=0;i<_prompts.length;i++){
            var val=_prompts[i].getValue();
            if (val){
                if ($.isNumeric(val)){
                    returnStr+=((i)?",":"") + Function.getFileName(val);
                } else {
                    returnStr+=((i)?",":"") + val;
                }               
            }
        }
        return returnStr;        
    };
    
    this.getHiddenValue=function(){
        if (_data.hidden){
            return _hiddenValue||[];
        }
        var returnStr=[];
        for (var i=0;i<_prompts.length;i++){
            var val=_prompts[i].getValue();
            if (val){
               returnStr.push(val);
            }
        }
        return returnStr;
    };
    
    var _addPrompt=function(){
       
        var newPrompt=new Classes.Parameters.PromptSelectPrompt(_this,data,_viewTemplates,null,_form);
        _prompts.push(newPrompt);
        newPrompt.setIndex(_prompts.length);
        newPrompt.eventChanged=_changed;
        _$panel.find(".prompt-object-container").append(newPrompt.get$Panel());
    };

    this.getNewPromptNamePrefix=function(){
        return (form.getValue("name")||"Prompt") + " " + data.displayName;
    };
    
    var _changed=function(){
        _checkMandatory();        
        if (_boundData){
            _boundData[_data.name]=_this.getValue();
            _boundData["_" + data.name]=_this.getHiddenValue();
        }
        
        if (_this.eventChanged){
            _this.eventChanged();
        }          
    };
    

    if (!_data.hidden){
            
    
        _$panel=$(Mustache.render(_viewTemplates.promptSelectView.result,data));
        _$panel.find("button[name='btAddPrompt']").click(_addPrompt);
        if (_data.readOnlyMode&&_data.readOnlyMode==="READ_ONLY"){
            _$panel.find("button[name='btAddPrompt']").prop('disabled', true);
        }
        //_$panel.find("a[name='btLinkPrompt']").click(_linkPrompt);
        _checkMandatory();
    }
    
};