/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.Parameters.Select=function(form,data,_viewTemplates){
    var _this=this;
    var _$panel;
    var _value;
    var _form=form;
    var _name=data.name;
    var _data=data;
    var _boundData;
    var _eventChangedSubscription;
    var _select;
    var _ignoreEvents=false;
    var _dataList;
    
    this.bindData=function(data){
        _boundData=data;
    };
    
    this.eventChanged=null;
    
    
    this.subscribeChangeEvent=function(eventHandler){
        _eventChangedSubscription=eventHandler;
    };
        
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getName=function(){
        return _name;
    };
    
    this.getValue=function(){   
        if (_data.hidden){
            return _value||"";
        }
        
        var selValue=_select.val();        
        if (selValue){
            if (selValue==="__EMPTY"){
                return "";
            } else {
                return selValue;
            }
            
        } else {
            return "";
        }
    };

    
    this.setValue=function(value){
        if (typeof value !=="undefined"){
            _value=value;          
        }        
        
        if (_data.hidden){
            return;
        }
        if (!_value){
            if (_data.multiselect){
                _value=[];
            } else {
                _value="__EMPTY";
            }          
        }
        
        if (_value!=="__EMPTY"){
            if (_data.multiselect){
                //Check if all selected values exists in the list
                var match=false;
                var notInList=[];
                for (var n=0;n<_value.length;n++){
                    match=false;                                
                    for (var i=0;i<_dataList.length;i++){
                        if (_dataList[i].id){
                            if (_dataList[i].id +""===_value[n]+""){
                                match=true;
                                break;
                            }
                        } else {
                            if (_dataList[i].children){
                                for (var j=0;j<_dataList[i].children.length;j++){
                                    if (_dataList[i].children[j].id +""===_value[n]+""){
                                        match=true;
                                        break;
                                    }
                                }
                            }
                        }                        
                    }

                    if (!match){
                        notInList.push({id:_value[n],text:_value[n]});
                    }
                }
                if (notInList.length>0){
                    _dataList.push({
                        text: 'Not in List',
                        children: notInList
                    });
                }
                this.populateData(_dataList);
            } else {
                //Check if selected value exists in the list
                var match=false;
                for (var i=0;i<_dataList.length;i++){
                    if (_dataList[i].id){
                        if (_dataList[i].id +""===_value+""){
                            match=true;
                            break;
                        }
                    } else {
                        if (_dataList[i].children){ //Loop through the option grooup
                            for (var j=0;j<_dataList[i].children.length;j++){
                                if (_dataList[i].children[j].id +""===_value+""){
                                    match=true;
                                    break;
                                }
                            }
                        }
                    }             
                }

                if (!match){
                    _dataList.push({
                        text: 'Not in List',
                        children: [{id:_value,text:_value}]
                    });
                }
                this.populateData(_dataList);
            }
        }
        
        _ignoreEvents=true;
        _select.val(_value).trigger("change");
        _ignoreEvents=false;        
        _checkMandatory();
    };
    
    this.populateData=function(data){
        if (!data||!data instanceof Array){
            data=[{id:"[No Data]",text:"No Data"}];            
        }
        _dataList=data;
        if (_dataList.length>0&&typeof _dataList[0]==="string"){
            var newDataList=[];
            for(var i=0;i<_dataList.length;i++){
                newDataList.push({id:_dataList[i],text:_dataList[i]});
            }
            _dataList=newDataList;
        }
        
        var tmpListData=data.slice(0);
        if (!_data.multiselect&&tmpListData.length>0&&tmpListData[0].id!=="__EMPTY"){
            tmpListData.unshift({id:"__EMPTY",text:"[None]"});
        }
        if (_select){
            _$panel.find(".select2").empty();
        }
        
        _select=_$panel.find(".select2").select2({
            data:tmpListData,
            dropdownParent: (_form.isModal()?_$panel:false),  //Needs to be true for modal dialog windows
            disabled: (_data.readOnlyMode&&_data.readOnlyMode==="READ_ONLY")            
        }).on("change",_changed);
        
        

    };        
    
    this.validate=function(){
        if (data.mandatory&&!_this.getValue()){
            return false;            
        }
        return true;
    };
    
    var _changed=function(){
        _value=_this.getValue();
        if (_ignoreEvents){
            return;
        }
        if (_boundData){
            _boundData[data.name]=_this.getValue();
        }
        
        if (_eventChangedSubscription){
            _eventChangedSubscription(_this.getValue());
        }
        
        _this.eventChanged();
        _checkMandatory();
    };
    
    var _checkMandatory=function(){
        if (data.mandatory){
            if (_this.getValue()){
                _$panel.find(".fa-warning").hide();
            } else {
                _$panel.find(".fa-warning").show();
            }
        } else {
            _$panel.find(".fa-warning").hide();
        }
    };
    
    var _getMergedData=function(){
        if (data.listData&&data.listData.length){
            if (data.scriptData){
                //Merge data
                var newData=[];
                for (var i=0;i<data.listData.length;i++){
                    newData.push(data.listData[i]);
                }
                for (var i=0;i<data.scriptData.length;i++){
                    newData.push(data.scriptData[i]);
                }
                return newData;
            } else {
                return data.listData;
            }            
            
        } else {
            return data.scriptData;
        }
    };
    
    
    /***
     * Constructor
     */
    if (!_data.hidden){
        
        
        _$panel=$(Mustache.render(_viewTemplates.selectView.result,data));

        //Press reload button
        _$panel.find("button[name='btReload']").click(function(){        
            _form.getMainApplication().getCacheHandler().getData(data.scriptRequestString,function(scriptData){
                data.scriptData=scriptData;
                _this.populateData(_getMergedData());
                _this.setValue();
            },true);

        });



        _this.populateData(_getMergedData());


        if (data.defaultValue){
            _this.setValue(data.defaultValue);
        }    
        _checkMandatory();
    } else {
        _value=data.defaultValue;
    }
};