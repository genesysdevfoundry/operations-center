/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.PromptManagement.Prompt=function(objectHandlerController,data,viewTemplates){
    var _this=this;
    var _$panel=null;    
    var _id_Object=data.values.id_Object;
    var _data=data;
    var _name=data.values.name;    
    var _id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;
    var _objectHandlerController=objectHandlerController;
    var _form;
    var _selectDepartmentHandler;
    var _languagePromptObjects=[];
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.saveAndClose=function(){
        _this.save(_this.close);
    };
    
    this.close=function(){
        objectHandlerController.getMainApplication().getEventHandler().fire("REMOVE_PANEL",_this.id_Panel);
    };
    
    this.getObjectId=function(){
        return _id_Object;
    };
    
    this.getObjectHandlerController=function(){
        return objectHandlerController;
    };
    
    this.getObjectTemplateId=function(){
        return _id_ObjectTemplate;
    };
    
    this.setBtRevertHandle=function(value){
        value.hide();
    };
    
     /**
     * Is set by contentController. 
     * _selectDepartmentHandler is a handler to the department select control in the tool bar.
     * @param {type} value
     * @returns {undefined}
     */
    
    this.setSelectDepartmentHandler=function(value){
        _selectDepartmentHandler=value;     
        if (_data.values.id_Department){
            _selectDepartmentHandler.val(_data.values.id_Department);
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        } else {
            _selectDepartmentHandler.val(objectHandlerController.getLastDepartmentId());
        }    
    };
    
    this.save=function(callback){   
       
        var saveData=_form.getValues();
        
        saveData.newObject=(_data.newObject)?true:false;
        
        saveData.id_Object=_id_Object;
        saveData.id_ObjectTemplate=_data.objectTemplate.id_ObjectTemplate;        
        saveData.languagePrompts=[];
        
        for (var i=0;i<_languagePromptObjects.length;i++){
            saveData.languagePrompts.push(_languagePromptObjects[i].getValue());
        }
        
        
        if (_selectDepartmentHandler){
            saveData.id_Department=_selectDepartmentHandler.val();
            objectHandlerController.setLastDepartmentId(_data.values.id_Department);
        }
        
        Functions.ajaxRequest("SaveObject",saveData,function(data){  
            
            if (!_id_Object){
                _id_Object=parseInt(data.id_Object);                
            }
            
            _data.newObject=false;
            
            _data.values=saveData;
            
            if (_name!==saveData.name){
                _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_NAME_CHANGED",{panelId:_this.id_Panel,name:_data.objectTemplate.name + " - " + saveData.name});
                _name=saveData.name;
            }
            
            _form.isSaved();   
            for (var i=0;i<_languagePromptObjects.length;i++){
                _languagePromptObjects[i].isSaved();
            }
                
            
            _$panel.find(".box").first().removeClass("box-warning").addClass("box-primary");
            
            
            _objectHandlerController.getMainApplication().getEventHandler().fire("UPDATE_OBJECT_LIST",_data.objectTemplate.id_ObjectTemplate);
        
            _objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_SAVED",_this.id_Panel);
            
            if (callback&&typeof(callback)==="function"){
                callback();
            }
            //Callback points back to the object select parameter, when the "new" button is clicked
            if (_data.callback){
                _data.callback(_id_Object);
                _data.callback=null;
            }
        });
    };
    
    
    this.formChanged=function(){
        _$panel.find(".box").first().removeClass("box-primary").addClass("box-warning"); 
        objectHandlerController.getMainApplication().getEventHandler().fire("PANEL_UNSAVED",_this.id_Panel);
    };
   
    var _fillLanguagePrompts=function(data){        
        
        for (var i=0;i<data.length;i++) {
            var languageData={
                language:data[i].name,
                languageCode:data[i].languageCode,
                prompt_Text:"",
                prompt_File:""
            };
        
            if (_data.values.languagePrompts){                      
                for (var n=0;n<_data.values.languagePrompts.length;n++){
                    if (_data.values.languagePrompts[n].languageCode===data[i].languageCode){
                        languageData.prompt_Text=_data.values.languagePrompts[n].prompt_Text;
                        languageData.prompt_File=_data.values.languagePrompts[n].prompt_File;
                        
                    }
                }            
            }
            var languagePromptObject=new Classes.PromptManagement.PromptLanguage(_this,languageData,viewTemplates);
            _$panel.find(".prompt-language-section").append(languagePromptObject.get$Panel());
            _languagePromptObjects.push(languagePromptObject);
        }        
    };
           
    _$panel=$(Mustache.render(viewTemplates.promptView.result,data.values));
    
    Functions.ajaxRequest("GetLanguages",null,_fillLanguagePrompts);
    
    var params={content:PromptManagement.promptParameters};
    _form=new Classes.DynamicForm.Form(objectHandlerController.getMainApplication(),params,true);
    _form.renderForm(function(){
        _$panel.find(".prompt-form").append(_form.get$Panel());
        _form.bindData(data.values);
        _form.eventChanged=_this.formChanged;
    });  
};