/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.PromptManagement.PromptList=function(mainApplication){
    var _this=this;
    var _table;    
    var _viewTemplates={
            promptListView:{
                url:"Classes/PromptManagement/PromptListView.html"},
          };
    var _$panel=null;
      
    var _updatePromptList=function(){
        Functions.ajaxRequest("GetPrompts",{},function(data){
            _table.updateData(data);
        });
    };
    
    
     var _renderAudioFile=function(data){
        if (data){            
            return "<button class='btn btn-default recording-button' data-link='" + data + "' style=\"padding-top:0;padding-bottom:0;\">RecId: " + data.replace(".wav","") + "</button>";          
        } else {
            return data;        
        }        
    };
    
    var _playRecording_Click=function(event){
        var filename=$(this).attr("data-link");
        Functions.ajaxRequest("PrepareMP3File",{filename:filename},function(data){            
            mainApplication.getEventHandler().fire("PLAY_PROMPT",{filename:data.tmpFilename});
        });
        event.stopPropagation();
    };
    
    var _drawPanel=function(data){                
        _$panel=$(_viewTemplates.promptListView.result);
        
        var columns=[
            {data:"name",title:"Name"},
            {data:"prompt_File",title:"Audio file",render:_renderAudioFile},
            {data:"prompt_Text",title:"Prompt Text"},
            {data:"description",title:"Description"},            
            {data:"created",title:"Created"},
            {data:"createdBy",title:"Created By"},
            {data:"changed",title:"Changed"},
            {data:"changedBy",title:"Changed By"}
        ];
        
        _table=new Classes.Common.ListGrid(_$panel.find(".table-container"),data,columns,false,function(_$table){
            _$table.find("tbody").on( 'click', 'button', _playRecording_Click);
        });
        
        _table.on_DbClick=_btOpenPrompt_Click;        
                        
        mainApplication.getEventHandler().fire("ADD_PANEL",{
            name:"Prompt List",
            id_Panel:"PROMPT_LIST",
            icon:"fa-files-o",
            $panel:_$panel,
            buttons:[
                {caption:"Open",name:"btOpen",onClick:_btOpenPrompt_Click},
                {caption:"New",name:"btNew",onClick: function(){mainApplication.getEventHandler().fire("NEW_PROMPT",{});}},
                {caption:"Delete",name:"btDelete",onClick:_btDeletePrompt_click},
                {caption:"Reload",name:"btReload",onClick:_updatePromptList}
            ]
        });
        
    };
    
    var _btDeletePrompt_click=function(){
        var selData=_table.getSelectedRecord();
        if (selData){
           bootbox.confirm("Are you sure you want to delete selected prompt?", function(data){
                if (data){
                    Functions.ajaxRequest("DeletePrompt",{id_Prompt:selData.id_Prompt},function(){
                        _updatePromptList();
                    });
                }
            });   
        }
    };
    
    var _btOpenPrompt_Click=function(){
        var selData=_table.getSelectedRecord();        
        if (selData){
           mainApplication.getEventHandler().fire("OPEN_PROMPT",selData.id_Prompt);                      
        }
    };

    var _eventListener=function(eventName,eventData){
        switch (eventName){
            case "OPEN_PROMPT_LIST":          
                if (_table&&_$panel){
                    mainApplication.getEventHandler().fire("SELECT_PANEL","PROMPT_LIST");
                } else {
                    Functions.ajaxRequest("GetPrompts",{},_drawPanel);
                }
            break;
            case "UPDATE_PROMPT_LIST":
                if (_table&&_$panel){
                   _updatePromptList();
                }
            break;
            case "PANEL_REMOVED":
                if (eventData==="PROMPT_LIST"){
                    _table.destroy();
                    _table=null;
                    _$panel=null;
                }
            break;
        }
    };
        
    /**Constructor*/
    mainApplication.getEventHandler().register(_eventListener);
    Functions.multiHTMLLoad(_viewTemplates);
};

