/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Classes.WorkflowDiagram.WorkflowDiagram=function(parent,viewTemplates,$diagramPanel){
    var _this=this;
    var _$diagramPanel=$diagramPanel;    
    var _jsPlumb;
    var _parent=parent;
    var _$panel=null;            
    var _blocks=[];
    var _idPanelCounter=0;
    var _blockCounter=0;
    
    this.get$Panel=function(){
        return _$panel;
    };
    
    this.getBlockCounter=function(){
        return _blockCounter;
    };
    
    this.get$DiagramPanel=function(){        
        return _$diagramPanel;
    };
    
    this.getJsPlumb=function(){         
        return _jsPlumb;
    };
        
    this.getParent=function(){
        return _parent;
    };
    
    this.clear=function(){
        _jsPlumb.reset();  
        _blocks=[];
        $diagramPanel.empty();
        $diagramPanel.append($("<div class=\"rubberband\" style=\"display:none;\"></div>"));
        
    };
    
    this.isSaved=function(){
        _$diagramPanel.find(".workflow-block").removeClass("workflow-block-changed");
    };
            
    this.getData=function(){
        var returnData={};
        for(var i=0;i<_blocks.length;i++){
            if (!_blocks[i].getDeletedFlag()){
                returnData["BLOCK_" + _blocks[i].getIndex()]={
                    positionData:_blocks[i].getPositionData(),
                    values:_blocks[i].getData().values,
                    id_Object:_blocks[i].getData().id_Object,
                    subRoutine:_blocks[i].getData().subRoutine,
                    connections:_getConnectionObject(_blocks[i].getConnections())
                };                
            }
        }
        return returnData;
    };
    

    
    this.getSelectedBlocks=function(){
        var returnData={};
        for(var i=0;i<_blocks.length;i++){                        
            if (!_blocks[i].getDeletedFlag()&&_blocks[i].get$Panel().hasClass("workflow-block-selected")){
                returnData["BLOCK_COPY_" + _blocks[i].getIndex()]={
                    positionData:_blocks[i].getPositionData(),
                    values:_blocks[i].getData().values,
                    id_Object:_blocks[i].getData().id_Object,
                    subRoutine:_blocks[i].getData().subRoutine,
                    connections:_getConnectionObject(_blocks[i].getConnections(),true)
                };                
            }
        }
        return returnData;
    };
    
    
    this.setData=function(data,selectResult){
        
        //Create blocks
        for (var key in data){
            if(key.indexOf("BLOCK_")===0){
                if (data[key].values&&data[key].values.description){
                    data[key].description=data[key].values.description||"";  //Is used only for the mustach template
                } else {
                    data[key].description="";
                }
                if (data[key].id_Object){                
                    var blockTemplate=parent.getBlockTemplatesById(data[key].id_Object);
                    if (blockTemplate){
                        data[key].name=blockTemplate.name;
                        data[key].icon=blockTemplate.icon;
                        data[key].form=blockTemplate.form;
                        data[key].subRoutine=blockTemplate.subRoutine;
                        data[key].connectors=blockTemplate.connectors;                    
                        data[key].id=_createBlock(data[key],data[key].positionData);
                    }
                } else {
                    //The input block
                    data[key].id=_createBlock({name:"Input",icon:"fa-arrow-circle-o-right",hideRemoveButton:true,description:"",connectors:[{connector:"Next"}]},data[key].positionData);
                }            
            }
        }
        
        //Create connections
        for (var key in data){
            if(key.indexOf("BLOCK_")===0){
                for (var connectionName in data[key].connections){
                    data[key].connections[connectionName];
                    var sourceBlockId=data[key].id;
                    var targetBlockId=(data[data[key].connections[connectionName]])?data[data[key].connections[connectionName]].id:0;

                    if (targetBlockId){
                        var connection=_jsPlumb.connect({                          
                            source:sourceBlockId,
                            target:targetBlockId,
                            anchors:["Continuous","AutoDefault"],
                            connectionType:"basic",                                 
                            paintStyle: { stroke: "#5c96bc", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },     
                            connectorStyle: { stroke: "#5c96bc", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },                        
                            ConnectionOverlays: [
                                [ "Arrow", {
                                    location: 1,
                                    id: "arrow",
                                    length: 14,
                                    foldback: 0.8
                                } ],
                                [ "Label", { label: "", id: "label", cssClass: "workflow-connector-label" }]
                            ]
                        });


                        if (connectionName.toString().toLowerCase()==="next"||connectionName===""){                                            
                            connection.removeOverlays("label");
                        } else {
                            connection.getOverlay("label").setLabel(connectionName);
                        }
                    }
                }
            }
        }    
        if (selectResult){
            
            this.resetSelection();
            for (var key in data){ 
                if(key.indexOf("BLOCK_")===0){
                    _$diagramPanel.find("#" + data[key].id).addClass("workflow-block-selected").addClass("workflow-block-changed");
                    _jsPlumb.addToDragSelection(data[key].id);
                }
            }
        }
    };
    
    var _getConnectionObject=function(connectors,onlySelected){ //onlySelected is used when we want to copy a mutiple blocks
        var returnData={};        
        for (var i=0;i<connectors.length;i++){
            var connectorName;
            if (connectors[i].getOverlay("label")){
                connectorName=connectors[i].getOverlay("label").getLabel();
            } else {
                connectorName="Next";
            }
            if (onlySelected){
                var tmpBlock=_getBlockById(connectors[i].targetId);
                if (tmpBlock&&tmpBlock.get$Panel().hasClass("workflow-block-selected")){
                    returnData[connectorName]="BLOCK_COPY_" + _getBlockById(connectors[i].targetId).getIndex();
                }                
            } else {
                returnData[connectorName]="BLOCK_" + _getBlockById(connectors[i].targetId).getIndex();
            }            
        }
        return returnData;
    };
    
    var _createBlock=function(blockData,positionData){
        
        
        var newBlock=new Classes.WorkflowDiagram.WorkflowBlock(_this,blockData,viewTemplates,positionData,_blockCounter);        
        _blockCounter++;        
        _blocks.push(newBlock);          
        
        return newBlock.getId();
        
    };
    
    var _getBlockById=function(id){
        for(var i=0;i<_blocks.length;i++){
            if (id===_blocks[i].getId()){
                return _blocks[i];
            }
        }
    };
    
    var _deleteConnection=function(connection){
        _jsPlumb.deleteConnection(connection);
        CustomModalDialog.modal("hide");
        parent.formChanged();
    };
    
    var _changeConnector=function(connection,newValue){        
        CustomModalDialog.modal("hide");
        parent.formChanged();
        connection.getOverlay("label").setLabel(newValue);        
    };
    
    var _showConnectionDialog=function(connection){
        if (!connection.getOverlay("label")){
            _deleteConnection(connection);
        }
        
        CustomModalDialog.find(".modal-dialog").empty();
        CustomModalDialog.find(".modal-dialog").append($(viewTemplates.connectionDialogView.result));
        
        
        
        var selectedLabel=connection.getOverlay("label").getLabel();
        var dataList=_getBlockById(connection.sourceId).getNonConnectedConnectors(selectedLabel);
        
        
                
        var select2=CustomModalDialog.find("select[name='selectConnectors']").select2({
            data:dataList            
        });
        $(function(){
           select2.val(selectedLabel).trigger("change");
        });
        
        
        CustomModalDialog.find("button[name='btOk']").click(function(){
            var value=select2.val();            
            _changeConnector(connection,value);
            
        });        
        CustomModalDialog.find("button[name='btDelete']").click(function(){
            _deleteConnection(connection);
        });                
        
        CustomModalDialog.modal("show");        
    };
    
    
    this.newWorkflow=function(){
        _blockCounter=0;
        _createBlock({name:"Input",icon:"fa-arrow-circle-o-right",hideRemoveButton:true,description:"",connectors:[{connector:"Next"}]},{top:5,left:5});
    };
    
    
    
    /***
     * 
     * Rubberband
     * 
     */   
       
    var startPoint = {};
    var rubberbandDrawingActive = false;
        
    var diagramContainer_MouseDown=function (event) {
        var offset = $(this).offset();
        startPoint.x = event.pageX-offset.left;         
        startPoint.y = event.pageY-offset.top;

        _$diagramPanel.find(".rubberband").css({top:startPoint.y, left:startPoint.x, height:1, width:1, position:'absolute'});
        _$diagramPanel.find(".rubberband").show();
    };
        
    var diagramContainer_MouseMove=function(event) {
            if (_$diagramPanel.find(".rubberband").is(":visible") !== true) { return; }
            var offset = $(this).offset();
            var t = (event.pageY - offset.top> startPoint.y) ? startPoint.y : event.pageY - offset.top;
            var l = (event.pageX - offset.left>= startPoint.x) ? startPoint.x : event.pageX- offset.left;
            
            wcalc = event.pageX - offset.left- startPoint.x;
            var w = (event.pageX - offset.left> startPoint.x) ? wcalc : (wcalc * -1); 
            
            hcalc = event.pageY - offset.top - startPoint.y;
            var h = (event.pageY - offset.top > startPoint.y) ? hcalc : (hcalc * -1); 
            
            _$diagramPanel.find(".rubberband").css({top:t, left:l, height:h, width:w, position:'absolute'});
        };
        
    var diagramContainer_MouseUp=function(event) {
            diagramContainer_FindSelectedItem();
            _$diagramPanel.find(".rubberband").hide();
        };
        
        
    this.resetSelection=function(){
        //Reset 
        _jsPlumb.clearDragSelection();
        _$diagramPanel.find(".workflow-block").each(function() {
            $(this).removeClass("workflow-block-selected");
        });  
    };
    
    var diagramContainer_FindSelectedItem=function() {
            if(_$diagramPanel.find(".rubberband").is(":visible") !== true) { return; }
            
            var rubberbandOffset = getTopLeftOffset(_$diagramPanel.find(".rubberband"));

            _this.resetSelection();
      
            //Select
            _$diagramPanel.find(".workflow-block").each(function() {
                var itemOffset = getTopLeftOffset($(this));
                if( itemOffset.top > rubberbandOffset.top &&
                    itemOffset.left > rubberbandOffset.left &&
                    itemOffset.right < rubberbandOffset.right &&
                    itemOffset.bottom < rubberbandOffset.bottom) {
                    $(this).addClass("workflow-block-selected");

                    var elementid = $(this).attr('id');
                    _jsPlumb.addToDragSelection(elementid);
                }
            });
        };
        
    var getTopLeftOffset=function(element) {
        var elementDimension = {};
        elementDimension.left = element.offset().left;
        elementDimension.top =  element.offset().top;
        elementDimension.right = elementDimension.left + element.outerWidth();
        elementDimension.bottom = elementDimension.top + element.outerHeight();

        return elementDimension;
    };
        
       
    
         
       
    /***
     * 
     * Constructor
     * 
     */
    
    _jsPlumb = jsPlumb.getInstance({
            Endpoint: ["Dot", {radius: 2}],
            Connector:"StateMachine",//"Flowchart",//
            HoverPaintStyle: {stroke: "#f39c12", strokeWidth: 2 },
            ConnectionOverlays: [
                [ "Arrow", {
                    location: 1,
                    id: "arrow",
                    length: 14,
                    foldback: 0.8
                } ],
                [ "Label", { label: "", id: "label", cssClass: "workflow-connector-label" }]
            ],
            Container: $diagramPanel
        });        
    
    
    $diagramPanel.droppable({
        drop: function( event, ui ) {

           var
               draggableDocumentOffset = ui.helper.offset(),
               droppableDocumentOffset = $(this).offset(),
               left = draggableDocumentOffset.left - droppableDocumentOffset.left,
               top = draggableDocumentOffset.top - droppableDocumentOffset.top;

           var blockData=parent.getBlockTemplates()[ui.draggable.attr("data-index")];
           
           blockData=jQuery.extend(true, {}, blockData)

           top=(Math.round((top)/10.0)*10);
           left=(Math.round((left)/10.0)*10);
           var newId=_createBlock(blockData,{top:top,left:left},true); 
           _$diagramPanel.find("#" + newId).addClass("workflow-block-selected").addClass("workflow-block-changed");
           parent.formChanged();
        }
    });    
    
    
    _jsPlumb.bind("connection", function (info) {
        
        var connectionName=_getBlockById(info.source.getAttribute("id")).getNextConnector();
        if (connectionName.toString().toLocaleLowerCase()==="next"){
            info.connection.removeOverlays("label");
        } else {
            info.connection.getOverlay("label").setLabel(connectionName);
        }
        //parent.formChanged();
        
    });
    
    _jsPlumb.bind("click", function (connector) {
        _showConnectionDialog(connector);
    });

    
    _jsPlumb.ready(function() {
        _$diagramPanel.mousedown(diagramContainer_MouseDown);
        _$diagramPanel.mousemove(diagramContainer_MouseMove);
        _$diagramPanel.mouseup(diagramContainer_MouseUp);
//        _$diagramPanel.click(diagramContainer_Click);
    });
};