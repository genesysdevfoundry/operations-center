/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Function.getFileName=function(id_Object){
    _filename=("000000" + id_Object).slice(-6) + ".wav";        
    return _filename;
};