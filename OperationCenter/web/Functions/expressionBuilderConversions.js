/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

Function.getExpressionBuilderFromSkillExpression=function(input){
    if (input.length===0){
        return null;
    } 
    if (input[0]==="("){
        input=input.substr(1);
    }
    input+=")";
    var outputObject;
    var status="NEW_EXPRESSION";
    var validCharacters="abcdefghijklmnopqrstuvwxyzåäö_1234567890$@{} ";
    var skillName="";
    var operator;
    var rules=[];
    var condition;
    var level;
    var operators=[
            {name:"less_or_equal",character:"<="},
            {name:"greater_or_equal",character:">="},
            {name:"not_equal",character:"!="},            
            {name:"greater",character:">"},
            {name:"less",character:"<"},
            {name:"equal",character:"="}
        ];
      
    var getOperator=function(input){
        for (var i=0;i<operators.length;i++){
            if (input.indexOf(operators[i].character)===0){
                return operators[i].name;
            }
        }
        return null;
    };
    
    for (var index=0;index<input.length;index++){
        if (input[index]==="("){
            outputObject=Function.getExpressionBuilderFromSkillExpression(input.substr(index+1));
            if (!outputObject){
                return null;
            }
            index+=outputObject.index;
            delete outputObject.index;
            rules.push(outputObject);
        }        
        
        if (status==="LEVEL"){
            if ($.isNumeric(input[index])){
                level+=input[index];                
            } else {
                rules.push({
                    id:skillName,
                    field:skillName,
                    type:"integer",
                    input:"select",
                    operator:operator,
                    value:level
                });
                status="CONDITION";
            }
        }
        
        if (status==="PRE_LEVEL"){
            if ($.isNumeric(input[index])){
                status="LEVEL";
                level+=input[index];
            } else if (input[index]!==" "){
                return null;
            } 
        }
        
        
        if (input[index]===")"){
            if (status==="NEW_EXPRESSION"){
                return null;
            }
            outputObject={
                    valid:true,
                    condition:condition,
                    rules:rules,
                    index:index
            };
            return outputObject;
        }
        
        if ((status==="NEW_EXPRESSION"||status==="SKILL_NAME"||status==="SKILL_NAME_WITH_SPACE")&&validCharacters.indexOf(input[index].toLowerCase())>=0){
            if (status==="SKILL_NAME"){
                if (input[index]===" "){
                    status="OPERATOR";
                } else {
                   skillName+=input[index];
                }                
            } else if (status==="SKILL_NAME_WITH_SPACE"){
                skillName+=input[index];
            } else {
                skillName="";
                if (input[index]!==" "){
                   skillName+=input[index];
                   status="SKILL_NAME";                
                }                                  
            }   
        } else {
            if (status==="SKILL_NAME"){
                status="OPERATOR";
            }
        }
        
               
        if (status==="OPERATOR"){
            operator=getOperator(input.substr(index));
            if (operator){
                if (operator.indexOf("_")>0){  //Contains two characters (ex ">=")
                    index++;
                }
                status="PRE_LEVEL";
                level="";
            } else {
                if (input[index]!==" "){
                    return null;
                }
            }
        }
        
        if (status==="CONDITION"){
            if (input[index]==="|"){
                if (condition==="AND"){
                    return null;
                }
                condition="OR";
                status="NEW_EXPRESSION";
            } else if (input[index]==="&"){
                if (condition==="OR"){
                    return null;
                }                
                condition="AND";
                status="NEW_EXPRESSION";
            } else if (input[index]!==" "){
                return null;
            }
        }

        if (input[index]==="\""){
            if (status==="SKILL_NAME_WITH_SPACE"){
                status="OPERATOR";
            } else {
                status="SKILL_NAME_WITH_SPACE";
                skillName="";
            }
        }
        
    }
};

Functions.getSkillExpressionFromExpressionBuilder=function(conditionJSON,notFirst){           
    var boolOperator;
    var operator;
    var outputString="";
    var skillNameQuotation="";
    
    if (conditionJSON&&conditionJSON.condition){
        boolOperator=(conditionJSON.condition==="AND")?" & ":" | ";
    } else {
        return "";
    }
    
    if (conditionJSON&&conditionJSON.rules){
        outputString+=(notFirst)?"(":"";
        for (var ruleIndex=0;ruleIndex<conditionJSON.rules.length;ruleIndex++){
            if (conditionJSON.rules[ruleIndex].condition){
                outputString+=((ruleIndex)?boolOperator:"") + Functions.getSkillExpressionFromExpressionBuilder(conditionJSON.rules[ruleIndex],true);
            } else {
                switch(conditionJSON.rules[ruleIndex].operator){
                case "greater":
                    operator=">";
                    break;
                case "less":
                    operator="<";
                    break;
                case "equal":
                    operator="=";
                    break;
                case "not_equal":
                    operator="!=";
                    break;
                case "less_or_equal":
                    operator="<=";
                    break;
                case "greater_or_equal":
                    operator=">=";
                    break;                
                case "ends_with":
                    operator="=";
                    break;                
                case "begins_with":
                    operator="=";
                    break;    
                }                
                skillNameQuotation = conditionJSON.rules[ruleIndex].id.includes(" ") ? "\"" : "";
                outputString+=((ruleIndex)?boolOperator:"") + skillNameQuotation + conditionJSON.rules[ruleIndex].id + skillNameQuotation + operator + conditionJSON.rules[ruleIndex].value;
            }
        }        
        outputString+=(notFirst)?")":"";;
    } else {
        return "";
    }
    return outputString;    
};
