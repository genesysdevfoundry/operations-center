/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

var codeSnippets=[
    {        
        "name": "Get Context Data",
        "category":"General",
        "code": "\n//Get all parameters from Context.xml\nvar contextParameters=serviceHandler.getContextParameters();\n"
    },{        
        "name": "Write to Log File",
        "category":"General",
        "code": "\nwriteToLog(\"Some Text....\");\n" 
    },{        
        "name": "Send Client Message",
        "category":"General",
        "code": "\nsendClientMessage(\"Client Message...\");\n"
    },{        
        "name": "Get Data From Object",
        "category":"General",
        "code": "\n//Fetch data from objects in OC\nvar data=getDataFromObject({\n   objectTemplateName:input.objectTemplateName,\n   objectName:input.objectName\n});\n\n"
    },{    
        "name": "Read data from Excel (xlsx)",
        "category":"General",
        "code": "//Will fetch and read the given xlsx file assuming first row contains column names. Returns an array of rows in key/value format\nvar excelContent = serviceHandler.getHandler(\"com.nordicoffice.backendengine.handlers.FileHandler\").readExcel(\"C:/tmp/example.xlsx\");\n\n"
    },{         
        "name": "Move file to new directory",
        "category":"General",
        "code": "//Move file from first argument to folder in second argument\nserviceHandler.getHandler(\"com.nordicoffice.backendengine.handlers.FileHandler\").moveFile(\"C:/tmp/example.xlsx\",\"C:/tmp2\");\n\n"
    },{ 
        "name": "Move all files from directory",
        "category":"General",
        "code": "//Move all files from source folder (first argument) to destination folder (second argument)\nserviceHandler.getHandler(\"com.nordicoffice.backendengine.handlers.FileHandler\").moveAllFiles(\"C:/tmp\",\"C:/tmp2\");\n\n"
    },{  
        "name": "Get all file names in folder",
        "category":"General",
        "code": "//Return all file names in a given folder as array\nvar fileNames = serviceHandler.getHandler(\"com.nordicoffice.backendengine.handlers.FileHandler\").getFileNamesFromFolder(\"C:/tmp\");\n\n"
    },{         
        "name": "Fetch Single Record",
        "category":"DB",
        "code": "\n//Fetch the first record in the query result as a single object\noutput=executeSQLRecord(\"select top 1 * from  <table> where <id>={{input.<id>}}\");\n"
    },{        
        "name": "Fetch Multiple Records",
        "category":"DB",
        "code": "\n//Fetch all records in the query result as an array \noutput=executeSQLRecordset(\"select * from  <table> where <id>={{input.<id>}}\");\n"
    },{        
        "name": "Execute SQL",
        "category":"DB",
        "code": "\n//Execute a sql statement. Will not return any records\noutput=executeSQLStatement(\"update <table> set <field>='{{input.<field>}}' where <id>={{input.<id>}}\");\n"
    },{        
        "name": "Get identity",
        "category":"DB",
        "code": "\n//Get identity after an insert\noutput=getIdentity();\n"
    },{        
        "name": "Connect to ConfigServer",
        "category":"Genesys",
        "code": "// Connect to configServer\n// application.userName/password is the user configured in the properties file. This must be encrypted and thus the last flag of the connection below is needed\n// session.userName/password is the user that was used when logging in to OC\n// session.userNameOC/passwordOC is the user specific user created by OC at login, which has possibly more rights to perform actions.\n\nvar confServiceHandler;\ntry {\n    confServiceHandler = serviceHandler.getHandler(\"com.nordicoffice.operationcenter.ConfServerHandler\");\n    confServiceHandler.connect(\n            application.configUser,\n            application.configPassword,\n            application.configAppName,\n            application.configServerName,\n            parseInt(application.configServerPort),\n            true //optional, set to true if password is encrypted\n            );\n\n} catch (err) {\n    writeToLog(\"Error Can't connect to config server\");\n    throwCustomError(\"Error Can't connect to config server\");\n}\n\n"
    },{  
        "name": "Add capacity to agent",
        "category":"Genesys",
        "code": "//Adds a capacity to a person. If both name and DBID is given, name will be used.\nconfServiceHandler.addCapacityToPerson(\n    {\n        \"name\":input.name,\n        \"ruleName\":input.ruleName,  //one of name or DBID is mandatory\n        \"ruleDBID\":input.ruleDBID   //one of name or DBID is mandatory\n    }\n);\n\n"
    },{ 
        "name": "Add default place to agent",
        "category":"Genesys",
        "code": "//Adds a default place to a person.\nconfServiceHandler.addDefaultPlaceToPerson(\n    {\n        \"name\":input.name,\n        \"placeName\":input.placeName  //Blank place name will remove place from agent\n    }\n);\n\n"
    },{ 
        "name": "Add DNs to place",
        "category":"Genesys",
        "code": "//Adds an array of DNs to a place\nconfServiceHandler.addDnsToPlace(\n    {\n        \"place\":\n            {\n                \"name\":input.name,\n                \"tenantDBID\":session.tenantDBID\n            },\n        \"dns\":\n            [\n                {\n                    \"tenantDBID\":session.tenantDBID,\n                    \"name\":input.name1,\n                    \"switchDBID\":input.switchDBID\n                \n                },{\n                    \"tenantDBID\":session.tenantDBID,\n                    \"name\":input.name2,\n                    \"switchDBID\":input.switchDBID\n                }\n            ]\n    }\n);\n\n"
    },{ 
        "name": "Add agent login to person",
        "category":"Genesys",
        "code": "//Add agent login to person or updates wrapup if login already present on person\nconfServiceHandler.addLoginToPerson(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"switchDBID\":input.switchDBID,\n        \"loginId\":input.loginId,\n        \"name\":input.name,\n        \"wrapup\":input.wrapup       //optional\n    }\n);\n\n"
    },{  
        "name": "Add person to access group",
        "category":"Genesys",
        "code": "//Adds agent to access group\nconfServiceHandler.addPersonToAccessGroup(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"accessGroup\":input.accessGroup,\n        \"name\":input.name\n    }\n);\n\n"
    },{
        "name": "Add/Update agent skills",
        "category":"Genesys",
        "code": "//Add/update skills on agent\n//Exampel: {\"name\":\"FrallBall\",\"tenantDBID\":1,\"skills\":[{\"skill\":\"english\",\"level\":2},{\"skill\":\"swedish\",\"level\":3}]}\n\nvar result = confServiceHandler.addSkillsToPerson({\n    \n    \"name\":input.name,\n    \"tenantDBID\":session.tenantDBID,\n    \"skills\":[\n        {\n            \"skill\":\"Swedish\",\n            \"level\":2\n        },{\n            \"skill\":\"English\",\n            \"level\":6\n        }\n    ]\n    \n});\n\n"
    },{
        "name": "Create agent",
        "category":"Genesys",
        "code": "//Create agent\nvar agent = confServiceHandler.createAgent(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"employeeId\":input.name,\n        \"firstName\":input.firstName,    //optional\n        \"lastName\":input.lastName,      //optional\n        \"folderDBID\":input.folderDBID,    //optional\n        \"emailAddress\":input.emailAddress,    //optional\n        \"password\":input.password,    //optional\n        \"externalId\":input.externalId,    //optional\n        \"enabled\":input.enabled     //optional (true/false, 1/0)\n    }\n);\n\n"
    },{        
        "name": "Create agent group",
        "category":"Genesys",
        "code": "//Create agent group\nvar agentGroup = confServiceHandler.createAgentGroup(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"script\": input.script,         //optional (expression for VAG)\n        \"folderDBID\":input.folderDBID   //optional\n    }\n);\n\n"
    },{        
        "name": "Create agent login",
        "category":"Genesys",
        "code": "//Create agent login\nvar agentLogin = confServiceHandler.createAgentLogin(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID,\n        \"folderDBID\":input.folderDBID       //optional\n    }\n);\n\n"
    },{
        "name": "Create calling list",
        "category":"Genesys",
        "code": "//Create calling list\nvar callingList = confServiceHandler.createCallingList(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"tableAccess\":input.tableAccess,                    //optional\n        \"treatments\":\"Treatment_Busy,Treatment_NoAnswer\",   //optional\n        \"maxAttempt\":input.maxAttempt,                      //optional\n        \"dailyFrom\":input.dailyFrom,                        //optional\n        \"dailyTill\":input.dailyTill                         //optional\n    }\n);\n\n"
    },{
        "name": "Create DN",
        "category":"Genesys",
        "code": "//Create DN\nvar dn = confServiceHandler.createDN(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID,\n        \"alias\":input.alias,            //optional\n        \"folderDBID\":input.folderDBID,  //optional\n        \"type\":input.type               //optional (i.e. CFGRoutingPoint, defaults to CFGExtension)\n    }\n);\n\n"
    },{
        "name": "Create place",
        "category":"Genesys",
        "code": "//Create place\nvar place = confServiceHandler.createPlace(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"folderDBID\":input.folderDBID //optional\n    }\n);\n\n"
    },{        
        "name": "Create skill",
        "category":"Genesys",
        "code": "//Create skill\nvar place = confServiceHandler.createSkill(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"folderDBID\":input.folderDBID //optional\n    }\n);\n\n"
    },{
        "name": "Create table access",
        "category":"Genesys",
        "code": "//Create table access\nvar tableAccess = confServiceHandler.createTableAccess(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"tableName\":input.tableName,\n        \"dap\":input.dapName,\n        \"format\":input.formatName,\n        \"folderDBID\":input.folderDBID    //optional\n    }\n);\n\n"
    },{        
        "name": "Delete agent login",
        "category":"Genesys",
        "code": "//Delete agent login\nvar deleteAgentLoginSuccessful = confServiceHandler.deleteAgentLogin(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID\n    }\n);\n\n"
    },{
        "name": "Delete DN",
        "category":"Genesys",
        "code": "//Delete DN\nvar deleteDNSuccessful = confServiceHandler.deleteDN(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID\n    }\n);\n\n"
    },{
        "name": "Delete person",
        "category":"Genesys",
        "code": "//Delete person\nvar deletePersonSuccessful = confServiceHandler.deletePerson(\n    {\n        \"name\":input.name\n    }\n);\n\n"
    },{
        "name": "Delete place",
        "category":"Genesys",
        "code": "//Delete place\nvar deletePlaceSuccessful = confServiceHandler.deletePlace(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name\n    }\n);\n\n"
    },{        
        "name": "Get access groups from person",
        "category":"Genesys",
        "code": "//Get access groups from a person. Returns an array of groups\nvar groups = confServiceHandler.getAccessGroupsFromPerson(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name\n    }\n);\n\n"
    },{           
        "name": "Get all access groups",
        "category":"Genesys",
        "code": "//Create agent\nvar allAccessGroups = confServiceHandler.getAllAccessGroups(session.tenantDBID);\n\n"
    },{ 
        "name": "Get all agent groups",
        "category":"Genesys",
        "code": "//Get list of all agent groups\nvar agentGroups = confServiceHandler.getAgentGroups({\"tenantDBID\":session.tenantDBID});\n\n"
    },{        
        "name": "Get application options",
        "category":"Genesys",
        "code": "//Get all options from genesys application object. Returns a Map of Maps\nvar applicationOptions = confServiceHandler.getApplicationOptions(input.applicationName);\n\n"
    },{         
        "name": "Get capacity rules",
        "category":"Genesys",
        "code": "//Get all capacity rules as array\nvar allRules = confServiceHandler.getCapacityRules(\n    {\n        \"tenantDBID\":session.tenantDBID\n    }\n);\n\n"
    },{          
        "name": "Get DN",
        "category":"Genesys",
        "code": "//Get DN\nvar dn = confServiceHandler.getCfgDN(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID\n    }\n);\n\n"
    },{   
        "name": "Get fields from format",
        "category":"Genesys",
        "code": "//Fetch all fields from a format. Will return an array with all fields on the format\n// {nullable: false, name: \"contact_info\", length: 128, type: \"varchar\"}\nvar format = confServiceHandler.getFieldsFromFormat(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name\n    }\n);\n\n"
    },{         
        "name": "Get Person",
        "category":"Genesys",
        "code": "//Get person information\nvar person = confServiceHandler.getPerson({\"name\":input.name});\n\n"
    },{  
        "name": "Get all Persons",
        "category":"Genesys",
        "code": "//Get list of all persons\nvar persons = confServiceHandler.getPersons({\"tenantDBID\":session.tenantDBID});\n\n"
    },{    
        "name": "Get all Places",
        "category":"Genesys",
        "code": "//Get list of all places\nvar places = confServiceHandler.getPlaces({\"tenantDBID\":session.tenantDBID});\n\n"
    },{      
        "name": "Get all Skills",
        "category":"Genesys",
        "code": "//Get list of all skills\nvar skills = confServiceHandler.getSkills({\"tenantDBID\":session.tenantDBID});\n\n"
    },{
        "name": "Get All Virtual Queues",
        "category":"Genesys",
        "code": "//Get all virtual queues\nvar result = confServiceHandler.getAllDNsByType(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"dnType\":\"CFGVirtACDQueue\"\n}\n);\n\n"
    },{
        "name": "Get All Routing Points",
        "category":"Genesys",
        "code": "//Get all routing points\nvar result = confServiceHandler.getAllDNsByType(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"dnType\":\"CFGRoutingPoint\"\n}\n);\n\n"
    },{
        "name": "Get All Extensions",
        "category":"Genesys",
        "code": "//Get all extensions\nvar result = confServiceHandler.getAllDNsByType(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"dnType\":\"CFGExtension\"\n  }\n);\n\n"
    },{    
        "name": "Get transaction list",
        "category":"Genesys",
        "code": "//Get transaction list option/options. Returns whole list or a section or an option (depending on input).\nvar options = confServiceHandler.getTransactionList(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"transactionListName\":input.transactionListName,\n        \"sectionName\":input.sectionName,        //optional\n        \"optionName\":input.optionName         //optional\n    }\n);\n\n"
    },{
        "name": "Move agent",
        "category":"Genesys",
        "code": "//Move agent\nconfServiceHandler.moveAgent(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"folderDBID\":input.folderDBID  \n    }\n);\n\n"
    },{        
        "name": "Remove person from access group",
        "category":"Genesys",
        "code": "//Removes agent from access group\nconfServiceHandler.removePersonFromAccessGroup(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"accessGroup\":input.accessGroup,\n        \"name\":input.name\n    }\n);\n\n"
    },{
        "name": "Add annex tab options",
        "category":"Genesys",
        "code": "//Get DN (change to other snippet if other object type)\nvar cfgObject = confServiceHandler.getCfgDN(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID\n    }\n);\n\n//Method takes genesys Cfg object as input so need to be fetched first.\nconfServiceHandler.setAnnexData(\n    {\n        object: cfgObject,\n        data: {\n            \"Section1\":\n                    {\n                        \"Option1\": \"Value1\",\n                        \"Option2\": \"Value2\"\n                    },\n            \"Section2\":\n                    {\n                        \"Option3\": \"Value3\",\n                        \"Option4\": \"Value4\"\n                    }\n        }\n    }\n);\n\n"
    },{
        "name": "Create transaction list section",
        "category":"Genesys",
        "code": "//Create transaction list section\nconfServiceHandler.transactionListCreateSection(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"transactionListName\":input.transactionListName,\n        \"sectionName\":input.sectionName,\n        \"data\":{\n            \"key1\":\"value1\",\n            \"key2\":\"value2\"\n        }\n    }\n);\n\n"
    },{
        "name": "Delete transaction list section",
        "category":"Genesys",
        "code": "//Delete transaction list section\nconfServiceHandler.transactionListDeleteSection(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"transactionListName\":input.transactionListName,\n        \"sectionName\":input.sectionName\n    }\n);\n\n"
    },{
        "name": "Check transaction list section existence",
        "category":"Genesys",
        "code": "//Check if transaction list section exist\nvar result = confServiceHandler.transactionListSectionExists(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"transactionListName\":input.transactionListName,\n        \"sectionName\":input.sectionName\n    }\n);\n\n"
    },{
        "name": "Update agent information",
        "category":"Genesys",
        "code": "//Update agent information\nconfServiceHandler.updateAgent(\n    {\n        \"name\":input.name,\n        \"firstName\":input.firstName,    //optional\n        \"lastName\":input.lastName,      //optional\n        \"emailAddress\":input.emailAddress,    //optional\n        \"password\":input.password,    //optional\n        \"externalId\":input.externalId,    //optional\n        \"enabled\":input.enabled     //optional (true/false, 1/0)\n    }\n);\n\n"
    },{
        "name": "Provision agent",
        "category":"Genesys",
        "code": "//input data that must be set: {\"name\":\"OSKAR\",\"firstName\":\"Frallan\",\"lastName\":\"Ballan\",\"switchDBID\":\"101\",\"accessGroup1\":\"Users\",\"accessGroup2\":\"All_Demosrv_Agents\"}\n\n//Create agent\nvar agent = confServiceHandler.createAgent(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"employeeId\":input.name,\n        \"firstName\":input.firstName,    //optional\n        \"lastName\":input.lastName,      //optional\n        //\"folderDBID\":input.folderDBID,    //optional\n        //\"emailAddress\":input.emailAddress,    //optional\n        //\"password\":input.password,    //optional\n        //\"externalId\":input.externalId,    //optional\n        //\"enabled\":input.enabled     //optional (true/false, 1/0)\n    }\n);\n\n//Create place\nvar place = confServiceHandler.createPlace(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        //\"folderDBID\":input.folderDBID //optional\n    }\n);\n\n//Create DN\nvar dn = confServiceHandler.createDN(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID,\n        //\"alias\":input.alias,            //optional\n        //\"folderDBID\":input.folderDBID,  //optional\n        //\"type\":input.type               //optional (i.e. CFGRoutingPoint, defaults to CFGExtension)\n    }\n);\n\n//Create agent login\nvar agentLogin = confServiceHandler.createAgentLogin(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"name\":input.name,\n        \"switchDBID\":input.switchDBID,\n        //\"folderDBID\":input.folderDBID       //optional\n    }\n);\n\n//Add agent login to person or updates wrapup if login already present on person\nconfServiceHandler.addLoginToPerson(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"switchDBID\":input.switchDBID,\n        \"loginId\":input.name,\n        \"name\":input.name,\n        //\"wrapup\":input.wrapup       //optional\n    }\n);\n\n//Adds a default place to a person.\nconfServiceHandler.addDefaultPlaceToPerson(\n    {\n        \"name\":input.name,\n        \"placeName\":input.name  //Blank place name will remove place from agent\n    }\n);\n\n//Adds an array of DNs to a place\nconfServiceHandler.addDnsToPlace(\n    {\n        \"place\":\n            {\n                \"name\":input.name,\n                \"tenantDBID\":session.tenantDBID\n            },\n        \"dns\":\n            [\n                {\n                    \"tenantDBID\":session.tenantDBID,\n                    \"name\":input.name,\n                    \"switchDBID\":input.switchDBID\n                }\n            ]\n    }\n);\n\n//Adds agent to access group\nconfServiceHandler.addPersonToAccessGroup(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"accessGroup\":input.accessGroup1,\n        \"name\":input.name\n    }\n);\n\n//Adds agent to access group\nconfServiceHandler.addPersonToAccessGroup(\n    {\n        \"tenantDBID\":session.tenantDBID,\n        \"accessGroup\":input.accessGroup2,\n        \"name\":input.name\n    }\n);\n\n"
    }    
];
