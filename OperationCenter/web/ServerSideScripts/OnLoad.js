/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */
writeToLog("OnLoad is running");

//Set log path
serviceHandler.setLogFileName("{catalina.home}/logs/OperationCenter.log");

var propertiesData=serviceHandler.readPropertiesFileParameters("operationcenter.properties");

//Store parameters into application storage
for (var key in propertiesData){
    serviceHandler.addApplicationData(key,propertiesData[key]);
}



//Connect to configServer
var confServiceHandler;
try {
    confServiceHandler = serviceHandler.getHandler("com.nordicoffice.operationcenter.ConfServerHandler");
    confServiceHandler.connect(
            propertiesData.userName,
            propertiesData.password,
            propertiesData.configAppName,
            propertiesData.configServerName,
            parseInt(propertiesData.configServerPort),
            true
            );
} catch (err) {
    writeToLog("Error Can't connect to config server");
    throwCustomError("Error Can't connect to config server");    
}

//Get all options from genesys application object. Returns a Map of Maps
var applicationOptions = confServiceHandler.getApplicationOptions(propertiesData.configAppName);

//Save data from Genesys application object
for (var optionSection  in applicationOptions){
    serviceHandler.addApplicationData(optionSection,applicationOptions[optionSection]);
}

//Set audio file storage parameter used in upload servlet
if (applicationOptions.audio&&applicationOptions.audio.AudioFileStorage){
    serviceHandler.addApplicationData("audioFileStorage",applicationOptions.audio.AudioFileStorage);
}



//Close connection to confserv
confServiceHandler.close();

//Init connector pool
serviceHandler.initDBConnectorPool({
    url: applicationOptions.database["pool.property.url"],
    driverClassName: applicationOptions.database["pool.property.driverClassName"],
    userName: applicationOptions.database["pool.property.username"],
    password: applicationOptions.database["pool.property.password"],
    validationInterval: 20000,
    testOnReturn: true
});


//Activate dynamic scripts
scripts=executeSQLRecordset("select  id_Script,Name, type , jSCode from script order by Name",true);
for (var i=0;i<scripts.length;i++){
    
    if (scripts[i].type==="WEB_SERVICE"){
        writeToLog("Adding service:" + scripts[i].name);
        serviceHandler.addService(scripts[i].name, null, scripts[i].jSCode);
    } else {
        writeToLog("Adding service with id:" + scripts[i].id_Script);
        serviceHandler.addService(null, parseInt(scripts[i].id_Script), scripts[i].jSCode);
    }
}