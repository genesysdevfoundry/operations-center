/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

//Get ObjectTemplate connected to Object
var objectData=executeSQLRecord("select ot.Id_ObjectTemplate, ot.scriptDelete, ot.type, ot.transactionListName, o.name from [ObjectTemplate] ot inner join [object] o on ot.id_ObjectTemplate=o.id_ObjectTemplate where id_Object={{#input.id_Object}}");

//Check if the user has permission to delete objects related to this object template (Requerie Delete persmission)
if (!session.isAdmin && !(session.objectTemplatePermissions["OT_ID_" + objectData.id_ObjectTemplate].indexOf("D")>=0)){
    sendClientMessage("You don't have permission to dselete this object.");
}


 if (objectData.scriptDelete){
    //formData.Id_Script_Save;
   input=addJSONDataToResult(executeSQLRecord("select Id_Object,Id_ObjectTemplate,Name,JSONData from [Object] where id_Object={{#input.id_Object}}"));
   writeToLog("Execute script on delete with, scriptId=" + objectData.scriptDelete);
   
   serviceHandler.executeService(parseInt(objectData.scriptDelete));
        
}

executeSQLUpdate("update object set deleted=getDate(),deletedBy='{{session.userName}}' where id_Object={{#input.id_Object}}");

executeSQLUpdate("DELETE FROM [ObjectRelation] WHERE ParentObjectType='OBJECT' AND ParentObjectId={{#input.id_Object}}");

//Delete the transaction list section 
if (objectData.type==="TRANSACTION_LIST_OBJECT_LIST"||objectData.type==="WORKFLOW_OBJECT_LIST"||objectData.type==="CALENDAR_OBJECT_LIST"){
    try {   
        confServiceHandler=serviceHandler.getHandler("com.nordicoffice.operationcenter.ConfServerHandler");
        confServiceHandler.connect(
            session.userNameOC,
            session.passwordOC,
            application.configAppName,                             
            application.configServerName,                             
            parseInt(application.configServerPort)
        );
        
    } catch (err){
        writeToLog("Error Can't connect to config server");    
        throwCustomError("Error Can't connect to config server");
    }
    
    try{
        confServiceHandler.transactionListDeleteSection({tenantDBID:session.tenantDBID,transactionListName:objectData.transactionListName,sectionName:objectData.name});
    } catch (err){
        writeToLog("Error deleting section in transaction list");            
    }
}





