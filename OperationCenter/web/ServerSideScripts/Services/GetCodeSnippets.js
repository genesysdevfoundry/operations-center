/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//Return all code snippets in a list
output=executeSQLRecordset("select  id_CodeSnippet,Category,Name, JSCode from CodeSnippet WHERE isnull(general,0)=1 or Id_Tenant={{#session.tenantDBID}}  order by Category,Name");

