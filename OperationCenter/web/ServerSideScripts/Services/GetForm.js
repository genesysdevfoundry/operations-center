/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


output=addJSONDataToResult(executeSQLRecord("select * from [Form] where id_Form={{#input.id_Form}} AND Id_Tenant={{#session.tenantDBID}}"));
output.sysAdmin=session.sysAdmin;