/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

output=addJSONDataToResult(executeSQLRecordset("select * from [Object] where id_Form={{#input.id_Form}} and Id_Tenant={{#session.tenantDBID}}"));