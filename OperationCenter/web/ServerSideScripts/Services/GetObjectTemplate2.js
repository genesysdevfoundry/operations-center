/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

/**
 * This service is used in the ObjectTemplate editor
 * 
 */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}

//Get Objecttemplate connected to Object
output=executeSQLRecord("select * from [ObjectTemplate] where id_ObjectTemplate={{#input.id_ObjectTemplate}} and Id_Tenant={{#session.tenantDBID}}");

if (output.workflowBlocks){    
    output.workflowBlocks=JSON.parse(output.workflowBlocks);       
}


if (output.permissions){
    try{
        output.permissions=JSON.parse(output.permissions);        
    } catch (ex){
        
    }
}

