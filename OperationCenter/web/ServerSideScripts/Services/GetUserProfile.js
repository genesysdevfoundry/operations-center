/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}


//Get departments as a kvp list
var kvpDepartments=executeSQLRecordset("select id_Department as [id],name as [text] from [Department] where Id_Tenant={{#session.tenantDBID}} and (id_Department in ({{session.departments}}) or '{{session.isAdmin}}'='true') order by Name");

var promptObjectTemplate=executeSQLRecord("select top 1 id_ObjectTemplate from ObjectTemplate where type='PROMPT_LIST' and Id_Tenant={{#session.tenantDBID}}");
var promptObjectTemplateId=promptObjectTemplate.id_ObjectTemplate||0;
session.promptObjectTemplateId=promptObjectTemplateId;



output={
    allAccessGroups:session.allAccessGroups,
    memberAccessGroups:session.memberAccessGroups,
    userFullName:session.firstName + " " + session.lastName,
    userName:session.userName,
    defaultDepartment:session.defaultDepartment,
    departments:kvpDepartments,
    promptObjectTemplateId:promptObjectTemplateId
};


        
