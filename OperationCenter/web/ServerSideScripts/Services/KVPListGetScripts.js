/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Return all scripts in a list
output=executeSQLRecordset("select  name, id_Script as [value] from script where Id_Tenant={{#session.tenantDBID}} and type='{{input.scriptParameter}}' and (isnull(hidden,0)=0 or '{{session.sysAdmin}}'='true') order by Name",true);
