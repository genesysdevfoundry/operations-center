/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

writeToLog("Logging out " + session.userName);
session={};
