/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user

writeToLog("recordingFilename:" + input.recordingFilename);
writeToLog("path:" + input.path);


if (input.path){
    serviceHandler.getHandler("com.nordicoffice.operationcenter.FileHandler").createPath(application.audio.AudioFileStorage + "/" + input.path);
} else {
    input.path="";
}




//Run ALawConverter bat file 
serviceHandler.getHandler("com.nordicoffice.operationcenter.AudioFileConverter").convertAudioFile(
        application.audio.AudioFileStorage + "/InProcess/" + input.recordingFilename,
        application.audio.AudioFileStorage + ("/" + input.path + "/").replace("//","/") + input.recordingFilename,
        application.audio.ALawConverter);


//Delete file from InProgress
serviceHandler.getHandler("com.nordicoffice.operationcenter.FileHandler").deleteFile(application.audio.AudioFileStorage + "/InProcess/" + input.recordingFilename);

output.success=true;
output.fileName=input.recordingFilename;
output.promptName=input.promptName;