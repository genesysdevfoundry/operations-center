/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");
}

//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}


var id_Form=input.id_Form;


var jsonData=JSON.stringify({content:input.content});

if (id_Form===0){
    
    //Insert new record
    executeSQLUpdate("INSERT INTO [dbo].[Form] ([Id_Tenant],[Created],[CreatedBy],[Changed],[ChangedBy],[Name],[Description],[TransparentData],[JSONData]) \
    VALUES ({{#session.tenantDBID}},getdate(),'{{session.userName}}',getdate(),'{{session.userName}}',\
           '{{input.name}}','{{input.description}}',{{#input.transparentData}},'{{jsonData}}')");
    id_Form=getIdentity();
}

else {
    //Update existing record
    executeSQLUpdate("UPDATE [dbo].[Form] SET [Changed]=getDate(),[ChangedBy]='{{session.userName}}',\
                [Name]='{{input.name}}',[JSONData]='{{jsonData}}',[Description]='{{input.description}}',[TransparentData]={{#input.transparentData}} \
                WHERE id_Form={{#id_Form}}");        
}

output.id_Form=id_Form;