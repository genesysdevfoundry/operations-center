/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */

//Activate dynamic script
var script=executeSQLRecord("select  id_Script,Name, type , jSCode from script where id_Script={{#input.id_Script}} order by Name",true);
    
if (script.type==="WEB_SERVICE"){
    writeToLog("Adding/updating service:" + script.name);
    serviceHandler.addService(script.name, null, script.jSCode);
} else {
    writeToLog("Adding/updating service with id:" + script.id_Script);
    serviceHandler.addService(null, parseInt(script.id_Script), script.jSCode);
}

