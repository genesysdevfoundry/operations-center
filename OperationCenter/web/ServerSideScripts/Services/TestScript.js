/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */
//Check if we have a logged in user
if (!session.userName){
    throwCustomError("NO_LOGIN_SESSION");    
}

//You need to be an admin to use this service
if (!session.isAdmin){
    throwCustomError("Access denide");    
}


serviceHandler.addService("TemptServiceName",null,input.jSCode);

var newInput={};
for (var key in input){
    if (key!=="jSCode"){
       newInput[key]=input[key];
    }
}
input=newInput;



serviceHandler.executeService("TemptServiceName");