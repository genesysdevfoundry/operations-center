/* Copyright © 2018, Genesys Telecommunications Laboratories, Inc. Released under MIT license. See LICENSE.md */
serviceHandler.importScript("CodeSnippets.js");


executeSQLStatement("DELETE FROM [dbo].[CodeSnippet] WHERE General=1");

for (var i=0;i<codeSnippets.length;i++){
    var code=codeSnippets[i].code;
    var name=codeSnippets[i].name;
    var category=codeSnippets[i].category;
    executeSQLUpdate("INSERT INTO [dbo].[CodeSnippet] (general,name,category,JScode) VALUES (1,'{{name}}','{{category}}','{{code}}')");            
}

output="SUCCESS";
